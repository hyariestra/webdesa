
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Shutter &mdash; Colorlib Website Template</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
	<link rel="stylesheet" href="assets/font/font-style.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<link rel="stylesheet" href="assets/css/jquery-ui.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="assets/font/flaticon.css">
	<link rel="stylesheet" href="assets/css/aos.css">
	<link rel="stylesheet" href="assets/css/fancybox.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/font/icomon/demo.css">

</head>
<body>
	<div class="site-wrap">
		<div class="site-mobile-menu">
			<div class="site-mobile-menu-header">
				<div class="site-mobile-menu-close mt-3">
					<span class="icon icon-cross js-menu-toggle"></span>
				</div>
			</div>
			<div class="site-mobile-menu-body"></div>
		</div>
		<header class="header-bar d-flex d-lg-block align-items-center">
			<div class="site-logo">
				<a href="index.html">Shutter</a>
			</div>
			<div class="d-inline-block d-xl-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon icon-menu h3"></span></a></div>
			<div class="main-menu">
				<ul class="js-clone-nav">
					<li class="active"><a href="index.html">Home</a></li>
					<li><a href="photos.html">Photos</a></li>
					<li><a href="bio.html">Bio</a></li>
					<li><a href="blog.html">Blog</a></li>
					<li><a href="contact.html">Contact</a></li>
				</ul>
				<ul class="social js-clone-nav">
					
					<li><a href="#"><span class="icon icon-instagram"></span></a></li>
				</ul>
			</div>
		</header>
		<main class="main-content">
			<div class="container-fluid photos">
				<div class="row pt-4 mb-5 text-center">
					<div class="col-12">
						<img width="30%" class="img-responsive" src="images/logo.png" alt="">
					</div>
				</div>
				<div class="row align-items-stretch">
					<?php 

					for ($i=1; $i <= 10; $i++) { 
						
						$image = $i.'.jpeg';


						if ($i % 10 == 1 ) {
							$col = 3;
						}elseif ($i % 10 == 2 ) {
							$col = 6;
						}elseif ($i % 10 == 3) {
							$col = 3;
						}elseif ($i % 10 == 4) {
							$col = 8;
						}elseif ($i % 10 == 5) {
							$col = 4;
						}elseif ($i % 10 == 6) {
							$col = 6;
						}elseif ($i % 10 == 7) {
							$col = 6;
						}elseif ($i % 10 == 8) {
							$col = 4;
						}elseif ($i % 10 == 9) {
							$col = 4;
						}elseif ($i % 10 == 10) {
							$col = 4;
						}


					 ?>


					<div class="col-6 col-md-6 col-lg-<?php echo $col ?>" data-aos="fade-up">
						<a href="images/<?php echo $image ?>" class="d-block photo-item" data-fancybox="gallery">
							<img src="images/<?php echo $image ?>" alt="Image" class="img-fluid">
							<div class="photo-text-more">
								<span class="icon icon-search"></span>
							</div>
						</a>
					</div>
					
					<?php } ?>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				</div>
				<div class="row justify-content-center">
					<div class="col-md-12 text-center py-5">
						<p>

							Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>

						</p>
					</div>
				</div>
			</div>
		</main>
	</div> 
	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="assets/js/jquery-ui.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
	<script src="assets/js/jquery.countdown.min.js"></script>
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<script src="assets/js/bootstrap-datepicker.min.js"></script>
	<script src="assets/js/aos.js"></script>
	<script src="assets/js/jquery.fancybox.min.js"></script>
	<script src="assets/js/main.js"></script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-23581568-13');
	</script>
</body>
</html>