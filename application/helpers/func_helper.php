<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



	//setFilterCookieDate()



	if ( ! function_exists('setFilterCookieDate') )



	{



		function setFilterCookieDate($tanggal = '', $tipe = 'startDateFilter')



		{



			$CI = & get_instance();



			$CI->session->set_userdata($tipe, $tanggal);



		}







	} //if ( ! function_exists('setFilterCookieDate') )



	//formatCurrency($value)


function formatCurrencyNonDecimal($value, $decimalAmount = 0, $thousandSeparator = 0, $decimalSeparator = 0)



		  	{



		  		$decimalAmount 		= 0;



		  		$decimalSeparator 	= ($decimalSeparator == 0) ? '.' : $decimalSeparator;



		  		$thousandSeparator  = ($thousandSeparator == 0) ? ',' : $thousandSeparator;







		  		$strCurrency = ($value >= 0) ? number_format($value, $decimalAmount, $decimalSeparator, $thousandSeparator) : "(".number_format(abs($value), $decimalAmount, $decimalSeparator, $thousandSeparator).")";



		  		return $strCurrency;



		  	}


		if ( ! function_exists('formatCurrency') )



		{



			function formatCurrency($value, $decimalAmount = 0, $thousandSeparator = 0, $decimalSeparator = 0)



		  	{



		  		$decimalAmount 		= 2;



		  		$decimalSeparator 	= ($decimalSeparator == 0) ? '.' : $decimalSeparator;



		  		$thousandSeparator  = ($thousandSeparator == 0) ? ',' : $thousandSeparator;







		  		$strCurrency = ($value >= 0) ? number_format($value, $decimalAmount, $decimalSeparator, $thousandSeparator) : "(".number_format(abs($value), $decimalAmount, $decimalSeparator, $thousandSeparator).")";



		  		return $strCurrency;



		  	}



		}  	//if ( ! function_exists('formatCurrency') )


		//terbilang



	if ( ! function_exists('terbilang') )



	{







			function terbilang($angka) {



			    // pastikan kita hanya berususan dengan tipe data numeric



			    $angka = (float)$angka;



			    



			    // array bilangan 



			    // sepuluh dan sebelas merupakan special karena awalan 'se'



			    $bilangan = array(



			            '',



			            'satu',



			            'dua',



			            'tiga',



			            'empat',



			            'lima',



			            'enam',



			            'tujuh',



			            'delapan',



			            'sembilan',



			            'sepuluh',



			            'sebelas'



			    );



			    



			    // pencocokan dimulai dari satuan angka terkecil



			    if ($angka < 12) {



			        // mapping angka ke index array $bilangan



			        return $bilangan[$angka];



			    } else if ($angka < 20) {



			        // bilangan 'belasan'



			        // misal 18 maka 18 - 10 = 8



			        return $bilangan[$angka - 10] . ' belas';



			    } else if ($angka < 100) {



			        // bilangan 'puluhan'



			        // misal 27 maka 27 / 10 = 2.7 (integer => 2) 'dua'



			        // untuk mendapatkan sisa bagi gunakan modulus



			        // 27 mod 10 = 7 'tujuh'



			        $hasil_bagi = (int)($angka / 10);



			        $hasil_mod = $angka % 10;



			        return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));



			    } else if ($angka < 200) {



			        // bilangan 'seratusan' (itulah indonesia knp tidak satu ratus saja? :))



			        // misal 151 maka 151 = 100 = 51 (hasil berupa 'puluhan')



			        // daripada menulis ulang rutin kode puluhan maka gunakan



			        // saja fungsi rekursif dengan memanggil fungsi terbilang(51)



			        return sprintf('seratus %s', terbilang($angka - 100));



			    } else if ($angka < 1000) {



			        // bilangan 'ratusan'



			        // misal 467 maka 467 / 100 = 4,67 (integer => 4) 'empat'



			        // sisanya 467 mod 100 = 67 (berupa puluhan jadi gunakan rekursif terbilang(67))



			        $hasil_bagi = (int)($angka / 100);



			        $hasil_mod = $angka % 100;



			        return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));



			    } else if ($angka < 2000) {



			        // bilangan 'seribuan'



			        // misal 1250 maka 1250 - 1000 = 250 (ratusan)



			        // gunakan rekursif terbilang(250)



			        return trim(sprintf('seribu %s', terbilang($angka - 1000)));



			    } else if ($angka < 1000000) {



			        // bilangan 'ribuan' (sampai ratusan ribu



			        $hasil_bagi = (int)($angka / 1000); // karena hasilnya bisa ratusan jadi langsung digunakan rekursif



			        $hasil_mod = $angka % 1000;



			        return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));



			    } else if ($angka < 1000000000) {



			        // bilangan 'jutaan' (sampai ratusan juta)



			        // 'satu puluh' => SALAH



			        // 'satu ratus' => SALAH



			        // 'satu juta' => BENAR 



			        // @#$%^ WT*



			        



			        // hasil bagi bisa satuan, belasan, ratusan jadi langsung kita gunakan rekursif



			        $hasil_bagi = (int)($angka / 1000000);



			        $hasil_mod = $angka % 1000000;



			        return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else if ($angka < 1000000000000) {



			        // bilangan 'milyaran'



			        $hasil_bagi = (int)($angka / 1000000000);



			        // karena batas maksimum integer untuk 32bit sistem adalah 2147483647



			        // maka kita gunakan fmod agar dapat menghandle angka yang lebih besar



			        $hasil_mod = fmod($angka, 1000000000);



			        return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else if ($angka < 1000000000000000) {



			        // bilangan 'triliun'



			        $hasil_bagi = $angka / 1000000000000;



			        $hasil_mod = fmod($angka, 1000000000000);



			        return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else {



			        return 'Wow...';



			}



		}



	}	//if ( ! function_exists('terbilang') )




	//terbilang

 	

	//getFieldValue()



	if ( ! function_exists('getFieldValue') )



	{



		function getFieldValue($table = '', $field, $sqlWhere)



		{



			$CI = & get_instance();



			$data = $CI->db->query("select COALESCE(".$field.", '0') as strValue from ".$table." where ".$sqlWhere);



			return ($data->num_rows() > 0) ? $data->first_row()->strValue : '0';



		}







	} //if ( ! function_exists('getFieldValue') )







	//RealDateTime



	if ( ! function_exists('RealDateTime') )



	{



		function RealDateTime($formatType = '', $includeTime = true, $tipeFilterDate = '')



		{	



			$CI =&get_instance();



			//Penyesuaian Pengambilan tanggal format date time indonesia



			//karena bisa jadi TimeZone di server berbeda



			//--------------------------------------------------------------------------

			

			$tahunPeriode = '2019'; //tahun di ambil dari periode transaksi



			$serverDate = mktime(date("G"), date("i"), date("s"), date("n"), date("j"), $tahunPeriode);		



			$diffGMT = substr(date("O", $serverDate), 1, 2);



			$dateDiffGMT = 60 * 60 * $diffGMT;





			if (substr(date("O", $dateDiffGMT), 0, 1) == '+')		



				$GMTDate = $serverDate - $dateDiffGMT;		



			else		



				$GMTDate = $serverDate + $dateDiffGMT;		





			$DateDiffIndonesian = 60 * 60 * 7;



			$newDate = date($tahunPeriode.'-m-d H:i:s', $GMTDate + $DateDiffIndonesian);



			if ($formatType == 'Year') 	 $newDate = date($tahunPeriode, $GMTDate + $DateDiffIndonesian);



			if ($formatType == 'Month')  $newDate = date('m', $GMTDate + $DateDiffIndonesian);



			if ($formatType == 'Day')	 $newDate = date('d', $GMTDate + $DateDiffIndonesian);



			if ($formatType  == 'Time')  $newDate = date('H:i:s', $GMTDate + $DateDiffIndonesian);



			if (!$includeTime) $newDate = date('d-m-'.$tahunPeriode, $GMTDate + $DateDiffIndonesian);



			



			//--------------------------------------------------------------------------	



			//check jika ada filter date ambil dari cookie tanggalnya



			$newDate = ($tipeFilterDate <> '') ? ($CI->session->userdata($tipeFilterDate) <>'')  ? $CI->session->userdata($tipeFilterDate) : $newDate  : $newDate;



			return $newDate;



		}



	}	//if ( ! function_exists('RealDateTime') )




	//formatDateIndo



	if ( ! function_exists('formatDateIndo') )



	{



		function formatDateIndo($paramDate, $separator = '-')



		{



			$arrDateTime = explode(' ', $paramDate);



	



			$arrDate = (count($arrDateTime) == 1) ? explode($separator, $paramDate) :  explode($separator, $arrDateTime[0]);



			$Year 	 = $arrDate[0];



			$Month 	 = $arrDate[1];



			$Day 	 = $arrDate[2];







			$newDate = $Day.$separator.$Month.$separator.$Year;







			return  $newDate;



		}



	} //if ( ! function_exists('formatDateIndo') )







	//formatDateDB



	if ( ! function_exists('formatDateDB') )



	{



		function formatDateDB($paramDate, $separator = '-')



		{



			$arrDate = explode($separator, $paramDate);



			$Year 	 = $arrDate[2];



			$Month 	 = $arrDate[1];



			$Day 	 = $arrDate[0];







			$newDate = $Year.$separator.$Month.$separator.$Day;







			return  $newDate;



		}



	}	//if ( ! function_exists('formatDateDB') )




    //SanitizeParanoid



	if ( ! function_exists('SanitizeParanoid') )



	{





		function SanitizeParanoid($data)



		{







			$CI = &get_instance();	



			$CI->security->xss_clean($data);



			$data = str_replace("'", "`", $data);

			$data = str_replace('"', '”', $data);

			$data = str_replace("\n", "", $data);

			$data = str_replace("\r", "", $data);

			$data = str_replace(">", "&gt", $data);

			$data = str_replace("<", "&lt", $data);



			

			$data = sanitize_sql_string($data);



			return $data;



		}



    }	//if ( ! function_exists('SanitizeParanoid') )



		//GetSettingValue($settingName)



		if ( ! function_exists('GetSettingValue') )



		{



			function GetSettingValue($settingName)



		  	{	


		  		$CI=& get_instance();


		  		$result = $CI->db->query("select nilai as SettingValue from set_setting where nama = '".$settingName."' ");
		  		

		  		$settingValue = ($result->num_rows() > 0) ?  $result->first_row()->SettingValue : '';


		  		return $settingValue;

		  	}



		}  	//if ( ! function_exists('GetSettingValue') )



		//strToCurrDB()



	if ( ! function_exists('strToCurrDB') )



	{



		function strToCurrDB($strCurrency, $replace = ',')



		{

			

			if (substr(trim($strCurrency),0,1) == '(' )

			{

				$strCurrency = str_replace('(', '-', $strCurrency);

				$strCurrency = str_replace(')', '', $strCurrency);

			}



			$strValue = str_replace($replace, '', $strCurrency);



			return  $strValue;



		}



	}//if ( ! function_exists('strToCurrDB') )	




		//GetAutoNum($value)



		if ( ! function_exists('GetAutoNum') )



		{



			function GetAutoNum($Tipe = 'Pemesanan')



		  	{

		  		$CI = &get_instance();

		  		$Tipe  	   = strtolower($Tipe);

		  		$tableName = 'trx_'.$Tipe;

		
		  		$selectQuery = $CI->db->query("select nomor as trxNumber from ".$tableName."

									  			order by id_".$Tipe." desc limit 1");



		  		$strZeroLength = 5;

		  		$strAwal = $Tipe == "ju" ? "JU" : "SA";


		  		//FORMAT = TIK/BLUD/001
		  		$strFirstNumber =  $strAwal.str_pad("0", $strZeroLength, "0", STR_PAD_LEFT);


		  		$lastNumber = ($selectQuery->num_rows() > 0) ?  $selectQuery->first_row()->trxNumber : $strFirstNumber;


		  		//check nomor yang lebih dari nomor sekarang (untuk menghindari deadlock number)
		  		$selectQuery = $CI->db->query("select nomor as trxNumber from ".$tableName."  

									  			where nomor LIKE '".GetLeftExcludeAutoNumber($lastNumber)."%' 

									  			order by nomor desc limit 1");


		  		if ($selectQuery->num_rows() > 0)
		  		{

					$lastNumber = 	$selectQuery->first_row()->trxNumber;

		  		}

		  		return $lastNumber; 
		  	}

		}  	//if ( ! function_exists('GetAutoNum') )







		//isAutoNumberFound



		if ( ! function_exists('isAutoNumberFound') )



		{



			function isAutoNumberFound($nomor, $tipe = 'JU', $jenis = '', $FieldIDtrxEdit = '', $IDTrxEdit = '',  $jenisBKMRinci ='')



		  	{



		  		$CI = & get_instance();	







		  		$tipe  = strtolower($tipe);



		  		$tableName = 'trx_'.$tipe;



		  		



		  		$IDUnitKerja    = $_SESSION['IDUnitKerja'];	



		  		$strValueUPTD   = GetSQLUPTDValue('select','value');





		  		$strWhereJenis = ($jenis == 'UM') ? " kode in ('".$jenis."', 'UMP') " : " kode = '".$jenis."' ";

		  		$strWhereJenis = ($jenis == 'UK') ? " kode in ('".$jenis."', 'UKP') " : $strWhereJenis;



		  		$strJenis = ($jenis <> '') ? " and id_sumber_trans in  (select id_sumber_trans from ref_sumber_trans where ".$strWhereJenis." ) " : "";



		  		

		  		$jenisBKMRinci  = str_replace("+", " ", $jenisBKMRinci);



		  		$strRinciJenis 	= $jenisBKMRinci <> '' ? " and tipe_bkm = '$jenisBKMRinci'  " : "";



		  		//handle untuk transaksi yang di edit



		  		//tambahkan idnya



		  		$strTrxEdit = ($IDTrxEdit <> '') ? " and ".$FieldIDtrxEdit." <> '".$IDTrxEdit."'  " : "";







			  	$selectQuery = $CI->db->query("select nomor as trxNumber from ".$tableName."  



									  		   where id_unit_kerja = '".$IDUnitKerja."' AND ".$strValueUPTD."  ".$strJenis." 

									  		   

									  		   and id_unit = '".$_SESSION['IDUnit']."' 



									  		   $strRinciJenis 



									  		   and nomor = '".$nomor."' ".$strTrxEdit);



		  		return $selectQuery->num_rows() > 0;







		  	}



		}	//if ( ! function_exists('isAutoNumberFound') )	






		//GetLastDateInMonth

		if ( ! function_exists('GetLastDateInMonth') )

		{

			function GetLastDateInMonth($month, $year = '')

			{	

				$year = ($year <> '') ? $year : RealDateTime ("Year");

				$newDate = mktime (0,0,0, $month, '01' , RealDateTime ("Year"));



	            return date('t', $newDate);

	 		

			}

		} //if ( ! function_exists('GetLastDateInMonth') )




		//GetMonthName



		if ( ! function_exists('GetMonthName') )



		{



			function GetMonthName($id) 



			{



				$strName = '';







				if ($id == 1) $strName = 'Januari';



				if ($id == 2) $strName = 'Februari';



				if ($id == 3) $strName = 'Maret';



				if ($id == 4) $strName = 'April';



				if ($id == 5) $strName = 'Mei';



				if ($id == 6) $strName = 'Juni';



				if ($id == 7) $strName = 'Juli';



				if ($id == 8) $strName = 'Agustus';



				if ($id == 9) $strName = 'September';



				if ($id == 10) $strName = 'Oktober';



				if ($id == 11) $strName = 'Nopember';



				if ($id == 12) $strName = 'Desember';







				return $strName;



			}



		} //if ( ! function_exists('GetMonthName') )




	//GetLeftExcludeAutoNumber



	if ( ! function_exists('GetLeftExcludeAutoNumber') )



	{



		function GetLeftExcludeAutoNumber($nomor = '') 



		{







			$hasil 		= "";



			$Text 		= $nomor;



			$txt 		= trim($Text);



		    $strLen 	= strlen($txt);



		    $afterChar 	= true;







		    for ($i=0;$i<$strLen;$i++)



		    {







		        $karakter = substr($txt, $i, 1);







		        if  (is_numeric($karakter)) 



		        {







		            if  ($afterChar) 



		            {







		                $idx = $i;







		                $hasil = $karakter;







		            }else



		            {







		                $hasil = $hasil.$karakter;







		            }







		            $afterChar = false;







		        }else



		        {



		            $afterChar = true;







		        } //if  (is_int($karakter)) 







		    } //for ($i=1;$i<$strLen;$i++)







	    	if ($hasil <> "")



	    	{ 



		        $akhiran = substr($txt, $idx + strlen($hasil), strlen($txt));







    			$hasil = substr($txt, 0, $idx).$akhiran;



			} 



			else



			{



				$hasil = $Text;



			}//if ($hasil <> "")







        	return $hasil;







    	} //function GetNextNo($nomor) 







	} //if ( ! function_exists('GetLeftExcludeAutoNumber') )



		



	//GetNextNo



	if ( ! function_exists('GetNextNo') )



	{



		function GetNextNo($nomor = '') 



		{







			$hasil 		= "";



			$Text 		= $nomor;



			$txt 		= trim($Text);



		    $strLen 	= strlen($txt);



		    $afterChar 	= true;







		    for ($i=0;$i<$strLen;$i++)



		    {







		        $karakter = substr($txt, $i, 1);







		        if  (is_numeric($karakter)) 



		        {







		            if  ($afterChar) 



		            {







		                $idx = $i;







		                $hasil = $karakter;







		            }else



		            {







		                $hasil = $hasil.$karakter;







		            }







		            $afterChar = false;







		        }else



		        {



		            $afterChar = true;







		        } //if  (is_int($karakter)) 







		    } //for ($i=1;$i<$strLen;$i++)







	    	if ($hasil <> "")



	    	{ 



		        $akhiran = substr($txt, $idx + strlen($hasil), strlen($txt));







		        $strLen = strlen($hasil);







		        $hasil = (int)($hasil) + 1;







		        if  ($strLen > 1) 



		        {







		        	$hasil = str_pad($hasil, $strLen, "0", STR_PAD_LEFT);



		        }



		        



    			$hasil = substr($txt, 0, $idx).$hasil.$akhiran;



			} 



			else



			{



				$hasil = $Text;



			}//if ($hasil <> "")







        	return $hasil;







    	} //function GetNextNo($nomor) 







	} //if ( ! function_exists('GetNextNo') )



		







	//terbilang



	if ( ! function_exists('terbilang') )



	{







			function terbilang($angka) {



			    // pastikan kita hanya berususan dengan tipe data numeric



			    $angka = (float)$angka;



			    



			    // array bilangan 



			    // sepuluh dan sebelas merupakan special karena awalan 'se'



			    $bilangan = array(



			            '',



			            'satu',



			            'dua',



			            'tiga',



			            'empat',



			            'lima',



			            'enam',



			            'tujuh',



			            'delapan',



			            'sembilan',



			            'sepuluh',



			            'sebelas'



			    );



			    



			    // pencocokan dimulai dari satuan angka terkecil



			    if ($angka < 12) {



			        // mapping angka ke index array $bilangan



			        return $bilangan[$angka];



			    } else if ($angka < 20) {



			        // bilangan 'belasan'



			        // misal 18 maka 18 - 10 = 8



			        return $bilangan[$angka - 10] . ' belas';



			    } else if ($angka < 100) {



			        // bilangan 'puluhan'



			        // misal 27 maka 27 / 10 = 2.7 (integer => 2) 'dua'



			        // untuk mendapatkan sisa bagi gunakan modulus



			        // 27 mod 10 = 7 'tujuh'



			        $hasil_bagi = (int)($angka / 10);



			        $hasil_mod = $angka % 10;



			        return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));



			    } else if ($angka < 200) {



			        // bilangan 'seratusan' (itulah indonesia knp tidak satu ratus saja? :))



			        // misal 151 maka 151 = 100 = 51 (hasil berupa 'puluhan')



			        // daripada menulis ulang rutin kode puluhan maka gunakan



			        // saja fungsi rekursif dengan memanggil fungsi terbilang(51)



			        return sprintf('seratus %s', terbilang($angka - 100));



			    } else if ($angka < 1000) {



			        // bilangan 'ratusan'



			        // misal 467 maka 467 / 100 = 4,67 (integer => 4) 'empat'



			        // sisanya 467 mod 100 = 67 (berupa puluhan jadi gunakan rekursif terbilang(67))



			        $hasil_bagi = (int)($angka / 100);



			        $hasil_mod = $angka % 100;



			        return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));



			    } else if ($angka < 2000) {



			        // bilangan 'seribuan'



			        // misal 1250 maka 1250 - 1000 = 250 (ratusan)



			        // gunakan rekursif terbilang(250)



			        return trim(sprintf('seribu %s', terbilang($angka - 1000)));



			    } else if ($angka < 1000000) {



			        // bilangan 'ribuan' (sampai ratusan ribu



			        $hasil_bagi = (int)($angka / 1000); // karena hasilnya bisa ratusan jadi langsung digunakan rekursif



			        $hasil_mod = $angka % 1000;



			        return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));



			    } else if ($angka < 1000000000) {



			        // bilangan 'jutaan' (sampai ratusan juta)



			        // 'satu puluh' => SALAH



			        // 'satu ratus' => SALAH



			        // 'satu juta' => BENAR 



			        // @#$%^ WT*



			        



			        // hasil bagi bisa satuan, belasan, ratusan jadi langsung kita gunakan rekursif



			        $hasil_bagi = (int)($angka / 1000000);



			        $hasil_mod = $angka % 1000000;



			        return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else if ($angka < 1000000000000) {



			        // bilangan 'milyaran'



			        $hasil_bagi = (int)($angka / 1000000000);



			        // karena batas maksimum integer untuk 32bit sistem adalah 2147483647



			        // maka kita gunakan fmod agar dapat menghandle angka yang lebih besar



			        $hasil_mod = fmod($angka, 1000000000);



			        return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else if ($angka < 1000000000000000) {



			        // bilangan 'triliun'



			        $hasil_bagi = $angka / 1000000000000;



			        $hasil_mod = fmod($angka, 1000000000000);



			        return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else {



			        return 'Wow...';



			}



		}



	}	//if ( ! function_exists('terbilang') )




	//terbilang



	if ( ! function_exists('terbilangsen') )



	{







			function terbilangsen($angka) {



			    // pastikan kita hanya berususan dengan tipe data numeric



			    $angka = substr($angka, -2);



			    



			    // array bilangan 



			    // sepuluh dan sebelas merupakan special karena awalan 'se'



			    $bilangan = array(



			            '',



			            'satu',



			            'dua',



			            'tiga',



			            'empat',



			            'lima',



			            'enam',



			            'tujuh',



			            'delapan',



			            'sembilan',



			            'sepuluh',



			            'sebelas'



			    );



			    



			    // pencocokan dimulai dari satuan angka terkecil



			    if ($angka < 12) {



			        // mapping angka ke index array $bilangan



			        return $bilangan[$angka];



			    } else if ($angka < 20) {



			        // bilangan 'belasan'



			        // misal 18 maka 18 - 10 = 8



			        return $bilangan[$angka - 10] . ' belas';



			    } else if ($angka < 100) {



			        // bilangan 'puluhan'



			        // misal 27 maka 27 / 10 = 2.7 (integer => 2) 'dua'



			        // untuk mendapatkan sisa bagi gunakan modulus



			        // 27 mod 10 = 7 'tujuh'



			        $hasil_bagi = (int)($angka / 10);



			        $hasil_mod = $angka % 10;



			        return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));



			    } else if ($angka < 200) {



			        // bilangan 'seratusan' (itulah indonesia knp tidak satu ratus saja? :))



			        // misal 151 maka 151 = 100 = 51 (hasil berupa 'puluhan')



			        // daripada menulis ulang rutin kode puluhan maka gunakan



			        // saja fungsi rekursif dengan memanggil fungsi terbilang(51)



			        return sprintf('seratus %s', terbilang($angka - 100));



			    } else if ($angka < 1000) {



			        // bilangan 'ratusan'



			        // misal 467 maka 467 / 100 = 4,67 (integer => 4) 'empat'



			        // sisanya 467 mod 100 = 67 (berupa puluhan jadi gunakan rekursif terbilang(67))



			        $hasil_bagi = (int)($angka / 100);



			        $hasil_mod = $angka % 100;



			        return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));



			    } else if ($angka < 2000) {



			        // bilangan 'seribuan'



			        // misal 1250 maka 1250 - 1000 = 250 (ratusan)



			        // gunakan rekursif terbilang(250)



			        return trim(sprintf('seribu %s', terbilang($angka - 1000)));



			    } else if ($angka < 1000000) {



			        // bilangan 'ribuan' (sampai ratusan ribu



			        $hasil_bagi = (int)($angka / 1000); // karena hasilnya bisa ratusan jadi langsung digunakan rekursif



			        $hasil_mod = $angka % 1000;



			        return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));



			    } else if ($angka < 1000000000) {



			        // bilangan 'jutaan' (sampai ratusan juta)



			        // 'satu puluh' => SALAH



			        // 'satu ratus' => SALAH



			        // 'satu juta' => BENAR 



			        // @#$%^ WT*



			        



			        // hasil bagi bisa satuan, belasan, ratusan jadi langsung kita gunakan rekursif



			        $hasil_bagi = (int)($angka / 1000000);



			        $hasil_mod = $angka % 1000000;



			        return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else if ($angka < 1000000000000) {



			        // bilangan 'milyaran'



			        $hasil_bagi = (int)($angka / 1000000000);



			        // karena batas maksimum integer untuk 32bit sistem adalah 2147483647



			        // maka kita gunakan fmod agar dapat menghandle angka yang lebih besar



			        $hasil_mod = fmod($angka, 1000000000);



			        return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else if ($angka < 1000000000000000) {



			        // bilangan 'triliun'



			        $hasil_bagi = $angka / 1000000000000;



			        $hasil_mod = fmod($angka, 1000000000000);



			        return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));



			    } else {



			        return 'Wow...';



			}



		}



	}	//if ( ! function_exists('terbilang') )


	//doExecuteView

	if ( ! function_exists('doExecuteView') )

	{

		function doExecuteView($view, $vars = array(), $return = FALSE)

		{

			$CI 			    = &get_instance();

			$CI->clientID  	    = "";

			$CI->viewPath       = APPPATH."modules/reports/".$CI->clientID."/views/";

            $CI->reportFullPath = $CI->viewPath.$view.".php";

            

            $isFileExistRef  	= file_exists($CI->reportFullPath);



            $CI->clientID2  	 = "";

			$CI->viewPath2       = APPPATH."modules/reports/".$CI->clientID2."/views/";

            $CI->reportFullPath2 = $CI->viewPath2.$view.".php";



            $isFileExist  		 = file_exists($CI->reportFullPath2);



			if ($isFileExistRef || $isFileExist)

			{

				return $CI->load->customView($view, $vars, $return); 

			}

			else

			{	

				return $CI->load->view($view, $vars, $return); 

			} //if ($isFileExist)

		}



	} //if ( ! function_exists('doExecuteView') )





	//doExecuteModel

	if ( ! function_exists('doExecuteModel') )

	{

		function doExecuteModel($model , $functionName = '', $object_name = NULL , $connect = FALSE)

		{

			$modelCustom		= "custom_".$model;

			$CI 			    = &get_instance();

			$CI->clientID  	    = "";

			$CI->viewPath       = APPPATH."modules/reports/".$CI->clientID."/models/";

            $CI->reportFullPath = $CI->viewPath.strtolower($modelCustom).".php";

            

            $isFileExist  = file_exists($CI->reportFullPath);

            

			if ($isFileExist)

			{

				return $CI->load->customModel($modelCustom, $functionName, $object_name, $connect); 

			}

			else

			{	

				return $CI->load->model($model, $object_name, $connect); 

			} //if ($isFileExist)

		}



	} //if ( ! function_exists('doExecuteModel') )



	//ConstructMessageResponse

	if ( ! function_exists('ConstructMessageResponse') )

	{

		function ConstructMessageResponse($messageResponseText = 'Unsuccesfully Retrieved Data', $alertType, $isNeedRefresh = false, $refreshURL = '', $glyphicon = '')

		{



			$messageResponseText = ($glyphicon <> '') ?  "<span class='glyphicon glyphicon-".$glyphicon."'>&nbsp;</span>".$messageResponseText : $messageResponseText;



			$messageResponseText = "<div class='alert alert-".$alertType."' id='alertMessage' alert-dismissible' role='alert'>".$messageResponseText."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>";	



			return $messageResponseText;

		}

	}	





/* End of file func_helper.php */	