<?php 


function getInfo($v)

{
	$ci =& get_instance();


	$val = $ci->db->query("SELECT ".$v." FROM mst_informasi")->first_row();

	return  $val->$v;

}

function debug_db(){
	$ci=& get_instance();
	echo '<pre>';
	print_r($ci->db->last_query());
	echo '</pre>';
}


function dateNow(){

	$timezone = new DateTimeZone('Asia/Jakarta');
	$date = new DateTime();
	$date->setTimeZone($timezone);
	return $date->format('Y-m-d H:i:s');
}


function dateBatas(){


	$date = dateNow();
	$date1 = str_replace('-', '/', $date);
	$tomorrow = date('Y-m-d H:i:s',strtotime($date1 . "+1 days"));

	return $tomorrow;

}



function getNameDomain()
{

	$name = $_SERVER['HTTP_HOST'];

	$name = explode('.', $name);

	return $name[0];

}

function getSettingDesa()
{
	$ci=& get_instance();
	$data = $ci->db->query("SELECT * FROM ref_desa WHERE desa_kode_domain = '".getNameDomain()."' ")->row_array();

	if (!empty($data['desa_logo_uniq']) OR $data['desa_logo_uniq'] != NULL) {
		$data['logo_desa'] = base_url('asset/foto_logo_desa/'.$data['desa_logo_uniq']);
	}else{
		$data['logo_desa'] = "";
	}

	if (!empty($data['desa_favicon_uniq']) OR $data['desa_favicon_uniq'] != NULL) {
		$data['favicon_desa'] = base_url('asset/foto_favicon/'.$data['desa_favicon_uniq']);
	}else{
		$data['favicon_desa'] = "";
	}

	return $data;
}


function getResponse($code,$pesan)
{

	if ($code==400) {
		$status = 'error';
	}elseif ($code==201) {
		$status = 'success';
	}else{
		$status = 'error';
	}


	$arr = array('status' => $code, 'status_code' => $status, 'message' =>  $pesan );    

	header('Content-Type: application/json');
	return json_encode( $arr );
}

function dateTimeIndoNow(){
	$timezone = new DateTimeZone('Asia/Jakarta');
	$date = new DateTime();
	$date->setTimeZone($timezone);

	return $date->format('Y-m-d H:i:s');
}

function getResponseWithCallBack($code,$pesan,$url)
{

	if ($code==400) {
		$status = 'error';
	}elseif ($code==201) {
		$status = 'success';
	}else{
		$status = 'error';
	}


	$arr = array('status' => $code, 'status_code' => $status, 'message' =>  $pesan, 'url' => $url );    

	header('Content-Type: application/json');
	return json_encode( $arr );
}


function compareMenu($menu)
{


	$query = authQuery();

	foreach ($query as $key => $value) {
		$val[] = $value['menu'];
	}

	$count  = count(array_intersect($menu, $val));

	$count = $count==0 ? 'none':'';

	return $count;

}

function menuAuth($menu)
{
	$query = authQuery();

	foreach ($query as $key => $value) {
		$val[] = $value['menu'];
	}

	$val['condition'] = in_array($menu, $val)?true:false;

	


	return $val;
}


function authorize($menu)
{
	
	$val = getValueRoles($menu);

	if ($val==false) {
		redirect("dashboard/notFound");
	} 

	return $val;
	
}


function authorizeView($menu)
{
	$val = getValueRoles($menu);

	return $val;
}





function getValueRoles($menu)
{
	$query = authQuery();

	$val = false;


	foreach ($query as $key => $value) {
		
		$values[] = $value['menu_auth'];
		
	}
	 
	if (in_array($menu, $values)) {
		$val = true;
	}


	return $val;
}

function checkedHelp($id,$menu)
{
	$ci =& get_instance();

	$ci->db->select('*')
	->from('akses_menu')->where('id_role', $id);
	$ambil = $ci->db->get();

	$query=$ambil->result_array();

	if ($query==null) {
		$val = '';
	} else {
		foreach ($query as $key => $value) {
			$val[] = $value['menu'];
		}


		$val = in_array($menu, $val)?'checked':'';
	}
	



	return $val;
}


function authQuery()
{
	$ci =& get_instance();

	$idRole = $ci->session->userdata['pengguna']['role'];
	
	$q =  $ci->db->query("SELECT menu_auth FROM `ref_menu_roles` a
	LEFT JOIN `be_menus` b ON b.`id_menu` = a.id_menu
	WHERE a.id_roles = ".$idRole." ")->result_array();

	return $q;

}


function panggil_file($namafile)

{

	//mengakses core dari codeingiter

	$ci =& get_instance();

	return $ci->load->view($namafile);

}



function tampil_pengaturan($namakolom)

{

	$ci =& get_instance();

	$ci->db->where("kolom",$namakolom);

	$ambil=$ci->db->get("pengaturan");

	$pecah=$ambil->row_array();

	return $pecah['isi'];

}

function tampil_halaman($kolom)

{

	$ci =& get_instance();

	$ci->db->where("judul",$kolom);

	$ambil=$ci->db->get("halamanstatis");

	$pecah=$ambil->row_array();

	return $pecah['judul'];

}

function funcSession()
{
		$ci =& get_instance();

	return $ci->session->all_userdata()['pengguna'];
}


function getSession()
{
	$ci =& get_instance();

	return $ci->session->all_userdata()['pengguna']['id_admin'];
}

function getSessionUser()
{
	$ci =& get_instance();

	return $ci->session->userdata("users");
}

 function getCategoryName($id)
{
	$ci =& get_instance();


	$val = $ci->db->query("SELECT * FROM kategori WHERE id_kategori = ".$id." ")->row();
	
	return $val;
}

 function getUserSeries()
{
	$ci =& get_instance();


	$val = $ci->db->query("SELECT id_series_master FROM series_user WHERE id_user = ".decrypt_url( getSessionUser()['user_id'] )." ")->result_array();
	
	$newArr = array();
	foreach ($val as $key => $value) {
		$newArr[] = $value['id_series_master'];
	}

	return $newArr;
}

function sliceArrayByCategoryByCode($data, $idCat, $slice )
{



	if ($idCat != NULL) {
			
		foreach ($data as $key => $value) {
			
			if ($value['slug_kategori'] != $idCat ) {
				unset($data[$key]);
			}
		}
	}

	$data = array_slice($data,0,$slice);


	return $data;

}

function sliceArrayByCategory($data, $idCat, $slice )
{

	if ($idCat != NULL) {
			
		foreach ($data as $key => $value) {
			
			if ($value['id_kategori'] != $idCat) {
				unset($data[$key]);
			}
		}
	}

	$data = array_slice($data,0,$slice);


	return $data;

}

function slugify($text)
{
  // replace non letter or digits by -
	$text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);

  // trim
	$text = trim($text, '-');

  // remove duplicate -
	$text = preg_replace('~-+~', '-', $text);

  // lowercase
	$text = strtolower($text);

	if (empty($text)) {
		return 'n-a';
	}

	return $text;
}

function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}


function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}


?>