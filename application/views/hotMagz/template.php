<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
	<?php echo getSettingDesa()['desa_metatag']; ?>
	
	<!-- Shareable -->
	<meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
	<title><?php include "phpmu-title.php"; ?></title>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/bootstrap.min.css'); ?>" media="screen">	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/jquery.bxslider.css'); ?>" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/font-awesome.css'); ?>" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/glyp-icon.css'); ?>" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/magnific-popup.css'); ?>" media="screen">	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/owl.carousel.css'); ?>" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/owl.theme.css'); ?>" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/ticker-style.css'); ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('themefront/hotMagz/css/style.css'); ?>" media="screen">
	

</head>



<body class="boxed">

	<!-- Container -->
	<div id="container">

		<!-- Header
		    ================================================== -->
		<header class="clearfix second-style">
			<!-- Bootstrap navbar -->
			<nav class="navbar navbar-default navbar-static-top" role="navigation">

			<div class="topbar clearfix">
        
			
				<!-- Logo & advertisement -->
				<div class="logo-advertisement">
					<div class="container">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="index.html"><img style="width: 400px; height: 80px;" src="<?php echo getSettingDesa()['logo_desa']; ?> " alt=""></a>
						</div>

						
					</div>
				</div>
				<!-- End Logo & advertisement -->

				<!-- navbar list container -->
				<div class="nav-list-container">
					<div class="container">

					
						<!-- Brand and toggle get grouped for better mobile display -->
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">

						
						<?php
						$fe_menu  =  $this->M_homepage->get_front_menu_tree();
						$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


						foreach ($fe_menu as $key => $fe) {
							
							if (!in_array($fe['id_menu'], $arrayMenu)) {
								unset($fe_menu[$key]);
							}

							if(!empty($fe['submenu'])){

								foreach ($fe['submenu'] as $keyz => $fe_sub) {

									if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

										unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
									}


								}
							}	

								//	unset($fe_menu[4]['submenu'][7]);
						}


						foreach ($fe_menu as $key => $fe) {


							if(empty($fe['submenu'])){
								echo '<li><a href='.base_url($fe['link_menu']).'>'.$fe['menu'].'</a></li>';

							}else{

								echo '
								<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$fe['menu'].'<span class="caret"></span></a>
							
								<ul class="dropdown-menu">';

								foreach ($fe['submenu']  as $keyz => $fe_submenu) {
									

									echo '<li><a href='.base_url($fe_submenu['link_submenu']).'>'.$fe_submenu['submenu'].'</a></li>';

								}
								echo 
								'</ul>
								</li>';
							}


						}?>

					</ul>

						<ul class="nav navbar-nav navbar-right">
							
							

						<form class="navbar-form navbar-left" action="/action_page.php">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Search">
							</div>
							<button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
							</form>
						</ul>
						
						</div><!-- /.navbar-collapse -->
					</div>
				</div>
				<!-- End navbar list container -->

			</nav>
			<!-- End Bootstrap navbar -->

		</header>
		<!-- End Header -->

		<!-- heading-news-section2
			================================================== -->
			<?php echo @$slider ?>
		<!-- End heading-news-section -->

		<!-- block-wrapper-section
			================================================== -->
		<section class="block-wrapper">
			<div class="container">
				<div class="row">
					
				<?php echo $content ?>

					<div class="col-sm-4">

						<!-- sidebar -->
						<div class="sidebar">

							

							<div class="widget tab-posts-widget">

								<ul class="nav nav-tabs" id="myTab">
									<li class="active">
										<a href="#option1" data-toggle="tab">Popular</a>
									</li>
									<li>
										<a href="#option2" data-toggle="tab">Recent</a>
									</li>
									<li>
										<a href="#option3" data-toggle="tab">Top Comments</a>
									</li>
								</ul>

								<div class="tab-content">
									<div class="tab-pane active" id="option1">
										<ul class="list-posts">
											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw1.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw2.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Sed arcu. Cras consequat. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw3.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus.  </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw4.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw5.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
									<div class="tab-pane" id="option2">
										<ul class="list-posts">

											

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw4.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw5.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw1.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw2.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Sed arcu. Cras consequat.</a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>										
									</div>
									<div class="tab-pane" id="option3">
										<ul class="list-posts">

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw4.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw1.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw3.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus.  </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw2.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Sed arcu. Cras consequat.</a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/listw5.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>										
									</div>
								</div>
							</div>

							<div class="widget features-slide-widget">
								<div class="title-section">
									<h1><span>Featured Posts</span></h1>
								</div>
								<div class="image-post-slider">
									<ul class="bxslider">
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/im3.jpg')?>" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
																<li><i class="fa fa-eye"></i>872</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/im1.jpg')?>" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
																<li><i class="fa fa-eye"></i>872</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/im2.jpg')?>" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
																<li><i class="fa fa-eye"></i>872</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>

							

							<div class="widget subscribe-widget">
								<form class="subscribe-form">
									<h1>Subscribe to RSS Feeds</h1>
									<input type="text" name="sumbscribe" id="subscribe" placeholder="Email"/>
									<button id="submit-subscribe">
										<i class="fa fa-arrow-circle-right"></i>
									</button>
									<p>Get all latest content delivered to your email a few times a month.</p>
								</form>
							</div>

							<div class="widget post-widget">
								<div class="title-section">
									<h1><span>Featured Video</span></h1>
								</div>
								<!-- <div class="news-post video-post">
									
								<iframe width="100%" height="269" src="https://www.youtube.com/embed/TZkBaU-82RQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div> -->
							</div>

							<div class="advertisement">
								<div class="desktop-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/300x250.jpg')?>" alt="">
								</div>
								<div class="tablet-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/200x200.jpg')?>" alt="">
								</div>
								<div class="mobile-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/300x250.jpg')?>" alt="">
								</div>
							</div>

						</div>
						<!-- End sidebar -->

					</div>

				</div>

			</div>

			<?php echo @$paralax ?>

		</section>
		<!-- End block-wrapper-section -->

		<!-- footer 
			================================================== -->
		<footer>
			<div class="container">
				<div class="footer-widgets-part">
					<div class="row">
						<div class="col-md-3">
							<div class="widget text-widget">
								<h1>About</h1>
								<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. </p>
								<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. </p>
							</div>
							
						</div>
						<div class="col-md-3">
							<div class="widget text-widget">
								<h1>FIND US ON</h1>
								<p><i class="fa fa-phone" aria-hidden="true"></i>  <?php echo getSettingDesa()['desa_telp']; ?></p>
								<p><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo getSettingDesa()['desa_email']; ?></p>
								<p><?php echo getSettingDesa()['desa_lokasi']; ?></p>
								
							</div>
							<div class="widget social-widget">
								<h1>Stay Connected</h1>
								<ul class="social-icons">

									

									<li><a href="<?php echo getSettingDesa()['desa_fb']; ?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
									<li><a href="<?php echo getSettingDesa()['desa_youtube']; ?>" class="youtube"><i class="fa fa-youtube"></i></a></li>
									<li><a href="<?php echo getSettingDesa()['desa_ig']; ?>" class="instagram"><i class="fa fa-instagram"></i></a></li>
								
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<div class="widget categories-widget">
								<h1>Hot Categories</h1>
								<ul class="category-list">
									<li>
										<a href="#">Business <span>12</span></a>
									</li>
									<li>
										<a href="#">Sport <span>26</span></a>
									</li>
									<li>
										<a href="#">LifeStyle <span>55</span></a>
									</li>
									<li>
										<a href="#">Fashion <span>37</span></a>
									</li>
									<li>
										<a href="#">Food <span>29</span></a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<div class="widget flickr-widget">
								<h1>Flickr Photos</h1>
								<ul class="flickr-list">
									<li><a href="#"><img src="<?php echo base_url('themefront/hotMagz/upload/flickr/1.jpg')?>" alt=""></a></li>
									<li><a href="#"><img src="<?php echo base_url('themefront/hotMagz/upload/flickr/2.jpg')?>" alt=""></a></li>
									<li><a href="#"><img src="<?php echo base_url('themefront/hotMagz/upload/flickr/3.jpg')?>" alt=""></a></li>
									<li><a href="#"><img src="<?php echo base_url('themefront/hotMagz/upload/flickr/4.jpg')?>" alt=""></a></li>
									<li><a href="#"><img src="<?php echo base_url('themefront/hotMagz/upload/flickr/5.jpg')?>" alt=""></a></li>
									<li><a href="#"><img src="<?php echo base_url('themefront/hotMagz/upload/flickr/6.jpg')?>" alt=""></a></li>
								</ul>
								<a href="#">View more photos...</a>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-last-line">
					<div class="row">
						<div class="col-md-6">
							<p>&copy; COPYRIGHT 2015 hotmagazine.com</p>
						</div>
						<div class="col-md-6">
							<nav class="footer-nav">
								<ul>
									<li><a href="index.html">Home</a></li>
									<li><a href="index.html">Purchase Theme</a></li>
									<li><a href="about.html">About</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->
	
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/jquery.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/jquery.migrate.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/count-animate.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/count-waypoint.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/jquery.bxslider.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/jquery.magnific-popup.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/bootstrap.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/jquery.ticker.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/jquery.imagesloaded.min.js')?>"></script>
  	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/jquery.isotope.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/owl.carousel.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/retina-1.1.0.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/hotMagz/js/script.js')?>"></script>

	<script>
    $(document).ready(function( $ ) {
        $('.time-animate').counterUp({
            delay: 10,
            time: 1000
        });
    });


		function share_fb(url) {


			window.open('https://www.facebook.com/sharer/sharer.php?u='+url,'facebook-share-dialog',"width=626, height=436")
		}

		function share_twitter(url) {


			var href = 'https://twitter.com/intent/tweet?text='+url;

			window.open(href, "Twitter", "height=285,width=550,resizable=1");

		}

		function shareHit(id) {
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('berita/hitshare') ?>", 
				data:  {id: id},
				dataType: "text",  
				cache:false,
				success: 
				function(data){

				}
          });// you have missed this bracket
			return false;


		}

		
			function like(id) {

					document.getElementById('like-button').setAttribute('onclick','dislike( '+id+' )');

					$.ajax({
						type: "POST",
						url: "<?php echo base_url('berita/hitlike') ?>", 
						data:  {id: id},
						dataType: "text",  
						cache:false,
						success: 
						function(data){

						}
					});// you have missed this bracket
					return false;


					}

			function dislike(id) {

					document.getElementById('like-button').setAttribute('onclick','like('+id+')');

					$.ajax({
						type: "POST",
						url: "<?php echo base_url('berita/hitdislike') ?>", 
						data:  {id: id},
						dataType: "text",  
						cache:false,
						success: 
						function(data){

						}
					});// you have missed this bracket
					return false;
}

</script>
</body>
</html>