<style>

.modal-backdrop {
    position: absolute !important;
}


	.roundAva{
		background-color: #f44336;
    padding: 34px;
    float: left;
	width: 100px;
	}
	.roundAva p{
	    color: white;
  		  font-size: 55px;
			margin-left: -4px;
	}
</style>
<div class="col-sm-8">

						<!-- block content -->
						<div class="block-content">

							<!-- single-post box -->
							<div class="single-post-box">
	
								<div class="title-post">
								<?php echo $this->session->userdata("pesan"); ?>
			
									<h1><?php echo $detail['berita']['judul'] ?> </h1>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i> <?php echo tgl_indo($detail['berita']['tanggal']) ?></li>
										<li><i class="fa fa-user"></i>by <a href="#"><?php echo $detail['berita']['nama']; ?></a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>0</span></a></li>
										<li><i class="fa fa-eye"></i>  <?php echo $detail['berita']['dibaca']; ?></li>
										<li><i class="fa fa-share"></i>  <?php echo $detail['berita']['dibagikan']; ?></li>
										<li><i class="fa fa-heart-o"></i>  <?php echo $detail['berita']['like_berita']; ?></li>
									</ul>
								</div>

								
			<div class="post-content">

                                <?php echo $detail['berita']['isi_berita']; ?>
				
								</div>

								<div class="post-tags-box">
									<ul class="tags-box">
										<li><i class="fa fa-tags"></i><span>Tags:</span></li>
										<?php

										foreach ($detail['tag'] as $key => $row) {
										
										?>
										<li><a href="#"><?php echo $row['nama_tag'] ?></a></li>
                                        <?php } ?>
									</ul>
								</div>

								<div class="share-post-box">
									<ul class="share-box">
										<li><i class="fa fa-share-alt"></i><span>Share Post</span></li>
										<li><a class="facebook" href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_fb('<?php echo $link?>'); return false;"  ><i class="fa fa-facebook"></i>Share on Facebook</a></li>
										<li><a class="twitter" href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_twitter('<?php echo $link?>'); return false;><i class="fa fa-twitter"></i>Share on Twitter</a></li>
										
									</ul>
								</div>
                                
			
								<div class="grid-box">

								<div class="title-section">
									<h1><span class="world">YOU MAY ALSO LIKE</span></h1>
								</div>

								<div class="row">
									
								<?php
								
								$arrayRecent = array_slice($recent, 0, 3);
								foreach ($arrayRecent as $key => $value) {
									
								// echo "<pre>";
								// print_r($arrayRecent);
								// echo "<pre>";
								

								?>
									
									<div class="col-md-4">
										<div class="news-post video-post">
											<img style="height: 159px;object-fit: cover;"  alt="<?php echo $value['judul'] ?>" src="<?php echo base_url('asset/'.$value['gambar']); ?>">
											<div class="hover-box">
												<h2><a href="<?php echo $value['url'] ?>"><?php echo $value['judul'] ?></a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i><?php echo $value['tanggal'] ?></li>
												</ul>
											</div>
										</div>
									</div>
										<?php } ?>
								</div>

							</div>
								
								<!-- End carousel box -->

								<!-- comment area box -->
								<div class="comment-area-box">
									<div class="title-section">
										<h1><span><?php echo $comments[0]['jumlah']?> Comments</span></h1>
									</div>
									<ul class="comment-tree">

									<?php
									
									foreach ($comments as $key => $value) {
									
									?>
										<li>
											<div class="comment-box">
												<div class="roundAva">
										<p><?php echo $value['first'] ?></p>
												</div>
												<div class="comment-content">
													<h4><?php echo $value['nama'] ?>
													<button style="float: right;" onclick="modalReply(<?php echo $value['idc'] ?>)"> <i class="fa fa-comment-o"></i> Reply</button>
													<span><i class="fa fa-clock-o"></i><?php echo $value['tgl'] ?></span>
													<p><?php echo $value['isi'] ?></p>
												</div>
											</div>
										</li>
										
										<?php } ?>

										

										

									</ul>
								</div>
								<!-- End comment area box -->

								<!-- contact form box -->
							
								<div class="contact-form-box">
									<div class="title-section">
										<h1><span>Leave a Comment</span> <span class="email-not-published">alamat email dan nomor telp anda tidak akan dipublikasikan</span></h1>
									</div>
								
									<form action="<?php echo base_url('berita/addKomen') ?>" id="comment-form">
								
							<input type="hidden" name="ids" value=" <?php echo $this->uri->segment(3) ?> ">
							<input type="hidden" name="seo" value=" <?php echo $this->uri->segment(4) ?> ">
									
									<div class="row">
											<div class="col-md-4">
												<label for="name">Name*</label>
												<input required id="name" name="name" type="text">
											</div>
											<div class="col-md-4">
												<label for="mail">E-mail*</label>
												<input id="mail" name="mail" type="text">
											</div>
											<div class="col-md-4">
												<label for="website">Phone Number</label>
												<input id="website" name="phone" type="text">
											</div>
										</div>
										<label for="comment">Comment*</label>
										<textarea required id="comment" name="comment"></textarea>
										<button type="submit" id="submit-contact">
											<i class="fa fa-comment"></i> Post Comment
										</button>
									</form>
								</div>
								<!-- End contact form box -->

							</div>
							<!-- End single-post box -->

						</div>
						<!-- End block content -->

					</div>
					
			

					<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>


<div class="modal fade" id="xmodalubahx"> 
	<div class="modal-dialog">
		<div class="modal-content">  
			<div class="modal-header">     
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>       
				<h4 class="modal-title"><i class="fa fa-comment-o"></i> Balas Post</h4>  
			</div>     
			<div class="modal-body">     
				<div class="form-horizontal">   
					<form id="formUbah">   
						<div class="row"> 
							<!-- <input type="text" name="idins" id="idins" /> -->
							<div class="col-sm-12">     
								<div class="form-group">  
								<input type="hidden" class="form-control" id="ubahID" name="ubahID" value=""/> 
									<label for="Nomor" class="col-sm-3  control-label">Nama Kategori</label>    
									<div class="col-sm-8">     
										<input class="form-control" id="ubahnama" name="ubahnama" value=""/>           
									</div>        
								</div>    
							</div>   
						</div>   
					</form>   
				</div>   
			</div>   
			<div class="modal-footer">      
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
				<button type="button" class="btn btn-primary" onclick="simpanubah(this,'')">Simpan Data</button> 
			</div>  
		</div><!-- /.modal-content -->  
	</div><!-- /.modal-dialog -->
</div>

<script>

function modalReply(id){
	
	$('#ubahnama').val(id);

	$("#xmodalubahx").modal("show");
}
</script>