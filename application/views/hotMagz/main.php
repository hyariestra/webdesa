<div class="col-sm-8">

						<!-- block content -->
						<div class="block-content">

						<div class="article-box">

								<div class="title-section">
									<h1><span>Principal’s Welcome</span></h1>
								</div>
							<div class="media"> 
									 <div class="media-body"> 
									<p>
									<img class="media-object" data-src="holder.js/64x64" src="<?php echo base_url('themefront/hotMagz/upload/users/lead.jpeg')?>" data-holder-rendered="true" style=" border: 5px solid #555; float:left;margin-right: 10px; width: 150px; height: 150px;">
										<b>Lorem Ipsum is simply dummy text of the printing </b>
										<br>
										Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

										The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.

									

						</p>
										</div> 
									</div>
								

								

								
							</div>


						
							<div class="article-box">

								<div class="title-section">
									<h1><span>Latest Articles</span></h1>
								</div>

								<?php
								
								$arrayItem = array_slice($item, 0, 5);

								foreach ($arrayItem as $item) {
									
								
	
								?>	

								<div class="news-post article-post">
									<div class="row">
										<div class="col-sm-5">
											<div class="post-gallery">
												<img width="100%" height="219" style="object-fit: cover;" alt="<?php echo $item['judul'] ?>" src="<?php echo base_url('asset/'.$item['gambar']); ?>">
											</div>
										</div>
										<div class="col-sm-7">
											<div class="post-content">
												<h2><a href="<?php echo $item['url'] ?>"><?php echo $item['judul'] ?></a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i><?php echo tgl_indo($item['tanggal']) ?></li>
													<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
													<li><i class="fa fa-eye"></i><?php echo $item['dibaca'] ?></li>
													<li><i class="fa fa-heart-o"></i><?php echo $item['like'] ?></li>
												</ul>
												<p><?php echo  strip_tags($item['isi_berita']) ?></p>
												<a href="<?php echo $item['url'] ?>" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Read More</a>
											</div>
										</div>
									</div>
								</div>

								<?php } ?>

							</div>


							<!-- google addsense -->
							<!-- <div class="advertisement">
								<div class="desktop-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/728x90-white.jpg')?>" alt="">
								</div>
								<div class="tablet-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/468x60-white.jpg')?>" alt="">
								</div>
								<div class="mobile-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/300x250.jpg')?>" alt="">
								</div>
							</div> -->
							<!-- End google addsense -->

							<!-- grid-box -->
							<div class="grid-box">

								<div class="title-section">
									<h1><span class="world">News in Video</span></h1>
								</div>
								
								<div class="row">
								<?php
								
									for ($i=0; $i < 3 ; $i++) { 
										
								
								?>

									<!-- <div class="col-md-4">
										<div class="news-post video-post">
											
										
										<iframe width="100%" height="170" src="https://www.youtube.com/embed/CmzKQ3PSrow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
										</div>
									</div> -->
									
									<?php } ?>
									
									
								</div>

							</div>
							<!-- End grid-box -->

							<!-- carousel box -->
							<div class="carousel-box owl-wrapper">

								<div class="title-section">
									<h1><span class="world">Lifestyle</span></h1>
								</div>

								<div class="owl-carousel" data-num="2">
								
									<div class="item">
										<div class="news-post image-post2">
											<div class="post-gallery">
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/im1.jpg')?>" alt="">
												<div class="hover-box">
													<div class="inner-hover">
														<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
														<ul class="post-tags">
															<li><i class="fa fa-clock-o"></i>27 may 2013</li>
															<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
															<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															<li><i class="fa fa-eye"></i>872</li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<ul class="list-posts">
											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list1.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list2.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list3.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>									
									</div>
								
									<div class="item">
										<div class="news-post image-post2">
											<div class="post-gallery">
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/im2.jpg')?>" alt="">
												<div class="hover-box">
													<div class="inner-hover">
														<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
														<ul class="post-tags">
															<li><i class="fa fa-clock-o"></i>27 may 2013</li>
															<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
															<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															<li><i class="fa fa-eye"></i>872</li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<ul class="list-posts">
											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list4.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list5.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list6.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>							
									</div>
								
									<div class="item">
										<div class="news-post image-post2">
											<div class="post-gallery">
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/im3.jpg')?>" alt="">
												<div class="hover-box">
													<div class="inner-hover">
														<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
														<ul class="post-tags">
															<li><i class="fa fa-clock-o"></i>27 may 2013</li>
															<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
															<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															<li><i class="fa fa-eye"></i>872</li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<ul class="list-posts">
											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list7.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list8.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="<?php echo base_url('themefront/hotMagz/upload/news-posts/list9.jpg')?>" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>					
									</div>

								</div>

							</div>
							<!-- End carousel box -->

							<!-- google addsense -->
							<div class="advertisement">
								<div class="desktop-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/728x90-white.jpg')?>" alt="">
								</div>
								<div class="tablet-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/468x60-white.jpg')?>" alt="">
								</div>
								<div class="mobile-advert">
									<span>Advertisement</span>
									<img src="<?php echo base_url('themefront/hotMagz/upload/addsense/300x250.jpg')?>" alt="">
								</div>
							</div>
							<!-- End google addsense -->

							<!-- article box -->
							
							<!-- End article box -->

							<!-- pagination box -->
							<div class="center-button">
									<a href="#"><i class="fa fa-arrow-right"></i> Berita Lebih Banyak</a>
								</div>
							<!-- End Pagination box -->

						</div>
						<!-- End block content -->

					</div>