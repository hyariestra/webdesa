<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
  <meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
  <meta name="google-site-verification" content="<?php echo getSettingDesa()['desa_metatag']; ?>" />
  <title><?php include "phpmu-title.php"; ?></title>
	
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('themefront/zcreative/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
	
	<!-- Custom CSS -->
	<link href="<?php echo base_url('themefront/zcreative/css/style.css'); ?>" rel="stylesheet">
	
	<!-- Animate.css -->
	<link href="<?php echo base_url('themefront/zcreative/css/animate.css'); ?>" rel="stylesheet" media="all" type="text/css">
	
	<!-- Lightbox.css -->
	<link href="<?php echo base_url('themefront/zcreative/css/lightbox.min.css'); ?>" rel="stylesheet" media="all" type="text/css">
	
	<!-- Custom Fonts -->
    <link href="<?php echo base_url('themefront/zcreative/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
	

	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" class="index-page">
<div class="wrap-body">

	<!--////////////////////////////////////Header-->
	<header>
		<!---Main Header--->
		<div class="main-header">
			<div class="logo">
				<img src="<?php echo getSettingDesa()['logo_desa']; ?>" class="img-responsive">
			</div>
			<!--Navigation-->
			<nav id="menu" class="navbar">
				<div class="container-fluid">
					<div class="navbar-header">
					  <span id="heading" class="visible-xs">Categories</span>
					  <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
						
					</div>

					
					
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							

						<?php

						$fe_menu  =  $this->M_homepage->get_front_menu_tree();
						$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


foreach ($fe_menu as $key => $fe) {
  
  if (!in_array($fe['id_menu'], $arrayMenu)) {
    unset($fe_menu[$key]);
  }

  if(!empty($fe['submenu'])){

    foreach ($fe['submenu'] as $keyz => $fe_sub) {

      if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

        unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
      }


    }
  }	


}




foreach ($fe_menu as $key => $fe) {



  if(empty($fe['submenu'])){
    echo '<li class="active"><a href="'.base_url($fe['link_menu']).'">'.$fe['menu'].'</a></li>';

  }else{

    echo '
	<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$fe['menu'].' <i class="fa fa-chevron-down"></i></a>
								<div class="dropdown-menu" style="">
									<div class="dropdown-inner">
										<ul class="list-unstyled">';

    foreach ($fe['submenu']  as $keyz => $fe_submenu) {
      

      echo '<li><a href="'.base_url($fe_submenu['link_submenu']).'">'.$fe_submenu['submenu'].'</a></li>';


    }
    echo 
    '</ul>									
	</div>
</div>';
  }


}?>



						</ul>
					</div>
				</div>
			</nav>
			<div class="hero-background">
				<!-- Static Header -->
				<div class="header-text">
					<div class="header-text-inner">
						<h1>Be Creative</h1>
					</div>
				</div><!-- /header-text -->
			</div>
		</div>
	</header>
	<!-- /Section: intro -->


	<!--////////////////////////////////////Container-->
	<?php echo $content ?>

	<!--////////////////////////////////////Footer-->
	<footer id="footer">
		<div class="container">
			<div class="wrap-footer">
				<ul class="list-inline footer-link link">
					<li><a href="<?php echo getSettingDesa()['desa_fb']; ?>">Facebook</a></li>
					<li><a href="<?php echo getSettingDesa()['desa_ig']; ?>">Instagram</a></li>
					
				</ul>
				<div class="copyright">
					<span>Copyright © 2021 - Designed by <a href="https://www.desahub.id" target="_blank">desahub</a></span>
				</div>
			</div> 
		</div>
	</footer>
	<!-- Footer -->
	<div id="page-top"><a href="#page-top" class="btn btn-toTop"><i class="fa fa-angle-double-up"></i></a></div>
	
	<!-- ========== Scripts ========== -->
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/jquery-3.1.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/jquery.localScroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/jquery.scrollTo.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/SmoothScroll.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/wow.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/isotope.pkgd.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/lightbox.min.js'); ?>"></script>
	
	<!-- Definity JS -->
	<script type="text/javascript" src="<?php echo base_url('themefront/zcreative/js/main.js'); ?>"></script>
	
</div>
</body>
</html>