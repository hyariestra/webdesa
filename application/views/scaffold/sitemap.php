<?php
  header('Content-type: application/xml; charset="ISO-8859-1"',true);  
?>
 
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
     <loc><?php echo base_url();?></loc>
     <priority>1.0</priority>
  </url>

<url>
  <loc><?php echo base_url('halaman/tentang-kami');?></loc>
  <lastmod>2020-07-18T18:00:15+00:00</lastmod>
  <changefreq>daily</changefreq>
  </url>
  
  

</urlset>
