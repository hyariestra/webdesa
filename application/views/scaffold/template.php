<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
  <meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
  <meta name="google-site-verification" content="<?php echo getSettingDesa()['desa_metatag']; ?>" />
  <title><?php include "phpmu-title.php"; ?></title>

  <!-- Favicons -->
  <link href="<?php echo base_url('themefront/scaffold/assets/img/favicon.ico')?>" rel="icon">
  <link href="<?php echo base_url('themefront/scaffold/assets/img/apple-touch-icon.png')?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url('themefront/scaffold/assets/vendor/bootstrap/css/bootstrap.min.css'); ?> " rel="stylesheet">
  <link href="<?php echo base_url('themefront/scaffold/assets/vendor/icofont/icofont.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/scaffold/assets/vendor/boxicons/css/boxicons.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/scaffold/assets/vendor/venobox/venobox.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/scaffold/assets/vendor/owl.carousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/scaffold/assets/vendor/aos/aos.css')?>" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url('themefront/scaffold/assets/css/style.css')?>" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Scaffold - v2.2.1
  * Template URL: https://bootstrapmade.com/scaffold-bootstrap-metro-style-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

<style>
  .img-why{
    width: 50px;
    margin-bottom: 10px;
  }
</style>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light">
        <a href="<?php echo base_url('/') ?>">
								<img src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="<?php echo getSettingDesa()['desa_nama']; ?>_logo">
							</a>
        </h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>

        <?php



$fe_menu  =  $this->M_homepage->get_front_menu_tree();
$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


foreach ($fe_menu as $key => $fe) {
  
  if (!in_array($fe['id_menu'], $arrayMenu)) {
    unset($fe_menu[$key]);
  }

  if(!empty($fe['submenu'])){

    foreach ($fe['submenu'] as $keyz => $fe_sub) {

      if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

        unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
      }


    }
  }	

    //	unset($fe_menu[4]['submenu'][7]);
}


foreach ($fe_menu as $key => $fe) {

  if(empty($fe['submenu'])){
    echo '<li class="active"> <a href='.base_url($fe['link_menu']).'>'.$fe['menu'].'</a> </li>';

  }else{

    echo '
    <li class="drop-down"><a href="#">'.$fe['menu'].'</a>
								<ul>';

    foreach ($fe['submenu']  as $keyz => $fe_submenu) {
      
      echo '<li><a href="'.base_url($fe_submenu['link_submenu']).'">'.$fe_submenu['submenu'].'</a></li>';
    
    }
    echo 
     '</ul>
		</li>';
  }


}?>

        </ul>
      </nav><!-- .nav-menu -->
  
    <div class="header-social-links">
        <a href="<?= getSettingDesa()['desa_fb'] ?>" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="<?= getSettingDesa()['desa_ig'] ?>" class="instagram"><i class="icofont-instagram"></i></a>
        <!-- <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> -->
      </div>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <!-- End Hero -->
<?php echo $content ?>
  <!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6">
            <div class="footer-info">
             <div class="image-footer">
             <img  style="width: 150px;" src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="<?php echo getSettingDesa()['desa_nama']; ?>_logo">
             </div>
              <p>
                <br>
              <?php echo getSettingDesa()['desa_lokasi']; ?><br>
              <br>
                <strong>Phone:</strong> <?php echo getSettingDesa()['desa_telp']; ?><br>
                <strong>Email:</strong> <?php echo getSettingDesa()['desa_email']; ?><br>
              </p>
              <div class="social-links mt-3">
                <a href="<?= getSettingDesa()['desa_fb'] ?>" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="<?= getSettingDesa()['desa_ig'] ?>" class="instagram"><i class="bx bxl-instagram"></i></a>
             
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>

            <?php
            
            foreach ($fe_menu as $key => $fe) {
            
              if(empty($fe['submenu'])){
                echo '<li><i class="bx bx-chevron-right"></i> <a href='.base_url($fe['link_menu']).'>'.$fe['menu'].'</a> </li>';
              }
            }

            
            ?>
       

            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
            <?php
                    
            
                    $ref_porto = $this->M_galeri->get_ref_porto();

            foreach ($ref_porto as $item) {
              ?>
                  <li><i class="bx bx-chevron-right"></i><?php echo $item['nama_porto'] ?></li>
                  
                  <?php } ?>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Berlangganan untuk mendapat berita atau promo terbaru dari kami</p>
            <form action="<?php echo base_url('berita/news_save_php') ?>" method="POST">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Scaffold</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/scaffold-bootstrap-metro-style-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="bx bxs-up-arrow-alt"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/jquery/jquery.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/jquery.easing/jquery.easing.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/php-email-form/validate.js')?> "></script>
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/isotope-layout/isotope.pkgd.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/venobox/venobox.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/owl.carousel/owl.carousel.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/scaffold/assets/vendor/aos/aos.js')?> "></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url('themefront/scaffold/assets/js/main.js')?> "></script>

</body>

</html>