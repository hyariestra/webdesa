<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="<?php echo base_url(''); ?>">Home</a></li>
          <li>Inner Page</li>
        </ol>
      </div>
    </section><!-- End Breadcrumbs -->

    <section class="inner-page">
      <div class="container">
       
            <?php echo $detail['isi_halaman']; ?>
       
      </div>
    </section>

  </main>