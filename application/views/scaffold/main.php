<section id="hero">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center" data-aos="fade-up">
          <div>
            <h1>Kita Bantu Untuk Mengembangkan Bisnis Anda</h1>
            <h2>Online Kan Bisnismu</h2>
            <a href="#about" class="btn-get-started scrollto">Get Started</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
          <img src="<?php echo base_url('themefront/scaffold/assets/img/hero-img.png')?> " class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section>
<main id="main">

    <!-- ======= About Section ======= -->
    <!-- <section id="about" class="about">
      <div class="container">

        <div class="row">
          <div class="col-lg-6" data-aos="zoom-in">
            <img src="<?php echo base_url('themefront/scaffold/assets/img/about.jpg')?> " class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 d-flex flex-column justify-contents-center" data-aos="fade-left">
            <div class="content pt-4 pt-lg-0">
              <h3>Learn more about us</h3>
              <p class="font-italic">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua.
              </p>
              <ul>
                <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="icofont-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperd</li>
              </ul>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate tera noden carma palorp mades tera.
              </p>
            </div>
          </div>
        </div>

      </div>
    </section> -->
    <!-- End About Section -->

    <!-- ======= Features Section ======= -->
    <!-- End Features Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Services</h2>
          <p>Apa yang dapat kami lakukan untuk anda.</p>
        </div>

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in">
            <div class="icon-box icon-box-pink">
              <div class="icon">
             <img class="img-why" src=" <?php echo base_url('themefront/scaffold/assets/img/icon/wireframe.png')?>" alt="">
              </div>
              <h4 class="title"><a href="#">Pengembangan Website</a></h4>
              <p class="description">Kami memahami dan menawarkan solusi untuk berbagai kebutuhan anda dalam pengembangan website untuk menjawab tantangan dan solusi bisnis anda seperti civitas academy, ecommerce/toko online, perusahaan, hotel/travel, portal berita dan juga maintenace rutin pengembangan website anda</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
            <div class="icon">
             <img class="img-why" src="<?php echo base_url('themefront/scaffold/assets/img/icon/settings.png')?>" alt="">
              </div>
              <h4 class="title"><a href="#">Sistem Informasi</a></h4>
              <p class="description">Kelola bisnis anda secara efektif dengan sistem informasi yang terintegrasi. kami membantu Anda dengan solusi yang tepat guna dengan kebutuhan perusahaan Anda Seperti aplikasi Finance, HRD, ERP .</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box icon-box-green">
            <div class="icon">
             <img class="img-why" src="<?php echo base_url('themefront/scaffold/assets/img/icon/clipboard.png')?>" alt="">
              </div>
              <h4 class="title"><a href="#">Konsultasi</a></h4>
              <p class="description">Hilangkan kekhawatiran Anda sekarang juga. Kami selalu ada untuk Anda. Jangan sungkan untuk mendiskusikan masalah Anda dengan kami. Kami menawarkan solusi IT dengan sudut pandang inovatif.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box icon-box-blue">
            <div class="icon">
             <img class="img-why" src="<?php echo base_url('themefront/scaffold/assets/img/icon/gallery.png')?>" alt="">
              </div>
              <h4 class="title"><a href="#">Content creator</a></h4>
              <p class="description">Kami juga dapat membantu melakukan branding kepada usaha anda, seperti pembuatan video company profile dan penulisan konten di laman wesbite anda</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Portfolio</h2>
          <p>Beberapa Hasil Perkerjaan Kami</p>
        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>

                    <?php
                    
                      foreach ($ref_porto as $item) {
                        ?>
                    <li data-filter=".<?php echo $item['label_porto'] ?>"><?php echo $item['nama_porto'] ?></li>
                    
                    <?php } ?>


          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

        <?php
        
        foreach ($porto as $key => $item) {
          
       ?>
        

        <div class="col-lg-4 col-md-6 portfolio-item <?php echo $item['label_porto'] ?>">
            <div style="width:350px;height:243px" class="portfolio-wrap">
              <img src="<?php echo base_url('asset/'.$item['gambar'].' ')?>" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><?php echo $item['judul_porto'] ?></h4>
               
              </div>
              <div class="portfolio-links">
                <a href="<?php echo base_url('asset/'.$item['gambar'].' ')?>" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
                <a href="<?php echo $item['url'] ?>" title="More Details"><i class="bx bx-link"></i></a>
              </div>
            </div>
          </div>
        
       <?php } ?>

        

        </div>

      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container">

        <div class="row" data-aos="zoom-in">
          <div class="col-lg-9 text-center text-lg-left">
            <h3>Yuk Konsultasi Gratis !</h3>
            <p> Kami dengan senang hati mendengarkan masalah dan keluhan anda agar dapat memberikan solusi terbaik dari tim terbaik pula.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a target="_blank" style="background : #11b519" class="cta-btn align-middle" href="https://web.whatsapp.com/send?phone=<?php echo getSettingDesa()['desa_telp']; ?>"><i class="icofont-brand-whatsapp"></i> whatsapp</a>
          </div>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Testimonials</h2>
          <p>Kepuasan klien yang menggunakan produk dan layanan dari kami.</p>
        </div>

        <div class="owl-carousel testimonials-carousel" data-aos="fade-up" data-aos-delay="100">

          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
             Support jika ada kesalahan teknis atau fitur yang kami tidak pahami sungguh memuaskan karena fast response   
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="<?php echo base_url('themefront/scaffold/assets/img/testimonials/testimonials-1.jpg')?> " class="testimonial-img" alt="">
            <h3>Restu Putra</h3>
            <h4>Staff TU SD Bhayangkara </h4>
          </div>

          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Aplikasi Akuntansi nya mudah digunakan dan dapat di custom sesuai keinginan kamu
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="<?php echo base_url('themefront/scaffold/assets/img/testimonials/testimonials-2.jpg')?> " class="testimonial-img" alt="">
            <h3>Nana Kamila</h3>
            <h4>Akuntan Koperasi</h4>
          </div>

          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Desa Karangwuni terbantu dengan ada nya website karena sebagai branding desa dan mengenalkan produk hasil desa kami lebih luas
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="<?php echo base_url('themefront/scaffold/assets/img/testimonials/testimonials-3.jpg')?> " class="testimonial-img" alt="">
            <h3>Rohmawati Sendi</h3>
            <h4>Carik Desa Karangwuni</h4>
          </div>

          

          

        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    <!-- End Team Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Mitra</h2>
          <p>Beberapa mitra kami yang PUAS dengan layanan kami.</p>
        </div>

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

    <?php

    
  
    foreach ($client  as $value) {

      ?>
      
    
      <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo" data-aos="zoom-in">
              <img title="<?php echo $value['nama_client'] ?>" src="<?php echo base_url('asset/'.$value['gambar'].' ')?> " class="img-fluid" alt="">
            </div>
          </div>
    
  
  <?php } ?>

          

    
        </div>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Pricing Section ======= -->
    <!-- End Pricing Section -->

    <!-- ======= F.A.Q Section ======= -->
    <section id="faq" class="faq">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Frequently Asked Questions</h2>
        </div>

        <ul class="faq-list">

          <li data-aos="fade-up">
            <a data-toggle="collapse" class="" href="#faq1">Berapa lama yang dibutuhkan untuk membuat sebuh website? <i class="icofont-simple-up"></i></a>
            <div id="faq1" class="collapse show" data-parent=".faq-list">
              <p>
                Pembuatan website memiliki tahap mulai dari analisis kebutuhan, membuat template, mengisi konten. Biasanya memakan waktu 1-30 hari.
              </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="100">
            <a data-toggle="collapse" href="#faq2" class="collapsed">Berapa biaya yang diperlukan dalam pembuatan sebuah website? <i class="icofont-simple-up"></i></a>
            <div id="faq2" class="collapse" data-parent=".faq-list">
              <p>
                Biaya pembuatan website juga sudah meliputi biaya maintenance selama kontrak masih berjalan, untuk biaya berbeda beda tergantung tingkat kesulitan website, untuk harga silahkan berkonsultasi dulu dengan tim kami tanpa dikenakan biaya.
              </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="200">
            <a data-toggle="collapse" href="#faq3" class="collapsed">Bagaimana jika terdapat bug pada website? <i class="icofont-simple-up"></i></a>
            <div id="faq3" class="collapse" data-parent=".faq-list">
              <p>
                Segala jenis bug pada fitur website akan kami benahi tanpa dikenakan biaya tambahan.
              </p>
            </div>
          </li>

          <li data-aos="fade-up" data-aos-delay="300">
            <a data-toggle="collapse" href="#faq4" class="collapsed">Bagaimana jika client tidak dapat mengoprasikan website nya? <i class="icofont-simple-up"></i></a>
            <div id="faq4" class="collapse" data-parent=".faq-list">
              <p>
                Kami akan adakan pelatihan kepada user / client agar dapat mengoprasikan website secara mandiri.
              </p>
            </div>
          </li>

          


        </ul>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Contact Us</h2>
        </div>

        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch" data-aos="fade-right">
            <div class="info">
              <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Lokasi:</h4>
                <p><?php echo getSettingDesa()['desa_lokasi']; ?></p>
              </div>

              <div class="email">
                <i class="icofont-envelope"></i>
                <h4>Email:</h4>
                <p><?php echo getSettingDesa()['desa_email']; ?></p>
              </div>

              <div class="phone">
                <i class="icofont-phone"></i>
                <h4>WA / Phone:</h4>
                <p><?php echo getSettingDesa()['desa_telp']; ?></p>
              </div>

              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d247.3498103085961!2d110.10480955216897!3d-7.286612641003283!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7077ba9f591285%3A0x8361df594846a999!2sJubug%2C%20Wanutengah%2C%20Parakan%2C%20Temanggung%20Regency%2C%20Central%20Java%2056254!5e0!3m2!1sen!2sid!4v1631090283283!5m2!1sen!2sid" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
            </div>

          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch" data-aos="fade-left">
            <form action="<?php echo base_url('pesan/kirim') ?>" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">Your Name</label>
                  <input type="text" name="name" class="form-control" id="name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                
                <div class="form-group col-md-6">
                  <label for="name">Your Email</label>
                  <input type="email" class="form-control" name="email" id="email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
                <div class="form-group col-md-12">
                  <label for="name">Telp</label>
                  <input type="text" name="telp" class="form-control" id="telp" data-rule="minlen:8" data-msg="Please enter at least 8 chars" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Subject</label>
                <input type="text" class="form-control" name="subject" id="subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <label for="name">Message</label>
                <textarea class="form-control" name="message" rows="10" data-rule="required" data-msg="Please write something for us"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main>