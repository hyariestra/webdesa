<style>
  .owl-stage-outer{
    height: 1000px;
  }
</style>
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="<?php echo base_url('/') ?>">Home</a></li>
          <li>Portfolio Details</li>
        </ol>
        <h2>Portfolio Details</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="portfolio-details-container">

          <div class="owl-carousel portfolio-details-carousel">

          <?php
          

          for ($i=1; $i < 4; $i++) { 
          
            if(!empty($detail['image'][$i]))
            {
              echo "<img src=".base_url()."asset/foto_porto/".rawurlencode($detail['image'][$i])." class='img-fluid' >";
            }else{
              echo "";
            }
          
         } ?>
          
          </div>

          <div class="portfolio-info">
            <h3>Project information</h3>
            <ul>
              <li><strong>Category</strong>: <?php echo $detail['nama_porto']; ?></li>
              <li><strong>Client</strong>: <?php echo $detail['client']; ?></li>
              <li><strong>Project Start</strong>: <?php echo $detail['project_start']; ?></li>
              <li><strong>Project End</strong>: <?php echo $detail['project_end']; ?></li>
              <li><strong>Project URL</strong>: <a target="_blank" href="http://<?php echo $detail['project_url']; ?>"><?php echo $detail['project_url']; ?></a></li>
            </ul>
          </div>

        </div>

        <div class="portfolio-description">
          <h2><?php echo $detail['judul'] ?></h2>
          <p>
          <?php echo $detail['isi_porto'] ?>  
        </p>
        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main>