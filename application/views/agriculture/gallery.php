<section id="page-content" class="index-page">
		<div class="wrap-container">
			
			
			<section class="content-box box-3 box-style-1">
				<div class="zerogrid">
					<div class="wrap-box"><!--Start Box-->
						<div class="header wow fadeInUp" data-wow-delay=".1s" data-wow-duration="1s">
							<h2>Photo Gallery</h2>
						</div>
						<div class="content wow fadeIn" data-wow-delay=".1s" data-wow-duration="1s">
							
							<div class="row">
								
								<?php
								
							foreach ($galeri as $key => $item) 
								
									
								{ ?>
								
								
								
								<div class="lg-1-4 md-1-3 sm-1-2">
									<div class="wrap-col">
										<div class="product"> 
											<div class="product-thumbnail"><a  href="<?php echo base_url('asset/'.$item['gambar']); ?>" data-lightbox="example-set" data-title="<?php echo $item['keterangan'] ?>"><img class="gal-img" src="<?php echo base_url('asset/'.$item['gambar']); ?>"></a></div>
											<div class="product-content">
												<div class="row">
													<h2 class="product-title"><a href="#"><?php echo $item['judul'] ?></a></h2>
													
												</div>
												
											</div>
										</div>
									</div>
								</div>

								<?php } ?>

							</div>
						</div>
					</div>
				</div>
			</section>
			
			
		
			
		</div>
		</section>