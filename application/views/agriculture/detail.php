<section id="page-content">
		<div class="wrap-container zerogrid">
			<div id="about-us">
				<article class="post-entry single-post">
					<div class="wrap-post">
						<div class="entry-header">
							<h1 class="entry-title"><?php echo $detail['berita']['judul'] ?></h1>
							<div class="entry-meta">
								<a href="#"><i class="fa fa-calendar"></i> <?php echo tgl_indo($detail['berita']['tanggal']) ?></a>
								<a href="<?php echo $detail['berita']['url_kategori'] ?>"><i class="fa fa-tag"></i> <?php echo $detail['berita']['nama_kategori'] ?></a>
							</div>
						</div>
						<div class="post-thumbnail-wrap">
						<?php echo $detail['berita']['image']; ?>

						</div>
						<div class="entry-content">
						<?php echo $detail['berita']['isi_berita']; ?>
						</div>
					</div>
				</article>
				<div class="zerogrid">
					<div class="comments-are">
						
					</div>
				</div>
			</div>
		</div>
	</section>