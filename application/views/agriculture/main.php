<section id="page-content" class="index-page">
		<div class="wrap-container">
			<!-----------------content-box-1-------------------->
			<section class="content-box box-1">
				<div class="zerogrid">
					<div class="wrap-box"><!--Start Box-->
						<div class="header wow fadeInUp" data-wow-delay=".1s" data-wow-duration="1s">
							<h2>Vision and Mission</h2>
						
						</div>
						<div class="content">
							<div class="row">
								<div class="lg-1-3 wow fadeInLeft" data-wow-duration=".8s">
									<div class="ft-item">
										
										<h5>Big Market</h5>
										<p>Improve the local and global market share</p>
									</div>
									<div class="ft-item">
										<h5>Technology</h5>
										<p>Using modern machines in the field new product development</p>
									</div>
									
								</div>
								<div class="lg-1-3">
									<img style="width: 100%;" src="<?php echo base_url('themefront/agriculture/images/frontcoff.jpg'); ?>" alt="" class="ft-image-center wow fadeInUp" data-wow-delay=".1s" data-wow-duration="1s" />
								</div>
								<div class="lg-1-3 t-right-xs wow fadeInRight" data-wow-duration=".8s">
									<div class="ft-item">
										
										<h5>Human Resources</h5>
										<p>Improving the quality of human resources through training</p>
									</div>
									<div class="ft-item">
										
										<h5>Commitments</h5>
										<p>Realizing sustainability commitments in all operations</p>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<!-----------------content-box-2-------------------->
			<section class="content-box box-2 box-style-2">
				<div class="zerogrid">
					<div class="wrap-box"><!--Start Box-->
						<div class="content wow fadeInLeft" data-wow-delay=".1s" data-wow-duration="1s">
							<div class="row">
								<div class="sm-1-2 offset-sm-2-4">
									<blockquote><p>To be a growing company and world-leading. Indonesian Commodities / Products dedicated to delivering superior value to customers, shareholders and employees</p></blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<!-----------------content-box-3-------------------->
			<section class="content-box box-3 box-style-1">
				<div class="zerogrid">
					<div class="wrap-box"><!--Start Box-->
						<div class="header wow fadeInUp" data-wow-delay=".1s" data-wow-duration="1s">
							<h2>BEST SELLER</h2>
						</div>
						<div class="content wow fadeIn" data-wow-delay=".1s" data-wow-duration="1s">
							
							<div class="row">
								
								<?php
								
					
								$selectedHeadline = sliceArrayByCategoryByCode($headline, 'produk-unggul', 8);

								foreach ($selectedHeadline as $key => $item) 
									
								{ ?>
								
								
								
								<div class="lg-1-4 md-1-3 sm-1-2">
									<div class="wrap-col">
										<div class="product"> 
											<div class="product-thumbnail"><a href="<?php echo $item['url']; ?>"><img class="gal-img" src="<?php echo base_url('asset/'.$item['gambar']); ?>"></a></div>
											<div class="product-content">
											<div class="row">
													<h2 class="product-title"><a href="<?php echo $item['url']; ?>"><?php echo $item['judul'] ?></a></h2>
													
												</div>
												
											</div>
										</div>
									</div>
								</div>

								<?php } ?>

							</div>
						</div>
					</div>
				</div>
			</section>
			
			<!-----------------content-box-4-------------------->
			<!-- <section class="content-box box-4">
				<div class="zerogrid-fluid">
					<div class="wrap-box">
						<div class="header wow fadeInUp" data-wow-delay=".1s" data-wow-duration="1s">
							<h2>Our Service</h2>
							<span class="intro">Lorem ipsum dolor sit amet consectetur adipisicing</span>
						</div>
						<div class="content wow fadeIn" data-wow-delay=".1s" data-wow-duration="1s">
							<div class="row">
								
								
							<?php
								
								
					
								$items = sliceArrayByCategoryByCode($itemAll, 'produk', 4);

								foreach ($items as $key => $item) 
									
								{ ?>

								<div class="lg-1-4 md-1-3 sm-1-2">
									<div class="portfolio-box zoom-effect">
									<img class="service-img" src="<?php echo base_url('asset/'.$item['gambar']); ?>" class="img-responsive"/>
										<div class="portfolio-box-caption">
											<div class="portfolio-box-caption-content">
												<div class="project-name">
													<?php echo $item['judul'] ?>
												</div>
												<div class="project-category">
													<?php echo $item['kategori'] ?>
												</div>
												<div class="project-button">
													<a href="<?php echo $item['url'] ?>" class="button button-skin">Read More</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			
			<!-----------------content-box-5-------------------->
			<section class="content-box box-5">
				<div class="zerogrid">
					<div class="wrap-box"><!--Start Box-->
						<div class="header wow fadeInUp" data-wow-delay=".1s" data-wow-duration="1s">
							<h2>Latest News</h2>
						
						</div>
						<div class="content wow fadeInRight" data-wow-delay=".1s" data-wow-duration="1s">
							<div class="row">
								
							<?php
									
							$selectedNews = sliceArrayByCategoryByCode($itemAll, 'news', 3);

							foreach ($selectedNews as $data) {
								
							?>
								
								<div class="sm-1-3">
									<div class="wrap-col">
										<div class="box-entry">
											<div class="box-entry-inner">
												<img class="gal-img" src="<?php echo base_url('asset/'.$data['gambar']); ?>" class="img-responsive"/>
												<div class="entry-details">
													<div class="entry-des">
														<span><?php echo tgl_indo($data['tanggal']) ?></span>
														<h3><a href="<?php echo $data['url'] ?>"><?php echo $data['judul'] ?></a></h3>
														<p><?php echo $data['isi_berita']; ?></p>
														<a class="button button-skin" href="<?php echo $data['url'] ?>">Read More</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<?php } ?>

							</div>
						</div>
					</div>
				</div>
			</section>
			
			<!-----------------content-box-6-------------------->
			
			
			<!-----------------content-box-7-------------------->
			
			
		</div>
		</section>