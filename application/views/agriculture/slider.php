<?php


$judul = $this->uri->uri_string() == '' ? 'Premium House Blend Coffee Java Island' : $detail['judul_halaman'];

?>

<div id="top-slide" class="main-header">
				<div class="bg-overlay">
					<!---Hero Content--->
					<div class="hero-content-wrapper">
					  <div class="hero-content">
						<h4 class="h-alt hero-subheading wow fadeIn" data-wow-duration="2s" data-wow-delay=".7s">Best Gardening Services</h4>
						<h1 class="hero-lead wow fadeInLeft" data-wow-duration="1.5s"><?php echo $judul; ?></h1>
						<a style="display: <?php echo $display ?>" href="single.html" class="button button-skin wow fadeIn" data-wow-duration="2s" data-wow-delay="1s">Read More</a>
					  </div>
					</div>
					<!---Scroller--->
					<div style="display: <?php echo $display ?>;" id="scroll-page-content">
						<a href="#page-content" class="scroller">
							<span class="scroller-text">scroll down</span>
							<div class="scroller-button">
								<i class="fa fa-angle-double-down"></i>
							</div>
						</a>
					</div>
				</div>
			</div>
        