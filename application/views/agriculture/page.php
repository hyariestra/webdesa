<style>
		img.gal-img{
		width: 275px;
    	height: 275px;
		object-fit: cover;
	}
</style>

<section id="container">
			<div class="wrap-container">
				<!-----------------Content-Box-------------------->
				<div id="main-content">
					<div class="wrap-content">
						<div class="row">
							
						<?php
						foreach ($item as $key => $value) {

							?>
							<article class="single-post zerogrid">
								<div class="row wrap-post"><!--Start Box-->
									<div style="margin-bottom: 30px !important;" class="entry-header">
										<span class="time"><?php echo tgl_indo($value['tanggal']) ?></span>
										<h2 class="entry-title"><a href="<?php echo $value['url'] ?>"><?php echo $value['judul']; ?></a></h2>
										<span class="cat-links"><a href="<?=  $value['url_kategori'] ?>"><?php echo $value['kategori'] ?></a></span>
									</div>
									<div class="post-thumbnail-wrap">
										<img class="gal-img" src="<?php echo base_url('asset/'.$value['gambar']); ?>">
									</div>
									<div class="entry-content">
										<p><?php echo  strip_tags($value['isi_berita']) ?></p>
										<center><a style="background-color: seagreen;" class="button " href="<?php echo $value['url'] ?>">Read More</a></center>
									</div>
								</div>
							</article>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>