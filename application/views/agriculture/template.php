<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="asset/img/fav.ico" rel="icon">
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
  <meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
  <meta name="google-site-verification" content="<?php echo getSettingDesa()['desa_metatag']; ?>" />
  <title><?php include "phpmu-title.php"; ?></title>
	
    <!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- CSS
  ================================================== -->
	<!-- Definity CSS -->
  	<link href="<?php echo base_url('themefront/agriculture/css/zerogrid.css'); ?>" rel="stylesheet" media="all" type="text/css">
	<link href="<?php echo base_url('themefront/agriculture/css/style.css'); ?>" rel="stylesheet" media="all" type="text/css">
	<link href="<?php echo base_url('themefront/agriculture/css/menu.css'); ?>" rel="stylesheet" media="all" type="text/css">
	<link href="<?php echo base_url('asset/global_asset/lightbox/dist/css/lightbox.min.css'); ?>" rel="stylesheet" media="all" type="text/css">
	<!-- Lightbox -->
	<!-- Video Background -->
	<!-- Animate.css -->
	<link href="<?php echo base_url('themefront/agriculture/css/animate.css'); ?>" rel="stylesheet" media="all" type="text/css">
	
	<!-- Custom Fonts -->
	<link href="<?php echo base_url('themefront/agriculture/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
	
	<!-- Owl Carousel Assets -->
    <link href="<?php echo base_url('themefront/agriculture/owl-carousel/owl.carousel.css'); ?>" rel="stylesheet">
    <!-- <link href="owl-carousel/owl.theme.css" rel="stylesheet"> -->
	
	<!-- JS -->
	
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<script src="js/css3-mediaqueries.js"></script>
	<![endif]-->
    
</head>

<!-- custom css -->

<style>
	.product-title{
		max-width: calc(100% - 44px) !important;
	}
	img.gal-img{
		width: 275px;
    	height: 275px;
		object-fit: cover;
	}
	img.service-img{
		width: 338px;
    	height: 475px;
		object-fit: cover;
	}
</style>


<?php  
$class = $this->uri->uri_string() == '' ? '' : 'sub-page'; 



?>

<body id="page-top" class="<?php echo $class; ?>" >
<div class="wrap-body">

	<!--////////////////////////////////////Header-->
	<header>
		<div class="wrap-header" >
			
			<!---Main Header--->
			<?php echo $slider; ?>
			
			<!---Top Menu--->
			<div id="cssmenu" >
			
				<ul>

<?php
		$fe_menu  =  $this->M_homepage->get_front_menu_tree();
		$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


		foreach ($fe_menu as $key => $fe) {
			
			if (!in_array($fe['id_menu'], $arrayMenu)) {
				unset($fe_menu[$key]);
			}

			if(!empty($fe['submenu'])){

				foreach ($fe['submenu'] as $keyz => $fe_sub) {

					if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

						unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
					}


				}
			}	

				//	unset($fe_menu[4]['submenu'][7]);
		}


		foreach ($fe_menu as $key => $fe) {


			if(empty($fe['submenu'])){
				echo '<li class="active"><a href="'.base_url($fe['link_menu']).'"><span>'.$fe['menu'].'</span></a></li>';

			}else{

				echo '
				<li class="has-sub"><a href="#"><span>'.$fe['menu'].'</span></a>
				<ul>';

				foreach ($fe['submenu']  as $keyz => $fe_submenu) {
				
					echo '<li class="last"><a href="'.base_url($fe_submenu['link_submenu']).'"><span>'.$fe_submenu['submenu'].'</span></a></li>';

				}
				echo 
				'</ul>
				</li>';
			}

		}?>

</ul>



			</div>
		</div>
	</header>


	<!--////////////////////////////////////Container-->
	<?php echo $content ?>
	<!--////////////////////////////////////Footer-->
	<footer id="page-footer">
		<div class="zerogrid wrap-footer">
			<div class="row">
				<div class="sm-1-4 col-footer-1">
					<div class="wrap-col">
						<h3 class="widget-title">About Us</h3>
						<p>Ut volutpat consectetur aliquam. Curabitur auctor in nis ulum ornare. Metus elit vehicula dui. Curabitur auctor in nis ulum ornare. Sed consequat, augue condimentum fermentum</p>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque la udantium</p>
					</div>
				</div>
				<div class="sm-1-4 col-footer-2">
					<div class="wrap-col">
						<h3 class="widget-title">Popular Post</h3>
						<ul>
						<?php
								
								$popular = $this->M_berita->get_berita_popular_selected(5,'news');

								foreach ($popular as $item) {
									
								?>
							<li><a href="<?php $item['url'] ?>"><?php echo $item['judul'] ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="sm-1-4 col-footer-4">
					<div class="wrap-col">
						<h3 class="widget-title">Gallery</h3>
						<div class="row">
							
						<?php 
							
							$galeri  = $this->M_galeri->get_galeri_homepage(3,'Y');

								foreach ($galeri as $item) {
				
							?>
							<div class="sm-1-3 xs-1-2">
								<div class="wrap-col">
									<a href="#"><img src="<?php echo base_url('asset/'.$item['gambar']); ?>"></a>
								</div>
							</div>

							<?php } ?>
						</div>
					</div>
				</div>
				<div class="sm-1-4 col-footer-2">
					<div class="wrap-col">
						<h3 class="widget-title">Contact Us</h3>
						<ul>
							<li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo getSettingDesa()['desa_email']; ?></a></li>
							<li><a href="#"><i class="fa fa-phone-square" aria-hidden="true"></i> <?php echo getSettingDesa()['desa_telp']; ?></a></li>
							<li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo getSettingDesa()['desa_lokasi']; ?></a></li>
						</ul>
					</div>
				</div>
				
			</div>
		</div>
		<div class="zerogrid bottom-footer">
			<div class="row">
				<div class="bottom-social">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-instagram"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-google-plus"></i></a>
					<a href="#"><i class="fa fa-pinterest"></i></a>
					<a href="#"><i class="fa fa-vimeo"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-youtube"></i></a>
				</div>
			</div>
			<div class="copyright">
				Copyright @ - <a href="<?php echo getSettingDesa()['desa_kode_domain']; ?>" rel='nofollow'><?php echo getSettingDesa()['desa_nama']; ?></a>
			</div>
		</div>
	</footer>
	
	<div id="page-top"><a href="#page-top" class="button button-toTop f-right"><i class="fa fa-angle-double-up"></i></a></div>
	
	<!-- ========== Scripts ========== -->
	<script src="<?php echo base_url('themefront/agriculture/js/jquery-3.1.1.min.js'); ?>"></script>
	<script src="<?php echo base_url('themefront/agriculture/js/menu.js'); ?>"></script>
	<script src="<?php echo base_url('themefront/agriculture/js/jquery.localScroll.min.js'); ?>"></script>
	<script src="<?php echo base_url('themefront/agriculture/js/jquery.scrollTo.min.js'); ?>"></script>
	<script src="<?php echo base_url('themefront/agriculture/js/SmoothScroll.js'); ?>"></script>
	<script src="<?php echo base_url('themefront/agriculture/js/wow.min.js'); ?>"></script>
	
	<!-- Owl Carousel JS -->
	<script src="<?php echo base_url('themefront/agriculture/owl-carousel/owl.carousel.js'); ?>"></script>
	
	<!-- Google Map -->
	<script src="<?php echo base_url('themefront/agriculture/js/google-map.js'); ?>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7V-mAjEzzmP6PCQda8To0ZW_o3UOCVCE&callback=initMap" async defer></script>
    
	
	<!-- Definity JS -->
	<script src="<?php echo base_url('themefront/agriculture/js/main.js'); ?>"></script>
	<script src="<?php echo base_url('asset/global_asset/lightbox/dist/js/lightbox-plus-jquery.min.js'); ?>"></script>
	
</div>
</body>
</html>