<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Baggage Ecommerce Category Bootstrap Responsive Web Template | Home :: W3layouts</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Baggage Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url("themefront/desahub/css/bootstrap.css")?> ">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="<?php echo base_url("themefront/desahub/css/style.css")?> " type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="<?php echo base_url("themefront/desahub/css/font-awesome.css")?>" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet">
    <!-- //Fonts -->
    <link rel="stylesheet" href="<?php echo base_url("themefront/desahub/slick/slick/slick.css")?>" >
    <link rel="stylesheet" href="<?php echo base_url("themefront/desahub/slick/slick/slick-theme.css")?>">

</head>

<body>

    <?php

    $kategory =  $this->M_menu->produk_tree();
    
    $this->M_menu->kunjungan()


    ?>

    <div class="main-sec inner-page">
        <!-- //header -->
        <header id="home">
            <div class="nav-menu">
                <!-- nav -->
                
                <div class="nav-top-wthree">

                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                      <a class="navbar-brand" href="#">Navbar</a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div  class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url('') ?>">Home</a>
                        </li>

                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url('about') ?>">About Us</a>
                        </li>

                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url('produk') ?>">Produk</a>
                        </li>

                        

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Dropdown
                          </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a>
                          </div>
                      </li>

                      <li class="nav-item dropdown has-megamenu">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Mega menus3  </a>
                        <div class="dropdown-menu megamenu" role="menu">
                            <div class="row">


                                <?php 


                                foreach ($kategory as $key => $value) 

                                    {?>

                                        <div class="col-md-3">
                                            <div class="col-megamenu">
                                                <h6 style="margin-left: 9%;" class="title"><?php echo $value['name_head_kategori'] ?></h6>
                                                <ul class="list-unstyled">
                                                    <?php 
                                                    foreach ($value['sub_kategory'] as $key => $v) {?>

                                                             <li><a class="sub-cate" href="<?php echo $v['id_sub_kategori'] ?>"><?php echo $v['name_sub_kategori'] ?></a></li>
                                                       

                                                        <?php } ?>

                                                    </ul>
                                                </div>  <!-- col-megamenu.// -->
                                            </div>

                                        <?php } ?>

                                    </div><!-- end row --> 
                                </div> <!-- dropdown-mega-menu.// -->
                            </li>


                        </ul>
                        <form action="#" method="post" class="newsletter">
                            <input class="search" type="search" placeholder="Search here..." required="">
                            <button class="form-control btn" value=""><span class="fa fa-search"></span></button>
                        </form>


                    </div>
                </nav>

                <div class="clearfix"></div>
            </div>
        </div>
    </header>
    <!-- //header -->
    <!--/banner-->

</div>

<section class="shipping-wthree">
    <?php
    echo $content;
    ?>

</section>



<section class="newsletter-w3pvt py-5">
    <div class="container py-md-5">
        <form method="post" action="#">
            <p class="text-center">Subscribe to the Handbags Store mailing list to receive updates on new arrivals, special offers and other discount information.</p>
            <div class="row subscribe-sec">
                <div class="col-md-9">
                    <input type="email" class="form-control" id="email" placeholder="Enter Your Email.." name="email" required="">

                </div>
                <div class="col-md-3">

                    <button type="submit" class="btn submit">Subscribe</button>
                </div>

            </div>
        </form>
    </div>
</section>
<!--//newsletter -->
<!--/shipping-->
<section class="shipping-wthree">
    <div class="shiopping-grids d-lg-flex">
        <div class="col-lg-4 shiopping-w3pvt-gd text-center">
            <div class="icon-gd"><span class="fa fa-truck" aria-hidden="true"></span>
            </div>
            <div class="icon-gd-info">
                <h3>FREE SHIPPING</h3>
                <p>On all order over $2000</p>
            </div>
        </div>
        <div class="col-lg-4 shiopping-w3pvt-gd sec text-center">
            <div class="icon-gd"><span class="fa fa-bullhorn" aria-hidden="true"></span></div>
            <div class="icon-gd-info">
                <h3>FREE RETURN</h3>
                <p>On 1st exchange in 30 days</p>
            </div>
        </div>
        <div class="col-lg-4 shiopping-w3pvt-gd text-center">
            <div class="icon-gd"> <span class="fa fa-gift" aria-hidden="true"></span></div>
            <div class="icon-gd-info">
                <h3>MEMBER DISCOUNT</h3>
                <p>Register &amp; save up to $29%</p>
            </div>

        </div>
    </div>

</section>
<!--//shipping-->
<!-- footer -->
<div class="footer_agileinfo_topf py-5">
    <div class="container py-md-5">
        <div class="row">
            <div class="col-lg-3 footer_wthree_gridf mt-lg-5">
                <h2><a href="index.html"><span>B</span>aggage
                </a> </h2>
                <label class="sub-des2">Online Store</label>
            </div>
            <div class="col-lg-3 footer_wthree_gridf mt-md-0 mt-4">
                <ul class="footer_wthree_gridf_list">
                    <li>
                        <a href="index.html"><span class="fa fa-angle-right" aria-hidden="true"></span> Home</a>
                    </li>
                    <li>
                        <a href="about.html"><span class="fa fa-angle-right" aria-hidden="true"></span> About</a>
                    </li>
                    <li>
                        <a href="shop.html"><span class="fa fa-angle-right" aria-hidden="true"></span> Shop</a>
                    </li>
                    <li>
                        <a href="shop.html"><span class="fa fa-angle-right" aria-hidden="true"></span>Collections</a>
                    </li>

                </ul>
            </div>
            <div class="col-lg-3 footer_wthree_gridf mt-md-0 mt-sm-4 mt-3">
                <ul class="footer_wthree_gridf_list">
                    <li>
                        <a href="single.html"><span class="fa fa-angle-right" aria-hidden="true"></span> Extra Page</a>
                    </li>

                    <li>
                        <a href="#"><span class="fa fa-angle-right" aria-hidden="true"></span> Terms & Conditions</a>
                    </li>
                    <li>
                        <a href="single.html"><span class="fa fa-angle-right" aria-hidden="true"></span> Shop Single</a>
                    </li>
                    <li>
                        <a href="contact.html"><span class="fa fa-angle-right" aria-hidden="true"></span> Contact Us</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 footer_wthree_gridf mt-md-0 mt-sm-4 mt-3">
                <ul class="footer_wthree_gridf_list">
                    <li>
                        <a href="login.html"><span class="fa fa-angle-right" aria-hidden="true"></span> Login </a>
                    </li>

                    <li>
                        <a href="register.html"><span class="fa fa-angle-right" aria-hidden="true"></span>Register</a>
                    </li>
                    <li>
                        <a href="#"><span class="fa fa-angle-right" aria-hidden="true"></span>Privacy & Policy</a>
                    </li>

                </ul>
            </div>
        </div>

        <div class="w3ls-fsocial-grid">
            <h3 class="sub-w3ls-headf">Follow Us</h3>
            <div class="social-ficons">
                <ul>
                    <li><a href="#"><span class="fa fa-facebook"></span> Facebook</a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span> Twitter</a></li>
                    <li><a href="#"><span class="fa fa-google"></span>Google</a></li>
                </ul>
            </div>
        </div>
        <div class="move-top text-center mt-lg-4 mt-3">
            <a href="#home"><span class="fa fa-angle-double-up" aria-hidden="true"></span></a>
        </div>
    </div>
</div>
<!-- //footer -->

<!-- copyright -->
<div class="cpy-right text-center py-3">
    <p>© 2019 Baggage. All rights reserved | Design by
        <a href="http://w3layouts.com"> W3layouts.</a>
    </p>

</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 <script src="<?php echo base_url("themefront/desahub/slick/slick/slick.min.js")?>"></script>

<script type="text/javascript">
    $(document).on('click', '.dropdown-menu', function (e) {
      e.stopPropagation();
  });
</script>


<script>

    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: false,
      asNavFor: '.slider-nav'
  });
    $('.slider-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      centerMode: true,
      focusOnSelect: true
  });

</script>
</body>

</html>
