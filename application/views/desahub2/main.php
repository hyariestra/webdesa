 <style type="text/css">
     .homepage-card .title:before, .homepage-card .title:after {
    background: rgba(80,55,55,0.15);
    content: "";
    display: block;
    height: 10px;
    margin: 0 auto;
    width: 100px;
    margin-bottom: 10px;
    margin-top: 10px;
}
.img-prog{

border-radius: 50%;
    display: block;
    margin: 0 auto;
}
.img-grid-1{
    width: 106%;
    padding: 6px;
    border-radius: 4%;

}

.grid-image .frame img {
    transition: all .3s;
 }
.grid-image .frame:hover img {
    transform: scale(1.1);
}

.frame{
       display: inline-block; /* shrink wrap to image */
    overflow: hidden; /* hide the excess */
}

.img-prog{
    border: solid 5px rgba(255, 13, 36, 0.3);
}

h2 { 
   position: absolute; 
   top: 200px; 
   left: 0; 
   width: 100%; 
}

 </style>

 <section class="py-5">
        <div class="container py-md-3">
            <div class="row grids-wthree-info text-center">
                <div class="col-md-12 ab-content">
                    <div class="homepage-card">
                        <h4 class="title">DesaHub Misi</h4>
                        <h4>Lorem ipsum dolor sit,Nulla pellentesque dolor ipsum laoreet eleifend integer,Pellentesque maximus libero.</h4>
                    </div>

            
                </div>
            
                  <div class="col-md-4">
                     <img class="img-prog img-responsive" src="<?php echo base_url('themefront/desahub/images/goal-pillar-food.jpg')?>" alt="">
                     MEDIA KREATIF
                    </div>

                    <div class="col-md-4">
                     <img class="img-prog img-responsive" src="<?php echo base_url('themefront/desahub/images/goal-pillar-food.jpg')?>" alt="">
                    </div>

                    <div class="col-md-4">
                     <img class="img-prog img-responsive" src="<?php echo base_url('themefront/desahub/images/goal-pillar-food.jpg')?>" alt="">
                    </div>

                

            </div>
        </div>
    </section>
  <section class="grid-image collections">
    <div class="container-fluid">
        <div class="container">

            <div class="row">
                <div class="col-md-6 frame">
                    <img class="img-grid-1" src="<?php echo base_url('themefront/desahub/images/bg1.jpg')?>" alt="">
                      <h2>A Movie in the Park:<br />Kung Fu Panda</h2>
                </div>

                <div class="col-md-3 frame">
                  <img class="img-grid-1" src="<?php echo base_url('themefront/desahub/images/bg2.jpg')?>" alt="">
              </div>

                <div class="col-md-3 frame"> <img class="img-grid-1" src="<?php echo base_url('themefront/desahub/images/bg3.jpg')?>" alt=""></div>
                
            </div>
        </div>
    </div>
    </section>
    <!-- / -->
    <!--/collections -->
    <section class=" py-5">
        <div class="container py-md-5">

            <h3 class="title-wthree mb-lg-5 mb-4 text-center">Safety Meets Style </h3>
            <div class="row text-center">


                <div class="col-md-4 content-gd-wthree">
                    <img src="themefront/images/c1.jpg" class="img-fluid" alt="" />
                </div>
                <div class="col-md-4 content-gd-wthree ab-content py-lg-5 my-lg-5">
                    <h4>Need Extra Space ?</h4>
                    <p>Lorem ipsum dolor sit,Nulla pellentesque dolor ipsum laoreet eleifend integer,Pellentesque maximus libero.</p>
                    <a href="shop.html" class="btn shop mt-3">Shop Now</a>

                </div>
                <div class="col-md-4 content-gd-wthree">
                    <img src="themefront/images/c2.jpg" class="img-fluid" alt="" />
                </div>
            </div>

        </div>
    </section>
    <!-- //collections-->
    <!-- /mid-section -->
    <section class="mid-section">
        <div class="d-lg-flex p-0">
            <div class="col-lg-6 bottom-w3pvt-left p-lg-0">
                <img src="themefront/images/ab1.jpg" class="img-fluid" alt="" />
                <div class="pos-wthree">
                    <h4 class="text-wthree">50% Off Any <br> Women's Bags</h4>
                    <a href="shop.html" class="btn shop mt-3">Shop Now</a>
                </div>
            </div>
            <div class="col-lg-6 bottom-w3pvt-left bottom-w3pvt-right p-lg-0">
                <img src="themefront/images/ab2.jpg" class="img-fluid" alt="" />
                <div class="pos-w3pvt">
                    <h4 class="text-w3pvt">30% Off Any <br> Men's Bags</h4>
                    <a href="shop.html" class="btn shop mt-3">Shop Now</a>
                </div>
            </div>
        </div>
    </section>
    <!-- //mid-section -->

    <!--/gallery -->
    <section class=" py-5">
        <div class="container py-md-5">


            <div class="row">
                <div class="col-lg-4 gallery-content-info text-center mt-lg-5">
                    <h3 class="title-wthree mb-lg-5 mb-4">Trending Now </h3>
                    <p>Lorem ipsum dolor sit,Nulla pellentesque dolor ipsum laoreet eleifend integer,Pellentesque maximus libero.</p>
                    <a href="shop.html" class="btn shop mt-3">Shop Now</a>

                </div>
                <div class="col-lg-8 gallery-content">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 gal-img">
                            <a href="#gal1"><img src="themefront/images/g1.jpg" alt="Baggage" class="img-fluid mt-4"></a>
                        </div>
                        <div class="col-md-4 col-sm-6 gal-img">
                            <a href="#gal2"><img src="themefront/images/g2.jpg" alt="Baggage" class="img-fluid mt-4"></a>
                        </div>
                        <div class="col-md-4 col-sm-6 gal-img">
                            <a href="#gal3"><img src="themefront/images/g3.jpg" alt="Baggage" class="img-fluid mt-4"></a>
                        </div>
                        <div class="col-md-4 col-sm-6 gal-img">
                            <a href="#gal1"><img src="themefront/images/g4.jpg" alt="Baggage" class="img-fluid mt-4"></a>
                        </div>
                        <div class="col-md-4 col-sm-6 gal-img">
                            <a href="#gal2"><img src="themefront/images/g5.jpg" alt="Baggage" class="img-fluid mt-4"></a>
                        </div>
                        <div class="col-md-4 col-sm-6 gal-img">
                            <a href="#gal3"><img src="themefront/images/g6.jpg" alt="Baggage" class="img-fluid mt-4"></a>
                        </div>

                    </div>
                    <!-- gallery popups -->
                    <!-- popup-->
                    <div id="gal1" class="popup-effect">
                        <div class="popup">
                            <img src="themefront/images/g1.jpg" alt="Popup image" class="img-fluid mt-4" />
                            <a class="close" href="#gallery">&times;</a>
                        </div>
                    </div>
                    <!-- //popup -->
                    <!-- popup-->
                    <div id="gal2" class="popup-effect">
                        <div class="popup">
                            <img src="themefront/images/g2.jpg" alt="Popup image" class="img-fluid mt-4" />
                            <a class="close" href="#gallery">&times;</a>
                        </div>
                    </div>
                    <!-- //popup -->
                    <!-- popup-->
                    <div id="gal3" class="popup-effect">
                        <div class="popup">
                            <img src="themefront/images/g3.jpg" alt="Popup image" class="img-fluid mt-4" />
                            <a class="close" href="#gallery">&times;</a>
                        </div>
                    </div>
                    <!-- //popup -->
                    <!-- //gallery popups -->

                </div>
            </div>

        </div>
    </section>
    <!-- //gallery-->
    <!--/newsletter -->
    