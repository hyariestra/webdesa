<style>
    iframe {
        width: 100%
    }

    img {
        max-width: 100%;
    }

    .coursecard-sidebar-single {
        border: 2px solid #f4f4f4;
        margin-bottom: 10px;
        padding: 10px;

    }

    .buttonBuy {
        margin-top: 20px;
    }
</style>


<section class="w3l-blogpost-content py-5">
    <div class="container py-md-5 py-4">
        <div class="row">
            <div class="text-11 col-lg-8">
                <div class="text11-content">
                    <h4><?php echo $detail['series']; ?></h4>
                    <img src="<?php echo $detail['gambar_thumb']; ?>" class="img-fluid radius-image" alt="" />

                    <?php echo $detail['description']; ?>

                    <div class="row justify-content-center mt-5">
                        <div class="col-md-12 text-center mb-3">
                            <h4 class="font-weight-bold"><i class="fa fa-picture-o" aria-hidden="true"></i> SCREENSHOTS</h4>
                        </div>

                        <?php

                        foreach ($detail['series_gambar'] as $key => $value) {


                        ?>
                            <div class="col-md-4 col-12 mb-4">
                                <div class="card text-center h-100 border-0 rounded shadow-custom">
                                    <div class="card-body p-2">
                                        <a href="<?php echo $value['gambar_screenshot']; ?>" data-lightbox="image-1" data-title="<?php echo $value['desc_series']; ?>">
                                            <img src="<?php echo $value['gambar_screenshot']; ?>" data-src="<?php echo $value['gambar_screenshot']; ?>" data-srcset="<?php echo $value['gambar_screenshot']; ?>">
                                        </a>
                                        <hr>
                                        <h6><?php echo $value['desc_series']; ?></h6>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>




                    <div class="row justify-content-center mt-5">

                        <div class="row">
                            <div class="col-md-4">

                            </div>
                            <div class="coursecard-sidebar-single col-md-4">
                                <div class="col-md-12 text-center">
                                    <h4 class="font-weight-bold"><i class="fa fa-book" aria-hidden="true"></i> GET COURSE</h4>
                                </div>
                                <h5 class="text-center"><?php echo $detail['series_price']; ?></h5>
                                <hr>
                                <ul>
                                    <li>
                                        <img src="<?php echo base_url('asset/global_asset/img/ic_check.svg') ?>" class="icon">
                                        Akses materi selamanya
                                    </li>

                                    <li>
                                        <img src="<?php echo base_url('asset/global_asset/img/ic_check.svg') ?>" class="icon">
                                        Free update materi
                                    </li>
                                    <li>
                                        <img src="<?php echo base_url('asset/global_asset/img/ic_check.svg') ?>" class="icon">
                                        Free update tutorial
                                    </li>

                                    <li>
                                        <img src="<?php echo base_url('asset/global_asset/img/ic_check.svg') ?>" class="icon">
                                        Group private diskusi
                                    </li>

                                    <li>
                                        <img src="<?php echo base_url('asset/global_asset/img/ic_check.svg') ?>" class="icon">
                                        Free source code
                                    </li>



                                </ul>

                                <form method="POST" action="<?php echo base_url('series/carts') ?>" class="form-kirim">

                                    <input name="id" value="<?php echo $detail['id_series_encrypt']; ?>" type="hidden">

                                    <div class="text-center buttonBuy">


                    
                                    <?php if ($detail['is_paid']==1) { ?>
                                      
                                        <button type="submit" class="btn btn-to-profil btn-block"><span style="font-weight: 600; color: white;"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> DAPATKAN KELAS</span></button>

                                            <?php } else { ?>

                                            <button style="background-color: #e32d2d;" type="submit" class="btn btn-to-profil btn-block"><span style="font-weight: 600; color: white;"><i class="fa fa-download" aria-hidden="true"></i> KLAIM GRATIS</span></button>
                                     
                                            <?php } ?>


                                    </div>

                                </form>


                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>


                    </div>



                    <div class="social-share-blog mt-5">
                        <ul class="social m-0 p-0">
                            <li>
                                <p class="m-0 mr-4">Share this post :</p>
                            </li>
                            <li>
                                <a href="#" onclick="shareHit();share_fb(''); return false;" class="facebook"><span class="fa fa-facebook"></span></a>
                            </li>
                            <li><a href="#" onclick="shareHit();share_twitter(''); return false;" class="twitter"><span class="fa fa-twitter"></span></a>

                            </li>
                        </ul>
                    </div>
                    <!-- <div class="nav-links">
                            <div class="nav-previous">
                                <a href="#url" rel="prev">
                                    <img src="<?php echo base_url('themefront/sekolah/assets/images/blog-s1.jpg') ?>" alt="" class="img-fluid radius-image" />
                                    <div class="post-data align-self">
                                        <span class="nav-arrow-label"><i
                                                class="fas fa-arrow-left me-1"></i>Previous</span>
                                    </div>
                                </a>
                            </div>
                            <div class="nav-next text-end">
                                <a href="#url" rel="next">
                                    <div class="post-data align-self">
                                        <span class="nav-arrow-label">Next<i class="fas fa-arrow-right ms-1"></i></span>
                                    </div>
                                    <img src="<?php echo base_url('themefront/sekolah/assets/images/blog-s2.jpg') ?>" alt="" class="img-fluid radius-image" />
                                </a>
                            </div>
                        </div> -->


                </div>
            </div>

            <?php echo $sidebar ?>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
     

        $(".form-kirim").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var formData = new FormData(form[0]);

            var actionUrl = form.attr('action');

            $.ajax({
                type: "POST",
                url: actionUrl,
                processData: false,
                contentType: false,
                data: formData, // serializes the form's elements.
                success: function(strMessage) {
                    processJson(strMessage);
                }
            });

        });



        function processJsonError(result) {
            result = result.responseJSON;
            processJson(result, true);
        }

        function processJson(result) {

            console.log(result);

            var row = result.url;

            var base = '<?php echo base_url() ?>';
            var url = row;

            var base_url = base + url;

            new Noty({
                text: result.message,
                type: result.status_code,
                timeout: 3000,
                theme: 'semanticui'
            }).show();

                window.location = base_url;
            
        }


    });
</script>