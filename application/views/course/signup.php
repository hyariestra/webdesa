<div id="signin">
    <h2 class="text-center">Sign Up Now</h2>
    <form method="POST" action="<?php echo base_url('account/signupfunc') ?>" class="form-kirim login-form">
        <div class="form-group">
            <label for="exampleInputEmail1" class="text-uppercase">Email</label>
            <input name="email" type="email" class="form-control" placeholder="">

        </div>

        <div class="form-group">
            <label for="exampleInputEmail1" class="text-uppercase">Name</label>
            <input name="name" type="text" class="form-control" placeholder="">

        </div>

        <div class="form-group">
            <label for="exampleInputPassword1" class="text-uppercase">Password</label>
            <input name="password" type="password" class="form-control" placeholder="">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1" class="text-uppercase">Confim Password</label>
            <input name="confpassword" type="password" class="form-control" placeholder="">
        </div>


        <div class="form-group">
            <label for="exampleInputPassword1" class="text-uppercase">No Telp</label>
            <input name="phone" type="number" class="form-control" placeholder="">
        </div>



        <div style="margin-top: 10px;margin-bottom: 10px; " class="form-group">

            <?php echo $captcha; ?>

        </div>
        <div class="form-group">

            <input name="captcha" placeholder="" class="form-control" type="text">
        </div>

        <div class="form-group">

            <input value="1" name="subs" type="checkbox" class="form-check-input">
            <small>Berlangganan untuk mendapatkan info jika ada update materi terbaru </small>
        </div>

        
        <div class="form-group">

            <button style="width: 100%;" type="submit" class="btn btn-login float-right">Sign Up</button>
            <div style="text-align: center;" class="or">

                -OR-
                
            </div>
            <a style="width: 100%;" href="<?php echo base_url('account/signin'); ?>" class="btn btn-signup float-right">Sign In</a>
        </div>

    </form>
</div>

<script>
    $(document).ready(function() {
     

        $(".form-kirim").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var formData = new FormData(form[0]);

            var actionUrl = form.attr('action');

            $.ajax({
                type: "POST",
                url: actionUrl,
                processData: false,
                contentType: false,
                data: formData, // serializes the form's elements.
                success: function(strMessage) {
                    processJson(strMessage);
                }
            });

        });



        function processJsonError(result) {
            result = result.responseJSON;
            processJson(result, true);
        }

        function processJson(result) {



            new Noty({
                text: result.message,
                type: result.status_code,
                timeout: 3000,
                theme: 'semanticui'
            }).show();

            if (result.status == 201) {
                window.location = '<?php echo base_url('account/signin') ?>';

            }
        }


    });
</script>