<div class="card-body">

    


    <?php

    foreach ($series as $item) { ?>

        <div class="row">



            <div class="col-md-3 mb-3">
                <img src="<?php echo base_url('asset/' . $item['image']); ?>" class="mr-3 shadow-custom w-100 lazy" style="object-fit: cover;border-radius: .3rem;background:#ffffff">
            </div>
            <div class="col-md-9 mb-3">

                <div class="mb-2" style="color: #696666;">

                    <strong>
                        <?php echo $item['step']; ?>
                    </strong>
                    of

                    <strong>
                        <?php echo $item['total_course']; ?>
                    </strong>

                    complete.

                </div>



                <div class="progress mb-2">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?php echo $item['percent']; ?>%;" aria-valuenow="<?php echo $item['percent']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $item['percent']; ?>"><?php echo $item['percent']; ?>%</div>
                </div>

                <h5 class="mt-2"><?php echo $item['series']; ?></h5>

                <div class="button-app mt-3">
                    <a href="<?php echo $item['url'] ?>" data-turbo="false" class="btn shadow btn-success btn-md"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Continue Learning</a>
                    <a href="https://t.me/joinchat/eQDejFqYHzE5YmNl" class="btn shadow-custom btn-primary btn-md"><i class="fa fa-telegram" aria-hidden="true"></i> Join Private Group</a>
                </div>

                <hr>
                <div class="mt-2">
                    Claimed by :
                    <p>
                        <b><?php echo $item['name']; ?></b> <i>(<?php echo $item['email']; ?>)</i>
                        at <?php echo  tgl_indo($item['claim_date']); ?>
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>

    <?php } ?>

    <div style="text-align: center">
        <nav class="d-flex justify-items-center justify-content-between">
            <div class="d-flex justify-content-between flex-fill d-sm-none">
                <ul class="pagination">

                    <li class="page-item disabled" aria-disabled="true">
                        <span class="page-link">« Previous</span>
                    </li>


                    <li class="page-item">
                        <a class="page-link" href="https://santrikoding.com/account/my-courses?page=2" rel="next">Next »</a>
                    </li>
                </ul>
            </div>

            <div class="d-none flex-sm-fill d-sm-flex align-items-sm-center justify-content-sm-between">
                

                <div>
                <div class="pagination-style text-start mt-5">
                    <?= $pagination; ?>
                </div>
                </div>
            </div>
        </nav>

    </div>
</div>