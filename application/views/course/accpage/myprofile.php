<style>
    .switchToggle input[type=checkbox] {
        height: 0;
        width: 0;
        visibility: hidden;
        position: absolute;
    }

    .switchToggle label {
        cursor: pointer;
        text-indent: -9999px;
        width: 70px;
        max-width: 70px;
        height: 30px;
        background: #d1d1d1;
        display: block;
        border-radius: 100px;
        position: relative;
    }

    .switchToggle label:after {
        content: '';
        position: absolute;
        top: 2px;
        left: 2px;
        width: 26px;
        height: 26px;
        background: #fff;
        border-radius: 90px;
        transition: 0.3s;
    }

    .switchToggle input:checked+label,
    .switchToggle input:checked+input+label {
        background: #3e98d3;
    }

    .switchToggle input+label:before,
    .switchToggle input+input+label:before {
        content: 'No';
        position: absolute;
        top: 5px;
        left: 35px;
        width: 26px;
        height: 26px;
        border-radius: 90px;
        transition: 0.3s;
        text-indent: 0;
        color: #fff;
    }

    .switchToggle input:checked+label:before,
    .switchToggle input:checked+input+label:before {
        content: 'Yes';
        position: absolute;
        top: 5px;
        left: 10px;
        width: 26px;
        height: 26px;
        border-radius: 90px;
        transition: 0.3s;
        text-indent: 0;
        color: #fff;
    }

    .switchToggle input:checked+label:after,
    .switchToggle input:checked+input+label:after {
        left: calc(100% - 2px);
        transform: translateX(-100%);
    }

    .switchToggle label:active:after {
        width: 60px;
    }

    .toggle-switchArea {
        margin: 10px 0 10px 0;
    }
</style>
<div class="card-body">

<form action="<?php echo base_url('account/update_profile'); ?>" class="form-kirim" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="username" class="col-4 col-form-label">Email</label>
            <div class="col-8">
                <input value="<?= $profile['email']; ?>" readonly class="form-control here" required="required" type="text">
            </div>
        </div>


        <div class="form-group">
            <label for="username" class="col-4 col-form-label">Name</label>
            <div class="col-8">
                <input value="<?= $profile['name']; ?>" class="form-control here" required="required" type="text">
            </div>
        </div>

        <div class="form-group">
            <label for="username" class="col-4 col-form-label">Phone Number</label>
            <div class="col-8">
                <input value="<?= $profile['phone']; ?>" name="phone" class="form-control here" required="required" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-4 col-form-label">Old Password</label>
            <div class="col-8">
                <input name="old_pass" placeholder="old password" class="form-control" type="password">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-4 col-form-label">New Password</label>
            <div class="col-8">
                <input name="new_pass" placeholder="new password" class="form-control" type="password">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-4 col-form-label">Retype Password</label>
            <div class="col-8">
                <input name="re_pass" placeholder="retype password" class="form-control" type="password">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-4 col-form-label">Avatar</label>
            <div class="col-8">
                <input name="avatar" class="form-control" type="file">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-4 col-form-label">Berlangganan Informasi </label>
            <div class="col-8">
                <div class="switchToggle">
                    <input <?php echo $profile['checkSubs'] ?> name="subs" type="checkbox" id="switch">
                    <label for="switch">Toggle</label>
                </div>
            </div>
        </div>

        <div style="margin-top: 30px;" class="form-group">
            <div class="col-8">
                <button name="submit" type="submit" class="btn btn-primary">Update My Profile</button>
            </div>
        </div>
    </form>

</div>


<script>
    $(document).ready(function() {
     

        $(".form-kirim").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var formData = new FormData(form[0]);

            var actionUrl = form.attr('action');

            $.ajax({
                type: "POST",
                url: actionUrl,
                processData: false,
                contentType: false,
                data: formData, // serializes the form's elements.
                success: function(strMessage) {
                    processJson(strMessage);
                }
            });

        });



        function processJsonError(result) {
            result = result.responseJSON;
            processJson(result, true);
        }

        function processJson(result) {

            new Noty({
                text: result.message,
                type: result.status_code,
                timeout: 3000,
                theme: 'semanticui'
            }).show();

            if (result.status == 201) {
                window.location = '';

            }
        }


    });
</script>