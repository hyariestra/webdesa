<style>
    .acc-block {
        background: #fcf2f2;
        background: -webkit-linear-gradient(to bottom, #FFB88C, #DE6262);
        /* background: linear-gradient(to bottom, #e8d5ca, #07070794); */
        /* float: left; */
        width: 100%;
        padding: 50px 0;
        margin-top: 41px;
        margin-bottom: -50px;

    }

    .content-acc {
        background-color: white;

    }

    .image-card-custom {
        background-color: #fbb034;
        border-radius: 50%;
        height: 150px;
        -o-object-fit: cover;
        object-fit: cover;
        padding: 7px;
        width: 150px;
    }
</style>
<section class="w3l-blogpost-content py-5">
    <section class="acc-block">
        <div class="container py-md-5 py-4">
            <div class="contents row">
                <div class="col-md-3">

                    <div class="list-group">
                        <div class="card border-0 rounded shadow-custom mb-3">
                            <div class="card-header text-dark">
                                <div>
                                    <h5><i class="fa fa-user"></i> MY PROFILE</h5>
                                </div>
                            </div>
                        
    
                            <div class="card-body">

                                <div class="text-center">
                                    <img class="image-card-custom" src="<?php echo base_url('asset/'.getSessionUser()['avatar']); ?>">
                                </div>
                                <div class="mentor text-center mt-3">
                                    <h5 class="font-weight-bold mt-2"><?php echo getSessionUser()['name']; ?></h5>

                                </div>

                            </div>
                        </div>

                        

                        <a href="<?php echo base_url('account/dashboard') ?>" class="list-group-item <?php echo $this->uri->segment(2) == 'dashboard'  ? 'active' : '';  ?>"><i class="fa fa-tachometer" aria-hidden="true"></i> <span>Dashboard</span></a>
                        <a href="<?php echo base_url('account/mycourse') ?>" class="list-group-item <?php echo $this->uri->segment(2) == 'mycourse'  ? 'active' : '';  ?>"><i class="fa fa-graduation-cap"></i> <span>My Course</span></a>
                        <a href="<?php echo base_url('account/profile') ?>" class="list-group-item <?php echo $this->uri->segment(2) == 'profile'  ? 'active' : '';  ?>"><i class="fa fa-user" aria-hidden="true"></i> <span> Profile</span></a>
                        <a href="<?php echo base_url('account/orders') ?>" class="list-group-item <?php echo $this->uri->segment(2) == 'orders'  ? 'active' : '';  ?>"><i class="fa fa-sticky-note-o" aria-hidden="true"></i> <span> Orders</span></a>
                        <a href="<?php echo base_url('account/logout') ?>" class="list-group-item"><i class="fa fa-sign-out" aria-hidden="true"></i> <span>Log Out</span></a>


                    </div>
                </div>
                <div class="col-md-8">
                    <div class="content-acc">
                        <div class="card-header text-dark">
                            <div>
                                <h5><i class="<?php echo $icon ?>"></i> <?php echo $title; ?></h5>
                            </div>
                        </div>
                        <?php echo $html; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>