<style>
    iframe {
        width: 100%
    }

    img {
        max-width: 100%;
    }

    .coursecard-sidebar-single {
        border: 2px solid #f4f4f4;
        margin-bottom: 10px;
        padding: 10px;

    }

    .buttonBuy {
        margin-top: 20px;
    }
</style>


<section class="w3l-blogpost-content py-5">
    <div class="container py-md-5 py-4">
        <div class="row">
            <div class="text-11 col-lg-12">
                <div class="table-responsive box-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Produk</th>
                                <th class="text-center">Jumlah</th>
                                <th class="text-center">Harga</th>
                                <th class="text-center">Total Harga</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="goods_data">


                            <?php

                            if (empty($item)) {


                            ?>

                                <tr class="row_data">
                                    <td style="text-align: center;" colspan="6">
                                        <i> :: Tidak Ada Produk :: </i>
                                    </td>
                                </tr>

                            <?php } ?>

                            <?php

                            foreach ($item as $key => $item) {


                            ?>

                                <form action="<?php echo base_url('carts/update_keranjang') ?>" class="form-kirim" method="post">
                                <tr class="row_data">
                                    <td class="col-sm-3">
                                        <a class="thumbnail" href="<?= base_url($item['url_series']); ?>"> <img class="media-object" src="<?php echo base_url('asset/' . $item['gambar']) ?>" style="width: 172px; height: 102px;"> </a>
                                    </td>
                                    <td class="col-sm-2 col-md-2"><strong><?php echo $item['nama_produk'] ?> <input name="id_produk[]" value="<?php echo $item['id'] ?>" type="hidden"> </strong></td>
                                    <td class="col-sm-1 col-md-1" style="text-align: center">
                                        <div style="width: 120px;" class="input-qty">

                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    
                                                </span>
                                                <input style="text-align: center;" width="20px" readonly="" min="1" type="text" id="quantity-<?php echo $item['id']; ?>" name="quantity[]" class="form-control input-number" value="1" max="1">
                                                <span class="input-group-btn">
                                                    
                                                </span>
                                            </div>


                                        </div>
                                    </td>
                                    <td class="col-sm-2 col-md-2 text-center"><strong><?php echo $item['harga_rupiah'] ?> <input class="price" value="<?php echo $item['harga']; ?>" type="hidden"> </strong></td>
                                    <td class="col-sm-2 col-md-2 text-center"><strong>
                                            <div class="totalpricehtml"></div> <input class="totalprice" value="" type="hidden">
                                        </strong></td>
                                    <td class="col-sm-1 col-md-1">
                                        <button onclick="delete_func('<?php echo $item['id']; ?>')" type="button" class="btn btn-danger">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>


                            <tr>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>

                                <td style="text-align: center;" class="text-right">
                                    <h3>Total</h3>
                                </td>
                                <td style="text-align: center;" class="text-right">
                                    <h3><strong> <span id="grandTot"> </span></strong></h3> <input id="grandtotText" name="grandtot" type="hidden">
                                </td>
                                <td> &nbsp; </td>
                            </tr>
                            <tr>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                                <td>

                                </td>
                                <td colspan="2">
                                    <a href="<?php echo base_url('series') ?>" class="btn btn-warning">
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i> Lanjut Belanja
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Checkout <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                    </button>
                                </td>

                            </tr>


                            </form>

                        </tbody>
                    </table>

                </div>
            </div>


        </div>
    </div>
</section>

<script>
    $(document).ready(function() {

        $(".form-kirim").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            var formData = new FormData(form[0]);
            
            var actionUrl = form.attr('action');

            $.ajax({
                type: "POST",
                url: actionUrl,
                processData: false,
                contentType: false,
                data: formData, // serializes the form's elements.
                success: function(strMessage) {
                    return false;
                   // processJson(strMessage);
                }
            });

        });



        function processJsonError(result) {
            result = result.responseJSON;
            processJson(result, true);
        }

        function processJson(result) {

            console.log(result);

            var row = result.url;

            var base = '<?php echo base_url() ?>';
            var url = row;

            var base_url = base + url;

            new Noty({
                text: result.message,
                type: result.status_code,
                timeout: 3000,
                theme: 'semanticui'
            }).show();

            window.location = base_url;

        }


    });
</script>


<script>
    $(document).ready(function() {
        hitungDet();
    });

    function minusButton(id) {
        var quantity = parseInt($('#quantity-' + id).val());

        // If is not undefined

        // Increment
        if (quantity > 0) {
            $('#quantity-' + id).val(quantity - 1);
        }


        hitungDet();

    };

    function incrementButton(id) {
        // Stop acting like a button
        // Get the field name
        var quantity = parseInt($('#quantity-' + id).val());

        $('#quantity-' + id).val(quantity + 1);

        hitungDet();

    };



    var hitungDet = function() {


        var grandTot = 0;

        $('#goods_data tr.row_data').each(function() {
            var detQty = parseInt($(this).find('.input-number').val());
            var detPrice = parseInt($(this).find('.price').val());
            var total = detQty * detPrice;
            // var isVat = $(this).find('.taxed').val();

            grandTot += total;

            $(this).find('.totalprice').val(total);
            $(this).find('.totalpricehtml').html(formatRupiah(total, "Rp. "));


        });

        $('#grandTot').html(formatRupiah(grandTot, "Rp. "));
        $('#grandtotText').val(grandTot);


    }

    function delete_func(id) {


        var url = '<?php echo base_url(); ?>';

        swal({
                title: "Are you sure?",
                text: "Anda Yakin menghapus produk ini dari keranjang",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url + 'carts/hapus_keranjang/' + id,
                        type: 'POST',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {

                            new Noty({
                                text: data.message,
                                type: data.status_code,
                                timeout: 3000,
                                theme: 'semanticui'
                            }).show();

                            if (data.status == 201) {
                                window.location = '<?php echo base_url('carts') ?>';

                            }

                        }
                    });
                } else {
                    swal("Data Batal Dihapus");
                }
            });

    }
</script>