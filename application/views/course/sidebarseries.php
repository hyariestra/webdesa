<style>
    .image-card-custom {
        background-color: #fbb034;
        border-radius: 50%;
        height: 150px;
        -o-object-fit: cover;
        object-fit: cover;
        padding: 7px;
        width: 150px;
    }

    .coursecard-sidebar-single {
        border: 2px solid #f4f4f4;
        margin-bottom: 10px;
        padding: 10px;

    }

    .btn-to-profil {
        background-color: #fbb034;
    }
</style>



<div class="sidebar-side col-lg-4 col-md-12 col-sm-12 mt-lg-0 mt-5 ps-md-5">
    <aside class="coursecard-sidebar-single sidebar">


        <!--Blog Category Widget-->
        <div class="sidebar-widget sidebar-blog-category">
            <div class="sidebar-title">
            </div>

            <div class="card border-0 rounded shadow-custom mb-3">
                <h5><i class="fa fa-pencil" aria-hidden="true"></i> DISUSUN OLEH</h5>
                <hr>
                <div class="card-body">

                    <div class="text-center">
                        <img class="image-card-custom" src="<?php echo $detail['foto_admin']; ?>">
                    </div>
                    <div class="mentor text-center mt-3">
                        <h5 class="font-weight-bold mt-2"><?php echo $detail['nama'] ?></h5>
                        <p><i>Full-Stack Web Developer, Content Creator and CO-Founder SantriKoding.com</i></p>
                    </div>
                    <div class="profile-setting text-center text-dark" style="margin-top: 10px">
                        <div class="social text-dark" style="font-size: 30px">
                            <a href="https://www.facebook.com/fikaridaulmaulayya" style="margin-right: 10px"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <!-- <a href="https://twitter.com/maulayyacyber" style="margin-right: 10px"> <i class="fab fa-twitter-square text-dark" style="color: white"></i></a>
                                <a href="https://www.linkedin.com/in/maulayyacyber/" style="margin-right: 10px"> <i class="fab fa-linkedin text-dark" style="color: white"></i></a>
                                <a href="https://github.com/maulayyacyber"> <i class="fab fa-github text-dark" style="color: white"></i></a> -->
                        </div>
                        <hr>
                        <a href="https://santrikoding.com/profile/@maulayyacyber" class="btn btn-to-profil btn-block"><span style="color: white;">LIHAT PROFIL SAYA</span></a>
                    </div>
                </div>
            </div>


        </div>

        <!-- Popular Post Widget-->
        <div class="sidebar-widget popular-posts">
            <div class="card-body">
                <h5 class="font-weight-bold"><i class="fa fa-list-ul"></i> DAFTAR ISI</h5>
                <hr>

                <div class="mt-3" style="overflow: hidden; height: 500px;overflow-y: scroll;padding-bottom:10px">



                    <?php

                    $keys = array_column($detail['series_folder'], 'order_folder');

                    array_multisort($keys, SORT_ASC, $detail['series_folder']);

                    foreach ($detail['series_folder'] as $key => $folder) { ?>

                        <ul class="list-group list-group-flush mb-3">

                            <h6 class="my-1 font-weight-bold"><i class="fa fa-folder-open"></i> <?php echo $folder['series_folder_name']; ?></h6>


                            <?php
                            
                            $keys = array_column($folder['series_detail'], 'order');

                            array_multisort($keys, SORT_ASC, $folder['series_detail']);
                            
                            foreach ($folder['series_detail'] as $key => $value)
                            { ?>

                                <li class="list-group-item" style="border-radius: .5rem!important;margin-left: 27px; margin-right:10px;">
                                    <i class="fa fa-file-alt"></i> <?php echo $value['series_title_detail']; ?>
                                </li>

                            <?php } ?>


                        </ul>


                    <?php } ?>



                </div>
                <hr>

            </div>

        </div>


    </aside>
</div>