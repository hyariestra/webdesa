<div class="sidebar-side col-lg-4 col-md-12 col-sm-12 mt-lg-0 mt-5 ps-md-5">
                    <aside class="sidebar">
                        <div class="sidebar-widget sidebar-blog-category">
                            <div class="sidebar-title">
                                <h4>Search here</h4>
                            </div>
                            <form method="GET" action="<?php echo base_url('berita') ?>" class="search-box" >
                                <div class="form-group">
                                    <input value="<?= @$cari ?>" type="search" name="search" placeholder="Search..." required="">
                                    <button><span class="fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>

                        <!--Blog Category Widget-->
                        <div class="sidebar-widget sidebar-blog-category">
                            <div class="sidebar-title">
                                <h4>Popular Topics</h4>
                            </div>
                            <ul class="blog-cat">
                               
                            <?php
                            
                            foreach ($kategori_count as $key => $value) {
                                
                            
                            
                            ?>
                            <li><a href="<?php echo $value['url_kategori'] ?>"><?php echo $value['nama_kategori'] ?> (<?php echo $value['counts'] ?>) </a></li>
                               
                            <?php } ?>
                            </ul>
                        </div>

                        <!-- Popular Post Widget-->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title">
                                <h4>Populer Posts</h4>
                            </div>

                            
                            <?php

                            foreach ($popular as $key => $value) {

                                ?>

                            <article class="post">
                                <figure class="post-thumb"><img src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt=""
                                        class="radius-image"></figure>
                                <div class="text"><a href="<?php echo $value['url'] ?>"><?php echo $value['judul']; ?></a>
                                </div>
                                <div class="post-info">October 28, 2021</div>
                            </article>

                            <?php } ?>


                        </div>

                        <!-- sidebar sticky -->
                        <div class="sidebar-sticky">
                            <div class="sidebar-sticky-fix">
                                <!-- Tags Widget-->
                                <div class="sidebar-widget popular-tags">
                                    <div class="sidebar-title">
                                        <h4>Tags</h4>
                                    </div>
                                    <?php  
                                    $tag = $this->M_tag->get_tag();

                                    foreach ($tag->result_array() as $key => $value) {
                                        
                                    ?>
                                    <a href="<?php echo base_url('berita?tag='.$value['seo_tag'])  ?>"><?php echo $value['seo_tag'] ?></a>
                                   

                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <!-- //sidebar sticky -->

                    </aside>
                </div>