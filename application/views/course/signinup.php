<style>
    .login-block {
        background: #DE6262;
        /* fallback for old browsers */
        background: -webkit-linear-gradient(to bottom, #FFB88C, #DE6262);
        /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to bottom, #FFB88C, #DE6262);
        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        /* float: left; */
        width: 100%;
        padding: 50px 0;
        margin-top: -50px;
        margin-bottom: -50px;

    }

    .banner-sec {
        background: url(https://static.pexels.com/photos/33972/pexels-photo.jpg) no-repeat left bottom;
        background-size: cover;
        min-height: 500px;
        border-radius: 0 10px 10px 0;
        padding: 0;
    }

    .content-login {
        background: #fff;
        border-radius: 10px;
        box-shadow: 15px 20px 0px rgba(0, 0, 0, 0.1);
    }

    .carousel-inner {
        border-radius: 0 10px 10px 0;
        width: 101% !important;
    }

    .carousel-caption {
        text-align: left;
        left: 5%;
    }

    .login-sec {
        padding: 50px 30px;
        position: relative;
    }

    .login-sec .copy-text {
        position: absolute;
        width: 80%;
        bottom: 20px;
        font-size: 13px;
        text-align: center;
    }

    .login-sec .copy-text i {
        color: #FEB58A;
    }

    .login-sec .copy-text a {
        color: #E36262;
    }

    .login-sec h2 {
        margin-bottom: 30px;
        font-weight: 800;
        font-size: 30px;
        color: #DE6262;
    }

    .login-sec h2:after {
        content: " ";
        width: 100px;
        height: 5px;
        background: #FEB58A;
        display: block;
        margin-top: 20px;
        border-radius: 3px;
        margin-left: auto;
        margin-right: auto
    }

    .btn-login {
        background: #DE6262;
        color: #fff;
        font-weight: 600;
    }

    .btn-signup {
        background: #48c28a;
        color: #fff;
        font-weight: 600;
    }


    .banner-text p {
        color: #fff;
    }

    @media screen and (max-width: 540px) {
        .banner-sec {
           display: none;
        }
    }

    /* For Tablets */
    @media screen and (min-width: 540px) and (max-width: 780px) {
        .banner-sec {
            display: none;
        }
    }
</style>
<section class="w3l-contact py-5" id="contact">
    <section class="login-block">
        <div class="container content-login">
            <div class="row">
                <div class="col-md-4 login-sec">
                    <?php echo $htmlSign ?>
                </div>
                <div class="col-md-8 banner-sec">

                </div>
            </div>
    </section>
</section>