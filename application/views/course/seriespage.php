<style>
    .coursecard-single {
        border: 2px solid #f4f4f4;
        margin-bottom: 10px;

    }
    .top-style{
        padding: 10px;
    }
    .meta-list {
    position: absolute;
    left: 11px;
    top: 10px;
}


</style>
<section class="w3l-blogpost-content py-5">
    <div class="container py-md-5 py-4">
        <?php echo $msg ?>
        <div class="row">
            <div class="col-lg-8 w3l-blog-block-5">
                <div class="row">
                    <?php


                    foreach ($item as $key => $value) {

                    ?>
                        <div class="col-lg-6 col-md-6">
                            <div class="coursecard-single">
                                <div class="grids5-info position-relative">
                                    <img  src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt="" class="series-img img-fluid" />
                                    <div class="meta-list">
                                        <a style="background-color: <?php echo $value['background']; ?>;" href="#"><?php echo $value['nama_label']; ?></a>
                                    </div>
                                </div>

                                <div class="top-style">
                                    <div class="content-main-top">
                                        <div class="content-top mb-4 mt-3">
                                            <ul class="list-unstyled d-flex align-items-center justify-content-between">
                                                <li> <i class="fas fa-book-open"></i> <?php echo $value['jumlah_materi'] ?> Materi</li>
                                                <!-- <li> <i class="fas fa-star"></i> 4.5</li> -->
                                            </ul>
                                        </div>
                                        <h5><a href="<?php echo $value['url'] ?>"><?php echo $value['judul'] ?></a></h5>
                                        <p><?php echo $value['description'] ?></p>
                                        <div class="top-content-border d-flex align-items-center justify-content-between mt-4 pt-4">
                                            <h5><?php echo $value['price'] ?></h5>
                                            <!-- <a class="btn btn-style-primary" href="courses.html">Know Details<i class="fas fa-arrow-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- pagination -->
                <div class="pagination-style text-start mt-5">
                    <?= $pagination; ?>
                </div>
                <!-- //pagination -->
            </div>
            <?php echo $sidebar ?>
        </div>
    </div>
</section>