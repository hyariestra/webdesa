<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
  <meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
  <meta name="google-site-verification" content="<?php echo getSettingDesa()['desa_metatag']; ?>" />
  <title><?php include "phpmu-title.php"; ?></title>
	
    
    <!-- CSS
	================================================== -->
  	<link rel="stylesheet" href="<?php echo base_url('themefront/zvintauge/css/zerogrid.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/zvintauge/css/style.css'); ?>">
	
	<!-- Custom Fonts -->
    <link href="<?php echo base_url('themefront/zvintauge/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
	
	
	<link rel="stylesheet" href="<?php echo base_url('themefront/zvintauge/css/menu.css'); ?>">
	<script src="<?php echo base_url('themefront/zvintauge/js/jquery1111.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('themefront/zvintauge/js/script.js'); ?>"></script>
	
	<!-- Owl Carousel Assets -->
    <link href="<?php echo base_url('themefront/zvintauge/owl-carousel/owl.carousel.css'); ?>" rel="stylesheet">
	
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/Items/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<script src="js/css3-mediaqueries.js"></script>
	<![endif]-->
	
</head>

<body class="home-page">
	<div class="wrap-body">
		<header class="">
			<div class="logo">
				<a href="<?php echo base_url('/') ?>"><img style="max-width: 250px;" class="img-responsive" src="<?php echo getSettingDesa()['logo_desa']; ?>" alt=""></a>
			</div>
			<div id="cssmenu" class="align-center">
				<ul>

				<?php
						$fe_menu  =  $this->M_homepage->get_front_menu_tree();
						$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


						foreach ($fe_menu as $key => $fe) {
							
							if (!in_array($fe['id_menu'], $arrayMenu)) {
								unset($fe_menu[$key]);
							}

							if(!empty($fe['submenu'])){

								foreach ($fe['submenu'] as $keyz => $fe_sub) {

									if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

										unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
									}


								}
							}	

								//	unset($fe_menu[4]['submenu'][7]);
						}


						foreach ($fe_menu as $key => $fe) {


							if(empty($fe['submenu'])){
								echo '<li><a href="'.base_url($fe['link_menu']).'"><span>'.$fe['menu'].'</span></a></li>';

							}else{

								echo '
								<li class="has-sub"><a href="#"><span>'.$fe['menu'].'</span></a>
								<ul>';

								foreach ($fe['submenu']  as $keyz => $fe_submenu) {
								
									echo '<li class="last"><a href="'.base_url($fe_submenu['link_submenu']).'"><span>'.$fe_submenu['submenu'].'</span></a>';

								}
								echo 
								'</ul>
								</li>';
							}

						}?>
				
				</ul>
			</div>
			<?php echo @$slider; ?>
		</header>
		<!--////////////////////////////////////Container-->
		<?php echo $content; ?>
		<!--////////////////////////////////////Footer-->
		<footer>
			<div class="zerogrid wrap-footer">
				<div class="row">
					<div class="col-1-4 col-footer-1">
						<div class="wrap-col">
							<h3 class="widget-title">Tentang Kami</h3>
							<p><?php echo getSettingDesa()['desa_footer']; ?></p>
							
						</div>
					</div>
					<div class="col-1-4 col-footer-2">
						<div class="wrap-col">
							<h3 class="widget-title">Popular Post</h3>
							<ul>
								<?php
								
								$popular = $this->M_berita->get_berita_popular(5);

								foreach ($popular as $item) {
									
								?>

									<li><a href="<?php $item['url'] ?>"><?php echo $item['judul'] ?></a></li>

								
								<?php } ?>
								
							</ul>
						</div>
					</div>
					<div class="col-1-4 col-footer-3">
						<div class="wrap-col">
							<h3 class="widget-title">Tag</h3>
							<?php
								$tags = $this->M_berita->get_tags();
							foreach ($tags as $item) {
								
							?>
								
								<a href="<?php echo base_url('berita?tag='.$item['seo_tag'])  ?>"><?php echo $item['nama_tag'] ?></a>
							
							<?php } ?>
						</div>
					</div>
					<div class="col-1-4 col-footer-4">
						<div class="wrap-col">
							<h3 class="widget-title">Gallery</h3>
							<div class="row">

							<?php 
							
							$galeri  = $this->M_galeri->get_galeri_homepage(3,'Y');

								foreach ($galeri as $item) {
				
							?>

								<div class="col-1-3">
									<div class="wrap-col">
										<a href="#"><img src="<?php echo base_url('asset/'.$item['gambar']); ?>" alt="<?php echo $item['judul'] ?>" ></a>
									
									</div>
								</div>
								
								
								<?php } ?>

								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="zerogrid bottom-footer">
				<div class="row">
					<div class="bottom-social">
						<a target="_blank" href="<?php echo getSettingDesa()['desa_fb']; ?>"><i class="fa fa-facebook"></i></a>
						<a target="_blank" href="<?php echo getSettingDesa()['desa_ig']; ?>"><i class="fa fa-instagram"></i></a>
					</div>
				</div>
				<div class="copyright">
					Copyright @ - Designed by <a href="https://www.desahub.id">desahub</a>
				</div>
			</div>
		</footer>
		<!-- carousel -->
		<script src="<?php echo base_url('themefront/zvintauge/owl-carousel/owl.carousel.js'); ?>"></script>
		<script>
		$(document).ready(function() {
		  $("#owl-slide").owlCarousel({
			autoPlay: 3000,
			items : 1,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [979,1],
			itemsTablet : [768, 1],
			itemsMobile : [479, 1],
			navigation: true,
			navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
			pagination: false
		  });
		});
		</script>
	</div>
</body>
</html>