<section id="container">
			<div class="wrap-container">
				<!-----------------Content-Box-------------------->
				<div id="main-content">
					<div class="wrap-content">
						<div class="row">
							<article class="single-post zerogrid">
								<div class="row wrap-post"><!--Start Box-->
									<div class="entry-header">
										<span class="time"><?php echo tgl_indo($detail['berita']['tanggal']) ?>, <?php echo $detail['berita']['nama'] ?>, dibaca <?php echo $detail['berita']['dibaca']?> kali</span>
										<h2 class="entry-title"><a href="#"><?php echo $detail['berita']['judul'] ?></a></h2>
										<span class="cat-links"><a href="<?php echo $detail['berita']['url_kategori'] ?>"><?php echo $detail['berita']['nama_kategori'] ?></a></span>
									</div>
									<div class="post-thumbnail-wrap">
									<?php echo $detail['berita']['image']; ?>
									</div>
									<div class="entry-content">
										
									<?php echo $detail['berita']['isi_berita']; ?>									</div>
								</div>
							</article>
							
						</div>
					</div>
				</div>
			</div>
		</section>