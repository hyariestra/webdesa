<section id="container">
			<div class="wrap-container">
				<!-----------------content-box-1-------------------->
				<section class="content-box box-1">
					<div class="zerogrid">
						<div class="wrap-box"><!--Start Box-->
							<div class="box-header">
								<h2>Melikan Epic</h2>
							</div>
							<div class="box-content">
								<p>
								Kami adalah Kalurahan Melikan, Rongkop, Gunungkidul, Yogyakarta. Kami menyuguhkan wisata dengan konsep berbeda. Kami menggali sejarah, budaya dan kehidupan asli kampung-kampung di Melikan nan eksotis untuk Anda. Datanglah ke Melikan dan dapatkan pengalaman hidup baru di desa yang sesungguhnya.
							</p>
							</div>
						</div>
					</div>
				</section>
				<!-----------------content-box-2-------------------->
				<section class="content-box box-style-1 box-2">
					<div class="zerogrid">
						<div class="wrap-box"><!--Start Box-->
							
							<div class="row">
							
							<?php 


					$headline1 = sliceArrayByCategory($itemAll,11,6);


						
							foreach ($headline1 as $key => $value) {
								
								?>

								<div class="col-1-3">
									<div class="wrap-col">
										<article>
											<div class="post-thumbnail-wrap">
												<a href="<?php echo $value['url'] ?>" class="portfolio-box">
													<img style="object-fit: cover;height: 306px;width: 306px;" src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt="">
												</a>
											</div>
											<div class="entry-header ">
											<h3 class="entry-title"><a href="<?php echo $value['url'] ?>"><?php echo $value['judul'] ?></a></h3>
												<div class="l-tags"><a href="<?=  $value['url_kategori'] ?>"><?php echo $value['kategori'] ?></a></div>
											</div>
										</article>
									</div>
								</div>

								<?php } ?>
								
							</div>


							


						</div>
					</div>
				</section>
				<!-----------------content-box-3-------------------->
				<section class="content-box box-3">
					<div class="zerogrid">
						<div class="wrap-box"><!--Start Box-->
							<div class="box-header">
								<h2>FILOSOFI KAMI</h2>
							</div>
							<div class="box-content">
								<div class="row">
									<div class="col-1-2">
										<div class="wrap-col">
											<p>Kami meyakini, wisata adalah hasrat untuk melihat sesuatu yang berbeda dan otentik. Wisata juga harus menjaga alam tetap lestari serta menghidupi budaya adiluhung yang telah menghidupi seluruh rangkaian sejarah selama ini. Melikan Epic adalah visi besar kami membangun Kalurahan Wisata sekaligus cara kami merawat desa tercinta.</p>
										</div>
									</div>
									<div class="col-1-2">
										<div class="wrap-col">
											<p>Kami adalah warga kampung yang sarat nilai budaya turun-temurun dengan kisah sejarah memikat hati dan hari-hari yang meriah oleh berbagai atraksi seni. Kami mengundang Anda datang, menikmati paket wisata ‘berbeda’ yang kami miliki. </p>
										</div>
									</div>
								</div>
								<!-- <div class="row">
									<blockquote><p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet vultatup duista.</p></blockquote>
								</div> -->
							</div>
						</div>
					</div>
				</section>
				<!-----------------content-box-4-------------------->
				<section class="content-box box-style-1 box-4">
					<div class="zerogrid" style="width: 100%">
						<div class="wrap-box"><!--Start Box-->

							
						<?php 


				$headline3 = sliceArrayByCategory($item,15,3);

								

	
				foreach ($headline3 as $key => $value) {
					
					if($key==1)
					{

						echo '<div class="row">
								<article>
									<div class="col-1-2 f-right">
										<img src='.base_url('asset/'.$value['gambar']).' alt="">
									</div>
									<div class="col-1-2">
										<div class="entry-content t-center">
											<h3>'.$value['judul'].'</h3>
											<p>'.$value['isi_berita'].'</p>
											<a class="button" href="'.$value['url'].'">Read More</a>
										</div>
									</div>
								</article>
							</div>';


					}else{

							echo '<div class="row">
								<article>
									<div class="col-1-2">
									<img src='.base_url('asset/'.$value['gambar']).' alt="">
									</div>
									<div class="col-1-2">
										<div class="entry-content t-center">
											<h3>'.$value['judul'].'</h3>
											<p>'.$value['isi_berita'].'</p>
											<a class="button" href="'.$value['url'].'">Read More</a>
										</div>
									</div>
								</article>
							</div>';
						}


					}
				?>

						</div>
					</div>
				</section>
			</div>
		</section>