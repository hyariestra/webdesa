<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">

	
	<!-- Shareable -->
	<meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
	<title><?php include "phpmu-title.php"; ?></title>
	<link rel="icon" type="image/x-icon" href="<?php echo getSettingDesa()['favicon_desa']; ?>">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/scripts/bootstrap/bootstrap.min.css'); ?>">
	<!-- IonIcons -->
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/scripts/ionicons/css/ionicons.min.css'); ?>">
	<!-- Toast -->
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/scripts/toast/jquery.toast.min.css'); ?>">
	<!-- OwlCarousel -->
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/scripts/owlcarousel/dist/assets/owl.carousel.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/scripts/owlcarousel/dist/assets/owl.theme.default.min.css'); ?>">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/scripts/magnific-popup/dist/magnific-popup.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/scripts/sweetalert/dist/sweetalert.css'); ?>">
	<!-- Custom style -->
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/css/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/css/skins/all.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/css/demo.css'); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/1.17.3/simplelightbox.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">




</head>

<body class="skin-orange">
	<header class="primary">
		<div class="firstbar">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-12">
						<div class="brand">
							<a href="<?php echo base_url('/') ?>">
								<img src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="<?php echo getSettingDesa()['desa_nama']; ?>_logo">
							</a>
						</div>						
					</div>
					<div class="col-md-6 col-sm-12">




						<form method="GET" action="<?php echo base_url('berita') ?>" class="search" autocomplete="off">
							<div class="form-group">
								<div class="input-group">

									<input  value="<?= @$search ?>" type="text" name="search" class="form-control" placeholder="masukan judul">									
									<div class="input-group-btn">
										<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
									</div>
								</div>
							</div>
							
						</form>								
					</div>
					
				</div>
			</div>
		</div>

		<!-- Start nav -->
		<nav class="menu">
			<div class="container">
				<div class="brand">
					<a href="<?php echo base_url('/') ?>">
						<img src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="<?php echo getSettingDesa()['desa_nama']; ?>_logo">
					</a>
				</div>
				<div class="mobile-toggle">
					<a href="#" data-toggle="menu" data-target="#menu-list"><i class="ion-navicon-round"></i></a>
				</div>
				<div class="mobile-toggle">
					<a href="#" data-toggle="sidebar" data-target="#sidebar"><i class="ion-ios-arrow-left"></i></a>
				</div>
				<div id="menu-list">
					<ul class="nav-list">
						<li class="for-tablet nav-title"><a>Menu</a></li>
					


						<?php



						$fe_menu  =  $this->M_homepage->get_front_menu_tree();
						$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


						foreach ($fe_menu as $key => $fe) {
							
							if (!in_array($fe['id_menu'], $arrayMenu)) {
								unset($fe_menu[$key]);
							}

							if(!empty($fe['submenu'])){

								foreach ($fe['submenu'] as $keyz => $fe_sub) {

									if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

										unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
									}


								}
							}	

								//	unset($fe_menu[4]['submenu'][7]);
						}




						foreach ($fe_menu as $key => $fe) {



							if(empty($fe['submenu'])){
								echo '<li><a href='.base_url($fe['link_menu']).'>'.$fe['menu'].'</a></li>';

							}else{

								echo '
								<li class="dropdown magz-dropdown">
								<a href="#">'.$fe['menu'].'<i class="ion-ios-arrow-right"></i></a>
								<ul class="dropdown-menu">';

								foreach ($fe['submenu']  as $keyz => $fe_submenu) {
									

									echo '<li><a href='.base_url($fe_submenu['link_submenu']).'>'.$fe_submenu['submenu'].'</a></li>';

								}
								echo 
								'</ul>
								</li>';
							}


						}?>

						



					</ul>
				</div>
			</div>
		</nav>
		<!-- End nav -->
	</header>


	<section>
		<div class="container">
			<?php echo $content ?>
		</div>
	</section>


	<!-- Start footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="block">
					
						<div class="block-body">
							<figure class="foot-logo">
								<img src="<?php echo getSettingDesa()['logo_desa']; ?>" class="img-responsive" alt="Logo">
							</figure>
							<p class="brand-description">
								<?php echo getSettingDesa()['desa_footer']; ?>
							</p>
						
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="block">
						<h1 class="block-title">Popular Tags <div class="right"></div></h1>
						<div class="block-body">
							<ul class="tags">
								<?php  
								$tag = $this->M_tag->get_tag();

								foreach ($tag->result_array() as $key => $value) {
									 
								 ?>

								<li><a href="<?php echo base_url('berita?tag='.$value['seo_tag'])  ?>"><?php echo $value['seo_tag'] ?></a></li>
								
								<?php } ?>
							</ul>
						</div>
					</div>
					<div class="line"></div>
					
				</div>
				
				<div class="col-md-4 col-xs-12 col-sm-6">
					<div class="block">
						<h1 class="block-title">Ikuti Kami</h1>
						<div class="block-body">
						
							<ul class="social trp">

								<li>
									<a target="_blank" href="<?= getSettingDesa()['desa_fb'] ?>" class="facebook">
										<svg><rect width="0" height="0"/></svg>
										<i class="ion-social-facebook"></i>
									</a>
								</li>
								
								<li>
									<a target="_blank" href="<?= getSettingDesa()['desa_youtube'] ?>" class="youtube">
										<svg><rect width="0" height="0"/></svg>
										<i class="ion-social-youtube-outline"></i>
									</a>
								</li>
								
								<li>
									<a target="_blank" href="<?= getSettingDesa()['desa_ig'] ?>" class="instagram">
										<svg><rect width="0" height="0"/></svg>
										<i class="ion-social-instagram-outline"></i>
									</a>
								</li>
								
								
								
								
								
							</ul>
						</div>
					</div>
					<div class="line"></div>
					<div class="block">
						<div class="block-body no-margin">
							<ul class="footer-nav-horizontal">
								<li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo getSettingDesa()['desa_email']; ?></a></li>
								<li><a href="#"><i class="fa fa-phone-square" aria-hidden="true"></i> <?php echo getSettingDesa()['desa_telp']; ?></a></li>
								<li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo getSettingDesa()['desa_lokasi']; ?></a></li>
							
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="copyright">
					
						<div>
							Made with <i class="ion-heart"></i> by <a href="http://desahub.id">Desahub.id</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>


	<!-- End Footer -->

	<!-- JS -->

	<link rel="stylesheet" href="<?php echo base_url('themefront/magz/css/demo.css'); ?>">

	<script src="<?php echo base_url('themefront/magz/js/jquery.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/js/jquery.migrate.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/scripts/bootstrap/bootstrap.min.js')?>"></script>
	<script>var $target_end=$(".best-of-the-week");</script>
	<script src="<?php echo base_url('themefront/magz/scripts/jquery-number/jquery.number.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/scripts/owlcarousel/dist/owl.carousel.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/scripts/magnific-popup/dist/jquery.magnific-popup.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/scripts/easescroll/jquery.easeScroll.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/scripts/sweetalert/dist/sweetalert.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/scripts/toast/jquery.toast.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/magz/js/e-magz.js')?>"></script>
	<script src=https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/1.17.3/simple-lightbox.min.js></script>
	<script>

		function share_fb(url) {


			window.open('https://www.facebook.com/sharer/sharer.php?u='+url,'facebook-share-dialog',"width=626, height=436")
		}

		function share_twitter(url) {

			
			var href = 'https://twitter.com/intent/tweet?text='+url;

			window.open(href, "Twitter", "height=285,width=550,resizable=1");


		}

		function shareHit(id) {
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('berita/hitshare') ?>", 
				data:  {id: id},
				dataType: "text",  
				cache:false,
				success: 
				function(data){

				}
          });// you have missed this bracket
			return false;


		}


		function like(id) {

			document.getElementById('like-button').setAttribute('onclick','dislike( '+id+' )');

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('berita/hitlike') ?>", 
				data:  {id: id},
				dataType: "text",  
				cache:false,
				success: 
				function(data){

				}
          });// you have missed this bracket
			return false;


		}

		function dislike(id) {

			document.getElementById('like-button').setAttribute('onclick','like('+id+')');

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('berita/hitdislike') ?>", 
				data:  {id: id},
				dataType: "text",  
				cache:false,
				success: 
				function(data){

				}
          });// you have missed this bracket
			return false;

		}




		var lightbox = $('.galeri a').simpleLightbox();
	</script>
</body>
</html>