<section class="search">
	<div class="container">
		<div class="row">
			<div class="col-md-3">

				<aside>
					<h2 class="aside-title">Urutkan</h2>
					<div class="aside-body">
						<div class="form-group">
							<label><a href="<?php echo base_url('berita?sort=popular') ?>">Paling Banyak Dibaca</a></label>
							<hr>
							<label><a href="<?php echo base_url('berita?sort=share') ?>">Paling Banyak Dibagikan</a></label>
							<hr>
							<label><a href="<?php echo base_url('berita?sort=like') ?>">Paling Banyak Disukai</a></label>
							<hr>
						</div>
					</div>
				</aside>
			</div>
			<div class="col-md-9">

				<div class="search-result">



					<?= $msg ?>
					
				</div>
				<div class="row">


					<?php



					foreach ($item as $key => $value) {



						?>
						<article class="col-md-12 article-list">
							<div class="inner">
								<figure>
									<a href="<?php echo $value['url'] ?>">
										<img src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt="Sample Article">
									</a>
								</figure>
								<div class="details">
									<div class="detail">
										<div class="category">
											<a href="<?=  $value['url_kategori'] ?>"><?php echo $value['kategori'] ?></a>
										</div>
										<div class="time"><?php echo tgl_indo($value['tanggal']) ?></div>
									</div>
									<h1><a href="<?php echo $value['url'] ?>"><?php echo $value['judul']; ?></a></h1>
									<p>
										<?php echo  strip_tags($value['isi_berita']) ?>
									</p>
									<footer>
										<a onclick="like(<?php echo $value['id']; ?>)" id="like-button" class="love">
											<i class="ion-android-favorite-outline"></i> <div><?php echo $value['like'] ?></div>
										</a>

										<a  style="margin-left: 10px"  href="#" class="btnx">
											<i class="fa fa-share-alt" aria-hidden="true"></i> <div><?php echo $value['dibagikan'] ?></div>
										</a>

										<a  style="margin-left: 10px"  href="#" class="btnx">
											<i class="fa fa-eye" aria-hidden="true"></i> <div><?php echo $value['dibaca'] ?></div>
										</a>

										<a class="btn btn-primary more" href="<?php echo $value['url'] ?>">
											<div>Selengkapnya</div>
											<div><i class="ion-ios-arrow-thin-right"></i></div>
										</a>
									</footer>
								</div>
							</div>
						</article>
					<?php } ?>

					<?= $pagination; ?>
				</div>
			</div>
		</div>
	</div>
</section>