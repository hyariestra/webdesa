<section class="home">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 col-xs-12">
				
				<div class="owl-carousel owl-theme slide" id="featured">
				

			
					
					<?php

					foreach ($headline as $key => $value) {
						


					?>
					<div class="item">
						<article class="featured">
							<div class="overlay"></div>
							<figure>
								<img src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt="<?php echo $value['judul']; ?>">
							</figure>
							<div class="details">
								<div class="category"><a href="<?=  $value['url_kategori'] ?>"><?php echo $value['kategori'] ?></a></div>
								<h1><a href="<?php echo $value['url'] ?>"><?php echo $value['judul']; ?></a></h1>
								<div class="time"><?php echo tgl_indo($value['tanggal']) ?></div>
							</div>
						</article>
					</div>

				<?php } ?>

				</div>



				<div class="line transparent little"></div>

				<div class="line top">
					<div>Berita Terbaru</div>
				</div>
				<div class="row">


					<?php



					foreach ($item as $key => $value) {



						?>
						<article class="col-md-12 article-list">
							<div class="inner">
								<figure>
									<a href="<?php echo $value['url'] ?>">
										<img src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt="Sample Article">
									</a>
								</figure>
								<div class="details">
									<div class="detail">
										<div class="category">
											<a href="<?=  $value['url_kategori'] ?>"><?php echo $value['kategori'] ?></a>
										</div>
										<div class="time"><?php echo tgl_indo($value['tanggal']) ?></div>
									</div>
									<h1><a href="<?php echo $value['url'] ?>"><?php echo $value['judul']; ?></a></h1>
									<p>
										<?php echo  strip_tags($value['isi_berita']) ?>
									</p>
									<footer>
										<a onclick="like(<?php echo $value['id']; ?>)" id="like-button" class="love">
											<i class="ion-android-favorite-outline"></i> <div><?php echo $value['like'] ?></div>
										</a>
										<a class="btn btn-primary more" href="<?php echo $value['url'] ?>">
											<div>Selengkapnya</div>
											<div><i class="ion-ios-arrow-thin-right"></i></div>
										</a>
									</footer>
								</div>
							</div>
						</article>

					<?php } ?>

				</div>
			</div>
			<div class="col-xs-6 col-md-4 sidebar" id="sidebar">
				<div class="sidebar-title for-tablet">Sidebar</div>
				<aside>
					
				</aside>
				<aside>
					<h1 class="aside-title">Populer <a href="<?php echo base_url('berita') ?>" class="all">Lihat Semua <i class="ion-ios-arrow-right"></i></a></h1>
					<div class="aside-body">
						
						
						<?php

					foreach ($popular as $key => $value) {
					

						?>
						
						<article class="article-mini">
							<div class="inner">
								<figure>
									<a href="<?php echo $value['url'] ?>">
										<img src="<?php echo base_url('asset/'.$value['gambar']); ?>" >
									</a>
								</figure>
								<div class="padding">
									<h1><a href="<?php echo $value['url'] ?>"><?php echo $value['judul']; ?></a></h1>
								</div>
							</div>
						</article>
					<?php } ?>

					</div>
				</aside>
				
				
				<aside>
					<h1 class="aside-title">Video
						<div class="carousel-nav" id="video-list-nav">
							<div class="prev"><i class="ion-ios-arrow-left"></i></div>
							<div class="next"><i class="ion-ios-arrow-right"></i></div>
						</div>
					</h1>
					<div class="aside-body">
						<ul class="video-list" data-youtube='"carousel":true, "nav": "#video-list-nav"'>
						
							<?php

						foreach ($video as $key => $value) {
							
							?>

							<li>
								<div class="embed-responsive embed-responsive-16by9">
									<iframe width="250" height="250" src="<?php echo $value['url_video'] ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</li>

						<?php } ?>
						
						</ul>
					</div>
				</aside>

				<aside>
					<div class="aside-body">
						<form class="newsletter">
							<input type="hidden" id="base" value="<?php echo base_url(); ?>">
							<div class="icon">
								<i class="ion-ios-email-outline"></i>
								<h1>Newsletter</h1>
							</div>
							<div class="input-group">
								<input type="email" id="email-text" class="form-control email" placeholder="Your mail">
								<div class="input-group-btn">
									<button class="btn btn-primary"><i class="ion-paper-airplane"></i></button>
								</div>
							</div>
							<p>By subscribing you will receive new articles in your email.</p>
						</form>
					</div>
				</aside>
				<!-- <aside id="sponsored">
					<h1 class="aside-title">Sponsored</h1>
					<div class="aside-body">
						<ul class="sponsored">
							<li>
								<a href="#">
									<img src="<?php echo base_url('themefront/magz/images/sp.jpg')?>" alt="User Picture">
								</a>
							</li> 
							<li>
								<a href="#">
									<img src="<?php echo base_url('themefront/magz/images/sp.jpg')?>" alt="Sponsored">
								</a>
							</li> 
							<li>
								<a href="#">
									<img src="<?php echo base_url('themefront/magz/images/sp.jpg')?>" alt="Sponsored">
								</a>
							</li> 
							<li>
								<a href="#">
									<img src="<?php echo base_url('themefront/magz/images/sp.jpg')?>" alt="Sponsored">
								</a>
							</li> 
						</ul>
					</div>
				</aside> -->
			</div>
		</div>
	</div>
</section>

<section class="best-of-the-week">
	<div class="container">
		<h1><div class="text">Galeri</div>
			<div class="carousel-nav" id="best-of-the-week-nav">
				<div class="prev">
					<i class="ion-ios-arrow-left"></i>
				</div>
				<div class="next">
					<i class="ion-ios-arrow-right"></i>
				</div>
			</div>
		</h1>
		<div class="owl-carousel owl-theme carousel-1">
			
			
			
			<?php

			foreach ($galeri as $key => $value) {
				

			?>
			
			<article class="article">
				<div class="inner">
					<figure>
						<div class="galeri">
							
						<a title="<?php echo $value['judul'] ?>" href="<?php echo base_url('asset/'.$value['gambar']); ?>">
							<img title="<?php echo $value['judul'] ?>" src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt="Sample Article">
						</a>
						</div>
					</figure>
					<div class="padding">
						
						<h2><a href=""><?php echo $value['judul'] ?></a></h2>
					
					</div>
				</div>
			</article>
		<?php } ?>

		</div>
	</div>
</section>
<script>


</script>