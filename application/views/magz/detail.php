<style>
	.main img{
		max-width: 100%;
	}
	.img-detail{
		width: 100%;
	}
		img {
	
		width: 100%  !important;
	}
</style>
<section class="single">
			<div class="container">
				<div class="row">

					<div class="col-md-4 sidebar" id="sidebar">
						
						<aside>
							<h1 class="aside-title">berita lain</h1>
							<div class="aside-body">


								<?php


								foreach ($recent as $key => $value) {
								
								if ($key==0)	
								
								{

								echo '<article class="article-fw">
									<div class="inner">
										<figure>
											<a href='.$value['url'].'>												
												<img src='.base_url('asset/'.$value['gambar']).'>
											</a>
										</figure>
										<div class="details">
											<h1><a href='.$value['url'].'>'.$value['judul'].'</a></h1>
											<p>
											'.$value['isi_berita'].'
											</p>
											<div class="detail">
												<div class="time">'.$value['tanggal'].'</div>
												<div class="category"><a href='.$value['url_kategori'].'>'.$value['kategori'].'</a></div>
											</div>
										</div>
									</div>
								</article>
								<div class="line"></div>';
							}else{

								echo '<article class="article-mini">
									<div class="inner">
										<figure>
											<a href='.$value['url'].'>												
												<img src='.base_url('asset/'.$value['gambar']).'>
											</a>
										</figure>
										<div class="padding">
											<h1><a href='.$value['url'].'>'.$value['judul'].'</a></h1>
											<div class="detail">
											<div class="category"><a href='.$value['url_kategori'].'>'.$value['kategori'].'</a></div>
											<div class="time">'.$value['tanggal'].'</div>
											</div>
										</div>
									</div>
								</article>';
							}
							}

								?>
								
								
							</div>
						</aside>
						<aside>
							<div class="aside-body">
								<form  class="newsletter">
									<input type="hidden" id="base" value="<?php echo base_url(); ?>">
									<div class="icon">
										<i class="ion-ios-email-outline"></i>
										<h1>Newsletter</h1>
									</div>
									<div class="input-group">
										<input  id="email-text" type="email" class="form-control email" placeholder="Your mail">
										<div class="input-group-btn">
											<button class="btn btn-primary"><i class="ion-paper-airplane"></i></button>
										</div>
									</div>
									<p>By subscribing you will receive new articles in your email.</p>
								</form>
							</div>
						</aside>
					</div>
					
				<div class="col-md-8">
						
						<article class="article main-article">
							<header>
								<h1><?php echo $detail['berita']['judul'] ?></h1>
								<ul class="details">
									<li>Ditulis <?php echo tgl_indo($detail['berita']['tanggal']) ?></li>
									<li><a href="<?php echo $detail['berita']['url_kategori'] ?>"><?php echo $detail['berita']['nama_kategori'] ?></a></li>
									<li>Oleh <a href="#"><?php echo $detail['berita']['nama'] ?></a></li>
								</ul>
							</header>
							<div class="main">
									<?php echo $detail['berita']['image']; ?>

								<?php echo $detail['berita']['isi_berita']; ?>
							</div>
							<footer>
								<div class="col">
									<ul class="tags">

										<?php

										foreach ($detail['tag'] as $key => $row) {
										
										?>

										<li><a href=""><?php echo $row['nama_tag'] ?></a></li>
											
										<?php } ?>

									</ul>
								</div>
								<div class="col">

										<a onclick="like(<?php echo $detail['berita']['id_berita']; ?>)" id="like-button" class="love"><i class="ion-android-favorite-outline"></i> <div><?php echo $detail['berita']['like_berita'] ?></div></a>
								</div>
							</footer>
						</article>
						<div class="sharing">
						<div class="title"><i class="ion-android-share-alt"></i> Bagikan berita ini</div>
							<ul class="social">
								<li>
									<a href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_fb('<?php echo $link?>'); return false;" class="facebook">
										<i class="ion-social-facebook"></i> Facebook
									</a>
								</li>
								<li>
									<a href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_twitter('<?php echo $link?>'); return false;" class="twitter">
										<i class="ion-social-twitter"></i> Twitter
									</a>
								</li>
								<!-- <li>
									<a href="#" class="googleplus">
										<i class="ion-social-googleplus"></i> Google+
									</a>
								</li> -->
								<!-- <li>
									<a href="#" class="linkedin">
										<i class="ion-social-linkedin"></i> Linkedin
									</a>
								</li> -->
								<!-- <li>
									<a href="#" class="skype">
										<i class="ion-ios-email-outline"></i> Email
									</a>
								</li> -->
								<li class="count">
									<?php echo $detail['berita']['dibagikan']; ?>
									<div>Dibagikan</div>
								</li>
							</ul>
						</div>
						

						<div class="line"><div>Berita terkait</div></div>
						<div class="row">
							<?php

							foreach ($infoterkait as $key => $value) {
								
						
							?>
							<article class="article related col-md-6 col-sm-6 col-xs-12">
								<div class="inner">
									<figure>
										<a href="<?php echo $value['url'] ?>">
											<img src="<?php echo base_url('asset/'.$value['gambar']); ?>">
										</a>
									</figure>
									<div class="padding">
										<h2><a href="<?php echo $value['url'] ?>"><?php echo substr($value['judul'],0,62) ?></a></h2>
										<div class="detail">
											<div class="category"><a href="<?php echo $value['url_kategori'] ?>"><?php echo $value['kategori']; ?></a></div>
											<div class="time"><?php echo tgl_indo($value['tanggal']) ?></div>
										</div>
									</div>
								</div>
							</article>
						<?php } ?>
						</div>
					
						
					</div>
				</div>
			</div>
		</section>