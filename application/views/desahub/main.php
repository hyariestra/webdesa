<section class="content">
<style type="text/css">
 
    .outerslide{
        background-image: url(<?php echo base_url('themefront/desahub/assets/images/bg1.jpg')?>);
    }

    * {
      box-sizing: border-box;
    }

 


    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }

    .slick-prev,
    .slick-next {
    font-size: 0;
    position: absolute;
    bottom: 20px;
    color: #d5122f;
    border: 0;
    background: none;
    z-index: 1;
}

.slick-prev {
    left: 20px;
}

.slick-next {
    right: 20px;
    text-align: right;
}



.slick-prev:hover:after,
.slick-next:hover:after {
    color: #7e7e7e;
}
.slide {
  position: relative;
}

.slide__caption a{
    color: white;
}

.slide__caption {
  bottom: 0;
  left: 0;
  position: absolute;
  width: 100%;
  z-index: 2;
  text-align: center;
 
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 20px 0;
    /* background: rgba(0,0,0,.7); */
}
.box{
    font-size: 25px;
    padding: 10px;
  
}

  </style>

<div class="lazy slider" data-sizes="50vw">
    
<?php


foreach ($sliders as $key => $value) {

    ?>

    <div class="slide">
    <img style="object-fit: cover; " data-lazy="<?php echo base_url('asset/'.$value['gambar'].' ')?>" data-srcset="<?php echo base_url('asset/'.$value['gambar'].' ')?>" data-sizes="100vw">
      <div class="slide__caption">
          <span class="box">
           <a href="<?php echo $value['url'] ?>"> <?php echo $value['judul'] ?></a>
          </span>
      </div>
    </div>
    
    <?php } ?>
        
</div>


<div class="w3l-homeblock3 py-5">

    <div class="container py-lg-5 py-md-4">

        <div class="left-right">
            <h3 class="section-title-left mb-sm-4 mb-2"> Terbaru</h3>  
        </div>

        <div class="row top-pics">
        

            <?php 


            $headline1 = sliceArrayByCategory($item,NULL,3);

            foreach ($headline1 as $key => $value) {


                    echo '<div  class="section-grid col-lg-4">
                    <div style="opacity: 0.9; background: url('.base_url('asset/'.$value['gambar']).') no-repeat 0px 0px;background-size: cover"  class="top-pic1">
                    <div class="card-body blog-details">
                    <a href="'.$value['url'].'" class="blog-desc">'.$value['judul'].'
                    </a>

                    </div>
                    </div>
                    </div>';
              
            }?>



    </div>
</div>
</div>


<div class="w3l-homeblock3 py-5">

    <div class="container py-lg-5 py-md-4">

        <div class="left-right">
            <h3 class="section-title-left mb-sm-4 mb-2"><?php echo getCategoryName(15)->nama_kategori; ?></h3>  
        </div>

        <div class="row top-pics">
        

            <?php 

        
            $headline2 = sliceArrayByCategory($headline,15,3);

           


            foreach ($headline2 as $key => $value) {


                    echo '<div  class="section-grid col-lg-4">
                    <div style="opacity: 0.9; background: url('.base_url('asset/'.$value['gambar']).') no-repeat 0px 0px;background-size: cover"  class="top-pic1">
                    <div class="card-body blog-details">
                    <a href="'.$value['url'].'" class="blog-desc">'.$value['judul'].'
                    </a>

                    </div>
                    </div>
                    </div>';
              
            }?>



    </div>
</div>
</div>


<div class="w3l-homeblock3 py-5">

    <div class="container py-lg-5 py-md-4">

        <div class="left-right">
            <h3 class="section-title-left mb-sm-4 mb-2"><?php echo getCategoryName(16)->nama_kategori; ?></h3>  
        </div>

        <div class="row top-pics">
        

            <?php 


            $headline3 = sliceArrayByCategory($headline,16,3);

            foreach ($headline3 as $key => $value) {


                    echo '<div  class="section-grid col-lg-4">
                    <div style="opacity: 0.9; background: url('.base_url('asset/'.$value['gambar']).') no-repeat 0px 0px;background-size: cover"  class="top-pic1">
                    <div class="card-body blog-details">
                    <a href="'.$value['url'].'" class="blog-desc">'.$value['judul'].'
                    </a>

                    </div>
                    </div>
                    </div>';
              
            }?>



    </div>
</div>
</div>
<div class="button-alls">

    <a href="<?php echo base_url('berita') ?>" class="button-inside"><b>Lihat Lainnya</b></a>
    
</div>
<div class="w3l-homeblock2 py-5">
    <div class="container py-lg-5 py-md-4">
        <!-- block -->
        <div class="left-right">
            <h3 class="section-title-left mb-sm-4 mb-2"> Our Service</h3>
          
        </div>
        <div class="row">
   
    
            <?php
            
            $service = array (
                0 => array(
                    'judul' => 'Konsultan Desa Membangun',
                  //  'gambar' => 'konsultasi_new.png',
                    'gambar' => 'satu.png',
                    'url' => 'halaman/konsultan-ekonomi-desa',
                ),
                1 => array(
                    'judul' => 'Media Digital Desa',
                 //   'gambar' => 'media_new.png',
                   'gambar' => 'dua.png',
                    'url' => 'halaman/media-digital-desa',
                ),
                2 => array(
                    'judul' => 'Content Creator',
                 //   'gambar' => 'konten_new.png',
                 'gambar' => 'tiga.png',
                    'url' => 'halaman/content-creator',
                )
              );
            
              foreach ($service as $key => $value) {
                  
        
            
            ?>

        <div class="col-lg-4 col-md-6 item">
            <div class="card">
                <div class="card-header p-0 position-relative">
                    <a href="<?php echo base_url(''.$value['url'].'')?>">
                        <img class="card-img-bottom d-block radius-image-full" src="<?php echo base_url('themefront/desahub/assets/images/'.$value['gambar'].' ')?>" data-srcset="<?php echo base_url('themefront/desahub/assets/images/'.$value['gambar'].' ')?>"
                        alt="Card image cap">
                    </a>
                </div>
                <div class="card-body blog-details">
                    <a href="<?php echo base_url(''.$value['url'].'')?>" class="blog-desc"><?php echo $value['judul'] ?>
                    </a>
                    
                </div>
            </div>
        </div>
        <?php } ?>

</div>
</div>
</div>


    <div class="w3l-homeblock2 py-5">
        <div class="container py-lg-5 py-md-4">
            <!-- block -->
            <div class="left-right">
                <h3 class="section-title-left mb-sm-4 mb-2"> Our Clients</h3>
            
            </div>
            <div class="regular slider">
        
        <?php

        foreach ($client as $key => $value) 

        { 


            ?>

            <div style="padding-right: 7px;">
                <img src="<?php echo base_url('asset/'.$value['gambar'].'')?>">
            </div>
            
            
            <?php } ?>
        
    </div>
    </div>
    </div>


<div class="w3l-homeblock2 w3l-homeblock6 py-5">
    <!-- <div class="container-fluid px-sm-5 py-lg-5 py-md-4">
 
        <h3 class="section-title-left mb-4"> Trending Now</h3>
        <div class="row">
            <div class="col-lg-6">
                <div class="bg-clr-white hover-box">
                    <div class="row">
                        <div class="col-sm-6 position-relative">
                            <a href="#blog-single.html">
                                <img class="card-img-bottom d-block radius-image-full" src="assets/images/trending1.jpg"
                                alt="Card image cap">
                            </a>
                        </div>
                        <div class="col-sm-6 card-body blog-details align-self">
                            <span class="label-blue">Sports</span>
                            <a href="#blog-single.html" class="blog-desc">Playing footbal with your feet is one thing.
                            </a>
                            <p>Lorem ipsum dolor sit amet consectetur ipsum adipisicing elit. Quis
                            vitae sit.</p>
                            <div class="author align-items-center mt-3">
                                <img src="assets/images/a2.jpg" alt="" class="img-fluid rounded-circle" />
                                <ul class="blog-meta">
                                    <li>
                                        <a href="author.html">Charlotte mia</a> 
                                    </li>
                                    <li class="meta-item blog-lesson">
                                        <span class="meta-value"> July 13, 2020 </span>. <span
                                        class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6">
                <div class="bg-clr-white hover-box">
                    <div class="row">
                        <div class="col-sm-6 position-relative">
                            <a href="#blog-single.html">
                                <img class="card-img-bottom d-block radius-image-full" src="assets/images/trending1.jpg"
                                alt="Card image cap">
                            </a>
                        </div>
                        <div class="col-sm-6 card-body blog-details align-self">
                            <span class="label-blue">Sports</span>
                            <a href="#blog-single.html" class="blog-desc">Playing footbal with your feet is one thing.
                            </a>
                            <p>Lorem ipsum dolor sit amet consectetur ipsum adipisicing elit. Quis
                            vitae sit.</p>
                            <div class="author align-items-center mt-3">
                                <img src="assets/images/a2.jpg" alt="" class="img-fluid rounded-circle" />
                                <ul class="blog-meta">
                                    <li>
                                        <a href="author.html">Charlotte mia</a> 
                                    </li>
                                    <li class="meta-item blog-lesson">
                                        <span class="meta-value"> July 13, 2020 </span>. <span
                                        class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>



</section>