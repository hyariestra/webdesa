<div class="w3l-homeblock2 py-5">
    <div class="container pt-md-4 pb-md-5">
        <!-- block -->
        <h3 style="text-transform: uppercase;" class="category-title mb-3"> <?= $item[0]['nama_kategori'] ?></h3>
        <div class="row">
            
            
            <?php
            
            
            foreach ($item as $key => $value) {
                
            
            
            ?>
            
            <div class="col-lg-4 col-md-6 mt-lg-5 mt-4">
                <div class="card">
                    <div class="card-header p-0 position-relative">
                        <a href="<?php echo $value['url'] ?>">
                            <img class="card-img-bottom d-block radius-image-full" src="<?php echo base_url('asset/'.$value['gambar']); ?>"
                                alt="Card image cap">
                        </a>
                    </div>
                    <div class="card-body blog-details">
                        <a href="<?php echo $value['url'] ?>" class="blog-desc"><?php echo $value['judul']; ?>
                        </a>
                        <p><?php echo  strip_tags($value['isi_berita']) ?></p>
                        <div class="author align-items-center mt-3 mb-1">
                            <img src="<?php echo base_url('asset/img/user.png'); ?>" alt="" class="img-fluid rounded-circle" />
                            <ul class="blog-meta">
                                <li>
                                    <a href="#"><?php echo $value['penulis'] ?></a> </a>
                                </li>
                                <li class="meta-item blog-lesson">
                                    <span class="meta-value"> <?php echo tgl_indo($value['tanggal']) ?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                <?php } ?>

        </div>

        <ul class="site-pagination text-center mt-md-5 mt-4">
              <?= $pagination; ?>
        </ul>

        	
    </div>
</div>