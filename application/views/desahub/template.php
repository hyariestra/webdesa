<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
  <link rel="icon" type="image/x-icon" href="<?php echo getSettingDesa()['favicon_desa']; ?>">
  <meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
  <meta name="google-site-verification" content="<?php echo getSettingDesa()['desa_metatag']; ?>" />
  <title><?php include "phpmu-title.php"; ?></title>

  <link href="//fonts.googleapis.com/css2?family=Hind:wght@300;400;500;600&display=swap" rel="stylesheet">
  <link href="//fonts.googleapis.com/css2?family=Libre+Baskerville:wght@400;700&display=swap" rel="stylesheet">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url('themefront/desahub/assets/css/style-starter.css'); ?> ">
  <link rel="stylesheet" href="<?php echo base_url('themefront/desahub/assets/css/liberty.css'); ?> ">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@700&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="<?php echo base_url('themefront/desahub/slick/slick.css'); ?> ">
   <link rel="stylesheet" href="<?php echo base_url('themefront/desahub/slick/slick-theme.css'); ?> ">
</head>
<body>
  <!-- header -->
  <header class="w3l-header">
    <style>
      .pages-content{
        margin-top: 40px;
      }
      body{
       font-family: 'Open Sans', sans-serif  !important;
     }
     .title-misi{
      text-align: center;
    }

    .img-block{
      display: block;
    }

    .homepage-card .title-misi:before, .homepage-card .title-misi:after {
      background: rgba(80,55,55,0.15);
      content: "";
      display: block;
      height: 10px;
      margin: 0 auto;
      width: 100px;
      margin-bottom: 10px;
      margin-top: 10px;
    }

    .title-child-tg {
     position: absolute;
     color: white; 
     font: bold 12px Helvetica, Sans-Serif; 
     letter-spacing: -1px;  
     background: rgb(0, 0, 0); /* fallback color */
     background: rgba(0, 0, 0, 0.7);
     padding: 10px; 


     position: absolute;
     top: 50%;
     left: 50%;
     transform: translate( -50%, -50% );
     text-align: center;
     color: white;
     font-weight: bold;

   }
   .title-tg { 
     position: relative;
     top: 70%;

   }
   .section-grid{
    margin-top: 5px;
    padding: 5px;
  }

  .box{
    position: relative;
    display: inline-block; /* Make the width of box same as image */
  }
  .box .text{

   position: absolute;
   z-index: 999;
   margin: 0 auto;
   /* left: 0; */
   right: 0;
   /* text-align: center; */
   top: 74%;
   background: rgba(0, 0, 0, 0.8);
   font-family: Arial,sans-serif;
   color: #fff;
   width: 80%;
   left: -217%;
 }
 .text-css{
   color: white; 
   padding: 10px;
   font-size: 20px;
 }

 .img-prog{
  width: 30%;
  border-radius: 50%;
  display: block;
  margin: 0 auto;
  border: solid 5px rgba(255, 13, 36, 0.3);
}
.title-bubble{
  text-align: center;
}


.w3l-homeblock3 .short-div:before {
    position: absolute;
    content: '';
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: #000;
    opacity: .6;
    border-radius: var(--border-radius-full);
    z-index: -1; }


.short-div {

  /* Location of the image */


  background-size: cover;
  padding: 20px;
  border-radius: var(--border-radius-full);
  height: 220px;
  display: grid;
  align-items: flex-end;
  position: relative;
  z-index: 1;
  margin-bottom: 10px;
  transition: .3s ease;
  -webkit-transition: .3s ease;
  -moz-transition: .3s ease;
  -ms-transition: .3s ease;
  -o-transition: .3s ease;
}

.slider-div {
  background: url(<?php echo base_url('themefront/desahub/assets/images/slid1.jpg')?>);

  background-repeat:no-repeat;
  background-size: 100%;
  height: 400px;

}

.slide-desc{
  margin-top: 38px;
  color: white;
  margin-left: 2%;
  font-size: 20px;
  background-color: #261d1d;
  padding: 5px;
}

.slide-card{
  padding-top: 25%;
}

@media only screen and (max-width: 600px) {
  .slider-div {
   height: 150px;
 }

 .slide-desc{
  color: white;
  margin-left: 2%;
  font-size: 10px;
  background-color: #261d1d;
  padding: 5px;
}

.slide-card{
  padding-top: 30%;
}

}

</style>
<div class="container">
 <!--/nav-->
 <nav class="navbar navbar-expand-lg navbar-light fill px-lg-0 py-0 px-sm-3 px-0">
   <a class="navbar-brand" href="<?php echo base_url('/'); ?>">
   <img width="150px" src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="<?php echo getSettingDesa()['desa_nama']; ?>_logo"></a>
			<!-- if logo is image enable this   
						<a class="navbar-brand" href="#index.html">
							<img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
						</a> -->


           <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
           data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
           aria-label="Toggle navigation">
           <!-- <span class="navbar-toggler-icon"></span> -->
           <span class="fa icon-expand fa-bars"></span>
           <span class="fa icon-close fa-times"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent">
          
       <ul class="navbar-nav">
         
        
       <?php



$fe_menu  =  $this->M_homepage->get_front_menu_tree();
$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


foreach ($fe_menu as $key => $fe) {
  
  if (!in_array($fe['id_menu'], $arrayMenu)) {
    unset($fe_menu[$key]);
  }

  if(!empty($fe['submenu'])){

    foreach ($fe['submenu'] as $keyz => $fe_sub) {

      if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

        unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
      }


    }
  }	

    //	unset($fe_menu[4]['submenu'][7]);
}




foreach ($fe_menu as $key => $fe) {



  if(empty($fe['submenu'])){
    echo '<li> <a class="nav-link" href='.base_url($fe['link_menu']).'>'.$fe['menu'].'</a> </li>';

  }else{

    echo '
    <li class="nav-item dropdown @@pages__active">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          '.$fe['menu'].' <span class="fa fa-angle-down"></span>
        </a>
  
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">';

    foreach ($fe['submenu']  as $keyz => $fe_submenu) {
      

      echo ' <a class="dropdown-item @@b__active" href="'.base_url($fe_submenu['link_submenu']).'">'.$fe_submenu['submenu'].'</a>';


    }
    echo 
    '</div>
    </li>';
  }


}?>



        
        
     
  </ul>
</div>
<!-- toggle switch for light and dark theme -->
<div class="mobile-position">
  <nav class="navigation">
   <div class="theme-switch-wrapper">
    <label class="theme-switch" for="checkbox">
     <input type="checkbox" id="checkbox">
     <div class="mode-container">
      <i class="gg-sun"></i>
      <i class="gg-moon"></i>
    </div>
  </label>
</div>
</nav>
</div>
<!-- //toggle switch for light and dark theme -->
</nav>
</div>
<!--//nav-->
</header>
<!-- //header -->
<!-- /homeblock1-->

<!-- //homeblock1-->




  <?php echo $content ?>

  <!-- footer-28 block -->
  <section style="margin-top: 50px;" class="app-footer">
    <footer class="footer-28 py-5">
      <div class="footer-bg-layer">
        <div class="container py-lg-3">
          <div class="row footer-top-28">
            <div class="col-lg-4 footer-list-28 copy-right mb-lg-0 mb-sm-5 mt-sm-0 mt-4">
              <a class="navbar-brand mb-3" href="<?php echo base_url('/') ?>">
              <img width="150px" src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="<?php echo getSettingDesa()['desa_nama']; ?>_logo"></a>
                <p class="copy-footer-28"> © 2020. All Rights Reserved. </p>
                <h5 class="mt-2">Design by <a href="<?php echo base_url('/') ?>"><?php echo getSettingDesa()['desa_nama']; ?></a></h5>
              </div>
              <div class="col-lg-8 row">
                <div class="col-sm-4 col-6 footer-list-28">
                  <h6 class="footer-title-28">Useful links</h6>
                  <ul>

                    <?php
                    

                    foreach ($fe_menu as $key => $fe) {

                    if(empty($fe['submenu'])){
                      echo '<li><a href='.base_url($fe['link_menu']).'>'.$fe['menu'].'</a></li>';
                      
                      }

                    } ?>

                  </ul>
                </div>
                <div class="col-sm-4 col-6 footer-list-28">
                  <h6 class="footer-title-28">Kontak</h6>
                  <ul>
                    <li><span class="fa fa-map-marker"></span> <?php echo getSettingDesa()['desa_lokasi']; ?></li>
                    <li><span class="fa fa-phone-square"></span> <?php echo getSettingDesa()['desa_telp']; ?></li>
                  </ul>
                </div>
                <div class="col-sm-4 col-6 footer-list-28 mt-sm-0 mt-4">
                  <h6 class="footer-title-28">Sosial Media</h6>
                  <ul class="social-icons">
                    <li class="facebook">
                      <a target="_blank" href="<?php echo getSettingDesa()['desa_fb']; ?>"><span class="fa fa-facebook"></span> Facebook</a>
                    </li>
                      <li class="twitter">
                        <a target="_blank" href="<?php echo getSettingDesa()['desa_ig']; ?>"><span class="fa fa-instagram"></span> Instagram</a>
                      </li>
                     
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </footer>

        <!-- move top -->
        <button onclick="topFunction()" id="movetop" title="Go to top">
          <span class="fa fa-angle-up"></span>
        </button>
        <script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
      scrollFunction()
    };

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("movetop").style.display = "block";
      } else {
        document.getElementById("movetop").style.display = "none";
      }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
  </script>
  <!-- /move top -->
</section>
<!-- Template JavaScript -->
<script src="<?php echo base_url('themefront/desahub/assets/js/jquery-3.3.1.min.js');?> "></script>

<!-- theme changer js -->
<script src="<?php echo base_url('themefront/desahub/assets/js/theme-change.js');?> "></script>
<!-- //theme changer js -->

<!-- courses owlcarousel -->
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('themefront/desahub/assets/js/owl.carousel.js');?> "></script>

<!-- bootstrap -->
<script src="<?php echo base_url('themefront/desahub/assets/js/bootstrap.min.js')?> "></script>
<script src="<?php echo base_url('themefront/desahub/slick/slick.js')?> "></script>

<script>
   $(document).on('ready', function() {
    $(".lazy").slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true,
      });
    });

    $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1
      });


   

function share_fb(url) {


  window.open('https://www.facebook.com/sharer/sharer.php?u='+url,'facebook-share-dialog',"width=626, height=436")
}

function share_twitter(url) {

  
  var href = 'https://twitter.com/intent/tweet?text='+url;

  window.open(href, "Twitter", "height=285,width=550,resizable=1");


}

function shareHit(id) {
  
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('berita/hitshare') ?>", 
    data:  {id: id},
    dataType: "text",  
    cache:false,
    success: 
    function(data){

    }
      });// you have missed this bracket
  return false;


}

</script>
</body>

</html>