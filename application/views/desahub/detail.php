<style>
  .text-list{
    font-family: 'Merriweather', sans-serif;
  }


  .text11-content img{
    max-width: 100%;
  }
  .img-detail{
    width: 100%;
  }
.card-img-bottom{

    width: 100%  !important;
  }

  img.card-img-bottom{
    width: auto;
    height: 300px !important;

  }


</style>

<section class="pages-content text-11 py-5">
  <div class="text11 py-lg-5 py-md-4">
    <div class="container">
      <div class="blog-title px-md-5">
        <h3 class="title-big"><?php echo $detail['berita']['judul'] ?></h3>
        <ul class="blog-list">
          <li>
            <p> Ditulis <strong> <?php echo tgl_indo($detail['berita']['tanggal']) ?></strong></p>
          </li>
          <li>
            <p> Oleh <a href="#author"><strong><?php echo $detail['berita']['nama'] ?></strong></a></p>
          </li>
          <li>
            <p> Published in <a href="#category"><strong><?php echo $detail['berita']['nama_kategori'] ?></strong></a></p>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
  
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="text11-content">

          <div class="text-list">

           <p>
            <?php echo $detail['berita']['image'] ?>
            <?php echo $detail['berita']['isi_berita'] ?></p>
          </div>
          
          <div class="social-share-blog mt-5">

            <ul class="column3 tags m-0 p-0">
              <li>
                <p class="m-0 mr-sm-4 mr-2">Tags :</p>
              </li>

              <?php

              foreach ($detail['tag'] as $key => $row) {

                ?>

                <li><a href="#url" class="btn-small"><?php echo $row['nama_tag'] ?></a></li>
              <?php } ?>


            </ul>
            <ul class="column3 social m-0 p-0">
              <li>
                <p class="m-0 mr-sm-4 mr-2">Share this post :</p>
              </li>
              <li>
                <a href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_fb('<?php echo $link?>'); return false;" class="facebook"><span class="fa fa-facebook-square"></span></a>
              </li>
              <li>
                <a href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_twitter('<?php echo $link?>'); return false;" class="twitter"><span class="fa fa-twitter"></span></a></li>
             
            </ul>
          </div>
          
        </div>

      </div>
      <div class="sidebar-side col-lg-4 col-md-12 col-sm-12 mt-lg-0 mt-5">
        <aside class="sidebar">


          <!-- //Author card Widget-->
          <!-- Popular Post Widget-->
          <div class="sidebar-widget popular-posts">
            <div class="sidebar-title">
              <h4>Recent Posts</h4>
            </div>

            <?php

            foreach ($recent as $key => $value) {


              ?>

              <article class="post">
                <figure class="post-thumb"><img src="<?php echo base_url('asset/'.$value['gambar']); ?>" ></figure>
                <div class="text"><a href="<?php echo $value['url'] ?>">
                  <?php echo $value['judul'] ?></a>
                </div>
                <div class="post-info"><?php echo $value['tanggal'] ?></div>
              </article>


            <?php } ?>


          </div>

          <!-- sidebar sticky -->
          
          <!-- //sidebar sticky -->

        </aside>
      </div>
    </div>
  </div>




</section>
<!-- //single post -->

<div class="w3l-homeblock2 py-5">
  <div class="container py-lg-5 py-md-4">
    <h3 class="section-title-left mb-4">You may also like </h3>
    <div class="row">



      <?php

      foreach ($infoterkait as $key => $value) {


        ?>

        <div class="col-lg-3 col-md-6 mt-lg-0 mt-4">
          <div class="card">
            <div class="card-header p-0 position-relative">
              <a href="<?php echo $value['url'] ?>">
                <img style="object-fit: cover;" width="20px" class="card-img-bottom d-block radius-image-full" src="<?php echo base_url('asset/'.$value['gambar']); ?>"
                >
              </a>
            </div>
            <div class="card-body blog-details">
              <span class="label-blue"><?php echo $value['kategori']; ?></span>
              <a href="<?php echo $value['url'] ?>" class="blog-desc"><?php echo substr($value['judul'],0,62) ?>
              </a>


            </div>
          </div>
        </div>

      <?php } ?>

    </div>
  </div>
</div>
<!-- footer-28 block -->