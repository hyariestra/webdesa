<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo getSettingDesa()['desa_title']; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo base_url("template/bootstrap/css/bootstrap.min.css") ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("template/dist/css/AdminLTE.min.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/dist/css/skins/skin-blue.min.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/plugins/datatables/dataTables.bootstrap.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/plugins/datepicker/datepicker3.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/plugins/bootstrap-sweetalert/sweet-alert.css") ?>">


  <link rel="stylesheet" href="<?php echo base_url("template/plugins/bootstrap-sweetalert/sweet-alert.css") ?>">

  <!-- select2 -->
  <link rel="stylesheet" href="<?php echo base_url("template/plugins/select2/select2.min.css") ?>">



  <!-- jQuery 2.1.4 -->
  <script src="<?php echo base_url("template/plugins/jQuery/jQuery-2.1.4.min.js") ?>"></script>
  <script src="<?php echo base_url("template/plugins/jQuery/ajaxform.js") ?>"></script>
  

  <script type="text/javascript" src="<?php echo base_url(); ?>template/plugins/noty/noty.js"></script>

  <link rel="stylesheet" href="<?php echo base_url(); ?>template/plugins/noty/noty.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/plugins/noty/themes/semanticui.css">

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.5.1/styles/default.min.css">


<link rel="stylesheet" title="Monokai Sublime" href="//highlightjs.org/static/demo/styles/base16/monokai.css">
</head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
-->
<style>
  .select2-container .select2-selection--single{
    height: 34px !important;
  }
</style>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $idAdmin = $this->session->userdata['pengguna']['id_admin'];
  
  $avatar = $this->session->userdata['pengguna']['foto_admin_uniq'];

  $avatars = !empty($avatar) ? 'foto_user/'.rawurlencode($avatar) : 'img/user.png';
  
  ?>
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo base_url(); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        
        <!-- logo for regular state and mobile devices -->

      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">


          <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url('asset/'.$avatars); ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata("pengguna")['nama']; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url('asset/'.$avatars); ?>" class="img-circle" alt="User Image">
                  <p>
                    <?php echo $this->session->userdata("pengguna")['nama']; ?>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?php echo base_url("user/profile/".encrypt_url($idAdmin)) ?>" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo site_url("pengguna/logout") ?>" class="btn btn-default btn-flat">Logout</a>
                  </div>
                </li>
              </ul>
            </li>
            
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">


         <?php

         $be_menu = $this->M_menu->get_be_menu_tree();
         $arrayMenu  =  $this->M_menu->getArrayDesaMenu();



         foreach ($be_menu as $key => $fe) {

          if (!in_array($fe['id_menu'], $arrayMenu)) {
            unset($be_menu[$key]);
          }

          if(!empty($fe['submenu'])){

            foreach ($fe['submenu'] as $keyz => $fe_sub) {

              if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

                unset($be_menu[$key]['submenu'][$fe_sub['id_submenu']]);
              }


            }
          } 

                //  unset($fe_menu[4]['submenu'][7]);
        }

     
       
        foreach ($be_menu as $key => $fe) {

          if( empty($fe['submenu']) OR $fe['is_dropdown'] == 0   ){
            echo '<li><a href='.base_url($fe['link_menu']).'><i class="'.$fe['logo_parent'].'"></i> <span>'.$fe['menu'].'</span></a></li>';

          }else{

            echo '
            <li class="treeview">

            <a href="#">
            <i class="'.$fe['logo_parent'].'"></i>
            <span>'.$fe['menu'].'</span>
            <i class="fa fa-angle-left pull-right"></i>
            </a><ul class="treeview-menu">';


            foreach ($fe['submenu']  as $keyz => $fe_submenu) {


              echo '
              <li><a href="'.base_url($fe_submenu['link_submenu']).'"><i class="'.$fe_submenu['logo_subparent'].'"></i> <span>'.$fe_submenu['submenu'].'</span></a></li>';

         //   echo '<li><a href='.$fe_submenu['link_submenu'].'>'.$fe_submenu['submenu'].'</a></li>';

            }
            echo 
            '  </ul>
            </li>';
          }


        }?>



        ?>




      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">

     <?php echo $this->session->userdata("pesan"); ?>

     <?php echo $konten; ?>
   </section><!-- /.content -->
 </div><!-- /.content-wrapper -->

 <!-- Main Footer -->
 <footer class="main-footer">
  <!-- To the right -->

  <!-- Default to the left -->
  <strong> <?php echo getSettingDesa()['desa_nama']; ?>.id </strong> All rights reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Create the tabs -->
  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
  </ul>
  <!-- Tab panes -->

</aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
       <div class="control-sidebar-bg"></div>
     </div><!-- ./wrapper -->

     <!-- REQUIRED JS SCRIPTS -->
     <!-- Bootstrap 3.3.5 -->
     <script src="<?php echo base_url("template/bootstrap/js/bootstrap.min.js") ?>"></script>
     <!-- AdminLTE App -->
     <script src="<?php echo base_url("template/dist/js/app.min.js") ?>"></script>

     <script src="<?php echo base_url("template/plugins/datatables/jquery.dataTables.min.js") ?>"></script>
     <script src="<?php echo base_url("template/plugins/datatables/dataTables.bootstrap.min.js") ?>"></script>
     <script src="<?php echo base_url("template/plugins/datepicker/bootstrap-datepicker.js") ?>"></script>
     <script src="<?php echo base_url("template/plugins/accounting/accounting.min.js") ?>"></script>
     <script src="<?php echo base_url("template/plugins/bootstrap-sweetalert/sweet-alert.min.js") ?>"></script>
     <script src="<?php echo base_url("template/plugins/filemanager/ckeditor/ckeditor.js")?>"></script>
     <script src="<?php echo base_url("template/plugins/select2/select2.full.min.js") ?>"></script>


     <script type="text/javascript">

      $( document ).ready(function() {
       $(function () {

        $(".select2").select2();
        var allowedContent = true; // to allow custom html like iframe or div's

        CKEDITOR.config.entities = false;   
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.entities_greek= false;
        CKEDITOR.config.entities_latin= false;  
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.image_prefillDimensions = false;
        // CKEDITOR.config.enterMode                = CKEDITOR.ENTER_DIV;
        // CKEDITOR.config.shiftEnterMode            = CKEDITOR.ENTER_BR;


        var $ckfield  =    CKEDITOR.replace('file-manager',{
          extraPlugins: 'codesnippet',
	codeSnippet_theme: 'monokai_sublime', 
          filebrowserImageBrowseUrl : '<?php echo base_url('template/plugins/filemanager/kcfinder/browse.php');?>',
          height: '300px',
          allowedContent             
          
        });
        $ckfield.on('change', function() {
          $ckfield.updateElement();         
        });
      });
     });

   </script>


   <script>
    $(function () {
      $("#tabelku").DataTable();

    });
  </script>
  <script type="text/javascript">
  // To make Pace works on Ajax calls
  $(document).ajaxStart(function() { });
  $('.ajax').click(function(){
    $.ajax({url: '#', success: function(result){
      $('.ajax-content').html('<hr>Ajax Request Completed !');
    }});
  });


  var url = window.location;
    // for sidebar menu entirely but not cover treeview
    $('ul.sidebar-menu a').filter(function() {
      return this.href == url;
    }).parent().addClass('active');

    // for treeview
    $('ul.treeview-menu a').filter(function() {
      console.log(this.href);
      return this.href == url;
    }).closest('.treeview').addClass('active');


    function formatRupiah(angka, prefix) {

var angka= angka.toString();

var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

// tambahkan titik jika yang di input sudah menjadi angka ribuan
if (ribuan) {
    separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
}

rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}

  </script>
  <script src="<?php echo base_url("template/plugins/auto-numeric/autoNumeric.js") ?>" type="text/javascript"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.5.1/highlight.min.js"></script>
  <script>hljs.highlightAll();</script>

</body>
</html>
