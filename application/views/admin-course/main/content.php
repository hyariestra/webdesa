<style>
  iframe {
    width: 100%
  }

  img {
    max-width: 100%;
  }

  .bs-callout-warning {
    border-left-color: #aa6708;
  }

  .bs-callout {
    padding: 20px;
    margin: 20px 0;
    border: 1px solid #eee;
    border-left-width: 5px;
    border-radius: 3px;
  }
</style>
<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0"> <b><?php echo $konten['series_title_detail']; ?></b></h5>
  </div>
  <div class="card-body">


    <p class="card-text"><?php echo $konten['series_content'];  ?></p>

    <?php if ($konten['files']) { ?>
      <div class="bs-callout bs-callout-warning" id="callout-navbar-role"> <i class="fa fa-paperclip" aria-hidden="true"></i> Attachment
        <ul>
          <?php

          foreach ($konten['files'] as $key => $value) {
          ?>
            <li><i><a download="<?php echo $value['file_series']; ?>" href=""><?php echo $value['file_series']; ?></a></i></li>
          <?php } ?>


        </ul>

      </div>
    <?php } else { ?>

      <div class="empty">

      </div>
    <?php } ?>

  </div>
</div>

<script>

$( document ).ready(function() {
    console.log( "ready!" );


    $('#save_steps').click(function(event){

//event.preventDefault();

//   return false;

    $.ajax({
        url: $(this).data('url'),
        type: 'post',
        dataType: 'json',
        data: {
        
          'id_detail' : $(this).data('id'),
        },
        success: function(data) {
                   // ... do something with the data...
                 }
    });
});

});



</script>