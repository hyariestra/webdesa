<section class="services-w3l-block py-5" id="features">
        <div class="container py-md-5 py-4">
            <div class="title-main text-center mx-auto mb-md-5 mb-4" style="max-width:500px;">
              
                <h3 class="title-style">MERDEKA AIR BERSIH BERSAMA SEKOLAH AIR</h3>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3 d-flex align-items-stretch">
                    <div class="icon-box icon-box-clr-1">
                        <div class="icon"><i class="fas fa-user-friends"></i></div>
                        <h4 class="title"><a href="#about">Pelatih Berkualitas</a></h4>
                        <p>Pelatih berpengalaman 10 tahun di dunia pengelolaan air bersih</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex align-items-stretch mt-md-0 mt-4">
                    <div class="icon-box icon-box-clr-2">
                        <div class="icon"><i class="fas fa-book-reader"></i></div>
                        <h4 class="title"><a href="#about">Materi Berkualitas</a></h4>
                        <p>Materi disusun dan diperbaharui terus-menerus sesuai kasus di lapangan</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex align-items-stretch mt-lg-0 mt-4">
                    <div class="icon-box icon-box-clr-3">
                        <div class="icon"><i class="fas fa-user-graduate"></i></div>
                        <h4 class="title"><a href="#about">Support Paska Pelatihan</a></h4>
                        <p>Sekolah Air terus mensupport seluruh komunitas paska pelatihan</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex align-items-stretch mt-lg-0 mt-4">
                    <div class="icon-box icon-box-clr-4">
                        <div class="icon"><i class="fas fa-university"></i></div>
                        <h4 class="title"><a href="#about">Menciptakan Peluang</a></h4>
                        <p>Sekolah Air menyiapkan beragam program bermanfaat paska pelatihan</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- home 4grids block -->
<div style="margin: 8px auto; display: block; text-align:center;">

<!---728x90--->
 
</div>

    <!-- home image with content block -->
    <section class="w3l-servicesblock pt-lg-5 pt-4 pb-5 mb-lg-5" id="about">
        <div class="container pb-md-5 pb-4">
            <div class="row pb-xl-5 align-items-center">
                <div class="col-lg-6 position-relative home-block-3-left pb-lg-0 pb-5">
                    <div class="position-relative">
                        <img src="<?php echo base_url('themefront/sekolah/assets/images/foto.jpg')?>" alt="" class="img-fluid radius-image">
                    </div>
                    <div class="imginfo__box">
                        <p>Kami berpengalaman dan kini berbagi <br> solusi pengelolaan air bersih bagi semua. </p>
                        <a target="_blank" href="https://wa.me/<?php echo getSettingDesa()['desa_telp']; ?>?text=Hallo%20Admin,%20saya%20ingin%20berkonsultasi%20tentang%20pelatihan"><i class="fa fa-whatsapp"></i> <?php echo getSettingDesa()['desa_telp']; ?></a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 offset-xl-1 mt-lg-0 mt-5 pt-lg-0 pt-5">
                    <h3 class="title-style">Sekolah Air Pilihan Terbaik!</h3>
                    <p class="mt-4">Pelatih dan materi pendidikan kami adalah hasil riset terus-menerus sesuai kondisi dan kasus di lapangan. Disampaikan pemateri dengan standar kualifikasi yang telah kami tentukan.</p>
                    <ul class="mt-4 list-style-lis pt-lg-1">
                        <li><i class="fas fa-check-circle"></i>Materi Pendidikan berkembang sesuai kebutuhan</li>
                        <li><i class="fas fa-check-circle"></i>Kami berpengalaman lapangan di berbagai pelosok Indonesia</li>
                        <li><i class="fas fa-check-circle"></i>Kami siapkan semua kebutuhan akomodasi peserta</li>
                    </ul>
                    <a href="contact.html" class="btn btn-style mt-5">Apply Now</a>
                </div>
            </div>
        </div>
    </section>
    <!-- //home image with content block -->
<div style="margin: 8px auto; display: block; text-align:center;">

<!---728x90--->
 
</div>

<div class="w3l-grids-block-5 home-course-bg py-5" id="courses">
        <div class="container py-md-5 py-4">
            <div class="title-main text-center mx-auto mb-md-5 mb-4" style="max-width:500px;">
                <p class="text-uppercase">Best Courses</p>
                <h3 class="title-style">Temukan Pelatihan Terbaik</h3>
            </div>
            <div class="row justify-content-center">

                <?php
                
                    $selectedPelatihan = sliceArrayByCategoryByCode($itemAll, 'pelatihan', 3);
        

                    foreach ($selectedPelatihan as $data) {

                
                ?>

                <div class="col-lg-4 col-md-6">
                    <div class="coursecard-single">
                        <div class="grids5-info position-relative">
                            <img src="<?php echo base_url('asset/'.$data['gambar']); ?>" alt="" class="img-news" />
                            <div style="display: none;" class="meta-list">
                                <a href="courses.html">Art & Design</a>
                            </div>
                        </div>
                        <div class="content-main-top">
                            
                            <h4><a href="<?php echo $data['url'] ?>"><?php echo $data['judul'] ?></a></h4>
                            <p><?php echo $data['isi_berita']; ?></p>
                            <div class="top-content-border d-flex align-items-center justify-content-between mt-4 pt-4">
                               
                                <a class="btn btn-style-primary" href="<?php echo $data['url'] ?>">Selengkapnya<i
                                        class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php } ?>

            </div>
            <div class="text-center mt-sm-5 mt-4 pt-sm-0 pt-1">
                <a class="btn btn-style btn-style-secondary mt-sm-3" href="<?php echo base_url('berita/kategori/pelatihan'); ?>">
                    Lihat Pelatihan Lainnya</a>
            </div>
        </div>
    </div>

    <!-- <section class="w3l-service-1 py-5">
        <div class="container py-md-5 py-4">
            <div class="title-main text-center mx-auto mb-md-5 mb-4" style="max-width:500px;">
                <p class="text-uppercase">Why Choose Us</p>
                <h3 class="title-style">Tools For Teachers And Learners</h3>
            </div>
            <div class="row content23-col-2 text-center">
                <div class="col-md-6">
                    <div class="content23-grid content23-grid1">
                        <h4><a href="about.html">Expert Teachers</a></h4>
                    </div>
                </div>
                <div class="col-md-6 mt-md-0 mt-4">
                    <div class="content23-grid content23-grid2">
                        <h4><a href="about.html">Safe Environment</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- <section class="w3-stats pt-4 pb-5" id="stats">
        <div class="container pb-md-5 pb-4">
            <div class="title-main text-center mx-auto mb-md-5 mb-4" style="max-width:500px;">
                <p class="text-uppercase">Our Statistics</p>
                <h3 class="title-style">We are Proud to Share with You</h3>
            </div>
            <div class="row text-center pt-4">
                <div class="col-md-3 col-6">
                    <div class="counter">
                        <img src="<?php echo base_url('themefront/sekolah/assets/images/icon-1.png')?>" alt="" class="img-fluid">
                        <div class="timer count-title count-number mt-sm-1" data-to="36076" data-speed="1500"></div>
                        <p class="count-text">Students Enrolled</p>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="counter">
                        <img src="<?php echo base_url('themefront/sekolah/assets/images/icon-2.png')?>" alt="" class="img-fluid">
                        <div class="timer count-title count-number mt-3" data-to="786" data-speed="1500"></div>
                        <p class="count-text">Our Branches</p>
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md-0 mt-5">
                    <div class="counter">
                        <img src="<?php echo base_url('themefront/sekolah/assets/images/icon-3.png')?>" alt="" class="img-fluid">
                        <div class="timer count-title count-number mt-3" data-to="3630" data-speed="1500"></div>
                        <p class="count-text">Total Courses</p>
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md-0 mt-5">
                    <div class="counter">
                        <img src="<?php echo base_url('themefront/sekolah/assets/images/icon-4.png')?>" alt="" class="img-fluid">
                        <div class="timer count-title count-number mt-3" data-to="6300" data-speed="1500"></div>
                        <p class="count-text">Awards Won</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->



    <section class="w3l-index4 py-5" id="testimonials">
        <div class="container py-md-5 py-4">
            <div class="content-slider text-center">
                <div class="clients-slider">
                    <div class="mask">
                        <ul>
                            
                        <?php
                        $no = 1;
                        foreach ($pemateri as $item) {
                            
                        
                        
                        ?>
                           <li class="anim<?php echo $no; ?>">
                                <img src="<?php echo base_url('asset/'.$item['gambar']); ?>" class="img-fluid rounded-circle"
                                    alt="client image" />
                                <blockquote class="quote"><q><?php echo $item['keterangan']; ?>
                                    </q> </blockquote>
                                <div class="source">- <?php echo $item['nama_pemateri']; ?></div>
                            </li>

                            <?php $no++;  }  ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="w3l-blog-block-5 py-5" id="blog">
        <div class="container py-md-5 py-4">
            <div class="title-main text-center mx-auto mb-md-5 mb-4" style="max-width:500px;">
                <p class="text-uppercase">Our News</p>
                <h3 class="title-style">Latest <span>Blog</span> Posts</h3>
            </div>
            <div class="row justify-content-center">
                
            <?php
									
                $selectedNews = sliceArrayByCategoryByCode($itemAll, null, 3);
    

                 foreach ($selectedNews as $data) {

                 ?>
                <div class="col-lg-4 col-md-6 mt-lg-0 mt-4">
                    <div class="blog-card-single">
                        <div class="grids5-info">
                            <a href="<?php echo $data['url'] ?>"><img class="img-news" src="<?php echo base_url('asset/'.$data['gambar']); ?>" alt="" /></a>
                            <div class="blog-info">
                                <h4><a href="<?php echo $data['url'] ?>"><?php echo $data['judul'] ?></a></h4>
                                <p><?php echo $data['isi_berita']; ?></p>
                                <div class="d-flex align-items-center justify-content-between mt-4">
                                    <a class="d-flex align-items-center" href="#" title="">
                                        <!-- <img class="img-fluid" src="<?php echo base_url('asset/'.$data['avatar']); ?>" alt="admin"
                                            style="max-width:40px"> -->
                                             <span class="small ms-2"><?php echo $data['penulis']; ?>
                                        </span>
                                    </a>
                                    <p class="date-text"><i class="far fa-calendar-alt me-1"></i><?php echo tgl_indo($data['tanggal']) ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php } ?>



            </div>
        </div>
    </div>