
<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
     <link rel="icon" type="image/x-icon" href="<?php echo getSettingDesa()['favicon_desa']; ?>">
    <meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
    <meta name="google-site-verification" content="<?php echo getSettingDesa()['desa_metatag']; ?>" />
  <title><?php include "phpmu-title.php"; ?></title>
    <link rel="stylesheet" href="<?php echo base_url('themefront/sekolah/assets/css/style-liberty.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/global_asset/slick-js/slick/slick.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/global_asset/slick-js/slick/slick-theme.css'); ?>">
   
</head>

<body>

<meta name="robots" content="noindex">
<body>
    <link rel="stylesheet" href="<?php echo base_url('themefront/sekolah/assets/css/font-awesome.min.css'); ?>">
<!-- New toolbar-->
<style type="text/css">
    html, body {
      margin: 0;
      padding: 0;
    }

    img.artikel{
        width: 200px;
        height: 300px;
        object-fit: cover;    
    }

    * {
      box-sizing: border-box;
    }

    .slick-dots{
    position: absolute;
    bottom: 20px;
    display: block;
    width: 100%;
    padding: 10;
    margin: 0;
    list-style: none;
    text-align: center;
    }
    .slick-dots li.slick-active button:before{
    font-size:15px;
    color:blue;
  
}


    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
  </style>
<style>
* {
  box-sizing: border-box;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
}

img.img-news{
    height: 350px;
object-fit: cover;

}


#w3lDemoBar.w3l-demo-bar {
  top: 0;
  right: 0;
  bottom: 0;
  z-index: 9999;
  padding: 40px 5px;
  padding-top:70px;
  margin-bottom: 70px;
  background: #0D1326;
  border-top-left-radius: 9px;
  border-bottom-left-radius: 9px;
}

#w3lDemoBar.w3l-demo-bar a {
  display: block;
  color: #e6ebff;
  text-decoration: none;
  line-height: 24px;
  opacity: .6;
  margin-bottom: 20px;
  text-align: center;
}

#w3lDemoBar.w3l-demo-bar span.w3l-icon {
  display: block;
}

#w3lDemoBar.w3l-demo-bar a:hover {
  opacity: 1;
}

#w3lDemoBar.w3l-demo-bar .w3l-icon svg {
  color: #e6ebff;
}
#w3lDemoBar.w3l-demo-bar .responsive-icons {
  margin-top: 30px;
  border-top: 1px solid #41414d;
  padding-top: 40px;
}
#w3lDemoBar.w3l-demo-bar .demo-btns {
  border-top: 1px solid #41414d;
  padding-top: 30px;
}
#w3lDemoBar.w3l-demo-bar .responsive-icons a span.fa {
  font-size: 26px;
}
#w3lDemoBar.w3l-demo-bar .no-margin-bottom{
  margin-bottom:0;
}
.toggle-right-sidebar span {
  background: #0D1326;
  width: 50px;
  height: 50px;
  line-height: 50px;
  text-align: center;
  color: #e6ebff;
  border-radius: 50px;
  font-size: 26px;
  cursor: pointer;
  opacity: .5;
}
.pull-right {
  float: right;
  position: fixed;
  right: 0px;
  top: 70px;
  width: 90px;
  z-index: 99999;
  text-align: center;
}
/* ============================================================
RIGHT SIDEBAR SECTION
============================================================ */

#right-sidebar {
  width: 90px;
  position: fixed;
  height: 100%;
  z-index: 1000;
  right: 0px;
  top: 0;
  margin-top: 60px;
  -webkit-transition: all .5s ease-in-out;
  -moz-transition: all .5s ease-in-out;
  -o-transition: all .5s ease-in-out;
  transition: all .5s ease-in-out;
  overflow-y: auto;
}


/* ============================================================
RIGHT SIDEBAR TOGGLE SECTION
============================================================ */

.hide-right-bar-notifications {
  margin-right: -300px !important;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}



@media (max-width: 992px) {
  #w3lDemoBar.w3l-demo-bar a.desktop-mode{
      display: none;

  }
}
@media (max-width: 767px) {
  #w3lDemoBar.w3l-demo-bar a.tablet-mode{
      display: none;

  }
}
@media (max-width: 568px) {
  #w3lDemoBar.w3l-demo-bar a.mobile-mode{
      display: none;
  }
  #w3lDemoBar.w3l-demo-bar .responsive-icons {
      margin-top: 0px;
      border-top: none;
      padding-top: 0px;
  }
  #right-sidebar,.pull-right {
      width: 90px;
  }
  #w3lDemoBar.w3l-demo-bar .no-margin-bottom-mobile{
      margin-bottom: 0;
  }
}
</style>
</div>

    <!-- header -->
    <header id="site-header" class="fixed-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="<?php echo base_url('/') ?>">
                <img width="70px" src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="<?php echo getSettingDesa()['desa_nama']; ?>_logo"></a>
                </a>
                <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon fa icon-expand fa-bars"></span>
                    <span class="navbar-toggler-icon fa icon-close fa-times"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarScroll">
                    
                    <ul class="navbar-nav ms-auto my-2 my-lg-0 navbar-nav-scroll">
                        
                    <?php
                        $fe_menu  =  $this->M_homepage->get_front_menu_tree();
                        $arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


                        foreach ($fe_menu as $key => $fe) {
                            
                            if (!in_array($fe['id_menu'], $arrayMenu)) {
                                unset($fe_menu[$key]);
                            }

                            if(!empty($fe['submenu'])){

                                foreach ($fe['submenu'] as $keyz => $fe_sub) {

                                    if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

                                        unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
                                    }


                                }
                            }	

                                //	unset($fe_menu[4]['submenu'][7]);
                        }


                        foreach ($fe_menu as $key => $fe) {


                            if(empty($fe['submenu'])){
                                
                            echo '<li class="nav-item">';
                            echo '<a class="nav-link" aria-current="page" href="'.base_url($fe['link_menu']).'">'.$fe['menu'].'</a>';
                            echo '</li>';

                            }else{


                               echo '<li class="nav-item dropdown">';
                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    '.$fe['menu'].'<i class="fas fa-angle-down"></i>
                                </a>';
                                echo '<ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">';

                                foreach ($fe['submenu']  as $keyz => $fe_submenu) {
                                
                                    echo '<li><a class="dropdown-item" href="'.base_url($fe_submenu['link_submenu']).'">'.$fe_submenu['submenu'].'</a></li>';

                                }
                                 echo '</ul>
                            </li>';

                            }

                        }?>





                    </ul>
                    <!-- search-right -->
                    <div class="header-search position-relative">
                        <div class="search-right mx-lg-2">
                            <a href="#search" class="btn search-btn p-0" title="search">
                                <i class="fas fa-search"></i></a>
                            <!-- search popup -->
                            <div id="search" class="pop-overlay">
                                <div class="popup">
                                    <form action="<?php echo base_url('berita') ?>" method="GET" class="search-box">
                                        <input value="<?= @$search ?>" type="search" placeholder="Enter Keyword..." name="search"
                                            required="required" autofocus="">
                                        <button type="submit" class="btn"><span class="fas fa-search"
                                                aria-hidden="true"></span></button>
                                    </form>
                                </div>
                                <a class="close" href="#close">×</a>
                            </div>
                            <!-- //search popup -->
                        </div>
                    </div>
                    <!--//search-right-->
                </div>
                <!-- toggle switch for light and dark theme -->
                <div class="cont-ser-position">
                    <nav class="navigation">
                        <div class="theme-switch-wrapper">
                            <label class="theme-switch" for="checkbox">
                                <input type="checkbox" id="checkbox">
                                <div class="mode-container">
                                    <i class="gg-sun"></i>
                                    <i class="gg-moon"></i>
                                </div>
                            </label>
                        </div>
                    </nav>
                </div>
                <!-- //toggle switch for light and dark theme -->
            </nav>
        </div>
    </header>
    <!-- //header -->

    <!-- banner section -->
    <?php echo $slider ?>
    <!-- //banner section -->

    <div class="main-content">
    <?php echo $content ?>
    </div>


    <section class="w3l-call-to-action-6">
        <div class="container py-md-5 py-sm-4 py-5">
            <div class="d-lg-flex align-items-center justify-content-between">
                <div class="left-content-call">
                    <h3 class="title-big">Konsultasi Gratis</h3>
                </div>
                <div class="right-content-call mt-lg-0 mt-4">
                    <ul class="buttons">
                        <li class="phone-sec me-lg-4"><i class="fa fa-whatsapp"></i>
                            <a class="call-style-w3" ><?php echo getSettingDesa()['desa_telp']; ?></a>
                        </li>
                        <li><a target="_blank" href="https://wa.me/<?php echo getSettingDesa()['desa_telp']; ?>?text=Hallo%20Admin,%20saya%20ingin%20berkonsultasi%20tentang%20pelatihan" class="btn btn-style btn-style-2 mt-lg-0 mt-3">Klik Disini</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- //call block -->

    <!-- footer block -->
    <footer class="w3l-footer-29-main">
        <div class="footer-29 pt-5 pb-4">
            <div class="container pt-md-4">
                <div class="row footer-top-29">
                    <div class="col-lg-4 col-md-6 footer-list-29">
                        <h6 class="footer-title-29">Contact Info </h6>
                        <p class="mb-2 pe-xl-5">Alamat : <?php echo getSettingDesa()['desa_lokasi']; ?>
                        </p>
                        <p class="mb-2">Telp : <?php echo getSettingDesa()['desa_telp']; ?></p>
                        <p class="mb-2">Email : <?php echo getSettingDesa()['desa_email']; ?></p>
                    </div>
                    <div class="col-lg-2 col-md-3 col-6 footer-list-29 mt-md-0 mt-4">
                        <ul>
                            <h6 class="footer-title-29">Quick Links</h6>


                            <?php
                            foreach ($fe_menu as $fe) {
                            ?>
                          

                            <li><a href="<?php echo base_url($fe['link_menu']); ?>"><?php echo $fe['menu'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-3 col-6 ps-lg-5 ps-lg-4 footer-list-29 mt-md-0 mt-4">
                        <ul>
                            <h6 class="footer-title-29">Popular Topic</h6>
                           
                                <?php
                                
                                
                                $kate = $this->M_berita->get_count_kategori();
                        
                                
                                foreach ($kate as $key => $value) {
                                    
                                

                                ?>

                      
                            <li><a href="<?php echo $value['url_kategori'] ?>"><?php echo $value['nama_kategori'] ?> (<?php echo $value['counts'] ?>) </a></li>
                        
                            <?php } ?>
                        
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-8 footer-list-29 mt-lg-0 mt-4 ps-lg-5">
                        <h6 class="footer-title-29">Subscribe</h6>
                        <form action="<?php echo site_url('berita/news_save_php') ?>" class="subscribe d-flex" method="post">
                            <input type="email" name="email" placeholder="Email Address" required="">
                            <button class="button-style"><span class="fa fa-paper-plane"
                                    aria-hidden="true"></span></button>
                        </form>
                        <p class="mt-3">Subscribe untuk mendapatkan info dan penawaran terbaik dari kami.</p>
                    </div>
                </div>
                <!-- copyright -->
                <p class="copy-footer-29 text-center pt-lg-2 mt-5 pb-2">© 2021 <?php echo getSettingDesa()['desa_nama']; ?>. All rights reserved. Design
                    by <a href="https://desahub.id/" target="_blank">desahub.id</a></p>
                <!-- //copyright -->
            </div>
        </div>
    </footer>
    <!-- //footer block -->

    <!-- Js scripts -->
    <!-- move top -->
    <button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fas fa-level-up-alt" aria-hidden="true"></span>
    </button>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("movetop").style.display = "block";
            } else {
                document.getElementById("movetop").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
    <!-- //move top -->
  
    <!-- common jquery plugin -->    
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script> <!-- //common jquery plugin -->

    <!-- /counter-->
    <script src="<?php echo base_url('themefront/sekolah/assets/js/counter.js');?> "></script>
    <!-- //counter-->

    <!-- theme switch js (light and dark)-->
    <script src="<?php echo base_url('themefront/sekolah/assets/js/theme-change.js');?> "></script>
    <!-- //theme switch js (light and dark)-->

    <script src="<?php echo base_url('asset/global_asset/slick-js/slick/slick.js');?> "></script>

    <!-- MENU-JS -->
    <script>
        $(window).on("scroll", function () {
            var scroll = $(window).scrollTop();

            if (scroll >= 80) {
                $("#site-header").addClass("nav-fixed");
            } else {
                $("#site-header").removeClass("nav-fixed");
            }
        });

        //Main navigation Active Class Add Remove
        $(".navbar-toggler").on("click", function () {
            $("header").toggleClass("active");
        });
        $(document).on("ready", function () {
            if ($(window).width() > 991) {
                $("header").removeClass("active");
            }
            $(window).on("resize", function () {
                if ($(window).width() > 991) {
                    $("header").removeClass("active");
                }
            });

          $(".lazy").slick({
            lazyLoad: 'ondemand', // ondemand progressive anticipated
            infinite: true,
            dots: true,
            arrows: false,
        });


        });

        function shareHit(id) {
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('berita/hitshare') ?>", 
				data:  {id: id},
				dataType: "text",  
				cache:false,
				success: 
				function(data){

				}
          });// you have missed this bracket
			return false;


		}


        function share_fb(url) {
            window.open('https://www.facebook.com/sharer/sharer.php?u='+url,'facebook-share-dialog',"width=626, height=436")
        }

        function share_twitter(url) {


             var href = 'https://twitter.com/intent/tweet?text='+url;

            window.open(href, "Twitter", "height=285,width=550,resizable=1");


        }

    
    </script>
    <!-- //MENU-JS -->

    <!-- disable body scroll which navbar is in active -->
    <script>
        $(function () {
            $('.navbar-toggler').click(function () {
                $('body').toggleClass('noscroll');
            })
        });
    </script>
    <!-- //disable body scroll which navbar is in active -->

    <!-- bootstrap -->
    <script src="<?php echo base_url('themefront/sekolah/assets/js/bootstrap.min.js');?> "></script>
    <!-- //bootstrap -->
    <!-- //Js scripts -->
  


	</body>

</html>