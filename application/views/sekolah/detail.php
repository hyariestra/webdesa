<style>
	
	iframe{
		width: 100%
	}
	img{
		max-width: 100%;
	}
</style>
<section class="w3l-blogpost-content py-5">
        <div class="container py-md-5 py-4">
            <div class="row">
                <div class="text-11 col-lg-8">
                    <div class="text11-content">
                        <h4><?php echo $detail['berita']['judul'] ?></h4>
                        <img src="	<?php echo $detail['berita']['gambar_custom']; ?>" class="img-fluid radius-image" alt="" /> 
                        
                        <?php echo $detail['berita']['isi_berita']; ?>
                        

                        
                        <div class="social-share-blog mt-5">
                            <ul class="social m-0 p-0">
                                <li>
                                    <p class="m-0 mr-4">Share this post :</p>
                                </li>
                                <li>
                                    <a href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_fb('<?php echo $link?>'); return false;" class="facebook"><span class="fa fa-facebook"></span></a>
                                </li>
                                <li><a href="#" onclick="shareHit(<?php echo $detail['berita']['id_berita']; ?>);share_twitter('<?php echo $link?>'); return false;" class="twitter"><span class="fa fa-twitter"></span></a>
                                
                                 </li>
                            </ul>
                        </div>
                        <!-- <div class="nav-links">
                            <div class="nav-previous">
                                <a href="#url" rel="prev">
                                    <img src="<?php echo base_url('themefront/sekolah/assets/images/blog-s1.jpg')?>" alt="" class="img-fluid radius-image" />
                                    <div class="post-data align-self">
                                        <span class="nav-arrow-label"><i
                                                class="fas fa-arrow-left me-1"></i>Previous</span>
                                    </div>
                                </a>
                            </div>
                            <div class="nav-next text-end">
                                <a href="#url" rel="next">
                                    <div class="post-data align-self">
                                        <span class="nav-arrow-label">Next<i class="fas fa-arrow-right ms-1"></i></span>
                                    </div>
                                    <img src="<?php echo base_url('themefront/sekolah/assets/images/blog-s2.jpg')?>" alt="" class="img-fluid radius-image" />
                                </a>
                            </div>
                        </div> -->
                        
                        
                    </div>
                </div>
                <?php echo $sidebar ?>
            </div>
        </div>
    </section>