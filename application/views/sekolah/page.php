<section class="w3l-blogpost-content py-5">
        <div class="container py-md-5 py-4">
            <?php echo $msg ?>
            <div class="row">
                <div class="col-lg-8 w3l-blog-block-5">
                    <div class="row">
                    <?php


                    foreach ($item as $key => $value) {

                  ?>
                    <div class="col-md-6 mt-4">
                            <div class="blog-card-single">
                                <div class="grids5-info">
                                    <a href="<?php echo $value['url'] ?>"><img class="artikel" src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt=""></a>
                                    <div class="blog-info">
                                        <h4><a href="<?php echo $value['url'] ?>"><?php echo $value['judul']; ?></a></h4>
                                        <p>	<?php echo  strip_tags($value['isi_berita']) ?></p>
                                        <div class="d-flex align-items-center justify-content-between mt-4">
                                            <a class="d-flex align-items-center" href="#blog" title="">
                                                <!-- <img class="img-fluid" src="<?php echo base_url('themefront/sekolah/assets/images/testi3.jpg')?>" alt="admin"
                                                    style="max-width:40px">  -->
                                                    <span class="small ms-2"><?php echo $value['penulis']; ?>
                                                </span>
                                            </a>
                                            <p class="date-text"><i class="far fa-calendar-alt me-1"></i><?php echo tgl_indo($value['tanggal']) ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- pagination -->
                    <div class="pagination-style text-start mt-5">
                    <?= $pagination; ?>
                    </div>
                    <!-- //pagination -->
                </div>
                <?php echo $sidebar ?>
            </div>
        </div>
    </section>