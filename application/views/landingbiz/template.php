<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>NewBiz Bootstrap Template - Index</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url('themefront/landingbiz/assets/vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/landingbiz/assets/vendor/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/landingbiz/assets/vendor/ionicons/css/ionicons.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/landingbiz/assets/vendor/owl.carousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/landingbiz/assets/vendor/venobox/venobox.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('themefront/landingbiz/assets/vendor/aos/aos.css')?>" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url('themefront/landingbiz/assets/css/style.css')?> " rel="stylesheet">

  <!-- =======================================================
  * Template Name: NewBiz - v2.2.0
  * Template URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<!-- style masjid-->


<style>
  
#intro{
width: 100%;
background-image:url(<?php echo base_url('themefront/landingbiz/assets/img/mjd/slide.jpeg')?> );
background-repeat:no-repeat;
background-position:center;
}
#header .logo img {
      max-height: 40px;
}

</style>


<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an text logo -->
        <!-- <h1><a href="index.html">NewBiz</a></h1> -->
        <a href="<?php echo base_url('') ?>"><img src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="<?php echo base_url('') ?>">Beranda</a></li>
          <li><a href="#about">Tentang</a></li>
          <li><a href="#services">Masjid</a></li>
          <li><a href="#portfolio">Agenda</a></li>
          <li><a href="#team">Artikel</a></li>
          <li><a href="#team">Kontak Kami</a></li>
        
        </ul>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->

  <!-- ======= Intro Section ======= -->
  <section id="intro" class="clearfix">
    <div class="container" data-aos="fade-up">

      <div class="intro-img" data-aos="zoom-out" data-aos-delay="200">
      <!--   <img src="<?php echo base_url('themefront/landingbiz/assets/img/intro-img.svg')?> " alt="" class="img-fluid"> -->
      </div>

      <div class="intro-info" data-aos="zoom-in" data-aos-delay="100">
        <h2>Masjid<br>Insan mulia sejahtera</h2>
        
      </div>

    </div>
  </section><!-- End Intro Section -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about">
      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <h3>Selamat Datang di Masjid
Insan Mulia Sejahtera
</h3>
        
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p>
            Adalah sebuah Masjid dan taman belajar agama Islam. Terbuka bagi siapapun dan selalu bergandeng-tangan untuk kebaikan dengan siapapun
            </p>

            <p>
              Kami selalu bermimpi membangun Rumah Allah, tempat kami berserah pada-Nya. Alhamdulillah, begitu banyak orang, begitu banyak kalangan mendukung rencana ini. Selamat Datang di Masjid Insan Mulia Sejahtera. Kami selalu menerima dengan bahagia setiap ajakan kebaikan kepada siapa saja.
            </p>

            <p>Pengelolanya adalah</p>

            <p>Dikelola Yayasan Insan Mulia Sejahtera, Masjid ini lahir setelah Keluarga Besar Sastra Sumardi  mewakafkan sebagian tanah keluarga. Masjid ini terletak di Desa Temuwuh, Dlingo, Bantul, Yogyakarta, tepat di depan Balai Desa.</p>
            
            <p>Terimakasih Kami..</p>
            <p>Daya dukung dari berbagai pihak mengalir tiada tara dan setapak demi setapak kami berjuang mewujudkan mimpi. Tiada kata yang mampu menggambarkan kebahagiaan dan terimakasih kami kepada semua pihak yang telah mendukung terwujudnya mimpi besar ini. Kami persembahkan sebuah tempat suci bagi siapa saja untuk berserah diri kepada Allah SWT dan sekaligus tempat anak-anak kami belajar mengenai Islam sejak dini.</p>

            

            

          </div>

          <div class="col-lg-6 background order-lg-2" data-aos="zoom-in">
            <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/38700.jpg')?>" class="img-fluid" alt="">
          </div>
        </div>

        

        

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <!-- End Services Section -->

    <!-- ======= Why Us Section ======= -->
    <!-- End Why Us Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="clearfix">
      <div class="container" data-aos="fade-up">

        

        

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/1.jpeg')?>" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">App 1</a></h4>
                <p>App</p>
                <div>
                  <a href="<?php echo base_url('themefront/landingbiz/assets/img/mjd/1.jpeg')?>" data-gall="portfolioGallery" title="App 1" class="venobox link-preview"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/2.jpeg')?>" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Web 3</a></h4>
                <p>Web</p>
                <div>
                  <a href="<?php echo base_url('themefront/landingbiz/assets/img/mjd/2.jpeg')?>" class="venobox link-preview" data-gall="portfolioGallery" title="Web 3"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/3.jpeg')?>" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">App 2</a></h4>
                <p>App</p>
                <div>
                  <a href="<?php echo base_url('themefront/landingbiz/assets/img/mjd/3.jpeg')?>" class="venobox link-preview" data-gall="portfolioGallery" title="App 2"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/4.jpeg')?> " class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Card 2</a></h4>
                <p>Card</p>
                <div>
                  <a href="<?php echo base_url('themefront/landingbiz/assets/img/mjd/4.jpeg')?> " class="venobox link-preview" data-gall="portfolioGallery" title="Card 2"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/5.jpeg')?> " class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Web 2</a></h4>
                <p>Web</p>
                <div>
                  <a href="<?php echo base_url('themefront/landingbiz/assets/img/mjd/5.jpeg')?> " class="venobox link-preview" data-gall="portfolioGallery" title="Web 2"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/6.jpeg')?> " class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">App 3</a></h4>
                <p>App</p>
                <div>
                  <a href="<?php echo base_url('themefront/landingbiz/assets/img/mjd/6.jpeg')?> " class="venobox link-preview" data-gall="portfolioGallery" title="App 3"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url('themefront/landingbiz/assets/img/mjd/7.jpeg')?> " class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Card 1</a></h4>
                <p>Card</p>
                <div>
                  <a href="<?php echo base_url('themefront/landingbiz/assets/img/mjd/7.jpeg')?> " class="venobox link-preview" data-gall="portfolioGallery" title="Card 1"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          

          

        </div>

      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->
    <!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    <!-- End Team Section -->

    <!-- ======= Clients Section ======= -->
    <!-- End Clients Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact">
      <div class="container-fluid" data-aos="fade-up">

        <div class="section-header">
          <h3>Contact Us</h3>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15806.804315631429!2d110.3213125!3d-7.9262573!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x91069e94e241e5fd!2sYAYASAN%20INSAN%20MULIA!5e0!3m2!1sen!2sid!4v1609863517515!5m2!1sen!2sid" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
            </div>
          </div>


          <div class="col-lg-6">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p><?php echo getSettingDesa()['desa_lokasi']; ?></p>
              </div>
              
              <div class="col-md-5 info">
                <i class="ion-ios-telephone-outline"></i>
                <p><?php echo getSettingDesa()['desa_telp']; ?></p>
              </div>
            </div>

            <div class="form">
              <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validate"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validate"></div>
                </div>
                <div class="mb-3">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>NewBiz</h3>
            <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">About us</a></li>
              <li><a href="#">Services</a></li>
              <li><a href="#">Terms of service</a></li>
              <li><a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Kontak kami</h4>
            <p>
             <?php echo getSettingDesa()['desa_lokasi']; ?><br>
              <strong>Phone:</strong> <?php echo getSettingDesa()['desa_telp']; ?><br>
             
            </p>
            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>NewBiz</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/jquery/jquery.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/jquery.easing/jquery.easing.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/php-email-form/validate.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/counterup/counterup.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/isotope-layout/isotope.pkgd.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/owl.carousel/owl.carousel.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/waypoints/jquery.waypoints.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/venobox/venobox.min.js')?> "></script>
  <script src="<?php echo base_url('themefront/landingbiz/assets/vendor/aos/aos.js')?> "></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url('themefront/landingbiz/assets/js/main.js')?> "></script>

</body>

</html>