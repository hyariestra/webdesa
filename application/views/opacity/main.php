<div class="row align-items-stretch">
	<?php

	foreach ($galeri as $key => $value) {

		$i = $key+1;

		if ($i % 10 == 1 ) {
			$col = 3;
		}elseif ($i % 10 == 2 ) {
			$col = 6;
		}elseif ($i % 10 == 3) {
			$col = 3;
		}elseif ($i % 10 == 4) {
			$col = 8;
		}elseif ($i % 10 == 5) {
			$col = 4;
		}elseif ($i % 10 == 6) {
			$col = 6;
		}elseif ($i % 10 == 7) {
			$col = 6;
		}elseif ($i % 10 == 8) {
			$col = 4;
		}elseif ($i % 10 == 9) {
			$col = 4;
		}elseif ($i % 10 == 10) {
			$col = 4;
		}


		?>


		<div class="col-6 col-md-6 col-lg-<?php echo $col ?>" data-aos="fade-up">
			<a href="<?php echo base_url('asset/'.$value['gambar']); ?>" class="d-block photo-item" data-fancybox="gallery">
				<img src="<?php echo base_url('asset/'.$value['gambar']); ?>" alt="Image" class="img-fluid">
				<div class="photo-text-more">
					<span class="icon icon-search"></span>
				</div>
			</a>
		</div>

	<?php } ?>

</div>