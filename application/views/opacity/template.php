
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="description" content="<?php echo getSettingDesa()['desa_deskripsi']; ?>">
	<meta name="author" content="<?php echo getSettingDesa()['desa_nama']; ?>">
	<meta name="keyword" content="<?php echo getSettingDesa()['desa_metakeyword']; ?>">
	
	<!-- Shareable -->
	<meta property="og:title" content="<?php include "phpmu-title.php"; ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="<?php include "phpmu-ogimage.php"; ?>" />
	<title><?php include "phpmu-title.php"; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/font/font-style.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/magnific-popup.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/jquery-ui.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/owl.carousel.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/owl.theme.default.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/bootstrap-datepicker.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/font/flaticon.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/aos.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/fancybox.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/css/style.css'); ?> ">
	<link rel="stylesheet" href="<?php echo base_url('themefront/opacity/assets/font/icomon/demo.css'); ?>">

</head>
<body>
	<div class="site-wrap">
		<div class="site-mobile-menu">
			<div class="site-mobile-menu-header">
				<div class="site-mobile-menu-close mt-3">
					<span class="icon icon-cross js-menu-toggle"></span>
				</div>
			</div>
			<div class="site-mobile-menu-body"></div>
		</div>
		<header class="header-bar d-flex d-lg-block align-items-center">
			<div class="site-logo">
				<a href="<?php echo base_url('/') ?>">Shutter</a>
			</div>
			<div class="d-inline-block d-xl-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon icon-menu h3"></span></a></div>
			<div class="main-menu">
				<ul class="js-clone-nav">


					<?php



					$fe_menu  =  $this->M_homepage->get_front_menu_tree();
					$arrayMenu  =  $this->M_homepage->getArrayDesaMenu();


					foreach ($fe_menu as $key => $fe) {

						if (!in_array($fe['id_menu'], $arrayMenu)) {
							unset($fe_menu[$key]);
						}

						if(!empty($fe['submenu'])){

							foreach ($fe['submenu'] as $keyz => $fe_sub) {

								if (!in_array($fe_sub['id_submenu'], $arrayMenu)) {

									unset($fe_menu[$key]['submenu'][$fe_sub['id_submenu']]);
								}


							}
						}	

								//	unset($fe_menu[4]['submenu'][7]);
					}

					foreach ($fe_menu as $key => $fe) {



						if(empty($fe['submenu'])){
							echo '<li><a href='.base_url($fe['link_menu']).'>'.$fe['menu'].'</a></li>';

						}else{

							echo '
							<li class="dropdown magz-dropdown">
							<a href="#">'.$fe['menu'].'<i class="ion-ios-arrow-right"></i></a>
							<ul class="dropdown-menu">';

							foreach ($fe['submenu']  as $keyz => $fe_submenu) {


								echo '<li><a href='.base_url($fe_submenu['link_submenu']).'>'.$fe_submenu['submenu'].'</a></li>';

							}
							echo 
							'</ul>
							</li>';
						}


					}?>

				</ul>
				<ul class="social js-clone-nav">
					
					<li><a href="<?= getSettingDesa()['desa_ig'] ?>"><span class="icon icon-instagram"></span></a></li>
				</ul>
			</div>
		</header>
		<main class="main-content">
			<div class="container-fluid photos">
				<div class="row pt-4 mb-5 text-center">
					<div class="col-12">
						<img width="30%" class="img-responsive" src="<?php echo getSettingDesa()['logo_desa']; ?>" alt="">
					</div>
				</div>
				
				<?php

				echo $content;

				?>

				<div class="row justify-content-center">
					<div class="col-md-12 text-center py-5">
						<p>

							Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by <a href="https://opacityarts.com/" target="_blank">Opacityarts</a>

						</p>
					</div>
				</div>
			</div>
		</main>
	</div> 

	<script src="<?php echo base_url('themefront/opacity/assets/js/jquery-3.3.1.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/jquery-migrate-3.0.1.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/jquery-ui.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/popper.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/owl.carousel.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/jquery.stellar.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/jquery.countdown.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/jquery.magnific-popup.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/bootstrap-datepicker.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/aos.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/jquery.fancybox.min.js')?>"></script>
	<script src="<?php echo base_url('themefront/opacity/assets/js/main.js')?>"></script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script>
</body>
</html>