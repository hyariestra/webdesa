<?php

class M_globalfunc extends CI_model

{

    var $table = 'kategori'; //nama tabel dari database
    var $column_order = array(1 => 'a.nama_kategori', 6 => 'a.tanggal', 7 => 'a.dibaca'); //field yang ada di table user
    var $column_search = array('nama_kategori'); //field yang diizin untuk pencarian 
    var $order = array('a.nama_kategori' => 'desc'); // default order 


    function get_provinsi()
    {

        $this->db->select('*');
        $this->db->from('blw_prov a');
        $query = $this->db->get()->result_array();
        return $query;
    }


    function get_kabkot_data($id)
    {

        $this->db->select('id,nama');
        $this->db->from('blw_kab a');
        $this->db->where('idprov', $id);
        $query = $this->db->get()->result_array();
        return $query;
    }

    function get_kecamatan($id)
    {

        $this->db->select('id,nama');
        $this->db->from('blw_kec a');
        $this->db->where('idkab', $id);
        $query = $this->db->get()->result_array();
        return $query;
    }

    function get_alamat_user($id)
    {

        $this->db->select('id_alamat,nama_alamat,nama_penerima');
        $this->db->from('alamat a');
        $this->db->where('id_user', $id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_desa_data($id)
    {
        $this->db->select('desa_id,desa_nama');
        $this->db->from('ref_desa_data a');
        $this->db->where('kecamatan_id', $id);
        $query = $this->db->get()->result_array();
        return $query;
    }


    function trimelement($data, $panjang)
    {

        $max_length = $panjang;

        if (strlen($data) > $max_length) {
            $offset = ($max_length - 3) - strlen($data);
            $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
        }

        return $data;
    }


    public function get_kurir()
    {
        $this->db->select('*');
        $this->db->from('blw_kurir a');
        $this->db->where('a.is_active', 1);
        $query = $this->db->get();


        return $query->result_array();

    }

    public function get_alamat_by_id($id)
    {
       $data =  $this->db->query("SELECT 
        a.id_alamat,
        a.nama_penerima,
        a.id_prov,
        a.id_kota,
        a.id_kecamatan, 
        a.alamat_lengkap,
        a.no_hp,
        CONCAT(b.nama,', ',c.nama,', ',d.`nama`) AS alamat,
        c.`kodepos`,
        b.`rajaongkir` AS id_prov_rj,
        c.`rajaongkir` AS id_kota_rj,
        d.`rajaongkir` AS id_kec_rj
         FROM alamat a
        LEFT JOIN `blw_prov` b ON a.`id_prov` = b.`id`
        LEFT JOIN `blw_kab` c ON a.`id_kota` = c.`id`
        LEFT JOIN `blw_kec` d ON a.`id_kecamatan` = d.`id`
        WHERE a.id_alamat = ".$id." ")->row_array();


        return $data;

    }
}
