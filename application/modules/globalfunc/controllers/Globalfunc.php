<?php

class Globalfunc extends CI_controller

{

	function __construct()

	{

		parent::__construct();
		$this->apiKey = '743d289916d537dee06eaf6a50c8aa42';
	}


	function index()

	{



		$this->themeadmin->tampilkan('tampilKategori', null);
	}






	public function get_desa()
	{

		$id = $_GET['id'];

		$data['desa'] = $this->M_globalfunc->get_desa_data($id);

		echo json_encode($data);
		exit;
	}


	function api_ongkir_post($data)
	{
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/" . $data,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: $this->apiKey"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return  "cURL Error #:" . $err;
		} else {
			return $response;
		}
	}

	public function get_provinsi_api()
	{
		$response = $this->api_ongkir_post('province');

		$data = json_decode($response, true);
		$dataProvinsi = $data['rajaongkir'];

		echo json_encode($dataProvinsi);
		exit;
	}


	public function get_kabupaten()
	{

		$prov_id = $_GET['prov_id'];

		$response = $this->api_ongkir_post('city?province=' . $prov_id);

		$data = json_decode($response, true);
		$dataKabupaten = $data['rajaongkir'];

		echo json_encode($dataKabupaten);
		exit;
	}


	public function get_kecamatan_api()
	{

		$kabkot_id = $_GET['kabkot_id'];

		$response = $this->api_ongkir_post('subdistrict?city=' . $kabkot_id);

		$data = json_decode($response, true);
		$dataKecamatan = $data['rajaongkir'];

		echo json_encode($dataKecamatan);
		exit;
	}


	public function api_ongkir_calc($origin,$des,$berat,$cour)
	{
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=".$origin."&originType=city&destination=".$des."&destinationType=subdistrict&weight=".$berat."&courier=".$cour,
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: $this->apiKey"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return "cURL Error #:" . $err;
		} else {
			return $response;
		}
	}


	public function get_paket()
	{
		
		$origin = 501;
		$des = $_GET['dest_id'];
		$berat =  $_GET['berat'];
		$cour = $_GET['cour_id'];


		$tarif = $this->api_ongkir_calc($origin,$des,$berat,$cour);		
		
		
		$data = json_decode($tarif, true);
		
		if($data['rajaongkir']['status']['code'] == 400){
			$paket['paket'] = array();	
		}else{
			$paket['paket'] = $data['rajaongkir']['results'][0]['costs'];

		}

		
		echo json_encode($paket);
		exit;
	}


	public function set_alamat()
	{
		
		$alamat_id = $_GET['alamat_id'];

		$data['alamat'] = $this->M_globalfunc->get_alamat_by_id($alamat_id);		


		echo json_encode($data);
		exit;
	}


	//dari table, bukan raja ongkir

	public function get_provinsi()
	{


		$data['kecamatan'] = $this->M_globalfunc->get_provinsi();

		echo json_encode($data);
		exit;
	}

	public function get_kabkot()
	{

		$prov_id = $_GET['prov_id'];

		$data['kabkot'] = $this->M_globalfunc->get_kabkot_data($prov_id);

		echo json_encode($data);
		exit;
	}

	
	public function get_kecamatan()
	{

		$kabkot_id = $_GET['kabkot_id'];

		$data['kecamatan'] = $this->M_globalfunc->get_kecamatan($kabkot_id);
	
		echo json_encode($data);
		exit;
	}


	public function get_alamat_user($id)
	{

		$data['alamat'] = $this->M_globalfunc->get_alamat_user($id);
	
		echo json_encode($data);
		exit;
	}


}
