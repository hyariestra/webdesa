<style type="text/css">
	.container1 input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}
	.delete{
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				
				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>


						<div class="form-group">

							<label>Nama Kategori</label>

							<input value="<?php echo @$berita['judul'] ?>" type="text" name="nama_kategori" class="judul_berita form-control">

						</div>

						

						<div class="form-group">

							<label>Status</label>
							<select class="form-control" name="status" id="">
								<option  <?php echo (@$berita['active'] == 1) ? "selected": "" ?> value=1>Aktif</option>
								<option  <?php echo (@$berita['active'] == 0) ? "selected": "" ?> value=0>Tidak Aktif</option>
							</select>

						</div>

					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('kategoribe') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>		

	</div>

</div>




<script>


$('.form-kirim').ajaxForm({ 
	dataType:  'json', 
	beforeSubmit: function(formData, jqForm, options){



	},
	success:   processJson,
	error: processJsonError
});


function processJsonError(result) {
	result = result.responseJSON;
	processJson(result, true);
}

function processJson(result) { 

	new Noty({
		text: result.message,
		type: result.status_code,
		timeout: 3000,
		theme: 'semanticui'
	}).show();

	if(result.status == 201){
		window.location = '<?php echo base_url('kategoribe') ?>';

	}
}

// $( ".nama_produk_class" ).keyup(function() {

// 	var nama = this.value;

// 	var str = this.value;
// 	str = str.replace(/\s+/g, '-').toLowerCase();


// 	$('.slug_class').val(str);


// });


</script>

