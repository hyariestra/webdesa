<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



/**



 * 



 */



class M_account extends CI_Model



{

	function signup($param)
	{
		$this->db->trans_begin();

		$result = $this->db->insert('users', $param);


		if ($result) {

			if ($param['subs'] == 1) {

				$result = $this->M_berita->simpan_email($param['email']);
			}
		}


		if ($result == false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		return [
			'result' => $result
		];
	}

	function checkEmailExist($email)
	{

		$this->db->select('email');
		$this->db->from('users a');
		$data = $this->db->where('a.email', $email);


		$ambil = $data->get();

		$get = $ambil->row_array();

		return $get;
	}


	function simpan_log($param)
	{

		$this->db->trans_begin();

		$result = $this->db->insert('login_log', $param);

		if ($result == false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		return [
			'result' => $result
		];
	}


	function getDetailProfile(){
		
		$this->db->select('*');
		$this->db->from('users a');
		$this->db->where('id_user', decrypt_url(getSessionUser()['user_id'])  );
		$data = $this->db->get()->row_array();


		$data['id_user'] = encrypt_url($data['id_user']);
		$data['checkSubs'] = $data['subs'] == 1 ? 'checked' : '';

		return $data;
	
	
		}

	function login_user($param)
	{


		$this->db->select('*,a.id_user AS user_id');
		$this->db->from('users a');
		$this->db->join('series_user b', 'b.id_user = a.id_user', 'left');
		$this->db->where('a.email', $param['email']);
		$this->db->where('a.password', $param['password']);
		$data = $this->db->get();

		
		
		
		$hitung = $data->num_rows();
		
		if ($hitung > 0) {
			
			$ambil = $data->row_array();
			
		
			$this->db->select('id_series_master');
			$this->db->from('series_user a');
			$this->db->where('id_user', $ambil['id_user']);
			$courseData = $this->db->get()->result_array();
			

			$newCourseId = array();

			foreach ($courseData as $key => $item) {
				$newCourseId[$key] = $item['id_series_master'];
			}
			
		
			$newArray = array();
			
			$newArray['user_id'] = encrypt_url($ambil['user_id']);
			$newArray['name'] = $ambil['name'];
			$newArray['email'] = $ambil['email'];
			$newArray['avatar'] =  !empty($ambil['avatar']) ? 'foto_user/' . rawurlencode($ambil['avatar']) : 'img/user.png';
			$newArray['course_id'] = $newCourseId;
			
			// foreach ($ambil as $item) {
			// 	$newArray[$item['email']]['user_id'] = encrypt_url($item['user_id']);
			// 	$newArray[$item['email']]['name'] = $item['name'];
			// 	$newArray[$item['email']]['email'] = $item['email'];
			// 	$newArray[$item['email']]['avatar'] = !empty($item['avatar']) ? 'foto_user/'.rawurlencode($item['avatar']) : 'img/user.png';
			// 	$newArray[$item['email']]['series'][$item['id_series_master']]['id_series_master'] = $item['id_series_master'];

			// }


			$this->session->set_userdata("users", $newArray);

			return true;
		} else {

			return false;
		}
	}


	public function dbSeries(){


		$data = "SELECT *,
		a.`id_series` AS series_id,
		IFNULL(
		(SELECT COUNT(b1.id_series_detail) AS s
		FROM `series_folder` a1 
		LEFT JOIN `series_detail` b1 ON a1.id_series_folder = b1.id_series_folder
		WHERE a1.id_series = a.id_series
		GROUP BY a1.id_series),0) AS total_course,
		
		IFNULL(
		(SELECT COUNT(a2.`id_series_step`) AS step 
		FROM `series_step` a2
		LEFT JOIN `series_detail` b2 ON a2.id_series_detail = b2.id_series_detail
		LEFT JOIN `series_folder` c2 ON c2.id_series_folder  = b2.id_series_folder
		WHERE c2.id_series = a.id_series
		GROUP BY c2.id_series),0) AS step,

		(SELECT b1.id_series_detail AS startstep
		FROM `series_folder` a1 
		INNER JOIN `series_detail` b1 ON a1.id_series_folder = b1.id_series_folder
		WHERE a1.id_series = a.id_series
		ORDER BY b1.order ASC LIMIT 1 ) AS startstep,

		b.`insert_time` AS claim_date
		FROM `series` a
		LEFT JOIN `series_user` b ON b.`id_series_master` = a.`id_series`
		LEFT JOIN `users` c ON c.`id_user` = b.`id_user`
   		WHERE b.id_user = " . decrypt_url(getSessionUser()['user_id']) . " 
		-- limit -- ";

		return $data;

	}

	public function jumlah_data()
	{
		$query = $this->dbSeries();

		$data = $this->db->query($query)->num_rows();

		return $data; 

	}

	public function getSeriesByAccount($perpage, $pagenum)
	{

		
		if ($pagenum > 1) {
			$limit = $perpage;
			$start = $perpage * ($pagenum - 1);
			$start  = ','.$start; 
		  } else {
			$limit = $perpage;
			$start = '';
		  }


		  $limit = "LIMIT $limit $start";   

		$query = $this->dbSeries();
        $query = str_replace('-- limit --', $limit, $query);
       
		$data = $this->db->query($query)->result_array();

		foreach ($data as $key => $item) {

			if ($item['total_course'] == 0) {
				$percent = 0;
			} else {
				$percent = ($item['step'] / $item['total_course']) * 100;
			}

			$data[$key]['image'] = !empty($item['gambar_uniq']) ? 'foto_series/' . $item['gambar_uniq'] : 'img/imgnotfound.png';
			$data[$key]['percent'] = $percent;
			$data[$key]['url'] = base_url('account/course/' . encrypt_url($item['series_id']) . '/' . $item['series_seo'] . '/' . $item['startstep']);
		}


		return $data;
	}

	function login_pengguna($inputan)

	{

		$em = $inputan['email'];



		$pass = md5($inputan['password']);


		// $this->db->where("email",$em);
		// $this->db->where("password",$pass);
		// $this->db->where("status",1);
		//$cek = $this->db->get("admin");


		$cek = $this->db->query("SELECT *, GROUP_CONCAT(`id_menu` SEPARATOR ',') AS id_menu FROM admin a
			LEFT JOIN `roles` b ON a.role = b.id
			LEFT JOIN `ref_menu_roles` c ON c.id_roles = b.id

			LEFT JOIN `ref_desa_users` d ON d.`id_admin` = a.`id_admin`
			LEFT JOIN `ref_desa` e ON e.`id_desa` = d.`id_desa`

			WHERE a.`email` = " . $this->db->escape($em) . "
			AND a.`password` = '" . $pass . "'
			AND a.`status` = 1

			AND e.`desa_kode_domain` = '" . getNameDomain() . "'

			GROUP BY a.id_admin");




		$hitung = $cek->num_rows();

		if ($hitung > 0) {



			$ambil = $cek->row_array();

			$this->session->set_userdata("pengguna", $ambil);

			return "sukses";
		} else {



			return "gagal";
		}
	}
}
