<?php



/**

 * 

 */

class Account extends CI_controller

{



	function __construct()


	{

		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];

	}


	function load_captcha()
	{
		$vals = [
			'word'          => strtoupper(substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 4)),
			'img_path'      => './asset/images/captcha/',
			'img_url'       => base_url('asset/images/captcha/'),
			'img_width'     => '210',
			'img_height'    => '70',
			'expiration'    => 7200,
			'word_length'   => 8,
			'font_size'     => '30',
			'img_id'        => 'Imageid',
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

			'colors'        => [
				'background' => [255, 255, 255],
				'border'    => [255, 255, 255],
				'text'      => [0, 0, 0],
				'grid'      => [255, 40, 40]
			]
		];

		$captcha = create_captcha($vals);


		$this->session->set_userdata('captcha', $captcha['word']);

		return $captcha['image'];
	}


	function signin()
	{



		$data['captcha'] = $this->load_captcha();
		$data['detail']['judul_halaman'] = 'Sign In';
		$data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none';
		$data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : '';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['htmlSign'] = $this->load->view($this->theme . "/signin", $data, true);

		$data['content'] = $this->load->view($this->theme . "/signinup", $data, true);


		echo $this->load->view($this->theme . "/template", $data);
	}

	function signup()
	{

		$data['captcha'] = $this->load_captcha();
		$data['detail']['judul_halaman'] = 'Sign Up';
		$data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none';
		$data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : '';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['htmlSign'] = $this->load->view($this->theme . "/signup", $data, true);

		$data['content'] = $this->load->view($this->theme . "/signinup", $data, true);


		echo $this->load->view($this->theme . "/template", $data);
	}


	function dashboard()
	{
		if (!$this->session->userdata("users"))
		
		{
			redirect("account/signin");

		}


		$data['detail']['judul_halaman'] = 'Sign Up';
		$data['sliders'] = array();
		$data['display'] = 'none';
		$data['displayMini'] = 'none';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
	
		$data['sidebar'] = $this->load->view($this->theme."/sidebar", $data, true);
		$data['title'] = 'DASHBOARD';
		$data['icon'] = 'fa fa-tachometer';
		$data['html'] = $this->load->view($this->theme . "/accpage/mydashboard", $data, true);
		$data['content'] = $this->load->view($this->theme."/accountpage", $data, true);
		echo $this->load->view($this->theme."/template", $data);
	}


	function course($id,$seo,$id_detail){


		if (!$this->session->userdata("users"))
		
		{
			redirect("account/signin");

		}


		if (!in_array( decrypt_url($id), getUserSeries() )) {
			redirect('404_override');
		}




		$data = array();

		$data['detail'] = $this->M_series->get_detail_series($id,$seo);

		
		$data['konten'] = $this->M_series->get_detail_konten($id,$id_detail);

		$data['finishStep'] = $this->M_series->get_detail_finish_steps();
		$data['id_detail'] = $id_detail;

	
	
		$data['content'] = $this->load->view("admin-course/main/content", $data, true);
		echo $this->load->view("admin-course/templatecourse", $data);

	}

	function mycourse()
	{
		if (!$this->session->userdata("users"))
		
		{
			redirect("account/signin");

		}

		$cari['key']  = array();
		$cari['tag']  = array();
		$total_rows  = $this->M_account->jumlah_data();

		$config = array();
		if ($cari['key'] or $cari['tag']) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config["base_url"] = site_url() . 'account/mycourse';
		$config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 10;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = 4;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		if ($this->uri->segment(3)) {
			$page = ($this->uri->segment(3));
		} else {
			$page = 1;
		}

	

		$data['detail']['judul_halaman'] = 'Sign Up';
		$data['series'] = $this->M_account->getSeriesByAccount( $config["per_page"], $page);
	
		$data['sliders'] = array();
		$data['display'] = 'none';
		$data['displayMini'] = 'none';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['sidebar'] = $this->load->view($this->theme."/sidebar", $data, true);
		$data['title'] = 'MY COURSE';
		$data['icon'] = 'fa fa-book';
		$data['html'] = $this->load->view($this->theme . "/accpage/mycourse", $data, true);
		$data['content'] = $this->load->view($this->theme."/accountpage", $data, true);
		echo $this->load->view($this->theme."/template", $data);
	}


	function update_profile(){


		if (!$this->session->userdata("users"))
		
		{
			redirect("account/signin");

		}


		$post = $this->input->post();

		$$result= $this->M_account->validasiPassword($post['old_pass'],$post['new_pass'],$post['re_pass']);


		echo '<pre>'; 
		print_r($post);
		echo '<pre>'; 

	}

	function profile()
	{
		if (!$this->session->userdata("users"))
		
		{
			redirect("account/signin");

		}

		$data['profile'] = $this->M_account->getDetailProfile();
	

		$data['sliders'] = array();
		$data['display'] = 'none';
		$data['displayMini'] = 'none';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['title'] = 'MY PROFILE';
		$data['icon'] = 'fa fa-user';
		$data['html'] = $this->load->view($this->theme . "/accpage/myprofile", $data, true);
		$data['content'] = $this->load->view($this->theme."/accountpage", $data, true);
		echo $this->load->view($this->theme."/template", $data);
	}



	function signinfunc(){

		$result = true;

		$post = $this->input->post();

		$post['capt'] = $this->session->userdata('captcha');

		if($post['email'] == ""){
			$err_msg[] = 'Email kosong';
			$result = false;
		}

		if ($post['password'] == "" ) {
			$err_msg[] = 'Password kosong';
			$result = false;
		}

		if ($post['captcha'] == "" ) {
			$err_msg[] = 'Captcha kosong';
			$result = false;
		}


		if ($post['capt']  != strtoupper($post['captcha'])) {
			$err_msg[] = 'Captcha tidak sesuai';
			$result = false;
		}

		if($result){

			$param = array(
				'email' => $post['email'],
				'password' => md5($post['password']),
			);

			$result = $this->M_account->login_user($param);

			if($result == false){
				$err_msg[] = 'Wrong email or password.';
			}


		}

		if ($result) {

			echo getResponse(201, 'Berhasil Login');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}

	}

	function signupfunc()
	{

		$result = true;

		$post = $this->input->post();


		$post['capt'] = $this->session->userdata('captcha');

		$check = $this->M_account->checkEmailExist($post['email']);

		if($check['email']){
			$err_msg[] = 'Email sudah terdaftar, gunakan email lain';
			$result = false;
		}

		if ($post['email'] == "" ) {
			$err_msg[] = 'Email kosong';
			$result = false;
		}

		
		if ($post['name'] == "" ) {
			$err_msg[] = 'Nama kosong';
			$result = false;
		}

		
		if ($post['password'] == "" ) {
			$err_msg[] = 'Password kosong';
			$result = false;
		}

		
		if ($post['confpassword'] == "" ) {
			$err_msg[] = 'Konfirmasi passsword kosong';
			$result = false;
		}
	
		if ($post['phone'] == "" ) {
			$err_msg[] = 'Telephone kosong';
			$result = false;
		}
		
		if ($post['captcha'] == "" ) {
			$err_msg[] = 'Captcha kosong';
			$result = false;
		}
	
		if ($post['password'] != $post['confpassword']) {
			$err_msg[] = 'Password tidak sesuai';
			$result = false;
		}

		if ($post['capt']  != strtoupper($post['captcha'])) {
			$err_msg[] = 'Captcha tidak sesuai';
			$result = false;
		}

		if($result){

			if(empty($post['subs'])){
				$post['subs'] = 0;
			}

			$param = array(
				'email' => $post['email'],
				'name' => $post['name'],
				'password' => md5($post['password']),
				'phone' => $post['phone'],
				'insert_time' => date("Y-m-d H:i:s"),
				'avatar' => null,
				'subs' => $post['subs'],
			);

			$result = $this->M_account->signup($param);
		}


		if ($result) {

			echo getResponse(201, 'Berhasil Mendaftar');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}

	function save_steps(){

		$param = array(
			'id_series_detail' => $_POST['id_detail'],
			'id_user' => decrypt_url(getSessionUser()['user_id']),
			'insert_time' => dateNow(),
		);


		$result = $this->M_series->save_steps($param);



	}

	function logout()

	{

		$this->session->unset_userdata("users");

		redirect("account/signin");
	}
}
