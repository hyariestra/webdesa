<?php 

class M_sliderbe extends CI_model

{

    var $table = 'slider'; //nama tabel dari database
    var $column_order = array(1 => 'a.judul_slider',5 => 'a.tanggal'); //field yang ada di table user
    var $column_search = array('judul_slider'); //field yang diizin untuk pencarian 
    var $order = array('a.tanggal' => 'desc'); // default order 


    function trimelement($data,$panjang)
    {

       $max_length = $panjang;

       if (strlen($data) > $max_length)
       {
        $offset = ($max_length - 3) - strlen($data);
        $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
    }

    return $data;
}


function get_detail_slider($id='')
{
 $data =  $this->db->query("SELECT * FROM `slider` WHERE id_slider =  ".$id." ")->row_array();

 return $data;
}




function simpan_slider($param)
{

 $this->db->trans_begin();




 $result = $this->db->insert('slider',$param);



 if ($result == false){
  $this->db->trans_rollback();

}else{
  $this->db->trans_commit();

}

return [
  'result' => $result
];
}   

function delete_slider($id)
{
    return $this->db->query("DELETE FROM slider where id_slider = ".$id." ");
}

function ubah_slider($param,$id)
{

    $this->db->trans_begin();




    $this->db->where('id_slider',$id);
    $result = $this->db->update('slider',$param);



    if ($result == false){
        $this->db->trans_rollback();

    }else{
        $this->db->trans_commit();

    }

    return [
        'result' => $result
    ];
}




public function raw_db_query()
{
    $this->db->select('*,a.status AS `status_slider`,a.tanggal AS `tanggal_slider` ');
    $this->db->from('slider a');
    $this->db->join('admin b', 'b.id_admin = a.id_user', 'left');
    $this->db->join('ref_desa_users d', 'b.id_admin = d.id_admin', 'left');
    $this->db->join('ref_desa e', 'e.id_desa = d.id_desa', 'left');
    $q =  $this->db->where('e.desa_kode_domain', getNameDomain() );


    return $q;


}


private function _get_datatables_query()
{

    $this->raw_db_query();


 $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if($i===0) // looping awal
                {
                	$this->db->group_start(); 
                	$this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                	$this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                	$this->db->group_end(); 
            }
            $i++;
        }

        

        if(isset($_POST['order'])) 
        {
        	$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
        	$this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_slider()
    {
    	$this->_get_datatables_query();
    	if($_POST['length'] != -1)
    		$this->db->limit($_POST['length'], $_POST['start']);
    	$query = $this->db->get();
    	return $query->result();
    }

    function count_filtered()
    {
    	$this->_get_datatables_query();
    	$query = $this->db->get();
    	return $query->num_rows();
    }

    public function count_all()
    {
    	$this->raw_db_query();
        $r = $this->db->count_all_results();

        return $r;
    }




}


?>