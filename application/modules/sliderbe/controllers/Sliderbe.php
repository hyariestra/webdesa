<?php 

class Sliderbe extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))

		{

			redirect("pengguna/login");

		}

	}
	

	function index()

	{



		$this->themeadmin->tampilkan('tampilSlider',null);


	}

	function get_data_slider()
	{
		$list = $this->M_sliderbe->get_datatables_slider();
	

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->status_slider == 'publish') {
				$status = "<span class='label label-danger'>Publish</span>";
			}else{
				$status = "<span class='label label-default'>Draft</span>";
			}

			$idp = encrypt_url($field->id_slider);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->judul_slider;
			$row[] = $field->url_slider;
			$row[] = $status;
			$row[] = $field->nama;
			$row[] = tgl_indo_timestamp($field->tanggal_slider);
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('sliderbe/ubah/'.encrypt_url($field->id_slider))." >
			<span class='glyphicon glyphicon-edit'></span></a>
			<a onclick=delete_func('$idp')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_sliderbe->count_all(),
			"recordsFiltered" => $this->M_sliderbe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['url'] = 'sliderbe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		
		$this->themeadmin->tampilkan('tambahSlider',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['url'] = 'sliderbe/edit/'.$id;
		$data['slider'] =$this->M_sliderbe->get_detail_slider($idEn);
		
		
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahSlider',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

		if ($post['judul_slider'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}


		if (empty($_FILES['gambar']['name'])) {

			$err_msg[] = 'Gambar Kosong';
			$result = false;

		}




		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_slider/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2097152 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {

			$param = array(
				'judul_slider'=>$post['judul_slider'],
				'text_slider'=>$post['text_slider'],
				'url_slider'=>$post['url_slider'],
				'gambar_uniq' => isset($namaFile) ? $namaFile : '',
				'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'tanggal'=> date("Y-m-d H:i:s"),
				'status'=>$post['status']	

			);

			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_sliderbe->simpan_slider($param);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['judul_slider'] == "") {

			$err_msg[] = 'Judul Slider';
			$result = false;

		}





		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_slider/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2097152 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {


			if (!isset($_FILES['gambar'])) {
				
				$param = array(
					'judul_slider'=>$post['judul_slider'],
					'text_slider'=>$post['text_slider'],
					'url_slider'=>$post['url_slider'],
					'id_user'=>getSession(),
					'id_desa'=>getSettingDesa()['id_desa'],
					'tanggal'=> date("Y-m-d H:i:s"),
					'status'=>$post['status']	

				);

			}else{

				$param = array(
					'judul_slider'=>$post['judul_slider'],
					'text_slider'=>$post['text_slider'],
					'url_slider'=>$post['url_slider'],
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'id_user'=>getSession(),
					'id_desa'=>getSettingDesa()['id_desa'],
					'tanggal'=> date("Y-m-d H:i:s"),
					'status'=>$post['status']	

				);

			}
			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_sliderbe->ubah_slider($param,$id);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_sliderbe->get_detail_slider($idEn);

		$dirUpload = "asset/foto_slider/";

		$files = $dirUpload.$data['gambar_uniq'];

		
		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}



		$result = $this->M_sliderbe->delete_slider($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>