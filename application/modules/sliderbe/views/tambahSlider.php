<style type="text/css">
	.container1 input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}
	.delete{
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				
				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>


						<div class="form-group">

							<label>Judul</label>

							<input value="<?php echo @$slider['judul_slider'] ?>" type="text" name="judul_slider" class="judul_slider form-control">

						</div>

						<div class="form-group">

							<label>Text</label>

							<input value="<?php echo @$slider['text_slider'] ?>" type="text" name="text_slider" class="text_slider form-control">

						</div>

						<div class="form-group">

							<label>Url</label>

							<input value="<?php echo @$slider['url_slider'] ?>" type="text" name="url_slider" class="url_slider form-control">

						</div>

						

						
						<div class="form-group">

							<label>Gambar Slider</label>

						</div>


						<div class="form-group">
							<table class="table table-bordered table-hover" style="width: 100%">

								<tbody>



									<div class="input-group">

										<input  accept="image/*" class="form-control" readonly="" type="file" name="gambar">
									</div>

									<img style="width: 300px" src="<?php echo base_url('asset/foto_slider/'.@$slider['gambar_uniq']) ?>" alt="">
									<p><i>*file yang diizinkan bertipe .png, .jpeg, .jpg dan maksimal 2Mb </i></p>
									
								</tbody>

							</table>
						</div>

						<div class="form-group">

							<label>Status</label>
							<select class="form-control" name="status" id="">
								<option  <?php echo (@$slider['status'] == 'publish') ? "selected": "" ?> value="publish">Publish</option>
								<option  <?php echo (@$slider['status'] == 'draft') ? "selected": "" ?> value="draft">Draft</option>
							</select>

						</div>

					</div>

					<div class="box-footer">

					<button style="display: none;" class="btn btn-primary buttonload" disabled>
					<i class="fa fa-spinner fa-spin"></i> Loading
					</button>

						<button id="mybutton" type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('sliderbe') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>		

	</div>

</div>




	<script>

		$('.form-kirim').ajaxForm({ 


		
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){
				$("#mybutton").hide();
				$(".buttonload").show();
			
			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			$("#mybutton").show();
				$(".buttonload").hide();

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('sliderbe') ?>';

			}
		}


	</script>

