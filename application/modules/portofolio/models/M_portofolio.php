<?php 

class M_portofolio extends CI_model

{

	var $table = 'berita'; //nama tabel dari database
    var $column_order = array(null, 'a.tanggal'); //field yang ada di table user
    var $column_search = array('judul','nama_kategori'); //field yang diizin untuk pencarian 
    var $order = array('a.tanggal' => 'desc'); // default order 


    function trimelement($data,$panjang)
    {

     $max_length = $panjang;

     if (strlen($data) > $max_length)
     {
      $offset = ($max_length - 3) - strlen($data);
      $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
    }

    return $data;
  }





public function get_detail_porto($id,$seo)
{
 $result = $this->db->query("SELECT * FROM `portofolio` a 
 LEFT JOIN `ref_porto` b ON a.id_ref_porto = b.`id_ref_porto`
 WHERE a.`id_porto` = '".$id."' AND a.`judul_porto_seo` = '".$seo."' ")->row_array();

  $data['judul'] = $result['judul_porto'];
  $data['isi_porto'] = $result['isi_porto'];
  $data['project_url'] = $result['project_url'];
  $data['image'][1] = $result['gambar_1'];
  $data['image'][2] = $result['gambar_2'];
  $data['image'][3] = $result['gambar_3'];

  if ($result['gambar_1'] == '') {
    unset($data['image'][1]);
  }
  if ($result['gambar_2'] == '') {
    unset($data['image'][2]);
  }
  if ($result['gambar_3'] == '') {
    unset($data['image'][3]);
  }

  if (!isset($data['image'])) {
    $data['image'][1] = 'img/imgnotfound.png';
  }

  $data['project_start'] = tgl_indo($result['project_start']);
  $data['project_end'] = $result['project_end'];
  $data['nama_porto'] = $result['nama_porto'];
  $data['client'] = $result['client'];


return $data;

}


public function getDate($date)
{


  $createDate = new DateTime($date);

  $strip = $createDate->format('Y-m-d');
  return $strip; 


}




}


?>