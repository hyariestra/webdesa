<?php 

class Portofolio extends CI_controller

{

	function __construct()

	{

		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];

	}


    function detail($id,$seo)

    {

    	$data['detail'] = $this->M_portofolio->get_detail_porto($id,$seo);
    
		// echo '<pre>'; 
		// print_r($data);
		// echo '<pre>'; 
		// die();

    	$link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    	$data['link'] = str_replace(' ', '', $link);


    
    	$data['content'] = $this->load->view($this->theme."/porto", $data, true);


    	echo $this->load->view($this->theme."/template", $data);


    }

    

    public function hitlike()
    {

    	$post = $this->input->post();


    	$result = $this->M_berita->update_like_berita($post['id']);

    	echo $result;

    }    

    public function hitshare()
    {

    	$post = $this->input->post();


    	$result = $this->M_berita->update_share_berita($post['id']);

    	echo $result;

    }

    public function hitdislike()
    {

    	$post = $this->input->post();


    	$result = $this->M_berita->update_dislike_berita($post['id']);



    	echo $result;



    }

    public function news_save()
    {
    
    	$post = $this->input->post();

    	$result = $this->M_berita->simpan_email($post['email']);

    	echo $result;
    }

	public function addKomen()
	{
		$ids = trim($_GET['ids']);
		$seo = trim($_GET['seo']);

		$params = array(
			'nama'=>$_GET['name'],
			'email'=>$_GET['mail'],
			'telp'=>$_GET['phone'],
			'isi'=>$_GET['comment'],
			'id_berita'=>$ids,
			'id_parent'=>NULL,
			'id_users'=>NULL,
			'insert_time'=> date("Y-m-d H:i:s")
		
		);
	
		$result = $this->M_berita->simpan_komen($params);

		$isipesan="<div class='alert alert-success' role='alert'><a href='#' class='alert-link'>Komentar sukses ditambahkan</a></div>";

		$this->session->set_flashdata("pesan",$isipesan);

		redirect("berita/detail/$ids/$seo");

	}





}

?>