<?php



/**



 * 



 */



class M_carts extends CI_model



{

	function simpan_keranjang($param)
	{

		$this->db->trans_begin();

		$result = $this->db->insert('series_carts', $param);

		if ($result == false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		return [
			'result' => $result
		];
	}



	function simpan_keranjang_($param)
	{

		$this->db->trans_begin();

		echo '<pre>'; 
		print_r($post);
		echo '<pre>'; 


		$result = $this->db->insert('series_carts', $param);

		if ($result == false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		return [
			'result' => $result
		];
	}


	function hapus_keranjang($id)
	{

		$this->db->trans_begin();

		$this->db->where('id_user', decrypt_url(getSessionUser()['user_id']));
		$this->db->where('id_series', $id);
		$result = $this->db->delete('series_carts');


		if ($result == false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		return [
			'result' => $result
		];
	}


	function hapus_keranjang_user()
	{

		$this->db->trans_begin();

		$this->db->where('id_user', decrypt_url(getSessionUser()['user_id']));
		$result = $this->db->delete('series_carts');


		if ($result == false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		return [
			'result' => $result
		];
	}

	

	function tampil_keranjang()
	{

		$data  =  $this->db->query("SELECT 
		b.series_seo, 
		b.series, 
		series_price,  
		SUM(a.qty) AS qty, 
		a.id_user, a.id_series, 
		b.gambar_uniq
		FROM `series_carts`  a
		LEFT JOIN series b ON b.`id_series` = a.`id_series`
		WHERE a.id_user  = " . decrypt_url(getSessionUser()['user_id']) . "
		GROUP BY a.id_series")->result_array();

		foreach ($data as $key => $value) {


			$data[$key]['id'] = $value['id_series'];
			$data[$key]['nama_produk'] = $value['series'];
			$data[$key]['harga'] = $value['series_price'];
			$data[$key]['harga_rupiah'] = rupiah($value['series_price']);
			$data[$key]['qty'] = $value['qty'];
			$data[$key]['harga_total_rupiah'] = rupiah($value['qty'] * $value['series_price']);
			$data[$key]['harga_total'] = $value['qty'] * $value['series_price'];
			$data[$key]['url_series'] = 'series/detail/' . encrypt_url($value['id_series']) . '/' . $value['series_seo'];
			$data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_series/' . rawurlencode($value['gambar_uniq']) : 'img/user.png';
		}

		return $data;
	}
}
