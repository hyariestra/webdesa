<?php 

class Beritabe extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))



		{



			redirect("pengguna/login");



		}

	}
	

	function index()

	{

		authorize('tampilberita');

		$this->themeadmin->tampilkan('tampilBerita',null);


	}

	function get_data_user()
	{
		$list = $this->M_beritabe->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->headline == 'Y') {
				$headline = "<span class='label label-success'>Y</span>";
			}else{
				$headline = "<span class='label label-default'>N</span>";
			}

			if ($field->rilis == 'publish') {
				$status = "<span class='label label-success'>Publish</span>";
			}else{
				$status = "<span class='label label-default'>Draft</span>";
			}

			$r =array();

			if (!empty($field->tag)) {
				

				$tag = explode(',', $field->tag);


				foreach ($tag as $key => $value) {

					$r[$key] = $this->M_beritabe->getTagName($value);

				}
			}


			$idBerita = encrypt_url($field->id_berita);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->judul;
			$row[] = $headline;
			$row[] = $status;
			$row[] = $field->nama_kategori;
			$row[] = $field->nama;
			$row[] = tgl_indo_timestamp($field->tanggal);
			$row[] = $field->dibaca." kali";
			$row[] = array_unique($r);
			$html = "";

			if (authorizeView('ubahberita')) {
			$html .= "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('beritabe/ubah/'.encrypt_url($field->id_berita))." >
			<span class='glyphicon glyphicon-edit'></span></a>";
			}

			if (authorizeView('hapusberita')) {
			$html .="<a onclick=delete_func('$idBerita')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";
			}
			$row[] = $html;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_beritabe->count_all(),
			"recordsFiltered" => $this->M_beritabe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		authorize('tambahberita');

		$data['url'] = 'beritabe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$data['kategori'] =$this->M_menu->produk_tree();
		$data['tag'] =$this->M_tag->getDataTag();

		$this->themeadmin->tampilkan('tambahBerita',$data);
	}


	public function ubah($id='')
	{

		authorize('ubahberita');
		$idEn = decrypt_url($id);
		$data['url'] = 'beritabe/edit/'.$id;
		$data['berita'] =$this->M_beritabe->get_detail_berita($idEn);
		$data['tag'] =$this->M_tag->getDataTag();
		$data['kategori'] =$this->M_menu->produk_tree();
		
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahBerita',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

		if ($post['judul_berita'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}

		if ($post['id_kategori'] == "") {

			$err_msg[] = 'Kategori Kosong';
			$result = false;

		}



		if (isset($post['tag'])) {
			$tag = implode(',', $post['tag']);

		}


		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '.' . $file_extension;
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_berita/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2097152 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {

			$param = array(
				'judul'=>$post['judul_berita'],
				'id_desa'=>getSettingDesa()['id_desa'],
				'id_kategori'=>$post['id_kategori'],
				'judul_seo'=>slugify($post['judul_berita']),
				'headline'=>$post['is_headline'],
				'isi_berita'=>$post['berita'],	
				'gambar_uniq' => isset($namaFile) ? $namaFile : '',
				'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'id_user'=>getSession(),
				'tanggal'=> date("Y-m-d H:i:s"),
				'tag' => isset($tag) ? $tag : '',
				'status' => $post['status'],
			);

			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_beritabe->simpan_berita($param);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['judul_berita'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}

		if ($post['id_kategori'] == "") {

			$err_msg[] = 'Kategori Kosong';
			$result = false;

		}



		if (isset($post['tag'])) {
			$tag = implode(',', $post['tag']);

		}


		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '.' . $file_extension;
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_berita/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2097152 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {


			if (!isset($_FILES['gambar'])) {
				
				$param = array(
					'judul'=>$post['judul_berita'],
					'id_desa'=>getSettingDesa()['id_desa'],
					'id_kategori'=>$post['id_kategori'],
					'judul_seo'=>slugify($post['judul_berita']),
					'headline'=>$post['is_headline'],
					'isi_berita'=>$post['berita'],	
					'id_user'=>getSession(),
					'tanggal_update'=> date("Y-m-d H:i:s"),
					'tag' => isset($tag) ? $tag : '',
					'status' => $post['status']
				);

			}else{
				$param = array(
					'judul'=>$post['judul_berita'],
					'id_desa'=>getSettingDesa()['id_desa'],
					'id_kategori'=>$post['id_kategori'],
					'judul_seo'=>slugify($post['judul_berita']),
					'headline'=>$post['is_headline'],
					'isi_berita'=>$post['berita'],	
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'id_user'=>getSession(),
					'tanggal'=> date("Y-m-d H:i:s"),
					'tag' => isset($tag) ? $tag : '',
					'status' => $post['status']
				);
			}
			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_beritabe->ubah_berita($param,$id);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		authorize('hapusberita');
		$idEn = decrypt_url($id);

		$data =$this->M_beritabe->get_detail_berita($idEn);

		$dirUpload = "asset/foto_berita/";

		$files = $dirUpload.$data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}


		$result = $this->M_beritabe->delete_berita($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>