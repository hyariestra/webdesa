<style type="text/css">

    .spanMargin{

        margin-right: 7px;



    }

    table, th, td{

        border: 1px solid #ddd !important;

    }

</style>

<div class="row">



    <div class="col-md-12">



        <div class="box">



            <div class="box-header">

            </div>


            <div class="box-body">

                <?php echo $this->breadcrumbcomponent->generate(); ?>


                <?php
    
                if (authorizeView('tambahberita')) {
            
                ?>
                <a style="margin-bottom: 10px;" href="<?php echo site_url('beritabe/tambah') ?>" class="btn btn-filter btn-danger "><i class="fa fa-plus"></i> Tambah Berita Baru</a>
               
                <?php } ?>
               
                <table id="table" class="table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Headline</th>
                            <th>Status</th>
                            <th>Kategori</th>
                            <th>Author</th>
                            <th>Tanggal</th>
                            <th>Dibaca</th>
                            <th>Tag</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    
                </table>
            </div>

        </div>

    </div>

</div>


<script type="text/javascript">
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({ 
         "lengthMenu": [[25, 50, 75, -1], [25, 50, 70, "All"]],
         "processing": true, 
         "serverSide": true, 
         "order": [], 

         "ajax": {
            "url": "<?php echo site_url('beritabe/get_data_user')?>",
            "type": "POST"
        },


        "columnDefs": [
        { 
            "searchable": true,
              "orderable": false,
              "className": "text-center",
              "targets": 0 
        },{
              "searchable": true,
              "orderable": true,
              "targets": 1
            },
            {
              "searchable": false,
              "orderable": false,
              "targets": 2
            },
            {
              "searchable": false,
              "orderable": false,
              "targets": 3
            },
            {
              "searchable": false,
              "orderable": false,
              "targets": 4
            },
            {
              "searchable": false,
              "orderable": false,
              "targets": 5
            },
            {
              "searchable": false,
              "orderable": false,
              "targets": 8
            },
            {
              "searchable": false,
              "orderable": false,
              "targets": 9
            }
        ],

    });

    });


    function delete_func(id) {
        console.log(id);

        var url = '<?php echo base_url(); ?>';

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {
             $.ajax({
                url: url+'beritabe/delete/'+id,
                type: 'POST',
                error: function() {
                    alert('Something is wrong');
                },
                success: function(data) {

                    processJson(data);

                }
            });
         } else {
            swal("Data Batal Dihapus");
        }
    });

    }


    function processJson(result) { 

       

        new Noty({
            text: result.message,
            type: result.status_code,
            timeout: 3000,
            theme: 'semanticui'
        }).show();

        if(result.status == 201){
            window.location = '<?php echo base_url('beritabe') ?>';

        }
    }

</script>

