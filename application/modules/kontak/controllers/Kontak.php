<?php 

class Kontak extends CI_controller

{

	function __construct()

	{

		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];

	}

	
	function index()

	{

		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none'; 
		$data['detail']['judul_halaman'] = 'Kontak Kami';
		$data['sliders'] = array();
		$data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : ''; 
		
		
		$data['slider'] = $this->load->view($this->theme."/slider", $data, true);
		$data['content'] = $this->load->view($this->theme."/kontak", $data, true);


		echo $this->load->view($this->theme."/template", $data);


	}


	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_beritabe->get_detail_berita($idEn);

		$dirUpload = "asset/foto_berita/";

		$files = $dirUpload.$data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}


		$result = $this->M_beritabe->delete_berita($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>