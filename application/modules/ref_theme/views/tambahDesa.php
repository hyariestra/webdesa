<style type="text/css">
	.container1 input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}
	.delete{
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				
				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>


						<ol class="breadcrumb">
							<li><b>Informasi Desa</b></li>
						</ol>

						<div class="form-group">

							<label>Nama Desa</label>

							<input value="<?php echo @$desa['desa_nama'] ?>" type="text" name="desa_nama" class="form-control">

						</div>

						<div class="form-group">

							<label>Desa Kode</label>

							<input value="<?php echo @$desa['desa_kode'] ?>" type="text" name="desa_kode" class="form-control">

						</div>

						<div class="form-group">

							<label>Desa Kode Domain</label>

							<input value="<?php echo @$desa['desa_kode_domain'] ?>" type="text" name="desa_kode_domain" class="form-control">

						</div>

						<div class="form-group">

							<label>Desa Lokasi</label>

							<textarea name="desa_lokasi" class="form-control"  id=""><?php echo @$desa['desa_lokasi'] ?></textarea>

						</div>

						<div class="form-group">

							<label>Desa No Perdes</label>

							<input value="<?php echo @$desa['desa_noperdes'] ?>" type="text" name="desa_noperdes" class="form-control">

						</div>

						<div class="form-group">

							<label>Desa NPWP</label>

							<input value="<?php echo @$desa['desa_npwp'] ?>" type="text" name="desa_npwp" class="form-control">

						</div>

						<div class="form-group">

							<label>Desa No Rekening</label>

							<input value="<?php echo @$desa['desa_norekening'] ?>" type="text" name="desa_norekening" class="form-control">

						</div>

						<div class="form-group">

							<label>Desa AN Rekening</label>

							<input value="<?php echo @$desa['desa_anrekening'] ?>" type="text" name="desa_anrekening" class="form-control">

						</div>


						<div class="form-group">

							<label>Email</label>

							<input value="<?php echo @$desa['desa_email'] ?>" type="text" name="desa_email" class="form-control">

						</div>

						<div class="form-group">

							<label>Telp</label>

							<input value="<?php echo @$desa['desa_telp'] ?>" type="text" name="desa_telp" class="form-control">

						</div>


						<div class="form-group">

							<label>Facebook</label>

							<input value="<?php echo @$desa['desa_fb'] ?>" type="text" name="desa_fb" class="form-control">

						</div>


						<div class="form-group">

							<label>Youtube</label>

							<input value="<?php echo @$desa['desa_youtube'] ?>" type="text" name="desa_youtube" class="form-control">

						</div>


						<div class="form-group">

							<label>Instagram</label>

							<input value="<?php echo @$desa['desa_ig'] ?>" type="text" name="desa_ig" class="form-control">

						</div>

					
						<div class="form-group">

							<label>Desa logo</label>

						</div>

						

						<div class="form-group">
							<table class="table table-bordered table-hover" style="width: 100%">

								<tbody>

									<tr role="row1">

										<td>

											<div class="input-group">

												<input  accept="image/*" class="form-control" readonly="" type="file" name="gambar">

												<span class="input-group-btn">

													<a class="repeat-remove btn btn-warning"  href="#modal-id"><i class="fa fa-trash" aria-hidden="true"></i></a>

												</span>

											</div>
											<br>
											<img width="200px" src="<?php echo base_url('asset/foto_logo_desa/'.@$desa['desa_logo_uniq']) ?>" alt="">
											<p><i>*file yang diizinkan bertipe .png, .jpeg, .jpg dan maksimal 2Mb </i></p>
										</td>

									</tr>
									<tr id="total_row">



									</tr>

								</tbody>

							</table>
						</div>

						



						<ol class="breadcrumb">
							<li><b>Informasi Website</b></li>
						</ol>



						<div class="form-group">

							<label>Title Website</label>

							<input value="<?php echo @$desa['desa_title'] ?>" type="text" name="desa_title" class="form-control">

						</div>

						<div class="form-group">

							<label>Deskripsi Website</label>

							<input value="<?php echo @$desa['desa_deskripsi'] ?>" type="text" name="desa_deskripsi" class="form-control">

						</div>

						<div class="form-group">

							<label>Metakeyword Website</label>

							<input value="<?php echo @$desa['desa_metakeyword'] ?>" type="text" name="desa_metakeyword" class="form-control">

						</div>


						<div class="form-group">

							<label>Status</label>
							<select class="form-control" name="status" id="">
								<option  <?php echo (@$desa['desa_status'] == 'active') ? "selected": "" ?> value="publish">Active</option>
								<option  <?php echo (@$desa['desa_status'] == 'nonactive') ? "selected": "" ?> value="draft">Non Active</option>
							</select>

						</div>

						<div class="form-group">

							<label>Tema</label>
							<select class="form-control" name="status" id="">
								<option  <?php echo (@$desa['desa_status'] == 'active') ? "selected": "" ?> value="publish">Active</option>
								<option  <?php echo (@$desa['desa_status'] == 'nonactive') ? "selected": "" ?> value="draft">Non Active</option>
							</select>

						</div>

					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('ref_desa') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>		

	</div>

</div>


<script>

		$('.form-kirim').ajaxForm({ 
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){

			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			console.log(result);

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('ref_desa') ?>';

			}
		}


	</script>



	

