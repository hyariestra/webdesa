<?php 

class Ref_theme extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))



		{



			redirect("pengguna/login");



		}

	}
	

	function index()

	{



		$this->themeadmin->tampilkan('tampilTheme',null);


	}

	function get_data_desa()
	{
		$list = $this->M_ref_desa->get_datatables();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {


			if ($field->desa_status == 'active') {
				$status = "<span class='label label-success'>active</span>";
			}else{
				$status = "<span class='label label-default'>non active</span>";
			}

			
			$idDesa = encrypt_url($field->id_desa);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->desa_kode;
			$row[] = $field->desa_nama;
			$row[] = $field->desa_kode_domain;
			$row[] = $status;
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('ref_desa/ubah/'.encrypt_url($field->id_desa))." >
			<span class='glyphicon glyphicon-edit'></span></a>
			<a onclick=delete_func('$idDesa')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_ref_desa->count_all(),
			"recordsFiltered" => $this->M_ref_desa->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['url'] = 'ref_desa/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
	
		$this->themeadmin->tampilkan('tambahDesa',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['url'] = 'ref_desa/edit/'.$id;
		$data['desa'] =$this->M_ref_desa->get_detail_desa($idEn);
		
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahDesa',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

		if ($post['desa_nama'] == "") {

			$err_msg[] = 'nama desa';
			$result = false;

		}

		if ($post['desa_kode'] == "") {

			$err_msg[] = 'desa kode kosong';
			$result = false;

		}

		if ($post['desa_kode_domain'] == "") {

			$err_msg[] = 'desa kode domain kosong';
			$result = false;

		}



		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_logo_desa/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 250000 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {

			$param = array(
				'desa_nama'=>$post['desa_nama'],
				'desa_kode'=>'this is kode',
				'desa_kode_domain'=>$post['desa_kode_domain'],
				'desa_lokasi'=>$post['desa_lokasi'],
				'desa_noperdes'=>$post['desa_noperdes'],
				'desa_npwp'=>$post['desa_npwp'],
				'desa_norekening'=>$post['desa_norekening'],
				'desa_anrekening'=>$post['desa_anrekening'],
				'desa_email'=>$post['desa_email'],
				'desa_telp'=>$post['desa_telp'],
				'desa_fb'=>$post['desa_fb'],
				'desa_youtube'=>$post['desa_youtube'],
				'desa_ig'=>$post['desa_ig'],
				'desa_logo_uniq' => isset($namaFile) ? $namaFile : '',
				'desa_logo' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'desa_title'=>$post['desa_title'],
				'desa_deskripsi'=>$post['desa_deskripsi'],
				'desa_metakeyword'=>$post['desa_metakeyword'],
				'tanggal'=> date("Y-m-d H:i:s"),
				'desa_status' => $post['status'],
			);

			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_ref_desa->simpan_desa($param);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['judul_berita'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}

		if ($post['id_kategori'] == "") {

			$err_msg[] = 'Kategori Kosong';
			$result = false;

		}



		if (isset($post['tag'])) {
			$tag = implode(',', $post['tag']);

		}


		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_berita/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 250000 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {


			if (!isset($_FILES['gambar'])) {
				
				$param = array(
					'judul'=>$post['judul_berita'],
					'id_kategori'=>$post['id_kategori'],
					'judul_seo'=>slugify($post['judul_berita']),
					'headline'=>$post['is_headline'],
					'isi_berita'=>$post['berita'],	
					'id_user'=>getSession(),
					'tanggal_update'=> date("Y-m-d H:i:s"),
					'tag' => isset($tag) ? $tag : '',
					'status' => $post['status']
				);

			}else{
				$param = array(
					'judul'=>$post['judul_berita'],
					'id_kategori'=>$post['id_kategori'],
					'judul_seo'=>slugify($post['judul_berita']),
					'headline'=>$post['is_headline'],
					'isi_berita'=>$post['berita'],	
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'id_user'=>getSession(),
					'tanggal'=> date("Y-m-d H:i:s"),
					'tag' => isset($tag) ? $tag : '',
					'status' => $post['status']
				);
			}
			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_beritabe->ubah_berita($param,$id);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_beritabe->get_detail_berita($idEn);

		$dirUpload = "asset/foto_berita/";

		$files = $dirUpload.$data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}


		$result = $this->M_beritabe->delete_berita($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>