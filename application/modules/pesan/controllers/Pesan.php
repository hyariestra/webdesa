<?php 

class Pesan extends CI_controller

{

	function __construct()

	{

		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];

	}


    function kirim()

    {

		$post = $this->input->post();

		$param = array(
			'nama'=>$post['name'],
			'email'=>$post['email'],
			'telp'=>$post['telp'],	
			'subject'=>$post['subject'],	
			'isi_pesan'=>$post['message'],	
			'id_desa'=>getSettingDesa()['id_desa'],
			'tanggal'=> date("Y-m-d H:i:s")	

		);

		$result = $this->M_pesan->simpan_pesan($param);

		if ($result) {

			echo 'OK';

		}else{

			echo '';
		}

    }

    

     

  
    public function news_save()
    {
    
    	$post = $this->input->post();

    	$result = $this->M_berita->simpan_email($post['email']);

    	echo $result;
    }

	public function addKomen()
	{
		$ids = trim($_GET['ids']);
		$seo = trim($_GET['seo']);

		$params = array(
			'nama'=>$_GET['name'],
			'email'=>$_GET['mail'],
			'telp'=>$_GET['phone'],
			'isi'=>$_GET['comment'],
			'id_berita'=>$ids,
			'id_parent'=>NULL,
			'id_users'=>NULL,
			'insert_time'=> date("Y-m-d H:i:s")
		
		);
	
		$result = $this->M_berita->simpan_komen($params);

		$isipesan="<div class='alert alert-success' role='alert'><a href='#' class='alert-link'>Komentar sukses ditambahkan</a></div>";

		$this->session->set_flashdata("pesan",$isipesan);

		redirect("berita/detail/$ids/$seo");

	}





}

?>