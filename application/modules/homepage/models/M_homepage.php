<?php 

class M_homepage extends CI_model

{

  public function getArrayDesaMenu()
  {
    
    $data = $this->db->query("SELECT * FROM `fe_menus_desa` WHERE id_desa = ".getSettingDesa()['id_desa']." ")->result_array();

    foreach ($data as $key => $value) {
      $data[$key] = $value['id_menu'];
    }

    return $data;

  }


 public function get_front_menu_tree()
 {

  $arrayMenu = $this->getArrayDesaMenu();

   $data = $this->db->query("SELECT 
    a.id_menu AS id_menu,
    a.id_parent AS id_parent,
    a.menu AS menu,
    a.`link_menu` AS link_menu,
    b.`id_menu` AS id_submenu,
    b.`id_parent` AS id_subparent,
    b.`menu` AS submenu,
    b.`link_menu` AS link_submenu
    FROM `fe_menus` a 
    LEFT JOIN fe_menus b ON a.`id_menu` = b.`id_parent` 
    ORDER BY a.order ASC
    ")->result_array();


    $temp = [];


    foreach ($data as $key => $value) {
    


    if ($value['id_parent']==NULL) {

   

      $temp[$value['id_menu']]['menu'] = $value['menu'];
      $temp[$value['id_menu']]['id_menu'] = $value['id_menu'];
      $temp[$value['id_menu']]['link_menu'] = $value['link_menu'];



      if ($value['id_subparent'] != "") {

        $temp[$value['id_menu']]['submenu'][$value['id_submenu']]['id_submenu'] = $value['id_submenu'];
        $temp[$value['id_menu']]['submenu'][$value['id_submenu']]['submenu'] = $value['submenu'];
        $temp[$value['id_menu']]['submenu'][$value['id_submenu']]['link_submenu'] = $value['link_submenu'];

      }

    }


  }



   return $temp;

 }


 public function get_front_menu()
 {
   $data = $this->db->query("SELECT * FROM fe_menu")->result_array();

   return $data;

 }


 public function get_front_submenu($id)
 {
   $data = $this->db->query("SELECT * FROM fe_submenu WHERE menu_id= ".$id." ORDER BY submenu_id ASC")->result_array();

   return $data;

 }



 public function kunjungan()
 {

  $ip      = $_SERVER['REMOTE_ADDR'];
  $country  = ip_info($ip, "Location")['country'];
  $country_code  = ip_info($ip, "Location")['country_code'];
  $continent  = ip_info($ip, "Location")['continent'];
  $continent_code  = ip_info($ip, "Location")['continent_code'];
  $tanggal = date("Y-m-d");
  $waktu   = time(); 
  $id_desa = getSettingDesa()['id_desa'];
  $cekk = $this->db->query("SELECT * FROM statistik WHERE ip='$ip' AND tanggal='$tanggal' AND id_desa = '$id_desa' ");
  $rowh = $cekk->row_array();
  if($cekk->num_rows() == 0){
    $datadb = array(
      'ip'=>$ip, 
      'tanggal'=>$tanggal, 
      'hits'=>'1', 
      'online'=>$waktu, 
      'country'=>$country, 
      'country_code'=>$country_code, 
      'continent'=>$continent, 
      'continent_code'=>$continent_code, 
      'id_desa'=>$id_desa );
    $this->db->insert('statistik',$datadb);
  }else{
    $hitss = $rowh['hits'] + 1;
    $datadb = array('ip'=>$ip, 'tanggal'=>$tanggal, 'hits'=>$hitss, 'online'=>$waktu,  'id_desa'=>$id_desa);
    $array = array('ip' => $ip, 'tanggal' => $tanggal);
    $this->db->where($array);
    $this->db->update('statistik',$datadb);
  }

}


}


?>