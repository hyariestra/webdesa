<?php 

class Homepage extends CI_controller



{

	function __construct()

	{

		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];

	}
	

	function index( $rowno=0)

	{


		$this->M_homepage->kunjungan();

		$jumlah_data = $this->M_berita->jumlah_data();
		$config['base_url'] = site_url('berita/index'); //site url
		$config['total_rows'] = $jumlah_data;
        $config["uri_segment"] = 3;  // uri parameter
     
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $rowperpage = 10;

    // Row position
        if($rowno != 0){
        	$rowno = ($rowno-1) * $rowperpage;
        }

        $cari['key'] = '';
		$cari['tag'] = '';
		$cari['sort'] = '';

        $data['item'] = $this->M_berita->get_berita_homepage($cari,$rowperpage,$rowno,$search_text='');
        $data['itemAll'] = $this->M_berita->get_berita_all();


        
		$series_record = $this->M_series->get_series_all();
		

        $data['series'] = sliceArrayByCategoryByCode($series_record, null, 6);

     

      
        $data['headline'] = $this->M_berita->get_berita_headline();
        $data['popular'] = $this->M_berita->get_berita_popular(5);
        $data['galeri']= $this->M_galeri->get_galeri_homepage(10,'Y');
        $data['client']= $this->M_galeri->get_client_homepage();
        $data['pemateri']= $this->M_galeri->get_pemateri_homepage();
        $data['video']= $this->M_videobe->get_video_homepage();
        $data['sliders'] = $this->M_galeri->get_slider();

        $data['ref_porto'] = $this->M_galeri->get_ref_porto();
        $data['porto'] = $this->M_galeri->get_porto();
        $data['display'] = $this->uri->uri_string() == '' ? '' : 'none'; 
        $data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : ''; 

 
        $data['slider'] = $this->load->view($this->theme."/slider", $data, true);
        $data['content'] = $this->load->view($this->theme."/main", $data, true);
        $data['paralax'] = $this->load->view($this->theme."/paralax", $data, true);
        echo $this->load->view($this->theme."/template", $data);


    }

    public function sitemap()
    {
           $data['post'] = $this->M_berita->get_all_post();

           echo $this->load->view($this->theme."/sitemap", $data, true);
    }


    public function notfound()

    {

    	$data = array();   
        $data['detail']['judul_halaman'] = 'not found';
        $data['detail']['breadcumb'] = 'not found';
		$data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none'; 
		$data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : ''; 
		$data['slider'] = $this->load->view($this->theme."/slider", $data, true);

    	$data['content'] = $this->load->view($this->theme."/404", $data, true);

    	echo $this->load->view($this->theme."/template", $data);

    }




}

?>