<?php 

/**
* 
*/
class Konfigurasi extends CI_controller
{

	
	public function index(){

		authorize('konfigurasi');
		$data['act'] = 'konfigurasi';
		$data['judul'] = 'Halaman Konfigurasi';

		$this->themeadmin->tampilkan('index_konfigurasi', $data);

	}

	public function simpan(){

		$nama 	= $this->input->post("nama");
		$nilai 	= $this->input->post("nilai");

		$this->db->trans_begin();

		$this->db->query("DELETE FROM set_setting");

		$i=0;
        foreach ($nama as $key => $value){

        	$this->db->query("INSERT INTO set_setting (nama, nilai)
        					  VALUES ('".$nama[$i]."', '".$nilai[$i]."')");

        	$i++;
        }

        if ($this->db->trans_status() === FALSE)
          {  
            $this->db->trans_rollback();  

            $this->session->set_flashdata('gagal_simpan', 'Simpan Data Gagal Mohon Dicek Kembali');
			redirect("konfigurasi");

          }  
          else
          {
             
            $this->db->trans_commit();

            $this->session->set_flashdata('berhasil_simpan', 'Simpan Data Berhasil');
			redirect("konfigurasi");
          
          }

	}
}

?>