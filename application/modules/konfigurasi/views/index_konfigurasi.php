
<!-- data pelanggan ada di var $pelanggan -->

<div class="row">

	<div class="col-md-12">

			<div class="box">

				<div class="box-header">

					<h3 class="box-title"><?php echo $judul ?></h3>

					<hr/>

				</div>

				<form action="<?php echo base_url("konfigurasi/simpan") ?>" method="post">

					<div class="box-body">
						
							<div class="row">

								<?php
							    	if ($this->session->flashdata('gagal_simpan') == TRUE) {
							    ?>
							    	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>
							    		<?php echo $this->session->flashdata('gagal_simpan');?>
							    	</div>
							   	<?php 
							   		}elseif($this->session->flashdata('berhasil_simpan') == TRUE){
							   	?>
							   		<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
							    		<?php echo $this->session->flashdata('berhasil_simpan');?>
							    	</div>
							    <?php } ?>

								<div class="col-md-6">

									<div class="form-group">
									    <label for="NamaPerusahaan">Akun Pajak</label>
									    <input type="hidden" name="nama[]" id="nama[]" value="akun_pajak">
									    <input type="text" class="form-control" id="nilai[]" name="nilai[]" value="<?php echo GetSettingValue("akun_pajak")?>">
									    <span>* Masukan Akun Pajak Untuk Laporan (contoh 07.03.01)</span>
								  	</div>
				
								</div>

								<div class="col-md-6">

									<div class="form-group">
									    <label for="NamaPerusahaan">Akun Penambahan Modal</label>
									    <input type="hidden" name="nama[]" id="nama[]" value="akun_penambahan_modal">
									    <input type="text" class="form-control" id="nilai[]" name="nilai[]" value="<?php echo GetSettingValue("akun_penambahan_modal")?>">
									    <span>* Masukan Akun Penambahan Modal Untuk Laporan (contoh 07.03.01)</span>
								  	</div>
				
								</div>

								<div class="col-md-6">

									<div class="form-group">
									    <label for="NamaPerusahaan">Akun Deviden</label>
									    <input type="hidden" name="nama[]" id="nama[]" value="akun_deviden">
									    <input type="text" class="form-control" id="nilai[]" name="nilai[]" value="<?php echo GetSettingValue("akun_deviden")?>">
									    <span>* Masukan Akun Deviden Untuk Laporan (contoh 07.03.01)</span>
								  	</div>
				
								</div>

								<div class="col-md-6">

									<div class="form-group">
									    <label for="NamaPerusahaan">Akun Laba Ditahan</label>
									    <input type="hidden" name="nama[]" id="nama[]" value="akun_laba_ditahan">
									    <input type="text" class="form-control" id="nilai[]" name="nilai[]" value="<?php echo GetSettingValue("akun_laba_ditahan")?>">
									    <span>* Masukan Akun Deviden Untuk Laporan (contoh 07.03.01)</span>
								  	</div>
				
								</div>

								<div class="col-md-6">

									<div class="form-group">
									    <label for="NamaPerusahaan">Presentase Pajak</label>
									    <input type="hidden" name="nama[]" id="nama[]" value="presentase_pajak">
									    <input type="text" class="form-control" id="nilai[]" name="nilai[]" value="<?php echo GetSettingValue("presentase_pajak")?>">
									    <span>* Masukan Presentase Pajak Untuk Laporan Laba Rugi (contoh 5)</span>
								  	</div>
				
								</div>
								
							</div>

					</div>

					<div class="box-footer">

						<button class="btn btn-primary pull-right" type="submit" id="btnSimpan">Simpan</button>

					</div>

				</form>

			</div>

	</div>

	<script type="text/javascript">
		
		$('document').ready(function(){

    	});

	</script>

</div>