<?php 

class Galeri extends CI_controller

{

	function __construct()

	{

		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];

	}


	function index()
	{
		
        $data['detail']['judul_halaman'] = 'Photo Gallery';
        $data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none'; 
        $data['galeri']= $this->M_galeri->get_all_galeri();
		$data['slider'] = $this->load->view($this->theme."/slider", $data, true);

        $data['content'] = $this->load->view($this->theme."/gallery", $data, true);


        echo $this->load->view($this->theme."/template", $data);

    }


}

?>