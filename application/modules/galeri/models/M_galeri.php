<?php 

class M_galeri extends CI_model

{



   function get_galeri_homepage($limit,$headline)
   {

     $this->M_galeribe->raw_db_galeri();
     $this->db->where('headline', 'Y');
      //  $this->db->limit($limit);  
     $galeri = $this->db->get()->result_array();

     $data = array();

     foreach ($galeri as $key => $value) 
	  	{


        $data[$key]['id'] = $value['id_galeri'];
        $data[$key]['judul'] = $this->trimelement($value['judul_galeri'],80);
        $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_galeri/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
       

 	   }

  	  return $data;

   }

	function get_all_galeri()
   {

     $this->M_galeribe->raw_db_galeri();
      //  $this->db->limit($limit);  
     $galeri = $this->db->get()->result_array();

     $data = array();

     foreach ($galeri as $key => $value) 
	  	{


        $data[$key]['id'] = $value['id_galeri'];
        $data[$key]['judul'] = $this->trimelement($value['judul_galeri'],80);
        $data[$key]['keterangan'] = $value['keterangan'];
        $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_galeri/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
       

 	   }

  	  return $data;

   }


function trimelement($data,$panjang)
{

   $max_length = $panjang;

   if (strlen($data) > $max_length)
   {
    $offset = ($max_length - 3) - strlen($data);
    $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
}

return $data;
}





function jumlah_data()

{


    $this->db->select('*');
    $this->db->from('berita a');
    $this->db->join('ref_desa_users b', 'a.id_user = b.id_admin', 'left');
    $this->db->join('ref_desa c', 'c.id_desa = b.id_desa', 'left');
    $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('c.desa_kode_domain', getNameDomain() );
    $data = $this->db->get()->num_rows();

    return $data;


}

function get_berita_popular($limit)
{

 $this->db->select('*');
 $this->db->from('berita a');
 $this->db->join('ref_desa_users b', 'a.id_user = b.id_admin', 'left');
 $this->db->join('ref_desa c', 'c.id_desa = b.id_desa', 'left');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('c.desa_kode_domain', getNameDomain() );
 $this->db->order_by('a.like_berita', 'desc');
 $this->db->limit($limit);  
 $data = $this->db->get()->result_array();

 foreach ($data as $key => $value) {


    $data[$key]['id'] = $value['id_berita'];
    $data[$key]['judul'] = $this->trimelement($value['judul'],80);
    $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'],150);
    $data[$key]['kategori'] = $value['nama_kategori'];
    $data[$key]['tanggal'] = $value['tanggal'];
    $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
    $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
    $data[$key]['like'] = $value['like_berita'];

}

return $data;

}

function get_slider()
{
    $this->db->select('*');
    $this->db->from('slider a');
    $this->db->join('ref_desa_users b', 'a.id_user = b.id_admin', 'left');
    $this->db->join('ref_desa c', 'c.id_desa = b.id_desa', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('c.desa_kode_domain', getNameDomain() );
    $this->db->order_by('a.tanggal', 'desc');
    $data = $this->db->get()->result_array();

    $newData = [];

    foreach ($data as $key => $value) {


        $newData[$key]['id'] = $value['id_slider'];
        $newData[$key]['judul'] = $this->trimelement($value['text_slider'],80);
        $newData[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_slider/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
        $newData[$key]['url'] = $value['url_slider'];
      
    }
    
    return $newData;


}
function get_client_homepage()
{
    $this->db->select('*');
    $this->db->from('client a');
    $this->db->join('ref_desa_users b', 'a.id_user = b.id_admin', 'left');
    $this->db->join('ref_desa c', 'c.id_desa = b.id_desa', 'left');
    $this->db->where('a.headline', 'Y');
    $this->db->where('c.desa_kode_domain', getNameDomain() );
    $this->db->order_by('a.tanggal', 'desc');
    $data = $this->db->get()->result_array();

    $newData = [];

    foreach ($data as $key => $value) {


        $newData[$key]['id'] = $value['id_client'];
        $newData[$key]['nama_client'] = $value['nama_client'];
        $newData[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_client/'.rawurlencode($value['gambar_uniq']) : 'img/imgnotfound.png';
        
      
    }
    
    return $newData;


}

function get_pemateri_homepage()
{
    $this->db->select('*');
    $this->db->from('pemateri a');
    $this->db->join('ref_desa_users b', 'a.id_user = b.id_admin', 'left');
    $this->db->join('ref_desa c', 'c.id_desa = b.id_desa', 'left');
    $this->db->where('a.headline', 'Y');
    $this->db->where('a.status', 'publish');
    $this->db->where('c.desa_kode_domain', getNameDomain() );
    $this->db->order_by('a.tanggal', 'desc');
    $data = $this->db->get()->result_array();

    $newData = [];

    foreach ($data as $key => $value) {


        $newData[$key]['id'] = $value['id_pemateri'];
        $newData[$key]['nama_pemateri'] = $value['nama_pemateri'];
        $newData[$key]['keterangan'] = $value['keterangan'];
        $newData[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_pemateri/'.rawurlencode($value['gambar_uniq']) : 'img/imgnotfound.png';
        
      
    }
    
    return $newData;


}

function get_porto()
{
        $this->db->select('*');
        $this->db->from('portofolio a');
        $this->db->join('ref_porto b', 'a.id_ref_porto = b.id_ref_porto', 'left');
        $this->db->join('ref_desa c', 'a.id_desa = c.id_desa', 'left');
        $this->db->where('a.status', 'publish');
        $this->db->where('c.desa_kode_domain', getNameDomain() );
        $this->db->order_by('a.id_ref_porto', 'desc');
        $res = $this->db->get()->result_array();


        $data = array();

        foreach ($res as $key => $value) {


            $data[$key]['id'] = encrypt_url($value['id_porto']);
            $data[$key]['judul_porto'] = $value['judul_porto'];
            $data[$key]['label_porto'] = $value['label_porto'];
            $data[$key]['gambar'] = !empty($value['gambar_1']) ? 'foto_porto/'.$value['gambar_1'] : 'img/imgnotfound.png';
            $data[$key]['url'] = base_url('portofolio/detail/'.$value['id_porto'].'/'.$value['judul_porto_seo']);
        }

return $data;


}

function get_ref_porto()
{
        $this->db->select('*');
        $this->db->from('ref_porto a');
        $this->db->join('ref_desa b', 'b.id_desa = b.id_desa', 'left');
        $this->db->where('a.status', 'publish');
        $this->db->where('b.desa_kode_domain', getNameDomain() );
        $this->db->order_by('a.id_ref_porto', 'desc');
        $res = $this->db->get()->result_array();


        foreach ($res as $key => $value) {


            $data[$key]['id'] = $value['id_ref_porto'];
            $data[$key]['label_porto'] = $value['label_porto'];
            $data[$key]['nama_porto'] = $value['nama_porto'];

        }

return $data;


}


}


?>