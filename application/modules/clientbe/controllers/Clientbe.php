<?php 

class Clientbe extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))

		{

			redirect("pengguna/login");

		}

	}
	

	function index()

	{



		$this->themeadmin->tampilkan('tampilClient',null);


	}

	function get_data_client()
	{
		$list = $this->M_clientbe->get_datatables_client();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->headline == 'Y') {
				$headline = "<span class='label label-danger'>Y</span>";
			}else{
				$headline = "<span class='label label-default'>N</span>";
			}

			$idGaleri = encrypt_url($field->id_client);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_client;
			$row[] = $headline;
			$row[] = $field->nama;
			$row[] = tgl_indo_timestamp($field->tanggal);
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('clientbe/ubah/'.encrypt_url($field->id_client))." >
			<span class='glyphicon glyphicon-edit'></span></a>
			<a onclick=delete_func('$idGaleri')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_clientbe->count_all(),
			"recordsFiltered" => $this->M_clientbe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['url'] = 'clientbe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		
		$this->themeadmin->tampilkan('tambahClient',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['url'] = 'clientbe/edit/'.$id;
		$data['client'] =$this->M_clientbe->get_detail_client($idEn);
		
		
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahClient',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

		if ($post['nama_client'] == "") {

			$err_msg[] = 'Nama Client';
			$result = false;

		}


		if (empty($_FILES['gambar']['name'])) {

			$err_msg[] = 'Gambar Kosong';
			$result = false;

		}




		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_client/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 250000 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {

			$param = array(
				'nama_client'=>$post['nama_client'],
				'headline'=>$post['is_headline'],
				'keterangan'=>$post['keterangan'],	
				'gambar_uniq' => isset($namaFile) ? $namaFile : '',
				'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'tanggal'=> date("Y-m-d H:i:s")	

			);

			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_clientbe->simpan_client($param);
		
		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['nama_client'] == "") {

			$err_msg[] = 'nama client';
			$result = false;

		}





		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_client/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 250000 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {


			if (!isset($_FILES['gambar'])) {
				
				$param = array(
					'nama_client'=>$post['nama_client'],
					'headline'=>$post['is_headline'],
					'keterangan'=>$post['keterangan'],	
					'id_user'=>getSession(),
					'id_desa'=>getSettingDesa()['id_desa'],
					'tanggal'=> date("Y-m-d H:i:s")
				);

			}else{

				$param = array(
					'nama_client'=>$post['nama_client'],
					'headline'=>$post['is_headline'],
					'keterangan'=>$post['keterangan'],	
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'id_user'=>getSession(),
					'id_desa'=>getSettingDesa()['id_desa'],
					'tanggal'=> date("Y-m-d H:i:s")

				);

			}
			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_clientbe->ubah_client($param,$id);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_clientbe->get_detail_client($idEn);

		$dirUpload = "asset/foto_client/";

		$files = $dirUpload.$data['gambar_uniq'];

		
		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}



		$result = $this->M_clientbe->delete_client($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>