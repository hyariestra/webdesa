<?php 

class M_clientbe extends CI_model

{

    var $column_order = array(null, 'a.tanggal'); //field yang ada di table user
    var $column_search = array('judul_galeri'); //field yang diizin untuk pencarian 
    var $order = array('a.tanggal' => 'desc'); // default order 


    function trimelement($data,$panjang)
    {

       $max_length = $panjang;

       if (strlen($data) > $max_length)
       {
        $offset = ($max_length - 3) - strlen($data);
        $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
    }

    return $data;
}


function get_detail_client($id='')
{
 $data =  $this->db->query("SELECT * FROM `client` WHERE id_client =  ".$id." ")->row_array();

 return $data;
}




function simpan_client($param)
{

 $this->db->trans_begin();




 $result = $this->db->insert('client',$param);



 if ($result == false){
  $this->db->trans_rollback();

}else{
  $this->db->trans_commit();

}

return [
  'result' => $result
];
}   

function delete_client($id)
{
    return $this->db->query("DELETE FROM client where id_client = ".$id." ");
}

function ubah_client($param,$id)
{

    $this->db->trans_begin();




    $this->db->where('id_client',$id);
    $result = $this->db->update('client',$param);



    if ($result == false){
        $this->db->trans_rollback();

    }else{
        $this->db->trans_commit();

    }

    return [
        'result' => $result
    ];
}




public function raw_db_client()
{
    $this->db->select('*');
    $this->db->from('client a');
    $this->db->join('admin b', 'b.id_admin = a.id_user', 'left');
    $this->db->join('ref_desa_users d', 'b.id_admin = d.id_admin', 'left');
    $this->db->join('ref_desa e', 'e.id_desa = d.id_desa', 'left');
    $q =  $this->db->where('e.desa_kode_domain', getNameDomain() );


    return $q;


}


private function _get_datatables_query()
{

    $this->raw_db_client();


 $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if($i===0) // looping awal
                {
                	$this->db->group_start(); 
                	$this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                	$this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                	$this->db->group_end(); 
            }
            $i++;
        }

        if(isset($_POST['order'])) 
        {
        	$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
        	$this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_client()
    {
    	$this->_get_datatables_query();
    	if($_POST['length'] != -1)
    		$this->db->limit($_POST['length'], $_POST['start']);
    	$query = $this->db->get();
    	return $query->result();
    }

    function count_filtered()
    {
    	$this->_get_datatables_query();
    	$query = $this->db->get();
    	return $query->num_rows();
    }

    public function count_all()
    {
    	$this->raw_db_client();
        $r = $this->db->count_all_results();

        return $r;
    }




}


?>