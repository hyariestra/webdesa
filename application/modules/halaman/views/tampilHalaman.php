<style type="text/css">

    .spanMargin{

        margin-right: 7px;



    }

    table, th, td{

        border: 1px solid #ddd !important;

    }

</style>

<div class="row">



    <div class="col-md-12">



        <div class="box">



            <div class="box-header">

            </div>


            <div class="box-body">

                <?php echo $this->breadcrumbcomponent->generate(); ?>


                <a style="margin-bottom: 10px;" href="<?php echo site_url('halamanbe/tambah') ?>" class="btn btn-filter btn-danger "><i class="fa fa-plus"></i> Tambah Berita Baru</a>
                <table id="table" class="table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis</th>
                            <th>Tanggal</th>
                            <th>Dibaca</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    
                </table>
            </div>

        </div>

    </div>

</div>


<script type="text/javascript">
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({ 
         "lengthMenu": [[25, 50, 75, -1], [25, 50, 70, "All"]],
         "processing": true, 
         "serverSide": true, 
         "order": [], 

         "ajax": {
            "url": "<?php echo site_url('halamanbe/get_data_halaman')?>",
            "type": "POST"
        },


        "columnDefs": [
        { 
            "targets": [ 0 ], 
            "orderable": false, 
        },
        ],

    });

    });


    function delete_func(id) {
        console.log(id);

        var url = '<?php echo base_url(); ?>';

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {
             $.ajax({
                url: url+'beritabe/delete/'+id,
                type: 'POST',
                error: function() {
                    alert('Something is wrong');
                },
                success: function(data) {

                    processJson(data);

                }
            });
         } else {
            swal("Data Batal Dihapus");
        }
    });

    }


    function processJson(result) { 

       

        new Noty({
            text: result.message,
            type: result.status_code,
            timeout: 3000,
            theme: 'semanticui'
        }).show();

        if(result.status == 201){
            window.location = '<?php echo base_url('beritabe') ?>';

        }
    }

</script>

