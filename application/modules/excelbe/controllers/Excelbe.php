<?php 

class Excelbe extends CI_controller

{

	function __construct()

	{

		parent::__construct();
		$this->load->library(array('excel','session'));

	}
	

	function index()

	{



		$this->load->view('tampilExcel',null);


	}

	function get_data_user()
	{
		$list = $this->M_beritabe->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->headline == 'Y') {
				$headline = "<span class='label label-success'>Y</span>";
			}else{
				$headline = "<span class='label label-default'>N</span>";
			}

			if ($field->rilis == 'publish') {
				$status = "<span class='label label-success'>Publish</span>";
			}else{
				$status = "<span class='label label-default'>Draft</span>";
			}

			$r =array();

			if (!empty($field->tag)) {
				

				$tag = explode(',', $field->tag);


				foreach ($tag as $key => $value) {

					$r[$key] = $this->M_beritabe->getTagName($value);

				}
			}


			$idBerita = encrypt_url($field->id_berita);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->judul;
			$row[] = $headline;
			$row[] = $status;
			$row[] = $field->nama_kategori;
			$row[] = $field->nama;
			$row[] = tgl_indo_timestamp($field->tanggal);
			$row[] = $field->dibaca." kali";
			$row[] = array_unique($r);
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('beritabe/ubah/'.encrypt_url($field->id_berita))." >
			<span class='glyphicon glyphicon-edit'></span></a>
			<a onclick=delete_func('$idBerita')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_beritabe->count_all(),
			"recordsFiltered" => $this->M_beritabe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['url'] = 'beritabe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$data['kategori'] =$this->M_menu->produk_tree();
		$data['tag'] =$this->M_tag->getDataTag();

		$this->themeadmin->tampilkan('tambahBerita',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['url'] = 'beritabe/edit/'.$id;
		$data['berita'] =$this->M_beritabe->get_detail_berita($idEn);
		$data['tag'] =$this->M_tag->getDataTag();
		$data['kategori'] =$this->M_menu->produk_tree();
		
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahBerita',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

		if ($post['judul_berita'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}

		if ($post['id_kategori'] == "") {

			$err_msg[] = 'Kategori Kosong';
			$result = false;

		}



		if (isset($post['tag'])) {
			$tag = implode(',', $post['tag']);

		}


		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_berita/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2500000 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {

			$param = array(
				'judul'=>$post['judul_berita'],
				'id_desa'=>getSettingDesa()['id_desa'],
				'id_kategori'=>$post['id_kategori'],
				'judul_seo'=>slugify($post['judul_berita']),
				'headline'=>$post['is_headline'],
				'isi_berita'=>$post['berita'],	
				'gambar_uniq' => isset($namaFile) ? $namaFile : '',
				'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'id_user'=>getSession(),
				'tanggal'=> date("Y-m-d H:i:s"),
				'tag' => isset($tag) ? $tag : '',
				'status' => $post['status'],
			);

			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_beritabe->simpan_berita($param);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['judul_berita'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}

		if ($post['id_kategori'] == "") {

			$err_msg[] = 'Kategori Kosong';
			$result = false;

		}



		if (isset($post['tag'])) {
			$tag = implode(',', $post['tag']);

		}


		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_berita/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2500000 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {


			if (!isset($_FILES['gambar'])) {
				
				$param = array(
					'judul'=>$post['judul_berita'],
					'id_desa'=>getSettingDesa()['id_desa'],
					'id_kategori'=>$post['id_kategori'],
					'judul_seo'=>slugify($post['judul_berita']),
					'headline'=>$post['is_headline'],
					'isi_berita'=>$post['berita'],	
					'id_user'=>getSession(),
					'tanggal_update'=> date("Y-m-d H:i:s"),
					'tag' => isset($tag) ? $tag : '',
					'status' => $post['status']
				);

			}else{
				$param = array(
					'judul'=>$post['judul_berita'],
					'id_desa'=>getSettingDesa()['id_desa'],
					'id_kategori'=>$post['id_kategori'],
					'judul_seo'=>slugify($post['judul_berita']),
					'headline'=>$post['is_headline'],
					'isi_berita'=>$post['berita'],	
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'id_user'=>getSession(),
					'tanggal'=> date("Y-m-d H:i:s"),
					'tag' => isset($tag) ? $tag : '',
					'status' => $post['status']
				);
			}
			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_beritabe->ubah_berita($param,$id);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_beritabe->get_detail_berita($idEn);

		$dirUpload = "asset/foto_berita/";

		$files = $dirUpload.$data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}


		$result = $this->M_beritabe->delete_berita($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}


	public function compare_arr(){
		if (isset($_FILES["fileExcel"]["name"])) {
			$path = $_FILES["fileExcel"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);

			


			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();	
			
				for($row=2; $row<=$highestRow; $row++)
				{
					
		
					$dbRaw[] = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
					$excelRaw[] = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					
				}
			}

			$db = $dbRaw;
			$excel = $excelRaw;
		
			foreach ($db as $key=> $item) {
			
				if (in_array($item, $excelRaw)) {
				 	unset($db[$key]);

				}
				
			}


			foreach ($excel as $key=> $item) {
			
				if (in_array($item, $dbRaw)) {
					unset($excel[$key]);

				}
				
			}

			echo "COMPARE DB TO EXCELL";
			echo '<pre>'; 
			print_r($db);
			echo '<pre>'; 


			
			echo "COMPARE EXCELL TO DB";
			echo '<pre>'; 
			print_r($excel);
			echo '<pre>'; 
		
			die();			
			
			if($insert){
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-ok"></span> Data Berhasil di Import ke Database');
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-remove"></span> Terjadi Kesalahan');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			echo "Tidak ada file yang masuk";
		}
	}


	public function import_excel(){
		if (isset($_FILES["fileExcel"]["name"])) {
			$path = $_FILES["fileExcel"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);

			


			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();	
			
				for($row=2; $row<=$highestRow; $row++)
				{
					$excelDate = 	$worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$miliseconds = ($excelDate - (25567 + 2)) * 86400 * 1000;
					$seconds = $miliseconds / 1000;

					$fixDate = date("Y-m-d", $seconds);
		
					$data[$row]['kode'] = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
					$data[$row]['merk'] = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$data[$row]['tanggal'] = $fixDate;
					$data[$row]['harga'] = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$data[$row]['aktiva'] = $worksheet->getCellByColumnAndRow(4, $row)->getValue();

				}
			}

		
			foreach ($data as $key => $item) {
				
				$this->db->query("UPDATE fxa_asset 
				SET  asset_name  = '".$item['merk']."',
				asset_start_date = '".$item['tanggal']."',
				asset_po_date =    '".$item['tanggal']."',
				asset_cost =       '".$item['harga']."',
				asset_total_cost = '".$item['harga']."',
				asset_short_name = '".$item['aktiva']."'
				WHERE asset_code = '".$item['kode']."';				
				");

				debug_db();
				

			}
			
			die();
			
			if($insert){
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-ok"></span> Data Berhasil di Import ke Database');
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-remove"></span> Terjadi Kesalahan');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			echo "Tidak ada file yang masuk";
		}
	}



	public function update_lokasi(){
		if (isset($_FILES["fileExcel"]["name"])) {
			$path = $_FILES["fileExcel"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);

			


			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();	
			
				for($row=2; $row<=$highestRow; $row++)
				{
					$excelDate = 	$worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$miliseconds = ($excelDate - (25567 + 2)) * 86400 * 1000;
					$seconds = $miliseconds / 1000;

					$fixDate = date("Y-m-d", $seconds);
		
					$data[$row]['kode'] = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
					$data[$row]['lokasi'] = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$data[$row]['workarea'] =  $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$data[$row]['building'] = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$data[$row]['floor'] = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$data[$row]['room'] = $worksheet->getCellByColumnAndRow(5, $row)->getValue();

				}
			}

			

		
			foreach ($data as $key => $item) {
				
				
				$this->db->query("

					UPDATE fxa_asset 
					SET asset_location_id = '".$item['lokasi']."',
					asset_work_area_id = '".$item['workarea']."',
					asset_building_id = '".$item['building']."',
					asset_floor_id = '".$item['floor']."',
					asset_room_id = '".$item['room']."'
					Where asset_assettype_id = '6' 
					and is_delete = '0'
					and asset_code =  '".$item['kode']."';
					");

			
				

			}
			
		
			if($insert){
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-ok"></span> Data Berhasil di Import ke Database');
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-remove"></span> Terjadi Kesalahan');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			echo "Tidak ada file yang masuk";
		}
	}



}

?>