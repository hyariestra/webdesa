<?php 

class M_beritabe extends CI_model

{

	var $table = 'berita'; //nama tabel dari database
    var $column_order = array(1 => 'a.judul',6 => 'a.tanggal',7 => 'a.dibaca'); //field yang ada di table user
    var $column_search = array('judul'); //field yang diizin untuk pencarian 
    var $order = array('a.tanggal' => 'desc'); // default order 


    function get_berita()
    {
    	return $this->db->query("SELECT * FROM berita");


    }


    function trimelement($data,$panjang)
    {

       $max_length = $panjang;

       if (strlen($data) > $max_length)
       {
        $offset = ($max_length - 3) - strlen($data);
        $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
    }

    return $data;
}



function simpan_berita($param)
{

 $this->db->trans_begin();




 $result = $this->db->insert('berita',$param);



 if ($result == false){
  $this->db->trans_rollback();

}else{
  $this->db->trans_commit();

}

return [
  'result' => $result
];
}   

function delete_berita($id)
{
    return $this->db->query("DELETE FROM berita where id_berita = ".$id." ");
}

function ubah_berita($param,$id)
{

    $this->db->trans_begin();




    $this->db->where('id_berita',$id);
    $result = $this->db->update('berita',$param);



    if ($result == false){
        $this->db->trans_rollback();

    }else{
        $this->db->trans_commit();

    }

    return [
        'result' => $result
    ];
}





function reArrayFiles(&$file_post) {

 $file_ary = array();
 $file_count = count($file_post['name']);
 $file_keys = array_keys($file_post);

 for ($i=0; $i<$file_count; $i++) {
  foreach ($file_keys as $key) {
   $file_ary[$i][$key] = $file_post[$key][$i];
}
}

return $file_ary;
}


public function raw_db()
{
$this->db->select('*,a.status AS rilis');
 $this->db->from('berita a');
 $this->db->join('admin b', 'b.id_admin = a.id_user', 'left');
 $this->db->join('kategori c', 'c.id_kategori = a.id_kategori', 'left');
 $this->db->join('ref_desa_users d', 'b.id_admin = d.id_admin', 'left');
 $this->db->join('ref_desa e', 'e.id_desa = d.id_desa', 'left');
$data =  $this->db->where('e.desa_kode_domain', getNameDomain() );


    return $data;


}


private function _get_datatables_query()
{


    $this->raw_db();



 $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if($i===0) // looping awal
                {
                	$this->db->group_start(); 
                	$this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                	$this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                	$this->db->group_end(); 
            }
            $i++;
        }
 
        if(isset($_POST['order'])) 
        {
        	$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
        	$this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
    	$this->_get_datatables_query();
    	if($_POST['length'] != -1)
    		$this->db->limit($_POST['length'], $_POST['start']);
    	$query = $this->db->get();
     
     
    	return $query->result();
    }

    function count_filtered()
    {
    	$this->_get_datatables_query();
    	$query = $this->db->get();
    	return $query->num_rows();
    }

    public function count_all()
    {

        $this->raw_db();
        $r = $this->db->count_all_results();

        return $r;
    }

    public function getTagName($value)
    {
    	$data =  $this->db->query(" SELECT nama_tag FROM tags WHERE id_tag = ".$value." ")->row_array();
    	
    	return $data['nama_tag'];

    }


        public function get_detail_berita($id='')
    {
        $data = $this->db->query("SELECT * FROM berita a
            LEFT JOIN `kategori` b ON b.`id_kategori` = a.`id_kategori`
            WHERE a.id_berita = ".$id." ")->row_array();
        return $data;

    }


}


?>