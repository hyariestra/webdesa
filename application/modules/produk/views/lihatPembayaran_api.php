<style>
  .block-inp {
    margin-bottom: 10px;
  }

  .pilihAlamat {
    margin-bottom: 10px;
  }
</style>
<div class="row">

  <div class="col-md-12">

    <div class="box">

      <div class="box-header with-border">

        <h3 class="box-title">Pembayaran</h3>



      </div>




      <div class="box-body">
        <div class="pilihAlamat">
          <p>Pilih Alamat Pengiriman</p>
          <select name="" id="input" class="form-control" required="required">
            <option value="">::Pilih Alamat Tujuan::</option>
            <option value="">2</option>
            <option value=""> + Tambah Alamat Baru</option>
          </select>
        </div>

        <form method="post" class="form-alamat" action="<?php echo $urlAlamat ?>">
          <div class="tambahalamat">
            <div class="block-inp">
              <b>Tambah Alamat Pengiriman</b>
            </div>
            <div class="" style="">
              <div class="block-inp">
                <input class="form-control" type="text" name="nama_alamat" placeholder="Simpan Sebagai? ex: Alamat Rumah, Alamat Kantor, Dll" required="">
              </div>
              <div class="block-inp">
                <input class="form-control" type="text" name="nama_penerima" placeholder="Nama Penerima" required="">
              </div>
              <div class="block-inp">
                <input class="form-control" type="text" name="no_hp" placeholder="No Handphone Penerima" required="">
              </div>
              <div class="block-inp">
                <textarea class="form-control" name="alamat_lengkap" placeholder="Alamat lengkap" required=""></textarea>
              </div>

              <div class="block-inp prov">



              </div>


              <div class="block-inp kabkot">

                <select onchange="load_kecamatan(this)" id="kabkot" class="select2 form-control" name="id_kota">
                </select>
              </div>


              <div class="block-inp kecamatan">
                <select id="kecamatan" class="select2 form-control" name="id_kecamatan">
                </select>
              </div>

            </div>
          </div>

          <button type="submit" class="btn btn-success">
            Simpan Alamat</span>
          </button>

        </form>


      </div>





    </div>




  </div>


</div>


<script>
  $(document).ready(function() {

    load_provinsi();

  });




  function load_provinsi() {

    $('select[name=id_prov]').append('<option value="">--Provinsi--</option>');

    $.ajax({
      dataType: "json",
      url: "<?php echo base_url('globalfunc/get_provinsi') ?>",
      data: {

      },
      success: function(result) {

        var html = '';
        html += "<select onchange='load_kabupaten(this)' id='provinsi' class='select2 form-control' name='id_prov'>";
        html += "<option value=''>::Pilih Provinsi::</option>";

        $.each(result.results, function(key, val) {

          html += "<option value=" + val.province_id + ">" + val.province + "</option>";
        });

        html += "</select>";

        $('.prov').append(html);
      }
    });
  }


  function load_kabupaten() {
    var prov_id = $('#provinsi').val();


    $('select[name=id_kota]').html("");
    $('select[name=id_kota]').append('<option value="">--Pilih Kabupaten / Kota--</option>');

    $.ajax({
      dataType: "json",
      url: "<?php echo base_url('globalfunc/get_kabupaten') ?>",
      data: {
        prov_id: prov_id
      },
      success: function(result) {

        $.each(result.results, function(key, val) {

          $('#kabkot').append("<option value=" + val.city_id + ">" + val.city_name + "</option>");
        });
        $('#kabkot').trigger('change');
      }
    });
  }

  function load_kecamatan() {

    var kabkot_id = $('#kabkot').val();

    console.log(kabkot_id);

    $('#kecamatan').html("");
    $('#kecamatan').append('<option value="">--Pilih Kecamatan--</option>');

    $.ajax({
      dataType: "json",
      url: "<?php echo base_url('globalfunc/get_kecamatan') ?>",
      data: {
        kabkot_id: kabkot_id
      },
      success: function(result) {

        $.each(result.results, function(key, val) {
          $('#kecamatan').append("<option  value=" + val.subdistrict_id + ">" + val.subdistrict_name + "</option>");
        });
        $('#kecamatan').trigger('change');
      }
    });
  }


  $('.form-alamat').ajaxForm({
    dataType: 'json',
    beforeSubmit: function(formData, jqForm, options) {



    },
    success: processJson,
    error: processJsonError
  });


  function processJsonError(result) {
    result = result.responseJSON;
    processJson(result, true);
  }

  function processJson(result) {


    var row = "produk/pembayaran";

    var base = '<?php echo base_url() ?>';
    var url = row;

    var base_url = base + url;


    new Noty({
      text: result.message,
      type: result.status_code,
      timeout: 3000,
      theme: 'semanticui'
    }).show();

    if (result.status == 201) {
      window.location = base_url;

    }
  }
</script>