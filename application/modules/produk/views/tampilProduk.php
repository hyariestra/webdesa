<style>
.product-grid{font-family:Raleway,sans-serif;text-align:center;padding:0 0 72px;border:1px solid rgba(0,0,0,.1);overflow:hidden;position:relative;z-index:1}
.product-grid .product-image{position:relative;transition:all .3s ease 0s}
.product-grid .product-image a{display:block}
.product-grid .product-image img{width:100%;height:300px;object-fit: cover;}
.product-grid .pic-1{opacity:1;transition:all .3s ease-out 0s}

.product-grid .pic-2{opacity:0;position:absolute;top:0;left:0;transition:all .3s ease-out 0s}
.product-grid .social{width:150px;padding:0;margin:0;list-style:none;opacity:0;transform:translateY(-50%) translateX(-50%);position:absolute;top:60%;left:50%;z-index:1;transition:all .3s ease 0s}
.product-grid:hover .social{opacity:1;top:50%}
.product-grid .social li{display:inline-block}
.product-grid .social li a{color:#fff;background-color:#333;font-size:16px;line-height:40px;text-align:center;height:40px;width:40px;margin:0 2px;display:block;position:relative;transition:all .3s ease-in-out}
.product-grid .social li a:hover{color:#fff;background-color:#ef5777}
.product-grid .social li a:after,.product-grid .social li a:before{content:attr(data-tip);color:#fff;background-color:#000;font-size:12px;letter-spacing:1px;line-height:20px;padding:1px 5px;white-space:nowrap;opacity:0;transform:translateX(-50%);position:absolute;left:50%;top:-30px}
.product-grid .social li a:after{content:'';height:15px;width:15px;border-radius:0;transform:translateX(-50%) rotate(45deg);top:-20px;z-index:-1}
.product-grid .social li a:hover:after,.product-grid .social li a:hover:before{opacity:1}
.product-grid .product-discount-label,.product-grid .product-new-label{color:#fff;background-color:#ef5777;font-size:12px;text-transform:uppercase;padding:2px 7px;display:block;position:absolute;top:10px;left:0}
.product-grid .product-discount-label{background-color:#333;left:auto;right:0}
.product-grid .rating{color:#FFD200;font-size:12px;padding:12px 0 0;margin:0;list-style:none;position:relative;z-index:-1}
.product-grid .rating li.disable{color:rgba(0,0,0,.2)}
.product-grid .product-content{background-color:#fff;text-align:center;padding:12px 0;margin:0 auto;position:absolute;left:0;right:0;bottom:-27px;z-index:1;transition:all .3s}
.product-grid:hover .product-content{bottom:0}
.product-grid .title{font-size:13px;font-weight:400;letter-spacing:.5px;text-transform:capitalize;margin:0 0 10px;transition:all .3s ease 0s}
.product-grid .title a{color:#828282}
.product-grid .title a:hover,.product-grid:hover .title a{color:#ef5777}
.product-grid .price{color:#333;font-size:17px;font-family:Montserrat,sans-serif;font-weight:700;letter-spacing:.6px;margin-bottom:8px;text-align:center;transition:all .3s}
.product-grid .price span{color:#999;font-size:13px;font-weight:400;text-decoration:line-through;margin-left:3px;display:inline-block}
.product-grid .add-to-cart{color:#000;font-size:13px;font-weight:600}
@media only screen and (max-width:990px){.product-grid{margin-bottom:30px}
}



.wrap-search{
    padding: 10px;
}
</style>

<div class="box">
    <div class="row">

        <div class="col-md-12">

        <div class="box-header with-border">

<h3 class="box-title">Produk</h3>

<div class="box-tools pull-right">

    <span class="btn btn-sm search-btn"><i class="fa fa-search"></i></span>              

</div>

</div>




<div class="col-md-12 boxArea">

    <div class="wrap-search">

    <div class="form-group">
        <label for="exampleInputEmail1">Nama Produk</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="masukan nama atau kode produk">
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Kategori Produk</label>
        <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-success"><i class="fa fa-search"></i></button>
                    </div><!-- /btn-group -->
                    <input readonly type="text"  class="form-control">
                    <input readonly type="hidden"  name="id_kategori" class="form-control">
                  </div>
    </div>
    </div>

</div>                                      


        </div>
    
    </div>
</div>

<div class="box">

    <div style="padding-top: 20px;" class="row">
                
    

    <div style="margin-bottom: 20px;" class="col-md-3 col-md-offset-9">
    <div class="form-group">
                      <label style="line-height: 2.5;" for="sort" class="col-sm-3 control-label">Urutkan:</label>
                      <div class="col-sm-9">
                     
                     <select name="" id="input" class="form-control" required="required">
                         <option value="">Terbaru</option>
                         <option value="">Terlaris</option>
                         <option value="">Harga Tertinggi</option>
                         <option value="">Harga Terendah</option>
                     </select>
                     
                      </div>
                    </div>
        </div>



<div class="col-md-12">

    <?php
    
    
    foreach ($item as $key => $value) {
        

    ?>
    <div style="margin-bottom: 20px;" class="col-md-3 col-sm-6">
            <div class="product-grid">
                <div class="product-image">
                    <a href="<?php echo base_url($value['url']) ?>">
                        <img class="pic-1" src="<?php echo base_url('asset/'.$value['gambar']); ?>">
                    
                    </a>
                    <ul class="social">
                        <li><a href="<?php echo base_url($value['url']) ?>" data-tip="liat detail produk"><i class="fa fa-search"></i></a></li>
                        <!-- <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li> -->
                    </ul>
           
                    <?php echo $value['sales'] ?>
                   <?php echo $value['diskon'] ?>
                </div>
                <!-- <ul class="rating">
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star"></li>
                    <li class="fa fa-star disable"></li>
                </ul> -->
                <div class="product-content">
                    <h3 class="title"><a href="<?php echo base_url($value['url']) ?>"><?php echo $value['nama_produk'] ?></a></h3>
                    <div class="price"><?php echo $value['harga_produk']; ?>
                        <span><?php echo $value['harga_potongan']; ?></span>
                    </div>
                  
                </div>
            </div>
        </div>
    
        <?php } ?>

    </div>
    
</div>
</div>
<div class="pagination-style">
            <?= $pagination; ?>
            </div>
<script>


$('.boxArea').hide();


        $('.search-btn').click(function() {

$('.boxArea').slideToggle('slow');

});
</script>