<style>
  .block-inp {
    margin-bottom: 10px;
  }

  .pilihAlamat {
    margin-bottom: 10px;
  }

  .expedisi_kosong {
    padding: 5px;
    background-color: #edb1b1;
    margin-top: 2px;
  }

  .box-alamat {
    border-style: solid;
    border-color: #00000017;
    padding: 10px;
    text-transform: uppercase;
  }

  .metode-warning {
    background-color: #fff1b9;
    padding: 10px;
  }
</style>
<div class="row">

  <div class="col-md-12">

    <div class="box">

      <div class="box-header with-border">

        <h3 class="box-title">Pembayaran</h3>



      </div>

      <div class="box-body">
        <div class="pilihAlamat">
          <p><b>Pilih Alamat Pengiriman</b></p>
          <select onchange="pilihAlamat()" name="" id="alamat-opt" class="form-control" required="required">
            <option value="">::Pilih Alamat Tujuan::</option>


            <?php

            foreach ($alamat as $item) {
            ?>
              <option value="<?php echo $item['id_alamat'] ?>"><?php echo $item['nama_alamat'] ?> (<?php echo $item['nama_penerima'] ?>)</option>

            <?php } ?>

            <option value="addAlamat"> + Tambah Alamat Baru</option>
          </select>
        </div>

        <div class="tambahalamat">
          <form method="post" class="form-alamat" action="<?php echo $urlAlamat ?>">
            <div class="block-inp">
              <b>Tambah Alamat Pengiriman</b>
            </div>
            <div class="" style="">
              <div class="block-inp">
                <input class="form-control" type="text" name="nama_alamat" placeholder="Simpan Sebagai? ex: Alamat Rumah, Alamat Kantor, Dll" required="">
              </div>
              <div class="block-inp">
                <input class="form-control" type="text" name="nama_penerima" placeholder="Nama Penerima" required="">
              </div>
              <div class="block-inp">
                <input class="form-control" type="text" name="no_hp" placeholder="No Handphone Penerima" required="">
              </div>

              <div class="block-inp">
                <textarea class="form-control" name="alamat_lengkap" placeholder="Alamat lengkap" required=""></textarea>
              </div>

              <div class="block-inp prov">

                <select style="width: 100%;" onchange="load_kabupaten(this)" id="provinsi" class="select2 form-control" name="id_prov">

                  <option value="">--Pilih Provinsi--</option>

                  <?php foreach ($provinsi as $key => $value) : ?>

                    <option value="<?php echo $value['id']; ?>"><?php echo $value['nama']; ?></option>

                  <?php endforeach ?>

                </select>
              </div>

              <div class="block-inp kabkot">

                <select style="width: 100%;" onchange="load_kecamatan(this)" id="kabkot" class="select2 form-control" name="id_kota">

                  <option value="">--Pilih Kabupaten / Kota--</option>

                </select>
              </div>

              <div class="block-inp kecamatan">
                <select style="width: 100%;" id="kecamatan" class="select2 form-control" name="id_kecamatan">
                  <option value="">--Pilih Kecamatan--</option>
                </select>
              </div>

            </div>

            <button type="submit" class="btn btn-success">
              Simpan Alamat</span>
            </button>

          </form>
        </div>

        <div class="setAlamatPengiriman">


        </div>


      </div>
    </div>
  </div>
</div>

<form method="post" class="form-pembayaran" action="<?php echo $urlPembayaran ?>">
<div class="box">
  <div class="box-body">
    <div class="col-md-2">
      <b> Kurir Pengiriman</b>
    </div>

    <div class="col-md-5">

      <select onchange="load_paket(this)" name="input-cour" id="input-cour" class="form-control" required="required">

        <option value="">::Pilih Ekspedisi::</option>
        <?php

        foreach ($kurir as $item) {

        ?>

          <option value="<?= $item['rajaongkir'] ?>"><?= $item['nama'] ?></option>


        <?php } ?>

      </select>

    </div>

    <div class="col-md-5">
      <select onchange="set_harga(this)" name="input-paket" id="input-paket" class="form-control" required="required">
        <option value="">::Pilih Paket Pengiriman::</option>
      </select>


      <div class="expedisi_kosong"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Paket pengiriman tidak ditemukan, silahkan pilih kurir lain</div>
    </div>
  </div>


</div>


<div class="box">
  <div class="box-body">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Produk</th>
          <th>Jumlah</th>
          <th class="text-center">Harga</th>
          <th class="text-center">Total Harga</th>

        </tr>
      </thead>
      <tbody id="goods_data">


        <?php


        foreach ($produk as $key => $item) {


        ?>


          <tr class="row_data">
            <td class="col-sm-8 col-md-6">
              <div class="media">
                <a class="thumbnail pull-left" href="<?= base_url($item['url_produk']); ?>"> <img class="media-object" src="<?php echo base_url('asset/' . $item['gambar']) ?>" style="width: 72px; height: 72px;"> </a>
                <div style="padding-left: 10px;" class="media-body">
                  <h4 class="media-heading"><a href="<?php echo base_url($item['url_produk']); ?>"><?php echo $item['nama_produk'] ?></a>
                    <br>
                    <input name="detail[<?php echo $item['id'] ?>][id_produk]" value="<?php echo $item['id'] ?>" type="hidden">
                    <input name="detail[<?php echo $item['id'] ?>][nama_produk]" value="<?php echo $item['nama_produk'] ?>" type="hidden">
                    <input name="detail[<?php echo $item['id'] ?>][qty]" value="<?php echo $item['qty'] ?>" type="hidden">
                    <input name="detail[<?php echo $item['id'] ?>][harga_produk]" value="<?php echo $item['harga_produk'] ?>" type="hidden">
                    <input name="detail[<?php echo $item['id'] ?>][berat_produk]" value="<?php echo $item['berat_produk'] ?>" type="hidden">
                  </h4>
                  <h5 class="media-heading"> kategori <a href="<?php echo base_url($item['url_kategori'])  ?>"><?php echo $item['nama_kategori'] ?></a></h5>

                </div>
              </div>
            </td>
            <td class="col-sm-1 col-md-1" style="text-align: center">
              <?php echo $item['qty'];  ?>
            </td>
            <td class="col-sm-2 col-md-2 text-center"><strong><?php echo $item['harga_rupiah'] ?></strong></td>
            <td class="col-sm-2 col-md-2 text-center"><strong>
                <div class="totalpricehtml"><?php echo $item['harga_total_rupiah'] ?>
                  <input class="inp-harga-total" value="<?= $item['harga_total'] ?>" type="hidden">
                </div>
              </strong></td>

          </tr>

        <?php } ?>

      </tbody>
    </table>
  </div>
</div>



<div class="box">
  <div class="box-body">
    <div class="table-responsive">
      <table class="table">
        <tbody>
          <tr>
            <th style="width:50%">Subtotal:</th>
            <td style="text-align: right;"> <?= rupiah($subTotal) ?> </td>
          </tr>
          <tr>
            <th>Ongkos Kirim</th>
            <td id="ongkirPrice" style="text-align: right;"></td>
          </tr>

          <tr>
            <th>Grand Total:</th>
            <td id="grand_tot" style="text-align: right;"></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div>
    <input id="etd_ekspedisi" name="etd_ekspedisi" class="inputekspedisi form-control" type="hidden">
    <input id="paket_ekspedisi" name="paket_ekspedisi" class="inputekspedisi form-control" type="hidden">
    <input id="harga_ekspedisi" name="harga_ekspedisi" class="inputekspedisi form-control" type="hidden">
    <input id="berat_ekspedisi" value="<?= $beratTotal ?>" name="berat_ekspedisi" class="form-control" type="hidden">
    <input id="destinasi_ekspedisi" name="destinasi_ekspedisi" class="form-control" type="hidden">
    <input id="alamat_pelanggan" name="alamat_pelanggan" class="form-control" type="hidden">
    <input id="sub_total" value="<?php echo $subTotal ?>" name="sub_total" class="form-control" type="hidden">
  </div>


</div>


<div class="box">
  <div class="box-body">
    <b>Metode Pembayaran</b>
    <div style="margin-top: 10px;" class="metode-warning">
      <i> Belum dapat menyelesaikan pesanan, silahkan lengkapi alamat dan total beserta ongkos kirim terlebih dahulu.</i>
    </div>

    <div class="tf-metode row">
      <div class="col-md-6">

        <select name="metode" id="input" class="form-control" required="required">
          <option value="tf">Transfer</option>
        </select>

      </div>

      <div class="col-md-6">
        <b><span style="font-size: 25px;" id="nominal-transfer"></span></b>
      </div>
      
      <div style="margin-top: 5px;" class="col-md-12">        
          <button type="submit" class="btn btn-success">
            Lanjut Ke Pembayaran  <i class="fa fa-money" aria-hidden="true"></i>
          </button>
      </div>     
    </div>
  </div>
</div>
</form>

<script>
  $(document).ready(function() {
    $('.tambahalamat').hide();
    $('.expedisi_kosong').hide();
    $('.tf-metode').hide();
  });

  function pilihAlamat() {

    var opt = $('#alamat-opt').val();

    $('#input-cour').val('');
    $('#input-paket').html('');
    $('.inputekspedisi').val('');



    if (opt == 'addAlamat') {
      $('.tambahalamat').show();

    } else {
      $('.tambahalamat').hide();


      $.ajax({
        dataType: "json",
        url: "<?php echo base_url('globalfunc/set_alamat') ?>",
        data: {
          alamat_id: opt
        },
        success: function(result) {

          setAlamat(result.alamat);

        }
      });





    }

  }

  function setAlamat(result) {


    $('.setAlamatPengiriman').html("");

    $('#destinasi_ekspedisi').val(result.id_kec_rj);
    $('#alamat_pelanggan').val(result.id_alamat);

    var html = '';
    html += '<div class="box-alamat">';
    html += '<b>Nama Penerima</b>';
    html += '<p>' + result.nama_penerima + '</p>';
    html += '<b>No HP:</b>';
    html += '<p> ' + result.no_hp + '</p>';
    html += '<b>Alamat Lengkap: </b>';
    html += '<p>' + result.alamat + '<p>';
    html += result.alamat_lengkap;
    html += '<p>KODEPOS ' + result.kodepos + '<p>';
    html += '</div>';

    $('.setAlamatPengiriman').append(html);

  }

  function load_kabupaten() {
    var prov_id = $('#provinsi').val();



    $('#kabkot').html("");
    $('#kabkot').append('<option value="">--Pilih Kabupaten / Kota--</option>');


    $.ajax({
      dataType: "json",
      url: "<?php echo base_url('globalfunc/get_kabkot') ?>",
      data: {
        prov_id: prov_id
      },
      success: function(result) {
    console.log(result.kabkot);
        $.each(result.kabkot, function(key, val) {


          $('#kabkot').append("<option value=" + val.id + ">" + val.nama + "</option>");
        });
        $('#kabkot').trigger('change');
      }
    });
  }

  function load_kecamatan() {

    var kabkot_id = $('#kabkot').val();

    $('#kecamatan').html("");
    $('#kecamatan').append('<option value="">--Pilih Kecamatan--</option>');

    $.ajax({
      dataType: "json",
      url: "<?php echo base_url('globalfunc/get_kecamatan') ?>",
      data: {
        kabkot_id: kabkot_id
      },
      success: function(result) {

        $.each(result.kecamatan, function(key, val) {



          $('#kecamatan').append("<option value=" + val.id + ">" + val.nama + "</option>");
        });
        $('#kecamatan').trigger('change');
      }
    });
  }


  function load_paket() {

    var cour_id = $('#input-cour').val();
    var dest_id = $('#destinasi_ekspedisi').val();
    var berat = $('#berat_ekspedisi').val();


    $('#input-paket').html("");
    //   $('#input-paket').append('<option value="">::Pilih Paket Pengiriman::</option>');

    $.ajax({
      dataType: "json",
      url: "<?php echo base_url('globalfunc/get_paket') ?>",
      data: {
        cour_id: cour_id,
        dest_id: dest_id,
        berat: berat,
      },
      success: function(result) {

        if (result.paket.length == 0) {
          $('.expedisi_kosong').show();
          $('.metode-warning').show();
          $('.tf-metode').hide();
        } else {
          $('.tf-metode').show();
          $('.metode-warning').hide();
        }


        $.each(result.paket, function(key, val) {



          var nominal = result.paket[key].cost[0].value;
          var etd = result.paket[key].cost[0].etd;

          var nilai = nominal + '|' + etd;


          $('#input-paket').append("<option value='" + nilai + "'>" + val.service + "</option>");
        });
        $('#input-paket').trigger('change');
      }
    });
  }


  function set_harga() {

    var text = '';
    var nilai = $('#input-paket').val();
        text = $( "#input-paket option:selected" ).text();

    if (nilai == null) {

      $('#etd_ekspedisi').val('');
      $('#harga_ekspedisi').val('');
      $('#paket_ekspedisi').val('');

    } else {


      var n = nilai.split("|");
      $('#harga_ekspedisi').val(n[0]);
      $('#etd_ekspedisi').val(n[1]);
      $('#paket_ekspedisi').val(text);
      $('.expedisi_kosong').hide();

      $('#ongkirPrice').html(formatRupiah(n[0], "Rp. "));

      hitungFunc();

    }


  }


  function hitungFunc() {
    var nom_ekspedisi = $('#harga_ekspedisi').val();
    var sub_total = $('#sub_total').val();

    var grandTotal = parseFloat(nom_ekspedisi) + parseFloat(sub_total);

    $('#grand_tot').html(formatRupiah(grandTotal, "Rp. "));
    $('#nominal-transfer').html(formatRupiah(grandTotal, "Rp. "));



  }

  $('.form-pembayaran').ajaxForm({
    dataType: 'json',
    beforeSubmit: function(formData, jqForm, options) {

    },
    success: processJson,
    error: processJsonError
  });


  $('.form-alamat').ajaxForm({
    dataType: 'json',
    beforeSubmit: function(formData, jqForm, options) {

    },
    success: processJson,
    error: processJsonError
  });


  function processJsonError(result) {
    result = result.responseJSON;
    processJson(result, true);
  }

  function processJson(result) {



    var row = result.url;
    var base = '<?php echo base_url() ?>';
    var url = row;

    var base_url = base + url;


    new Noty({
      text: result.message,
      type: result.status_code,
      timeout: 3000,
      theme: 'semanticui'
    }).show();

    if (result.status == 201) {
      window.location = base_url;

    }
  }
</script>