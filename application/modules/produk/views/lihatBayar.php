<style>
  .toggled {
    background-color: red;
  }

  .bs-callout-danger {
    border-left-color: #ce4844;
  }

  .bs-callout {
    padding: 20px;
    margin: 20px 0;
    border: 1px solid #27ae60;
    border-left-width: 5px;
    border-radius: 3px;
  }
</style>


<section class="invoice">
<div class="box">

    <!-- title row -->
    <div class="row">
      <div class="col-md-12">
        <h2 class="page-header">
          <i class="fa fa-check-circle"></i> INVOICE NUMBER <?= $invoice['kode_invoice'] ?>
          <small class="pull-right">Tanggal: <?= $invoice['tanggal_invoice'] ?></small>
        </h2>
      </div><!-- /.col -->
    </div>
    <!-- info row -->
    <div class="box-body">
    <div class="row invoice-info">
      <div class="col-md-12">
        <b>PEMBAYARAN</b>

        <p>Mohon lakukan pembayaran sejumlah <b> <?= $invoice['total_bayar'] ?> </b></p>
        <p>Pilih Metode Pembayaran:</p>
        <div class="row">


          <?php


          for ($i = 1; $i < 4; $i++) {

          ?>

            <div class="col-md-4 bix">
              <a href="#" onclick="showMetode('<?php echo $i ?>');return false;">

                <div style="background-color: #8c8f92;  padding: 10px;
                color: white;
                font-size: 30px;
                text-align: center;
                border-radius: 10px;" class="all-box box-metode-<?php echo $i ?>">
                  <i class="fa fa-university" aria-hidden="true"></i>
                  <p>Transfer Manual</p>
                </div>

              </a>
            </div>


          <?php } ?>


        </div>

      </div>


    </div>



    <?php

for ($i = 1; $i < 4; $i++) {



?>

  <div style="margin-top: 20px;" class="all-instruksi instruksi-<?php echo $i ?>">
    <div class="col-md-12">
      <div class="row">

        <div class="bs-callout bs-callout-danger" id="callout-glyphicons-empty-only">
          <h4>Bank BCA: 8025266265 <?php echo $i ?>
            <p>a/n Septy Sumaryani</p>
          
          
        </div>


      </div>
    </div>
  </div>

<?php } ?>


<div style="margin-top: 20px;" class="penting">
      <div class="col-md-12">
        <div class="row">


          <h4><b>PENTING:</b></h4>

          <ul>
            <li>Mohon lakukan pembayaran dalam 1x24 jam</li>
            <li>Sistem akan otomatis mendeteksi apabila pembayaran sudah masuk</li>
            <li>Apabila sudah transfer dan status pembayaran belum berubah, mohon konfirmasi pembayaran manual di bawah</li>
            <li>Pesanan akan dibatalkan secara otomatis jika Anda tidak melakukan pembayaran.</li>
          </ul>
        </div>
      </div>
    </div>

    </div>
    <!-- --content -->

    <div class="box-footer">
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eius voluptatum nemo ad maxime dicta ipsa dolorem pariatur debitis ducimus sed soluta, eum unde provident repellat, vitae adipisci placeat? Non, aliquid!
    </div>

 
</section><!-- /.content -->
</div>

<script>
  $('.all-instruksi').hide();

  function showMetode(id) {
    console.log(id);

    $('.all-box').css({
      "backgroundColor": "#8c8f92"
    });

    $(".box-metode-" + id).css({
      "backgroundColor": "#27ae60"
    });

    $('.all-instruksi').hide();
    $('.instruksi-'+id).show();

  }

  function lala() {

    var f = $('input[name="options"]:checked').val();
    if ($("#option1").is(":checked")) {
      $('.btnHousing').addClass("toggled");
    }

  }
</script>