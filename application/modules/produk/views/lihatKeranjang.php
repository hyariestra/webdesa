<div class="row">

    <div class="col-md-12">

        <div class="box">

            <div class="box-header">

                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th class="text-center">Harga</th>
                                <th class="text-center">Total Harga</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <tbody id="goods_data">

                            <?php

                            if (empty($produk)) {


                            ?>

                                <tr class="row_data">
                                    <td style="text-align: center;" colspan="5">
                                        <i> :: Tidak Ada Produk :: </i>
                                    </td>
                                </tr>

                            <?php } ?>



                            <?php

                            foreach ($produk as $key => $item) {


                            ?>
                                <form action="<?php echo base_url('produk/update_keranjang') ?>" class="form-kirim" method="post">
                                    <tr class="row_data">
                                        <td class="col-sm-8 col-md-6">
                                            <div class="media">
                                                <a class="thumbnail pull-left" href="<?= base_url($item['url_produk']); ?>"> <img class="media-object" src="<?php echo base_url('asset/' . $item['gambar']) ?>" style="width: 72px; height: 72px;"> </a>
                                                <div style="padding-left: 10px;" class="media-body">
                                                    <h4 class="media-heading"><a href="<?php echo base_url($item['url_produk']); ?>"><?php echo $item['nama_produk'] ?></a> <input name="id_produk[]" value="<?php echo $item['id'] ?>" type="hidden"> </h4>
                                                    <h5 class="media-heading"> kategori <a href="<?php echo base_url($item['url_kategori'])  ?>"><?php echo $item['nama_kategori'] ?></a></h5>

                                                </div>
                                            </div>
                                        </td>
                                        <td class="col-sm-1 col-md-1" style="text-align: center">
                                            <div style="width: 120px;" class="input-qty">

                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button onclick="minusButton('<?php echo $item['id'];  ?>')" type="button" class="quantity-left-minus btn btn-default btn-number" data-type="minus" data-field="">
                                                            <span class="glyphicon glyphicon-minus"></span>
                                                        </button>
                                                    </span>
                                                    <input width="20px" readonly min=1 type="text" id="quantity-<?php echo $item['id'];  ?>" name="quantity[]" class="form-control input-number" value="<?php echo $item['qty'];  ?>" min="1" max="100">
                                                    <span class="input-group-btn">
                                                        <button onclick="incrementButton('<?php echo $item['id'];  ?>')" type="button" class="quantity-right-plus btn btn-default btn-number" data-type="plus" data-field="">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </button>
                                                    </span>
                                                </div>


                                            </div>
                                        </td>
                                        <td class="col-sm-2 col-md-2 text-center"><strong><?php echo $item['harga_rupiah'] ?> <input class="price" value="<?php echo $item['harga']; ?>" type="hidden"> </strong></td>
                                        <td class="col-sm-2 col-md-2 text-center"><strong>
                                                <div class="totalpricehtml"></div> <input class="totalprice" value="" type="hidden">
                                            </strong></td>
                                        <td class="col-sm-1 col-md-1">
                                            <button onclick="delete_func(<?php echo $item['id'] ?>)" type="button" class="btn btn-danger">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                        </td>
                                    </tr>

                                <?php } ?>


                                <tr>
                                    <td>   </td>
                                    <td>   </td>

                                    <td class="text-right">
                                        <h3>Total</h3>
                                    </td>
                                    <td colspan="2" class="text-right">
                                        <h3><strong> <span id="grandTot"> </span></strong></h3> <input id="grandtotText" name="grandtot" type="hidden">
                                    </td>
                                </tr>
                                <tr>
                                    <td>   </td>
                                    <td>   </td>
                                    <td>   </td>
                                    <td>
                                        <a href="<?php echo base_url('produk') ?>" class="btn btn-warning">
                                            <span class="glyphicon glyphicon-shopping-cart"></span> Lanjut Belanja
                                        </a>
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-success">
                                            Checkout <span class="glyphicon glyphicon-play"></span>
                                        </button>
                                    </td>
                                </tr>

                                </form>


                        </tbody>
                    </table>

                </div>



            </div>

        </div>

    </div>


</div>


<script>
    $(document).ready(function() {
        hitungDet();
    });

    function minusButton(id) {
        var quantity = parseInt($('#quantity-' + id).val());

        // If is not undefined

        // Increment
        if (quantity > 0) {
            $('#quantity-' + id).val(quantity - 1);
        }


        hitungDet();

    };

    function incrementButton(id) {
        // Stop acting like a button
        // Get the field name
        var quantity = parseInt($('#quantity-' + id).val());

        $('#quantity-' + id).val(quantity + 1);

        hitungDet();

    };



    var hitungDet = function() {


        var grandTot = 0;

        $('#goods_data tr.row_data').each(function() {
            var detQty = parseInt($(this).find('.input-number').val());
            var detPrice = parseInt($(this).find('.price').val());
            var total = detQty * detPrice;
            // var isVat = $(this).find('.taxed').val();

            grandTot += total;

            $(this).find('.totalprice').val(total);
            $(this).find('.totalpricehtml').html(formatRupiah(total, "Rp. "));


        });

        $('#grandTot').html(formatRupiah(grandTot, "Rp. "));
        $('#grandtotText').val(grandTot);


    }

    function delete_func(id) {


        var url = '<?php echo base_url(); ?>';

        var user = '<?php echo $user_id; ?>';

        swal({
                title: "Are you sure?",
                text: "Anda Yakin menghapus produk ini dari keranjang",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url + 'produk/hapus_keranjang/' + user + '/' + id,
                        type: 'POST',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {

                            new Noty({
                                text: data.message,
                                type: data.status_code,
                                timeout: 3000,
                                theme: 'semanticui'
                            }).show();

                            if (data.status == 201) {
                                window.location = '<?php echo base_url('produk/keranjang') ?>';

                            }

                        }
                    });
                } else {
                    swal("Data Batal Dihapus");
                }
            });

    }

    $('.form-kirim').ajaxForm({
        dataType: 'json',
        beforeSubmit: function(formData, jqForm, options) {



        },
        success: processJson,
        error: processJsonError
    });


    function processJsonError(result) {
        result = result.responseJSON;
        processJson(result, true);
    }

    function processJson(result) {


        var row = "produk/pembayaran";

        var base = '<?php echo base_url() ?>';
        var url = row;

        var base_url = base + url;


        new Noty({
            text: result.message,
            type: result.status_code,
            timeout: 3000,
            theme: 'semanticui'
        }).show();

        if (result.status == 201) {
            window.location = base_url;

        }
    }
</script>