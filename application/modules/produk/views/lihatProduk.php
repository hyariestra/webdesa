<style type="text/css">
  .keterangan {
    padding: 10px;
  }

  img {
    max-width: 100%;
  }

  .input-qty {
    margin-bottom: 10px;
    width: 120px;
  }



  .preview-thumbnail.nav-tabs {
    border: none;
    margin-top: 15px;
  }

  .preview-thumbnail.nav-tabs li {
    width: 18%;
    margin-right: 2.5%;
  }

  .preview-thumbnail.nav-tabs li img {
    max-width: 100%;
    display: block;
  }

  .preview-thumbnail.nav-tabs li a {
    padding: 0;
    margin: 0;
  }

  .preview-thumbnail.nav-tabs li:last-of-type {
    margin-right: 0;
  }

  .tab-content {
    overflow: hidden;
  }

  .tab-content img {
    width: 100%;
    -webkit-animation-name: opacity;
    animation-name: opacity;
    -webkit-animation-duration: .3s;
    animation-duration: .3s;
  }

  .card {
    padding: 1em;
  }

  @media screen and (min-width: 997px) {


    .details {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -webkit-flex-direction: column;
      -ms-flex-direction: column;
      flex-direction: column;
    }

    .colors {
      -webkit-box-flex: 1;
      -webkit-flex-grow: 1;
      -ms-flex-positive: 1;
      flex-grow: 1;
    }

    .product-title,
    .price,
    .sizes,
    .colors {
      text-transform: UPPERCASE;
      font-weight: bold;
    }

    .checked,
    .price span {
      color: #ff9f1a;
    }

    .product-title,
    .rating,
    .product-description,
    .price,
    .vote,
    .sizes {
      margin-bottom: 15px;
    }

    .product-title {
      margin-top: 0;
    }

    .size {
      margin-right: 10px;
    }

    .size:first-of-type {
      margin-left: 40px;
    }

    .color {
      display: inline-block;
      vertical-align: middle;
      margin-right: 10px;
      height: 2em;
      width: 2em;
      border-radius: 2px;
    }

    .color:first-of-type {
      margin-left: 20px;
    }

    .add-to-cart,
    .like {
      background: #ff9f1a;
      padding: 1.2em 1.5em;
      border: none;
      text-transform: UPPERCASE;
      font-weight: bold;
      color: #fff;
      -webkit-transition: background .3s ease;
      transition: background .3s ease;
    }

    .add-to-cart:hover,
    .like:hover {
      background: #b36800;
      color: #fff;
    }

    .not-available {
      text-align: center;
      line-height: 2em;
    }

    .not-available:before {
      font-family: fontawesome;
      content: "\f00d";
      color: #fff;
    }

    .orange {
      background: #ff9f1a;
    }

    .green {
      background: #85ad00;
    }

    .blue {
      background: #0076ad;
    }

    .tooltip-inner {
      padding: 1.3em;
    }

    @-webkit-keyframes opacity {
      0% {
        opacity: 0;
        -webkit-transform: scale(3);
        transform: scale(3);
      }

      100% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
      }
    }

    @keyframes opacity {
      0% {
        opacity: 0;
        -webkit-transform: scale(3);
        transform: scale(3);
      }

      100% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
      }
    }

    /*# sourceMappingURL=style.css.map */
</style>


<div class="row">

  <div class="col-md-12">

    <div class="box">

      <div class="box-header">

        <div class="box-body">
          <div class="card">
            <div class="container-fliud">
              <div class="">
                <div class="preview col-md-7">

                  <div class="preview-pic tab-content">

                    <?php
                    $no = 1;
                    foreach ($produk['gambar'] as $key => $item) {
                      $active = $no == 1 ? 'active' : '';
                    ?>
                      <div class="tab-pane <?php echo $active ?>" id="pic-<?php echo $no ?>"><img src="<?php echo base_url('asset/' . $item['gambar']) ?>" /></div>

                    <?php $no++;
                    } ?>

                  </div>

                  <ul class="preview-thumbnail nav nav-tabs">

                    <?php
                    $noBawah = 1;
                    foreach ($produk['gambar'] as $key => $item) {

                      $actived = $noBawah == 1 ? 'class=active' : '';
                    ?>
                      <li <?php echo $actived ?>><a data-target="#pic-<?php echo $noBawah ?>" data-toggle="tab"><img src="<?php echo base_url('asset/' . $item['gambar']) ?>" /></a></li>

                    <?php $noBawah++;
                    } ?>
                  </ul>

                </div>
                <div class="details col-md-5">
                  <h3 class="product-title"><?php echo $produk['nama_produk'];  ?> [<?php echo $produk['kode_produk'];  ?>]</h3>
                  <h4 class="price">Harga: <span><?php echo $produk['harga_produk'];  ?></span></h4>
                  <h4 class="price">Berat: <span><?php echo $produk['berat_produk'];  ?> GRAM</span></h4>
                  <h5 style="display: none;" class="sizes">sizes:
                    <span class="size" data-toggle="tooltip" title="small">s</span>
                    <span class="size" data-toggle="tooltip" title="medium">m</span>
                    <span class="size" data-toggle="tooltip" title="large">l</span>
                    <span class="size" data-toggle="tooltip" title="xtra large">xl</span>
                  </h5>
                  <h5 style="display: none;" class="colors">colors:
                    <span class="color orange not-available" data-toggle="tooltip" title="Not In store"></span>
                    <span class="color green"></span>
                    <span class="color blue"></span>
                  </h5>
                  <form action="<?php echo base_url($url) ?>" class="form-kirim" method="post">
                    <div class="input-qty">

                      <div class="input-group">
                        <span class="input-group-btn">
                          <button type="button" class="quantity-left-minus btn btn-default btn-number" data-type="minus" data-field="">
                            <span class="glyphicon glyphicon-minus"></span>
                          </button>
                        </span>
                        <input readonly min=1 type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                        <span class="input-group-btn">
                          <button type="button" class="quantity-right-plus btn btn-default btn-number" data-type="plus" data-field="">
                            <span class="glyphicon glyphicon-plus"></span>
                          </button>
                        </span>
                      </div>


                    </div>
                    <div class="action">
                      <button class="add-to-cart btn btn-default" type="submit">add to cart</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="keterangan">
          <?php echo $produk['keterangan_produk'];  ?>

        </div>


      </div>

    </div>
    <a href="<?php echo base_url('produk') ?>" class="btn btn-success">Kembali</a>
  </div>


</div>




<script>
  var quantitiy = 0;
  $('.quantity-right-plus').click(function(e) {

    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    var quantity = parseInt($('#quantity').val());

    // If is not undefined

    $('#quantity').val(quantity + 1);


    // Increment

  });

  $('.quantity-left-minus').click(function(e) {
    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    var quantity = parseInt($('#quantity').val());

    // If is not undefined

    // Increment
    if (quantity > 0) {
      $('#quantity').val(quantity - 1);
    }
  });

  $('.form-kirim').ajaxForm({
    dataType: 'json',
    beforeSubmit: function(formData, jqForm, options) {



    },
    success: processJson,
    error: processJsonError
  });


  function processJsonError(result) {
    result = result.responseJSON;
    processJson(result, true);
  }

  function processJson(result) {


    var row = "produk/keranjang";

    var base = '<?php echo base_url() ?>';
    var url = row;

    var base_url = base + url;


    new Noty({
      text: result.message,
      type: result.status_code,
      timeout: 3000,
      theme: 'semanticui'
    }).show();

    if (result.status == 201) {
      window.location = base_url;

    }
  }
</script>