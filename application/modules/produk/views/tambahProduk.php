<style type="text/css">
	.container1 input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}
	.delete{
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>




<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title">Produk</h3>

				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">

						



						<div class="form-group">

							<label>Nama Produk</label>

							<input  type="text" name="nama_produk" class="nama_produk_class form-control">

						</div>

		
						<div class="form-group">

							<label>kategori</label>

							<input id="kategoris" data-toggle="modal" data-target="#myModal" readonly=""  type="text" class="form-control">

							<input name="id_kategori" id="id_kategorinama" type="hidden">
						</div>

						<div class="form-group">

							<label>Kode Produk</label>

							<input type="text" name="kode_produk" class="form-control">

						</div>

						<div class="form-group">

							<label>Stok</label>

							<input type="number" name="stok_produk" class="form-control">

						</div>

						<div class="form-group">

							<label>Berat (Gram)</label>

							<input type="number" name="berat_produk" class="form-control">

						</div>

						<div class="form-group">

							<label>Harga</label>

							<input id="harga_produk" type="number" name="harga_produk" class="form-control">

						</div>

						<div class="form-group">

							<label>Diskon (%)</label>

							<input  id="diskon" type="number" name="diskon" class="form-control">

						</div>


						<div class="form-group">

							<label>Harga Setelah Diskon</label>

							<input id="harga_diskon" readonly="" type="number" name="harga_diskon" class="form-control">

						</div>

						<div class="form-group">

							<label>Deskripsi</label>

							<textarea name="keterangan_produk" class="form-control" id="file-manager"></textarea>

						</div>

						<div class="form-group">

							<label>Gambar</label>

						</div>

						<div class="form-group">
							<button  type="button" class="repeat-add btn btn-success">Tambah Gambar</button>
						</div>

						<div class="form-group">
							<table class="table table-bordered table-hover" style="width: 100%">

								<tbody>

									<tr role="row1">

										<td>

											<div class="input-group">

												<span class="input-group-addon">
													<input class="thumbClass" value="1" name="thumb[0]" title="jadikan Tumbnail" type="checkbox" aria-label="...">
												</span>
												<input  accept="image/*" class="form-control" readonly="" type="file" name="gambar[]">

											</div>

										</td>


									</tr>
									<tr id="total_row">

									</tr>

								</tbody>

							</table>
						</div>

					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>

			</div>

		</div>		

	</div>

</div>

<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Kategori</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-hover" id="">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Kategori</th>
							<th>Pilih</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;

						foreach ($kategori as $key => $value) { 
							echo "<tr>";
							echo	 "<th>".$no."</th>";
							echo	 "<th>".$value['kategori']."</th>";
							echo	 "<th>";
							echo	"</th>";
							echo "</tr>";	

							if(!empty($value['subkategori']))
							{

								$noSub = 1;
								foreach ($value['subkategori'] as $key => $value) { 
									
									echo "<tr>";
									echo  "<td> &nbsp; &nbsp;  &nbsp; &nbsp;".$noSub." <input type='hidden' value=".$value['id_subkategori']."></td>";
									echo  "<td> &nbsp; &nbsp;  &nbsp; &nbsp;".$value['nama_subkategori']."</td>";
									echo "<td>";
									echo "<button onclick='chosecategory(this)'  type='button' data-toggle='modal' data-target='#myModal' class='btn btn-xs btn-warning'> <span class='glyphicon glyphicon-plus-sign'></span>";
									echo "</button>";
									echo "</td>";
									echo "</tr>";	
									$noSub++;
								}
							}


							$no++;
						 } ?>
						
						 </tbody>

					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<script>


		function chosecategory(obj)
		{
			var idpelanggan = $(obj).parent().parent().find("td").eq(0).find("input:first").val();
			console.log(idpelanggan);
			namapelanggan = $(obj).parent().parent().find("td").eq(1).text();

			$("#kategoris").val(namapelanggan);
			$("#id_kategorinama").val(idpelanggan);

			$("#myModal").modal("remove");
		}



		$( "#diskon" ).keyup(function() {

			var dInput = this.value;
			var harga_produk = $('#harga_produk').val();
			var harga_diskon = '';

			var diskon  = (dInput * harga_produk) / 100;

			if (dInput!='') {
				var harga_diskon = harga_produk - diskon;

			}

			$('#harga_diskon').val(harga_diskon);

		});

		$(document).on('click', '.repeat-remove', function(e){

			e.preventDefault();

			$(this).closest('tr').remove();



		});   
		var lanjut = 0;

		$(".repeat-add").click(function(e){

			e.preventDefault();


			lanjut = lanjut + 1;

			var masukan = "<tr role='row"+lanjut+"'>";

			masukan += "<td>";


			masukan += "<div class='input-group'>";

			masukan += "<span class='input-group-addon'>";
			masukan += "<input value='1' name='thumb["+lanjut+"]' class='thumbClass' title='jadikan Tumbnail' type='checkbox' aria-label='...'>";
			masukan += "</span>";

			masukan += "<input  accept='image/*' class='form-control' readonly='' type='file' name='gambar[]'>";


			masukan += "<span class='input-group-btn'>";

			masukan +=  "<a class='repeat-remove btn btn-warning'  href='#modal-id'><i class='fa fa-trash' aria-hidden='true'></i></a>";

			masukan += "</span>";

			masukan += "</div>";

			masukan += "</td>";
			masukan+= "</tr>";

			$('#total_row').before(masukan);         





		});  




		$('.form-kirim').ajaxForm({ 
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){

				var check = $('.thumbClass:checkbox:checked').length;
				console.log(check);
				if (check > 1) {
					alert('Maksimal satu thumbnail');
					return false;
				}

				if (check  == "") {
					alert('Pilih Satu Thumbnail');
					return false;
				}

			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			console.log(result);

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('produkbe') ?>';

			}
		}

		$( ".nama_produk_class" ).keyup(function() {

			var nama = this.value;

			var str = this.value;
			str = str.replace(/\s+/g, '-').toLowerCase();
			

			$('.slug_class').val(str);


		});


	</script>