<?php

class M_produk extends CI_model

{



    function jumlah_data($cari = '', $kategori = '')

    {

        $this->db->select('*');
        $this->db->from('produk a');
        $this->db->join('kategori_produk b', 'a.id_kategori = b.id_kategori', 'left');
        $this->db->join('produk_gambar c', 'c.id_produk = a.id_produk', 'left');
        $this->db->where('c.is_thumb', 1);


        if (!empty($kategori)) {
            $this->db->where('d.slug_kategori', $kategori);
        }

        if (!empty($cari['key'])) {
            $this->db->like('a.judul', $cari['key'], 'both');
        }

        $this->db->group_by('a.id_produk');



        $data = $this->db->get()->num_rows();

        return $data;
    }


    function simpan_keranjang($param)
    {

        $this->db->trans_begin();


        $result = $this->db->insert('keranjang_produk', $param);

        $insert_id = $this->db->insert_id();

        if ($result == false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return [
            'result' => $result,
            'lastid' => $insert_id

        ];
    }

    function simpan_alamat($param)
    {

        $this->db->trans_begin();




        $result = $this->db->insert('alamat', $param);

        $insert_id = $this->db->insert_id();

        if ($result == false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return [
            'result' => $result
        ];
    }


    function simpan_invoice($param, $detail)
    {

        $this->db->trans_begin();

        $result = $this->db->insert('trans_invoice', $param);

        $insert_id = $this->db->insert_id();


        foreach ($detail as $key => $item) {

            $params = array(
                'id_invoice' => $insert_id,
                'id_produk' => $item['id_produk'],
                'nama_beli' => $item['nama_produk'],
                'harga_beli' => $item['harga_produk'],
                'berat_beli' => $item['berat_produk'],
                'jumlah_beli' => $item['qty'],
                'total_beli' => $item['harga_produk'] * $item['qty'],


            );

            $result = $result && $this->db->insert('trans_invoice_detail', $params);
        }

        if ($result == false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return [
            'result' => $result,
            'id' => encrypt_url($insert_id),

        ];
    }




    function get_produk_list($cari, $perpage, $pagenum, $kategori = '')
    {


        if ($pagenum > 1) {
            $limit = $perpage;
            $start = $perpage * ($pagenum - 1);
        } else {
            $limit = $perpage;
            $start = 0;
        }


        $this->db->select('*');
        $this->db->from('produk a');
        $this->db->join('kategori_produk b', 'a.id_kategori = b.id_kategori', 'left');
        $this->db->join('produk_gambar c', 'c.id_produk = a.id_produk', 'left');
        $this->db->where('c.is_thumb', 1);

        if (!empty($cari['key'])) {
            $this->db->like('a.nama_produk', $cari['key'], 'both');
        }

        if (!empty($cari['sort'] == 'popular')) {
            $this->db->order_by('a.dibaca', 'desc');
        }

        if (!empty($cari['sort'] == 'share')) {
            $this->db->order_by('a.dibagikan', 'desc');
        }

        if (!empty($cari['sort'] == 'like')) {
            $this->db->order_by('a.like_berita', 'desc');
        }


        $this->db->limit($limit, $start);
        $data = $this->db->get()->result_array();


        foreach ($data as $key => $value) {


            $data[$key]['id'] = $value['id_produk'];
            $data[$key]['nama_produk'] = $value['nama_produk'];
            $data[$key]['harga_produk'] = rupiah($value['harga_produk']);
            $data[$key]['harga_potongan'] = $value['harga_potongan'] == 0 || $value['harga_potongan'] == null ? '' : rupiah($value['harga_potongan']);
            $data[$key]['diskon'] = $value['diskon'] == 0 || $value['diskon'] == null ? '' : ' <span class="product-discount-label">' . $value['diskon'] . '%</span>';
            $data[$key]['sales'] = $value['diskon'] > 0 || $value['diskon'] != null ? '' : '<span class="product-new-label">Sale</span>';
            $data[$key]['harga_potongan'] = $value['harga_potongan'] == 0 || $value['harga_potongan'] == null ? '' : rupiah($value['harga_potongan']);
            $data[$key]['url'] = 'produk/view/' . $value['id_produk'] . '/' . $value['slug'];
            $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_produk/' . rawurlencode($value['gambar_uniq']) : 'img/user.png';
        }

        return $data;
    }


    function hapus_keranjang($user, $id)
    {
        return $this->db->query("DELETE FROM `keranjang_produk` WHERE id_user = " . $user . " AND id_produk = " . $id . " ");
    }

    function hapus_keranjang_user($user)
    {
        return $this->db->query("DELETE FROM `keranjang_produk` WHERE id_user = " . $user . " ");
    }

    function get_invoice_sequence()
    {
        return $this->db->query("SELECT IFNULL(MAX(sequence),0) AS angka FROM `trans_invoice`");
    }


    function tampil_keranjang($id_user)
    {



        $data  =  $this->db->query("SELECT 
            b.slug, 
            kode_produk, nama_produk, 
            harga_produk, diskon, 
            `nama_kategori`, 
            SUM(qty) AS qty, 
            a.id_user, a.id_produk , 
            (SELECT gambar_uniq FROM `produk_gambar` WHERE id_produk = a.id_produk AND is_thumb =1 LIMIT 1) AS gambar_uniq,
            c.slug_kategori,
            b.berat_produk
            FROM `keranjang_produk`  a
            LEFT JOIN produk b ON b.`id_produk` = a.`id_produk`
            LEFT JOIN `kategori_produk` c ON c.`id_kategori` = b.`id_kategori`
            WHERE a.id_user  = '" . $id_user . "'
            GROUP BY a.id_produk")->result_array();

        foreach ($data as $key => $value) {


            $data[$key]['id'] = $value['id_produk'];
            $data[$key]['nama_produk'] = $value['nama_produk'];
            $data[$key]['harga'] = $value['harga_produk'];
            $data[$key]['harga_rupiah'] = rupiah($value['harga_produk']);
            $data[$key]['diskon'] = $value['diskon'];
            $data[$key]['nama_kategori'] = $value['nama_kategori'];
            $data[$key]['qty'] = $value['qty'];
            $data[$key]['harga_total_rupiah'] = rupiah($value['qty'] * $value['harga_produk']);
            $data[$key]['harga_total'] = $value['qty'] * $value['harga_produk'];
            $data[$key]['url_produk'] = 'produk/view/' . $value['id_produk'] . '/' . $value['slug'];
            $data[$key]['url_kategori'] = 'produk/kategori/' . $value['slug_kategori'];
            $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_produk/' . rawurlencode($value['gambar_uniq']) : 'img/user.png';
        }

        return $data;
    }


    public function tampil_invoice($id)
    {
        $query  =  $this->db->query("SELECT 
        *,d.`nama` AS prov, e.`nama` AS kab, f.`nama` AS kec, e.kodepos AS kode_pos
          FROM `trans_invoice` a
          LEFT JOIN `trans_invoice_detail` b ON a.`id_invoice` = b.`id_invoice`
          LEFT JOIN `alamat` c ON c.`id_alamat` = a.`alamat_pengiriman`
          LEFT JOIN `blw_prov` d ON d.`id` = c.`id_prov`
          LEFT JOIN `blw_kab` e ON e.`id` = c.`id_kota`
          LEFT JOIN `blw_kec` f ON f.`id` = c.`id_kecamatan`
         WHERE a.`id_invoice` = '" . $id . "' ")->result_array();


        $data = array();

        foreach ($query as $item) {

            $data[$item['id_invoice']]['id_invoice'] = $item['id_invoice'];
            $data[$item['id_invoice']]['kode_invoice'] = $item['kode_invoice'];
            $data[$item['id_invoice']]['tanggal_invoice'] = tgl_indo($item['tanggal_invoice']);
            $data[$item['id_invoice']]['tanggal_batas_bayar'] = tgl_indo($item['tanggal_batas_bayar']);
            $data[$item['id_invoice']]['sub_total'] = rupiah($item['sub_total']);
            $data[$item['id_invoice']]['biaya_pengiriman'] = rupiah($item['biaya_pengiriman']);
            $data[$item['id_invoice']]['total_bayar'] = rupiah($item['total_bayar']);
            $data[$item['id_invoice']]['nama_penerima'] = $item['nama_penerima'];
            $data[$item['id_invoice']]['provinsi'] = $item['prov'];
            $data[$item['id_invoice']]['kabupaten'] = $item['kab'];
            $data[$item['id_invoice']]['kecamatan'] = $item['kec'];
            $data[$item['id_invoice']]['alamat_lengkap'] = $item['alamat_lengkap'];
            $data[$item['id_invoice']]['kodepos'] = $item['kode_pos'];
            $data[$item['id_invoice']]['alamat'] = $item['prov'].', '.$item['kab'].', '.$item['kec'];


            $data[$item['id_invoice']]['inv_detail'][$item['id_produk']]['id_produk'] = $item['id_produk'];
            $data[$item['id_invoice']]['inv_detail'][$item['id_produk']]['nama_beli'] = $item['nama_beli'];
            $data[$item['id_invoice']]['inv_detail'][$item['id_produk']]['harga_beli'] = rupiah($item['harga_beli']);
            $data[$item['id_invoice']]['inv_detail'][$item['id_produk']]['jumlah_beli'] = $item['jumlah_beli'];
            $data[$item['id_invoice']]['inv_detail'][$item['id_produk']]['total_beli'] = rupiah($item['total_beli']);

        }



        return $data;
    }
}
