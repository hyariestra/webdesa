<?php

class Produk extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna")) {



			redirect("pengguna/login");
		}
	}


	function index()

	{
		$cari['key'] = $this->input->get('search');
		$cari['sort'] = $this->input->get('sort');

		$total_rows = $this->M_produk->jumlah_data($cari, $kategori = '');

		$config = array();
		if ($cari['key']) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config["base_url"] = site_url() . 'produk/index';
		$config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 16;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = 4;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		if ($this->uri->segment(3)) {
			$page = ($this->uri->segment(3));
		} else {
			$page = 1;
		}



		if ($cari['key'] == '') {
			$data['msg'] = 'Terdapat ' . $total_rows . ' berita';
			$data['cari'] = '';
		} else {
			$data['msg'] = 'Terdapat ' . $total_rows . ' berita dengan pencarian <b>' . $cari['key'] . '</b>';
			$data['cari'] = $cari['key'];
		}

		$products = $this->M_produk->get_produk_list($cari, $config["per_page"], $page, $kategori = '');
		$data['item'] = $products;



		$this->themeadmin->tampilkan('tampilProduk', $data);
	}


	function view($id, $seo)

	{

		authorize('lihatprodukbe');
		$produk = $this->M_produkbe->get_detail_produk($id, $seo);

		$data['url'] = 'produk/simpan_keranjang/' . $id;
		$data['produk'] = $produk[$id];

		$this->themeadmin->tampilkan('lihatProduk', $data);
	}


	public function simpan_keranjang($id)
	{
		$post = $this->input->post();
		$result = true;
		$err_msg = 'Terjadi Kesalahan Silahkan Hubungi Admin';

		$param = array(
			'id_desa' => getSettingDesa()['id_desa'],
			'id_produk' => $id,
			'id_user' => getSession(),
			'qty' => $post['quantity'],
			'tanggal' => dateNow()
		);

		$result = $this->M_produk->simpan_keranjang($param);



		if ($result['result']) {

			echo getResponseWithCallBack(201, 'Produk Berhasil Masuk ke Keranjang', encrypt_url(getSession()));
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}


	public function keranjang()
	{

		$id = getSession();

		$data['produk'] = $this->M_produk->tampil_keranjang($id);
		$data['user_id'] = $id;

		$this->themeadmin->tampilkan('lihatKeranjang', $data);
	}

	public function update_keranjang()
	{
		$post = $this->input->post();

		$result = $this->M_produk->hapus_keranjang_user(getSession());

		foreach ($post['id_produk'] as $key => $item) {


			$param = array(
				'id_desa' => getSettingDesa()['id_desa'],
				'id_produk' => $item,
				'id_user' => getSession(),
				'qty' => $post['quantity'][$key],
				'tanggal' => dateNow()
			);

			$result = $this->M_produk->simpan_keranjang($param);
		}

		if ($result) {

			echo getResponse(201, 'Lanjut Ke Pembayaram');
		} else {

			echo getResponse(400, 'Gagal Ke Pembayaram');
		}
	}


	public function simpan_alamat()
	{
		$post = $this->input->post();


		$result = true;

		if ($post['nama_alamat'] == "") {

			$err_msg[] = 'Nama Alamat Tujuan Kosong';
			$result = false;
		}
		if ($post['nama_penerima'] == "") {

			$err_msg[] = 'Nama Peneriman Kosong';
			$result = false;
		}
		if ($post['no_hp'] == "") {

			$err_msg[] = 'No Handphone Kosong';
			$result = false;
		}
		if ($post['alamat_lengkap'] == "") {

			$err_msg[] = 'Alamat Kosong';
			$result = false;
		}


		if ($post['id_prov'] == "") {

			$err_msg[] = 'Provinsi Kosong';
			$result = false;
		}

		if ($post['id_kota'] == "") {

			$err_msg[] = 'Kabupaten / Kota Kosong';
			$result = false;
		}

		if ($post['id_kecamatan'] == "") {

			$err_msg[] = 'Kecamatan Kosong';
			$result = false;
		}

		$param = array(
			'nama_alamat' => $post['nama_alamat'],
			'nama_penerima' => $post['nama_penerima'],
			'no_hp' =>  $post['no_hp'],
			'alamat_lengkap' => $post['alamat_lengkap'],
			'id_desa' => getSettingDesa()['id_desa'],
			'id_prov' => $post['id_prov'],
			'id_kota' => $post['id_kota'],
			'id_kecamatan' => $post['id_kecamatan'],
			'id_user' => getSession(),
			'insert_time' => dateNow()
		);

		if ($result) {

			$result = $this->M_produk->simpan_alamat($param);
		}


		if ($result) {

			echo getResponseWithCallBack(201, 'Alamat Berhasil ditambahkan', 'produk/pembayaran');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}

	public function simpan_invoice()
	{
		$post = $this->input->post();

		$result = true;


		if ($post['alamat_pelanggan'] == "") {

			$err_msg[] = 'Alamat Kosong';
			$result = false;
		}
		if ($post['input-cour'] == "") {

			$err_msg[] = 'Ekspedisi Kosong';
			$result = false;
		}

		if ($post['paket_ekspedisi'] == "") {

			$err_msg[] = 'Paket Ekspedisi Kosong';
			$result = false;
		}

		

	
		$param = array(
			'id_user' => getSession(),
			'id_desa' => getSettingDesa()['id_desa'],
			'sequence'=>$this->generateCodeInvoice()['notransaksi'],
			'kode_invoice'=>$this->generateCodeInvoice()['code'],
			'tanggal_invoice' => dateNow(),
			'tanggal_batas_bayar' => dateBatas(),
			'sub_total' => $post['sub_total'],
			'biaya_pengiriman' => $post['harga_ekspedisi'],
			'total_bayar' => $post['sub_total'] + $post['harga_ekspedisi'],
			'alamat_pengiriman' => $post['alamat_pelanggan'],
			'ekspedisi' => $post['input-cour'],
			'paket_ekspedisi' => $post['paket_ekspedisi'],
			'berat' => $post['berat_ekspedisi'],
			'waktu_tempuh' => $post['etd_ekspedisi'],
			'pembayaran' => $post['metode'],
			'status_invoice' => 0,
			
		);

	

		if ($result) {

			$res = $this->M_produk->simpan_invoice($param,$post['detail']);
		
			if($res['result']){
				$this->M_produk->hapus_keranjang_user(getSession());
			}

		}

	
		if ($res['result']) {

			echo getResponseWithCallBack(201, 'Alamat Berhasil ditambahkan', 'produk/bayar/'.$res['id']);
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}

	public function bayar($id)
	{
		$id = decrypt_url($id);

		$inv = $this->M_produk->tampil_invoice($id);

		$data['invoice'] = $inv[$id];

		$this->themeadmin->tampilkan('lihatBayar', $data);
	}

	public function pembayaran()
	{
		$data['provinsi'] = $this->M_globalfunc->get_provinsi();
		$data['alamat'] = $this->M_globalfunc->get_alamat_user(getSession());
		$data['kurir'] = $this->M_globalfunc->get_kurir();

		$data['urlAlamat'] = 'produk/simpan_alamat';
		$data['urlPembayaran'] = 'produk/simpan_invoice';

		$data['produk'] = $this->M_produk->tampil_keranjang(getSession());


		$subTotal =  0;
		$beratTotal = 0;
		foreach ($data['produk'] as $item) {
			$subTotal += $item['harga_total'];
			$beratTotal += $item['berat_produk'];
		}

		$data['subTotal'] = $subTotal;
		$data['beratTotal'] = $beratTotal;


		$this->themeadmin->tampilkan('lihatPembayaran', $data);
	}

	public function hapus_keranjang($user, $id)
	{


		$result = $this->M_produk->hapus_keranjang($user, $id);


		if ($result) {

			echo getResponse(201, 'Data Berhasil Dihapus');
		} else {

			echo getResponse(400, 'Data gagal Dihapus');
		}
	}


	public function generateCodeInvoice()
	{

		$dataangka = $this->M_produk->get_invoice_sequence()->first_row()->angka;

		//$notransaksi = $number->first_row()->no_transaksi;
		$notrans = $dataangka;
		$notrans = strrev($notrans);
		$notrans = substr($notrans, 0, 4);
		$notrans = (int)strrev($notrans);
		$notrans = $notrans + 1;
		$data['notransaksi'] = $notrans;
		$notrans = STR_PAD($notrans, 4, "0", STR_PAD_LEFT);
		$notransaksi = $notrans;

		$date = date('Ymd');

		$data['code'] = 'INV/'.$date.'/'.$notransaksi; 
		
		return $data;

	}
}
