<style type="text/css">
	.container1 input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}
	.delete{
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				
				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>


						<div class="form-group">

							<label>Nama</label>

							<input value="<?php echo @$pemateri['nama_pemateri'] ?>" type="text" name="nama_pemateri" class="judul_berita form-control">

						</div>

						<div class="form-group">

							<label>Tampilkan di halaman depan</label>

							<select class="form-control" name="is_headline" id="">
								<option  <?php echo (@$pemateri['headline'] == 'N') ? "selected": "" ?> value="N">Tidak</option>
								<option  <?php echo (@$pemateri['headline'] == 'Y') ? "selected": "" ?> value="Y">Iya</option>
							</select>

						</div>

						<div class="form-group">

							<label>Keterangan</label>

							<textarea name="keterangan" class="form-control"  id=""><?php echo @$pemateri['keterangan'] ?></textarea>

						</div>

						

						<div class="form-group">

							<label>Gambar</label>

						</div>


						<div class="form-group">
							<table class="table table-bordered table-hover" style="width: 100%">

								<tbody>



									<div class="input-group">

										<input  accept="image/*" class="form-control" readonly="" type="file" name="gambar">
									</div>

									<img style="width: 300px" src="<?php echo base_url('asset/foto_pemateri/'.@$pemateri['gambar_uniq']) ?>" alt="">
									<p><i>*file yang diizinkan bertipe .png, .jpeg, .jpg dan maksimal 2Mb </i></p>
									
								</tbody>

							</table>
						</div>

						<div class="form-group">

							<label>Status</label>
							<select class="form-control" name="status" id="">
								<option  <?php echo (@$pemateri['status'] == 'publish') ? "selected": "" ?> value="publish">Publish</option>
								<option  <?php echo (@$pemateri['status'] == 'draft') ? "selected": "" ?> value="draft">Draft</option>
							</select>

						</div>

					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('pemateribe') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>		

	</div>

</div>




	<script>

		$('.form-kirim').ajaxForm({ 
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){

			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			console.log(result);

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('pemateribe') ?>';

			}
		}


	</script>

