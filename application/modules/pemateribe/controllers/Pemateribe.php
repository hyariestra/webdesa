<?php 

class Pemateribe extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))

		{

			redirect("pengguna/login");

		}

	}
	

	function index()

	{



		$this->themeadmin->tampilkan('tampilPemateri',null);


	}

	function get_data_pemateri()
	{
		$list = $this->M_pemateribe->get_datatables_pemateri();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->headline == 'Y') {
				$headline = "<span class='label label-danger'>Y</span>";
			}else{
				$headline = "<span class='label label-default'>N</span>";
			}

			$idPemateri = encrypt_url($field->id_pemateri);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_pemateri;
			$row[] = $headline;
			$row[] = $field->nama;
			$row[] = tgl_indo_timestamp($field->tanggal);
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('pemateribe/ubah/'.encrypt_url($field->id_pemateri))." >
			<span class='glyphicon glyphicon-edit'></span></a>
			<a onclick=delete_func('$idPemateri')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_pemateribe->count_all(),
			"recordsFiltered" => $this->M_pemateribe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['url'] = 'pemateribe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		
		$this->themeadmin->tampilkan('tambahPemateri',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['url'] = 'pemateribe/edit/'.$id;
		$data['pemateri'] =$this->M_pemateribe->get_detail_pemateri($idEn);
		
		
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahPemateri',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

		if ($post['nama_pemateri'] == "") {

			$err_msg[] = 'nama kosong';
			$result = false;

		}


		if (empty($_FILES['gambar']['name'])) {

			$err_msg[] = 'Gambar Kosong';
			$result = false;

		}




		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_pemateri/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2097152 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {

			$param = array(
				'nama_pemateri'=>$post['nama_pemateri'],
				'nama_pemateri_seo'=>slugify($post['nama_pemateri']),
				'headline'=>$post['is_headline'],
				'keterangan'=>$post['keterangan'],	
				'gambar_uniq' => isset($namaFile) ? $namaFile : '',
				'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'tanggal'=> date("Y-m-d H:i:s"),
				'status'=>$post['status']	

			);

			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_pemateribe->simpan_pemateri($param);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['nama_pemateri'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}


		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '_' . $_FILES['gambar']['name'];
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_pemateri/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2097152 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {


			if (!isset($_FILES['gambar'])) {
				
				$param = array(
					'nama_pemateri'=>$post['nama_pemateri'],
					'nama_pemateri_seo'=>slugify($post['nama_pemateri']),
					'headline'=>$post['is_headline'],
					'keterangan'=>$post['keterangan'],	
					'id_user'=>getSession(),
					'id_desa'=>getSettingDesa()['id_desa'],
					'tanggal'=> date("Y-m-d H:i:s"),
					'status'=>$post['status']	
				);

			}else{

				$param = array(
					'nama_pemateri'=>$post['nama_pemateri'],
					'nama_pemateri_seo'=>slugify($post['nama_pemateri']),
					'headline'=>$post['is_headline'],
					'keterangan'=>$post['keterangan'],	
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'id_user'=>getSession(),
					'id_desa'=>getSettingDesa()['id_desa'],
					'tanggal'=> date("Y-m-d H:i:s"),
					'status'=>$post['status']	

				);

			}
			move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_pemateribe->ubah_pemateri($param,$id);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_pemateribe->get_detail_pemateri($idEn);

		$dirUpload = "asset/foto_pemateri/";

		$files = $dirUpload.$data['gambar_uniq'];

		
		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}



		$result = $this->M_pemateribe->delete_pemateri($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>