<?php



/**

 * 

 */

class Pengguna extends CI_controller

{



	function __construct()


	{



		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];



		$this->load->model('M_pengguna');
	}

	function login()

	{

		$this->load->model("M_pengguna");

		//skrip login

		$inputan = $this->input->post();


		$timezone = new DateTimeZone('Asia/Jakarta');
		$date = new DateTime();
		$date->setTimeZone($timezone);

		if ($inputan) {

			$hasil = $this->M_pengguna->login_pengguna($inputan);

			if ($hasil == "sukses") {
				$param = array(
					'ip' =>  $_SERVER['REMOTE_ADDR'],
					'date' => $date->format('Y-m-d H:i:s'),
					'id_desa' => getSettingDesa()['id_desa'],
					'username' => $inputan['email'],
					'status' => 1,

				);

				$this->M_pengguna->simpan_log($param);

				$isipesan = "<br><div class='alert alert-info'>Login sukses</div>";

				$this->session->set_flashdata("pesan", $isipesan);

				redirect("dashboard");
			} else {
				$param = array(
					'ip' =>  $_SERVER['REMOTE_ADDR'],
					'date' => $date->format('Y-m-d H:i:s'),
					'id_desa' => getSettingDesa()['id_desa'],
					'username' => $inputan['email'],
					'status' => 0,

				);

				$this->M_pengguna->simpan_log($param);

				$isipesan = "<br><div class='alert alert-danger'>Login Gagal!</div>";

				$this->session->set_flashdata("pesan", $isipesan);

				redirect("pengguna/login");
			}
		}



		$data['judul'] = "Login Admin";

		//taruh di view login.php

		$this->load->view("login", $data);
	}







	function admin()

	{

		$session = isset($_SESSION['ses_admin']) ? $_SESSION['ses_admin'] : '';

		if ($session == "") {

			$this->login();
		} else {
			redirect("");
		}
	}



	function logout()

	{

		$this->session->unset_userdata("pengguna");

		redirect("pengguna/login");
	}
}
