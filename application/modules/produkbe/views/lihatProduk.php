<style type="text/css">


.keterangan
{
	padding: 10px;
}

img {
  max-width: 100%; }




.preview-thumbnail.nav-tabs {
  border: none;
  margin-top: 15px; }
  .preview-thumbnail.nav-tabs li {
    width: 18%;
    margin-right: 2.5%; }
    .preview-thumbnail.nav-tabs li img {
      max-width: 100%;
      display: block; }
    .preview-thumbnail.nav-tabs li a {
      padding: 0;
      margin: 0; }
    .preview-thumbnail.nav-tabs li:last-of-type {
      margin-right: 0; }

.tab-content {
  overflow: hidden; }
  .tab-content img {
    width: 100%;
    -webkit-animation-name: opacity;
            animation-name: opacity;
    -webkit-animation-duration: .3s;
            animation-duration: .3s; }

.card {
  padding: 1em;}

@media screen and (min-width: 997px) {


.details {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
      -ms-flex-direction: column;
          flex-direction: column; }

.colors {
  -webkit-box-flex: 1;
  -webkit-flex-grow: 1;
      -ms-flex-positive: 1;
          flex-grow: 1; }

.product-title, .price, .sizes, .colors {
  text-transform: UPPERCASE;
  font-weight: bold; }

.checked, .price span {
  color: #ff9f1a; }

.product-title, .rating, .product-description, .price, .vote, .sizes {
  margin-bottom: 15px; }

.product-title {
  margin-top: 0; }

.size {
  margin-right: 10px; }
  .size:first-of-type {
    margin-left: 40px; }

.color {
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
  height: 2em;
  width: 2em;
  border-radius: 2px; }
  .color:first-of-type {
    margin-left: 20px; }

.add-to-cart, .like {
  background: #ff9f1a;
  padding: 1.2em 1.5em;
  border: none;
  text-transform: UPPERCASE;
  font-weight: bold;
  color: #fff;
  -webkit-transition: background .3s ease;
          transition: background .3s ease; }
  .add-to-cart:hover, .like:hover {
    background: #b36800;
    color: #fff; }

.not-available {
  text-align: center;
  line-height: 2em; }
  .not-available:before {
    font-family: fontawesome;
    content: "\f00d";
    color: #fff; }

.orange {
  background: #ff9f1a; }

.green {
  background: #85ad00; }

.blue {
  background: #0076ad; }

.tooltip-inner {
  padding: 1.3em; }

@-webkit-keyframes opacity {
  0% {
    opacity: 0;
    -webkit-transform: scale(3);
            transform: scale(3); }
  100% {
    opacity: 1;
    -webkit-transform: scale(1);
            transform: scale(1); } }

@keyframes opacity {
  0% {
    opacity: 0;
    -webkit-transform: scale(3);
            transform: scale(3); }
  100% {
    opacity: 1;
    -webkit-transform: scale(1);
            transform: scale(1); } }

/*# sourceMappingURL=style.css.map */
</style>


<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<div class="box-body">
				<div class="card">
			<div class="container-fliud">
				<div class="">
					<div class="preview col-md-7">
						
						<div class="preview-pic tab-content">
						
						<?php
						$no = 1;
						foreach ($produk['gambar'] as $key => $item) {
							$active = $no== 1 ? 'active' : ''; 
						?>
								<div class="tab-pane <?php echo $active ?>" id="pic-<?php echo $no ?>"><img src="<?php echo base_url('asset/'.$item['gambar']) ?>" /></div>

						<?php  $no++; } ?>
					
						</div>

						<ul class="preview-thumbnail nav nav-tabs">

						<?php
						$noBawah = 1;
						foreach ($produk['gambar'] as $key => $item) {

							$actived = $noBawah == 1 ? 'class=active' : ''; 
						?>
						  <li <?php echo $actived ?>><a data-target="#pic-<?php echo $noBawah ?>" data-toggle="tab"><img src="<?php echo base_url('asset/'.$item['gambar']) ?>" /></a></li>
						
						  <?php  $noBawah++; } ?>
						</ul>
						
					</div>
					<div class="details col-md-5">
						<h3 class="product-title"><?php echo $produk['nama_produk'];  ?> [<?php echo $produk['kode_produk'];  ?>]</h3>
						<h4 class="price">Harga: <span><?php echo number_format($produk['harga_produk']);  ?></span></h4>
						<h5 class="sizes">sizes:
							<span class="size" data-toggle="tooltip" title="small">s</span>
							<span class="size" data-toggle="tooltip" title="medium">m</span>
							<span class="size" data-toggle="tooltip" title="large">l</span>
							<span class="size" data-toggle="tooltip" title="xtra large">xl</span>
						</h5>
						<h5 class="colors">colors:
							<span class="color orange not-available" data-toggle="tooltip" title="Not In store"></span>
							<span class="color green"></span>
							<span class="color blue"></span>
						</h5>
						<div class="action">
							<button class="add-to-cart btn btn-default" type="button">add to cart</button>
							<button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>
						</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					<hr>
					<div class="keterangan">
					<?php echo $produk['keterangan_produk'];  ?>
					
					</div>
					
					
					</div>
					
					</div>		
					
					</div>

</div>

<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Kategori</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-hover" id="">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Kategori</th>
							<th>Pilih</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;

						foreach ($kategori as $key => $value) { 
							echo "<tr>";
							echo	 "<th>".$no."</th>";
							echo	 "<th>".$value['kategori']."</th>";
							echo	 "<th>";
							echo	"</th>";
							echo "</tr>";	

							if(!empty($value['subkategori']))
							{

								$noSub = 1;
								foreach ($value['subkategori'] as $key => $value) { 
									
									echo "<tr>";
									echo  "<td> &nbsp; &nbsp;  &nbsp; &nbsp;".$noSub." <input type='hidden' value=".$value['id_subkategori']."></td>";
									echo  "<td> &nbsp; &nbsp;  &nbsp; &nbsp;".$value['nama_subkategori']."</td>";
									echo "<td>";
									echo "<button onclick='chosecategory(this)'  type='button' data-toggle='modal' data-target='#myModal' class='btn btn-xs btn-warning'> <span class='glyphicon glyphicon-plus-sign'></span>";
									echo "</button>";
									echo "</td>";
									echo "</tr>";	
									$noSub++;
								}
							}


							$no++;
						 } ?>
						
						 </tbody>

					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<script>


		function chosecategory(obj)
		{
			var idpelanggan = $(obj).parent().parent().find("td").eq(0).find("input:first").val();
			console.log(idpelanggan);
			namapelanggan = $(obj).parent().parent().find("td").eq(1).text();

			$("#kategoris").val(namapelanggan);
			$("#id_kategorinama").val(idpelanggan);

			$("#myModal").modal("remove");
		}



		$( "#diskon" ).keyup(function() {

			var dInput = this.value;
			var harga_produk = $('#harga_produk').val();
			var harga_diskon = '';

			var diskon  = (dInput * harga_produk) / 100;

			if (dInput!='') {
				var harga_diskon = harga_produk - diskon;

			}

			$('#harga_diskon').val(harga_diskon);

		});

		$(document).on('click', '.repeat-remove', function(e){

			e.preventDefault();

			$(this).closest('tr').remove();



		});   
		var lanjut = 0;

		$(".repeat-add").click(function(e){

			e.preventDefault();


			lanjut = lanjut + 1;

			var masukan = "<tr role='row"+lanjut+"'>";

			masukan += "<td>";


			masukan += "<div class='input-group'>";

			masukan += "<span class='input-group-addon'>";
			masukan += "<input value='1' name='thumb["+lanjut+"]' class='thumbClass' title='jadikan Tumbnail' type='checkbox' aria-label='...'>";
			masukan += "</span>";

			masukan += "<input  accept='image/*' class='form-control' readonly='' type='file' name='gambar[]'>";


			masukan += "<span class='input-group-btn'>";

			masukan +=  "<a class='repeat-remove btn btn-warning'  href='#modal-id'><i class='fa fa-trash' aria-hidden='true'></i></a>";

			masukan += "</span>";

			masukan += "</div>";

			masukan += "</td>";
			masukan+= "</tr>";

			$('#total_row').before(masukan);         





		});  




		$('.form-kirim').ajaxForm({ 
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){

				var check = $('.thumbClass:checkbox:checked').length;
				console.log(check);
				if (check > 1) {
					alert('Maksimal satu thumbnail');
					return false;
				}

				if (check  == "") {
					alert('Pilih Satu Thumbnail');
					return false;
				}

			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			console.log(result);

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('produkbe') ?>';

			}
		}

		$( ".nama_produk_class" ).keyup(function() {

			var nama = this.value;

			var str = this.value;
			str = str.replace(/\s+/g, '-').toLowerCase();
			

			$('.slug_class').val(str);


		});


	</script>