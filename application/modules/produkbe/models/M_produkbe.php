<?php 

class M_produkbe extends CI_model

{

	var $table = 'berita'; //nama tabel dari database
    var $column_order = array(1 => 'a.judul',6 => 'a.tanggal',7 => 'a.dibaca'); //field yang ada di table user
    var $column_search = array('judul'); //field yang diizin untuk pencarian 
    var $order = array('a.tanggal' => 'desc'); // default order 


    function get_berita()
    {
    	return $this->db->query("SELECT * FROM berita");


    }


    function trimelement($data,$panjang)
    {

       $max_length = $panjang;

       if (strlen($data) > $max_length)
       {
        $offset = ($max_length - 3) - strlen($data);
        $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
    }

    return $data;
}



function simpan_berita($param)
{

 $this->db->trans_begin();




 $result = $this->db->insert('berita',$param);



 if ($result == false){
  $this->db->trans_rollback();

}else{
  $this->db->trans_commit();

}

return [
  'result' => $result
];
}   

function delete_berita($id)
{
    return $this->db->query("DELETE FROM berita where id_berita = ".$id." ");
}

function ubah_berita($param,$id)
{

    $this->db->trans_begin();




    $this->db->where('id_berita',$id);
    $result = $this->db->update('berita',$param);



    if ($result == false){
        $this->db->trans_rollback();

    }else{
        $this->db->trans_commit();

    }

    return [
        'result' => $result
    ];
}





function reArrayFiles(&$file_post) {

 $file_ary = array();
 $file_count = count($file_post['name']);
 $file_keys = array_keys($file_post);

 for ($i=0; $i<$file_count; $i++) {
  foreach ($file_keys as $key) {
   $file_ary[$i][$key] = $file_post[$key][$i];
}
}

return $file_ary;
}


public function raw_db()
{
$this->db->select('*,a.status AS rilis');
 $this->db->from('berita a');
 $this->db->join('admin b', 'b.id_admin = a.id_user', 'left');
 $this->db->join('kategori c', 'c.id_kategori = a.id_kategori', 'left');
 $this->db->join('ref_desa_users d', 'b.id_admin = d.id_admin', 'left');
 $this->db->join('ref_desa e', 'e.id_desa = d.id_desa', 'left');
$data =  $this->db->where('e.desa_kode_domain', getNameDomain() );


    return $data;


}


private function _get_datatables_query()
{


    $this->raw_db();



 $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if($i===0) // looping awal
                {
                	$this->db->group_start(); 
                	$this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                	$this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                	$this->db->group_end(); 
            }
            $i++;
        }
 
        if(isset($_POST['order'])) 
        {
        	$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
        	$this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
    	$this->_get_datatables_query();
    	if($_POST['length'] != -1)
    		$this->db->limit($_POST['length'], $_POST['start']);
    	$query = $this->db->get();
     
     
    	return $query->result();
    }

    function count_filtered()
    {
    	$this->_get_datatables_query();
    	$query = $this->db->get();
    	return $query->num_rows();
    }

    public function count_all()
    {

        $this->raw_db();
        $r = $this->db->count_all_results();

        return $r;
    }

    public function getTagName($value)
    {
    	$data =  $this->db->query(" SELECT nama_tag FROM tags WHERE id_tag = ".$value." ")->row_array();
    	
    	return $data['nama_tag'];

    }


        public function get_detail_produk($id,$seo)
    {
        $data = $this->db->query("SELECT * FROM `produk` a
        LEFT JOIN `produk_gambar` b ON a.`id_produk` = b.`id_produk`
        WHERE a.`id_produk` = ".$id." AND a.slug = '".$seo."'  ")->result_array();


        $produk = array();

        foreach ($data as $item) {
            
            $produk[$item['id_produk']]['id_produk'] = $item['id_produk'];
            $produk[$item['id_produk']]['nama_produk'] = $item['nama_produk'];
            $produk[$item['id_produk']]['kode_produk'] = $item['kode_produk'];
            $produk[$item['id_produk']]['keterangan_produk'] = $item['keterangan_produk'];
            $produk[$item['id_produk']]['harga_produk'] = rupiah($item['harga_produk']);
            $produk[$item['id_produk']]['berat_produk'] = $item['berat_produk'];
            
            
            $produk[$item['id_produk']]['gambar'][$item['id_gambar']]['id_gambar'] = $item['id_gambar'];
            $produk[$item['id_produk']]['gambar'][$item['id_gambar']]['gambar'] = !empty($item['gambar_uniq']) ? 'foto_produk/'.rawurlencode($item['gambar_uniq']) : 'img/imgnotfound.png';


        }



        return $produk;

    }


    public function get_kategori_produk()
	{

	
		$data = $this->db->query("SELECT 
        a.id_kategori AS id_kategori,
        a.id_parent AS id_parent,
        a.nama_kategori AS nama_kategori,
        b.`id_kategori` AS id_subkategori,
        b.`id_parent` AS id_subparentkategori,
        b.`nama_kategori` AS nama_subkategori
        FROM `kategori_produk` a 
        LEFT JOIN kategori_produk b ON a.`id_kategori` = b.`id_parent` 
        WHERE 
        1=1
        AND a.active = 1
		")->result_array();


		$temp = [];


		foreach ($data as $key => $value) {



			if ($value['id_parent']==NULL) {



				$temp[$value['id_kategori']]['kategori'] = $value['nama_kategori'];
				$temp[$value['id_kategori']]['id_kategori'] = $value['id_kategori'];
			


				if ($value['id_subkategori'] != "") {

					$temp[$value['id_kategori']]['subkategori'][$value['id_subkategori']]['id_subkategori'] = $value['id_subkategori'];
					$temp[$value['id_kategori']]['subkategori'][$value['id_subkategori']]['id_subparentkategori'] = $value['id_subparentkategori'];
					$temp[$value['id_kategori']]['subkategori'][$value['id_subkategori']]['nama_subkategori'] = $value['nama_subkategori'];

				}

			}


		}



		return $temp;

	}


    function simpan_produk($param,$gambar,$thumb)
	{
	
		$this->db->trans_begin();

        $pesan = array();

		$result = $this->db->insert('produk',$param);

		$insert_id = $this->db->insert_id();

		$valid_ext = array('png','jpeg','jpg','gif');

		if ($result) {
            
            $i = 0;
			foreach ($gambar as $key => $value) {
                
				$file_extension = pathinfo($value['name'], PATHINFO_EXTENSION);
				$file_extension = strtolower($file_extension);

			
				$namaFile = $value['name'];
				$namaFileUniq = rand().round(microtime(true)).'.'.$file_extension;
				$namaSementara = $value['tmp_name'];
				$dirUpload = "asset/foto_produk/";

				if(!in_array($file_extension,$valid_ext)){
		
                    $pesan[] = 'Ekstensi file tidak sesuai';
                    $result = false;	 
				
                }


                if ($value['size'] > 2097152 OR $value['size']==0) {
                    $pesan[] = 'Ukuran file melebihi 2mb';
                    $result = false;
                }
                



                if ($result) {

                    move_uploaded_file($namaSementara, $dirUpload.$namaFileUniq);
					$param_gambar = array(
						'id_produk'=> $insert_id,
						'gambar'=> $namaFile,
						'gambar_uniq'=> $namaFileUniq,
						'is_thumb'=> isset($thumb[$i]) ? $thumb[$i] : null,

					);
					$this->db->insert('produk_gambar',$param_gambar);
                  	$i++;
                }


				
			}

		}

	
		if ($result == false){
			$this->db->trans_rollback();
			
		}else{
			$this->db->trans_commit();
			
		}

		return [
			'result' => $result,
			'pesan' => $pesan
		];
	}


}


?>