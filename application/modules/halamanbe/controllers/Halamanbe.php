<?php 

class Halamanbe extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))



		{



			redirect("pengguna/login");



		}

	}
	

	function index()

	{



		$this->themeadmin->tampilkan('tampilHalaman',null);


	}

	function get_data_halaman()
	{
		$list = $this->M_halamanbe->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {


			$idhalaman = encrypt_url($field->id_halaman);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_jenis;
			$row[] = tgl_indo_timestamp($field->tanggal);
			$row[] = $field->dibaca." kali";
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('halamanbe/ubah/'.encrypt_url($field->id_halaman))." >
			<span class='glyphicon glyphicon-edit'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_halamanbe->count_all(),
			"recordsFiltered" => $this->M_halamanbe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['url'] = 'halamanbe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$data['jenis'] =$this->M_halamanbe->get_all_jenis();

		$this->themeadmin->tampilkan('tambahHalaman',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$data['url'] = 'halamanbe/edit/'.$id;
		$data['jenis'] =$this->M_halamanbe->get_all_jenis();
		$data['halaman'] =$this->M_halamanbe->get_detail_halaman_be($idEn);



		$this->themeadmin->tampilkan('tambahHalaman',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

		if ($post['id_jenis'] == "") {

			$err_msg[] = 'Jenis halaman kosong';
			$result = false;

		}

		
		if ($post['judul_halaman'] == "") {

			$err_msg[] = 'Judul halaman kosong';
			$result = false;

		}


		if ($result) {

			$param = array(
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'id_jenis'=>$post['id_jenis'],
				'isi_halaman'=>$post['isi_halaman'],	
				'tanggal'=> date("Y-m-d H:i:s"),
				
			);

			$result = $this->M_halamanbe->simpan_halaman($param);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['id_jenis'] == "") {

			$err_msg[] = 'Jenis halaman kosong';
			$result = false;

		}


		
		if ($post['judul_halaman'] == "") {

			$err_msg[] = 'Judul halaman kosong';
			$result = false;

		}



		if ($result) {

			$param = array(
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'id_jenis'=>$post['id_jenis'],
				'judul_halaman'=>$post['judul_halaman'],
				'isi_halaman'=>$post['isi_halaman'],	
				'tanggal_update'=> date("Y-m-d H:i:s"),
				
			);

			$result = $this->M_halamanbe->ubah_halaman($param,$id);


		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_beritabe->get_detail_berita($idEn);

		$dirUpload = "asset/foto_berita/";

		$files = $dirUpload.$data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {
			
			if (file_exists($files)) {
				unlink($files);
			} 

		}


		$result = $this->M_beritabe->delete_berita($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>