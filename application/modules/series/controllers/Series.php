<?php

class Series extends CI_controller

{

	function __construct()

	{

		parent::__construct();
		$this->theme = getSettingDesa()['desa_theme'];
	}


	public function index($kategori = '')
	{
		$cari['key'] = $this->input->get('search');
		$cari['tag'] = $this->input->get('tag');
		$cari['sort'] = $this->input->get('sort');


		$total_rows = $this->M_series->jumlah_data($cari, $kategori = '');
		$config = array();
		if ($cari['key'] or $cari['tag']) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config["base_url"] = site_url() . 'series/index';
		$config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 10;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = 4;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		if ($this->uri->segment(3)) {
			$page = ($this->uri->segment(3));
		} else {
			$page = 1;
		}



		if ($cari['key'] == '') {
			$data['msg'] = 'Terdapat ' . $total_rows . ' series';
			$data['cari'] = '';
		} else {
			$data['msg'] = 'Terdapat ' . $total_rows . ' series dengan pencarian <b>' . $cari['key'] . '</b>';
			$data['cari'] = $cari['key'];
		}

		$users_record = $this->M_series->get_series_homepage($cari, $config["per_page"], $page, $kategori = '');
		$data['item'] = $users_record;
		$data['kategori_count'] = $this->M_berita->get_count_kategori();

		$data['popular'] = $this->M_berita->get_berita_popular(5);
		$data['detail']['judul_halaman'] = 'Course';
		$data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none';
		$data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : '';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['sidebar'] = $this->load->view($this->theme . "/sidebar", $data, true);
		$data['content'] = $this->load->view($this->theme . "/seriespage", $data, true);
		echo $this->load->view($this->theme . "/template", $data);
	}



	function detail($id, $seo)

	{

		$this->M_series->update_dibaca_series($id);

		$data['detail'] = $this->M_series->get_detail_series($id);

		if (empty($data['detail'])) {
			redirect('404_override');
		}


		$data['comments'] = $this->M_berita->get_comments($id);

		$link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$data['link'] = str_replace(' ', '', $link);


		$data['recent'] = $this->M_berita->berita_recent(4, $id);
		$data['popular'] = $this->M_berita->get_berita_popular(5);

		$data['detail']['judul_halaman'] = '';
		$data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none';
		$data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : '';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['kategori_count'] = $this->M_berita->get_count_kategori();
		$data['sidebar'] = $this->load->view($this->theme . "/sidebarseries", $data, true);

		$data['content'] = $this->load->view($this->theme . "/seriesdetail", $data, true);


		echo $this->load->view($this->theme . "/template", $data);
	}

	function carts()
	{

		$result = true;

		if (!getSessionUser()) {

			$msg = '';
			$url = 'account/signin';
			$err_msg[] = 'Please login first';
			$result = false;
		}


		$post = $this->input->post();


		
	


		if ($result) {


			$series = $this->M_series->get_detail_series($post['id']);

			if ($series['is_paid'] == 1) {

	
				$qty = isset($post['quantity']) ? $post['quantity'] : 1;

				//kosong dulu

				$param = array(
					'id_desa' => getSettingDesa()['id_desa'],
					'id_series' => decrypt_url($post['id']),
					'id_user' => decrypt_url(getSessionUser()['user_id']),
					'qty' => $qty,
					'tanggal' => dateNow()
				);

				 $result = $this->M_carts->simpan_keranjang($param);

				if($result){
					$msg = 'Course Added Successfully to Carts';
					$url = 'carts';
				}


			} else {

				$param = array(
					'id_user' => decrypt_url(getSessionUser()['user_id']),
					'id_series_master' => decrypt_url($post['id']),
					'insert_time' => dateTimeIndoNow(),

				);

				$result =  $this->M_series->insert_series_user($param);

				$msg = 'Course Added Successfully';
				$url = 'account/mycourse';
			}
		}

		if ($result) {

			echo getResponseWithCallBack(201, $msg, $url);
		} else {

			echo getResponseWithCallBack(400, implode("<hr>", $err_msg), $url);
		}
	}

	public function kategori($kategori = '')
	{
		$cari['key'] = $this->input->get('search');
		$cari['tag'] = $this->input->get('tag');
		$cari['sort'] = $this->input->get('sort');

		$uri =  $this->uri->segment(3);


		$total_rows = $this->M_berita->jumlah_data($cari, $kategori);
		$getCategoryName = $this->M_berita->get_category_name($kategori);

		$data['nama_kategori'] = $getCategoryName['nama_kategori'];

		$config = array();
		if ($cari['key'] or $cari['tag']) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config["base_url"] = site_url() . 'berita/kategori/' . $uri . '/index';
		$config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 10;
		$config['use_page_numbers'] = TRUE;

		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		if ($this->uri->segment(5)) {
			$page = ($this->uri->segment(5));
		} else {
			$page = 1;
		}



		if ($cari['key'] == '') {
			$data['msg'] = 'Terdapat ' . $total_rows . ' berita';
		} else {
			$data['msg'] = 'Terdapat ' . $total_rows . ' berita dengan pencarian '  . $cari['key'];
		}



		$users_record = $this->M_berita->get_berita_homepage($cari, $config["per_page"], $page, $kategori);



		$data['detail']['judul_halaman'] = $data['nama_kategori'];
		$data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['item'] = $users_record;
		$data['kategori_count'] = $this->M_berita->get_count_kategori();
		$data['popular'] = $this->M_berita->get_berita_popular(5);


		$data['sliders'] = array();
		$data['display'] = $this->uri->uri_string() == '' ? '' : 'none';
		$data['displayMini'] = $this->uri->uri_string() == '' ? 'none' : '';
		$data['slider'] = $this->load->view($this->theme . "/slider", $data, true);
		$data['sidebar'] = $this->load->view($this->theme . "/sidebar", $data, true);
		$data['content'] = $this->load->view($this->theme . "/page", $data, true);
		// echo "<prE>";
		// print_r($data);
		// echo "<prE>";
		echo $this->load->view($this->theme . "/template", $data);
	}
}
