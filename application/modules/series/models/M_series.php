<?php

class M_series extends CI_model

{

  var $table = 'berita'; //nama tabel dari database
  var $column_order = array(null, 'a.tanggal'); //field yang ada di table user
  var $column_search = array('judul', 'nama_kategori'); //field yang diizin untuk pencarian 
  var $order = array('a.tanggal' => 'desc'); // default order 


  function trimelement($data, $panjang)
  {

    $max_length = $panjang;

    if (strlen($data) > $max_length) {
      $offset = ($max_length - 3) - strlen($data);
      $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
    }

    return $data;
  }

  function data($number, $offset)
  {
    return $query = $this->db->get('user', $number, $offset)->result();
  }


  function update_dibaca_series($id)
  {

    $this->db->set('dilihat', 'dilihat+1', FALSE);
    $this->db->where('id_series', decrypt_url($id));
    $this->db->update('series');
  }

  function berita_recent($limit, $id)
  {

    $this->db->select('*');
    $this->db->from('berita a');
    $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('a.id_desa', getSettingDesa()['id_desa']);
    $this->db->where('a.id_berita >', $id);
    $this->db->order_by('a.id_berita', 'desc');
    $this->db->limit($limit);
    $data = $this->db->get()->result_array();

    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_berita'];
      $data[$key]['judul'] = $this->trimelement($value['judul'], 80);
      $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'], 150);
      $data[$key]['kategori'] = $value['nama_kategori'];
      $data[$key]['url_kategori'] =  base_url('berita/kategori/' . $value['slug_kategori']);
      $data[$key]['tanggal'] = tgl_indo($value['tanggal']);
      $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/' . $value['gambar_uniq'] : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('berita/detail/' . $value['id_berita'] . '/' . $value['judul_seo']);
      $data[$key]['like'] = $value['like_berita'];
      $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/' . rawurlencode($value['foto_admin_uniq']) : 'img/user.png';
    }

    return $data;
  }


  function info_terkait($limit, $tag, $id)
  {
    $pisah_kata  = explode(",", $tag);
    $jml_katakan = (int)count($pisah_kata);
    $jml_kata = $jml_katakan - 1;
    $cari = "SELECT * FROM berita a
  LEFT JOIN `kategori` b ON b.`id_kategori` = a.`id_kategori` ";
    $cari .= " WHERE  a.id_desa  =  '" . getSettingDesa()['id_desa'] . "' AND a.id_berita != " . $id . "   ";
    $cari .= " AND";
    $cari .= " ( ";
    for ($i = 0; $i <= $jml_kata; $i++) {

      $cari .= " tag LIKE '%$pisah_kata[$i]%' ";
      if ($i < $jml_kata) {
        $cari .= " OR ";
      }
    }
    $cari .= " ) ";
    $cari .= " ORDER BY id_berita DESC LIMIT $limit";
    $cari = $this->db->query($cari);


    $data = array();

    foreach ($cari->result_array() as $key => $value) {


      $data[$key]['id'] = $value['id_berita'];
      $data[$key]['judul'] = $this->trimelement($value['judul'], 80);
      $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'], 150);
      $data[$key]['url_kategori'] =  base_url('berita/kategori/' . $value['slug_kategori']);
      $data[$key]['kategori'] = $value['nama_kategori'];
      $data[$key]['tanggal'] = $value['tanggal'];
      $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/' . $value['gambar_uniq'] : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('berita/detail/' . $value['id_berita'] . '/' . $value['judul_seo']);
      $data[$key]['like'] = $value['like_berita'];
      $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/' . rawurlencode($value['foto_admin_uniq']) : 'img/user.png';
    }

    return $data;
  }

  function jumlah_data($cari = '', $kategori = '')

  {


    $this->db->select('*,a.gambar_uniq AS gambar_series, (SELECT COUNT(`id_series_detail`) FROM `series_folder` a1
  LEFT JOIN `series_detail` b1 ON b1.`id_series_folder` = a1.id_series_folder
  WHERE a1.id_series = a.id_series )  AS jumlah_materi ');
    $this->db->from('series a');
    $this->db->where('a.is_publish', '1');


    if (!empty($kategori)) {
      $this->db->where('d.slug_kategori', $kategori);
    }

    if (!empty($cari['key'])) {
      $this->db->like('a.judul', $cari['key'], 'both');
    }

    if (!empty($cari['tag'])) {

      $id_tag = $this->get_id_tag($cari['tag']);
      $this->db->like('a.tag', $id_tag, 'both');
    }



    $data = $this->db->get()->num_rows();

    return $data;
  }

  function get_berita_popular($limit)
  {

    $this->db->select('*');
    $this->db->from('berita a');
    $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('a.id_desa', getSettingDesa()['id_desa']);
    $this->db->order_by('a.like_berita', 'desc');
    $this->db->limit($limit);
    $data = $this->db->get()->result_array();

    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_berita'];
      $data[$key]['judul'] = $this->trimelement($value['judul'], 80);
      $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'], 150);
      $data[$key]['kategori'] = $value['nama_kategori'];
      $data[$key]['tanggal'] = $value['tanggal'];
      $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/' . $value['gambar_uniq'] : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('berita/detail/' . $value['id_berita'] . '/' . $value['judul_seo']);
      $data[$key]['like'] = $value['like_berita'];
      $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/' . rawurlencode($value['foto_admin_uniq']) : 'img/user.png';
    }

    return $data;
  }

  function get_berita_popular_selected($limit, $kategori)
  {

    $this->db->select('*');
    $this->db->from('berita a');
    $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('d.slug_kategori', $kategori);
    $this->db->where('a.id_desa', getSettingDesa()['id_desa']);
    $this->db->order_by('a.like_berita', 'desc');
    $this->db->limit($limit);
    $data = $this->db->get()->result_array();

    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_berita'];
      $data[$key]['judul'] = $this->trimelement($value['judul'], 80);
      $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'], 150);
      $data[$key]['kategori'] = $value['nama_kategori'];
      $data[$key]['tanggal'] = $value['tanggal'];
      $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/' . $value['gambar_uniq'] : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('berita/detail/' . $value['id_berita'] . '/' . $value['judul_seo']);
      $data[$key]['like'] = $value['like_berita'];
      $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/' . rawurlencode($value['foto_admin_uniq']) : 'img/user.png';
    }

    return $data;
  }


  public function get_category_name($kategori)
  {
    $this->db->select('*');
    $this->db->from('kategori a');
    $this->db->where('a.slug_kategori', $kategori);
    $data = $this->db->get()->row_array();

    return $data;
  }

  function get_tags()
  {
    $this->db->select('*');
    $this->db->from('tags a');
    $this->db->where('a.status', 'Y');
    $this->db->where('a.id_desa', getSettingDesa()['id_desa']);
    $this->db->or_where('a.id_desa IS NULL', null, false);
    $data = $this->db->get()->result_array();


    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_tag'];
      $data[$key]['nama_tag'] = $value['nama_tag'];
      $data[$key]['seo_tag'] = $value['seo_tag'];
    }

    return $data;
  }


  function get_berita_headline()
  {

    $this->db->select('*');
    $this->db->from('berita a');
    $this->db->join('ref_desa_users b', 'a.id_user = b.id_admin', 'left');
    $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('a.headline', 'Y');
    $this->db->where('a.id_desa', getSettingDesa()['id_desa']);
    $this->db->order_by('a.id_berita', 'desc');
    $data = $this->db->get()->result_array();

    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_berita'];
      $data[$key]['judul'] = $this->trimelement($value['judul'], 80);
      $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'], 150);
      $data[$key]['kategori'] = $value['nama_kategori'];
      $data[$key]['url_kategori'] =  base_url('berita/kategori/' . $value['slug_kategori']);
      $data[$key]['tanggal'] = $value['tanggal'];
      $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/' . rawurlencode($value['gambar_uniq']) : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('berita/detail/' . $value['id_berita'] . '/' . $value['judul_seo']);
      $data[$key]['like'] = $value['like_berita'];
    }

    return $data;
  }


  function get_id_tag($value = '')
  {
    $r = $this->db->query("SELECT id_tag FROM tags WHERE seo_tag = '" . $value . "'  ")->row_array();

    return $r['id_tag'];
  }



  function get_series_all()
  {


    $this->db->select('*,a.gambar_uniq AS gambar_series, (SELECT COUNT(`id_series_detail`) FROM `series_folder` a1
 LEFT JOIN `series_detail` b1 ON b1.`id_series_folder` = a1.id_series_folder
 WHERE a1.id_series = a.id_series )  AS jumlah_materi ');
    $this->db->from('series a');
    $this->db->join('ref_label lab', 'lab.id_label = a.label_id', 'left');
    $this->db->where('a.is_publish', '1');


    $this->db->group_by('a.id_series');
    $this->db->order_by('a.insert_time', 'desc');

    $data = $this->db->get()->result_array();


    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_series'];
      $data[$key]['jumlah_materi'] = $value['jumlah_materi'];
      $data[$key]['nama_label'] = $value['nama_label'];
      $data[$key]['price'] = rupiah($value['series_price']);
      $data[$key]['judul'] = $this->trimelement($value['series'], 80);
      $data[$key]['description'] = $this->trimelement(strip_tags($value['description']), 150);
      $data[$key]['gambar'] = !empty($value['gambar_series']) ? 'foto_series/' . rawurlencode($value['gambar_series']) : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('series/detail/' . encrypt_url($value['id_series']) . '/' . $value['series_seo']);

    }

    return $data;
  }


  function get_series_homepage($cari, $perpage, $pagenum, $kategori = '')
  {


    if ($pagenum > 1) {
      $limit = $perpage;
      $start = $perpage * ($pagenum - 1);
    } else {
      $limit = $perpage;
      $start = 0;
    }



    $this->db->select('*,a.gambar_uniq AS gambar_series, (SELECT COUNT(`id_series_detail`) FROM `series_folder` a1
    LEFT JOIN `series_detail` b1 ON b1.`id_series_folder` = a1.id_series_folder
   WHERE a1.id_series = a.id_series )  AS jumlah_materi ');
    $this->db->from('series a');
    $this->db->join('ref_label lab', 'lab.id_label = a.label_id', 'left');
    $this->db->where('a.is_publish', '1');

    if (!empty($kategori)) {
      $this->db->where('d.slug_kategori', $kategori);
    }

    if (!empty($cari['key'])) {
      $this->db->like('a.judul', $cari['key'], 'both');
    }

    if (!empty($cari['tag'])) {

      $id_tag = $this->get_id_tag($cari['tag']);
      $this->db->like('a.tag', $id_tag, 'both');
    }

    if (!empty($cari['sort'] == 'popular')) {
      $this->db->order_by('a.dibaca', 'desc');
    }

    if (!empty($cari['sort'] == 'share')) {
      $this->db->order_by('a.dibagikan', 'desc');
    }

    if (!empty($cari['sort'] == 'like')) {
      $this->db->order_by('a.like_berita', 'desc');
    }


    $this->db->group_by('a.id_series');
    $this->db->order_by('a.insert_time', 'desc');

    $this->db->limit($limit, $start);
    $data = $this->db->get()->result_array();


    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_series'];
      $data[$key]['jumlah_materi'] = $value['jumlah_materi'];
      $data[$key]['nama_label'] = $value['nama_label'];
      $data[$key]['price'] = rupiah($value['series_price']);
      $data[$key]['judul'] = $this->trimelement($value['series'], 80);
      $data[$key]['description'] = $this->trimelement(strip_tags($value['description']), 150);
      //  $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
      $data[$key]['gambar'] = !empty($value['gambar_series']) ? 'foto_series/' . rawurlencode($value['gambar_series']) : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('series/detail/' . encrypt_url($value['id_series']) . '/' . $value['series_seo']);
      // $data[$key]['like'] = $value['like_berita'];
      // $data[$key]['dibagikan'] = $value['dibagikan'];
      // $data[$key]['dibaca'] = $value['dibaca'];
      // $data[$key]['penulis'] = $value['nama'];
      // $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

    }

    return $data;
  }


  function get_count_kategori()
  {

    $this->db->select('COUNT(a.id_kategori) AS kategori,
 COUNT(d.id_kategori) AS counts,
 d.nama_kategori,
 d.`slug_kategori`,
 judul');
    $this->db->from('berita a');
    $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
    $this->db->join('admin e', 'e.id_admin = a.id_user', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('a.id_desa', getSettingDesa()['id_desa']);
    $this->db->group_by("a.id_kategori");
    $this->db->order_by("counts", "desc");

    $data = $this->db->get()->result_array();


    foreach ($data as $key => $value) {


      $data[$key]['kategori'] = $value['nama_kategori'];
      $data[$key]['url_kategori'] =  base_url('berita/kategori/' . $value['slug_kategori']);
      $data[$key]['counts'] = $value['counts'];
    }

    return $data;
  }

  public function update_like_berita($id)
  {

    return  $this->db->query("UPDATE berita SET like_berita = like_berita + 1 WHERE id_berita = '" . $id . "' ");
  }

  public function update_share_berita($id)
  {

    return  $this->db->query("UPDATE berita SET dibagikan = dibagikan + 1 WHERE id_berita = '" . $id . "' ");
  }

  public function update_dislike_berita($id)
  {

    return  $this->db->query("UPDATE berita SET like_berita = like_berita - 1 WHERE id_berita = '" . $id . "' ");
  }

  function getNametag($id)
  {
    $tags = $this->db->query("SELECT nama_tag,seo_tag FROM tags WHERE id_tag = " . $id . " ")->row_array();

    return $tags;
  }


  public function insert_series_user($params)
  {
    $this->db->trans_begin();

    $result = $this->db->insert('series_user', $params);

    if ($result == false) {
      $this->db->trans_rollback();
    } else {
      $this->db->trans_commit();
    }

    return [
      'result' => $result
    ];
  }

  public function get_detail_konten($id, $id_det)
  {
    $query = $this->db->query("SELECT *,

  COALESCE ((SELECT c1.id_series_detail FROM `series` a1 
  INNER JOIN `series_folder` b1 ON a1.id_series = b1.id_series 
  INNER JOIN `series_detail` c1 ON c1.id_series_folder = b1.id_series_folder 
  WHERE a1.id_series = " . decrypt_url($id) . " 
  AND b1.id_series_folder = a.`id_series_folder`
  AND c1.order > a.order ORDER BY c1.order ASC LIMIT 1),
  (SELECT c1.id_series_detail FROM `series` a1 
  INNER JOIN `series_folder` b1 ON a1.id_series = b1.id_series 
  INNER JOIN `series_detail` c1 ON c1.id_series_folder = b1.id_series_folder 
  WHERE a1.id_series = " . decrypt_url($id) . " 
  AND b1.id_series_folder > a.`id_series_folder` 
  ORDER BY c1.order ASC LIMIT 1) 
  , NULL ) AS nextstep


    FROM `series_detail` a
    LEFT JOIN series_folder b ON a.id_series_folder = b.id_series_folder
    LEFT JOIN series c ON c.id_series = b.id_series
    WHERE 1=1
    AND id_series_detail = " . $id_det . " ")->row_array();


    $query['files'] = $this->db->query("SELECT * FROM series_file WHERE id_series_detail = " . $query['id_series_detail'] . " ")->result_array();

    $query['url']  = base_url() . 'account/course/' . encrypt_url($query['id_series']) . '/' . $query['series_seo'] . '/' . $query['nextstep'];
    $query['urlNow']  = base_url() . 'account/course/' . encrypt_url($query['id_series']) . '/' . $query['series_seo'] . '/' . $query['id_series_detail'];

    $query['buttons'] = $query['nextstep'] == null ? false : true;


    return $query;
  }

  public function get_detail_series($id)
  {
    $data['series'] = $this->db->query("SELECT *,a.id_series AS id_series_master,  `a`.`gambar_uniq` AS `gambar_series`,
    (SELECT c1.id_series_detail  FROM `series` a1
    INNER JOIN `series_folder` b1 ON a1.id_series = b1.id_series
    INNER JOIN `series_detail` c1 ON c1.id_series_folder = b1.id_series_folder
    WHERE a1.id_series = `a`.`id_series` AND b1.id_series_folder = c.`id_series_folder` AND c1.order > c.order 
    ORDER BY c1.order ASC LIMIT 1) AS nextstep
    FROM `series` `a` 
    LEFT JOIN `series_folder` `b` ON `b`.`id_series` = `a`.`id_series` 
    INNER JOIN `series_detail` `c` ON `c`.`id_series_folder` = `b`.`id_series_folder` 
    LEFT JOIN admin d  ON d.`id_admin` = a.`created_by`
    LEFT JOIN series_gambar e  ON e.`id_series` = a.`id_series`
    WHERE `a`.`id_series` = '" . decrypt_url($id) . "' ")->result_array();
    // $data['berita']['image'] =  !empty( $data['berita']['gambar_uniq']) ? '<img class="img-detail" src="'.base_url('asset/foto_berita/'.rawurlencode($data['berita']['gambar_uniq'])).'"><br><br><br>' : '';
    //$data['berita']['url_kategori'] =  base_url('berita/kategori/'.$data['berita']['slug_kategori']);
    //$data['series']['gambar_custom'] =  !empty( $data['series']['gambar_series']) ? base_url('asset/foto_series/'.rawurlencode($data['series']['gambar_series'])) : '';

    if ($data['series']) {

      $newData = array();

      foreach ($data['series'] as $key => $item) {
        $newData[$item['id_series_master']]['id_series'] = $item['id_series_master'];
        $newData[$item['id_series_master']]['id_series_encrypt'] = encrypt_url($item['id_series_master']);
        $newData[$item['id_series_master']]['series'] = $item['series'];
        $newData[$item['id_series_master']]['gambar_thumb'] = !empty($item['gambar_series']) ? base_url('asset/foto_series/' . rawurlencode($item['gambar_series'])) : base_url('asset/img/user.png');
        $newData[$item['id_series_master']]['description'] = $item['description'];
        $newData[$item['id_series_master']]['series_price'] = rupiah($item['series_price']);
        $newData[$item['id_series_master']]['id_admin'] = $item['id_admin'];
        $newData[$item['id_series_master']]['nama'] = $item['nama'];
        $newData[$item['id_series_master']]['is_paid'] = $item['is_paid'];
        $newData[$item['id_series_master']]['foto_admin'] = !empty($item['foto_admin_uniq']) ? base_url('asset/foto_user/' . rawurlencode($item['foto_admin_uniq'])) : '';
        $newData[$item['id_series_master']]['series_folder'][$item['id_series_folder']]['id_series_folder'] = $item['id_series_folder'];
        $newData[$item['id_series_master']]['series_folder'][$item['id_series_folder']]['series_folder_name'] = $item['series_folder_name'];
        $newData[$item['id_series_master']]['series_folder'][$item['id_series_folder']]['order_folder'] = $item['order_folder'];
        $newData[$item['id_series_master']]['series_folder'][$item['id_series_folder']]['series_detail'][$item['id_series_detail']]['id_series_detail'] = $item['id_series_detail'];
        $newData[$item['id_series_master']]['series_folder'][$item['id_series_folder']]['series_detail'][$item['id_series_detail']]['series_title_detail'] = $item['series_title_detail'];
        $newData[$item['id_series_master']]['series_folder'][$item['id_series_folder']]['series_detail'][$item['id_series_detail']]['series_url_detail'] =  base_url('account/course/' . encrypt_url($item['id_series_master']) . '/' . $item['series_seo'] . '/' . $item['id_series_detail']);
        $newData[$item['id_series_master']]['series_folder'][$item['id_series_folder']]['series_detail'][$item['id_series_detail']]['order'] = $item['order'];
        $newData[$item['id_series_master']]['series_gambar'][$item['id_gambar']]['id_gambar'] = $item['id_series_folder'];
        $newData[$item['id_series_master']]['series_gambar'][$item['id_gambar']]['desc_series'] = $item['desc_series'];
        $newData[$item['id_series_master']]['series_gambar'][$item['id_gambar']]['gambar_screenshot'] = !empty($item['gambar_series_uniq']) ? base_url('asset/foto_screenshots/' . rawurlencode($item['gambar_series_uniq'])) : '';

        $return =  $newData[decrypt_url($id)];
      }
    } else {

      $return = array();
    }
    // echo '<pre>'; 
    // print_r($newData);
    // echo '<pre>'; 
    // die();

    return  $return;
  }


  function get_detail_finish_steps()
  {



    $this->db->select('*');
    $this->db->from('series_step a');
    $this->db->where('a.id_user', decrypt_url(getSessionUser()['user_id']));
    $data = $this->db->get()->result_array();


    $newArr = array();

    foreach ($data as $key => $value) {
      $newArr[$value['id_series_detail']]['id_series_detail'] = $value['id_series_detail'];
    }


    return $newArr;
  }

  public function getDate($date)
  {


    $createDate = new DateTime($date);

    $strip = $createDate->format('Y-m-d');
    return $strip;
  }


  function save_steps($param)
  {

    $this->db->trans_begin();

    $result = $this->db->insert('series_step', $param);

    if ($result == false) {
      $this->db->trans_rollback();
    } else {
      $this->db->trans_commit();
    }

    return [
      'result' => $result
    ];
  }

  function get_all_post()
  {
    $this->db->select('*');
    $this->db->from('berita a');
    $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
    $this->db->where('a.status', 'publish');
    $this->db->where('a.id_desa', getSettingDesa()['id_desa']);
    $data = $this->db->get()->result_array();

    foreach ($data as $key => $value) {


      $data[$key]['id'] = $value['id_berita'];
      $data[$key]['judul'] = $this->trimelement($value['judul'], 80);
      $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'], 150);
      $data[$key]['kategori'] = $value['nama_kategori'];
      $data[$key]['url_kategori'] =  base_url('berita/kategori/' . $value['slug_kategori']);
      $data[$key]['tanggal'] = $this->getDate($value['tanggal']);
      $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/' . $value['gambar_uniq'] : 'img/imgnotfound.png';
      $data[$key]['url'] = base_url('berita/detail/' . $value['id_berita'] . '/' . $value['judul_seo']);
      $data[$key]['like'] = $value['like_berita'];
      $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/' . rawurlencode($value['foto_admin_uniq']) : 'img/user.png';
    }

    return $data;
  }
}
