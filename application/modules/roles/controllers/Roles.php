<?php 



/**

* 

*/

class Roles extends CI_controller

{



	function __construct()



	{



		parent::__construct();



		if (!$this->session->userdata("pengguna"))



		{



			redirect("Pengguna/login");



		}



		$this->load->model('M_roles');

		$this->load->model('user/M_user');



	}





	function index()



	{



	//	authorize('roles');

		$data['judul']="Daftar Roles";

		$data['act'] = 'roles';



		$data['roles']=$this->M_roles->tampil_roles();



		$this->themeadmin->tampilkan('tampilroles',$data);



	}



	function tambah()



	{

		authorize('roles');

		$data['judul']="Tambah Roles";

		$data['url'] = 'roles/simpan';

		$data['act'] = 'userRoles';
		$data['roles'] =  $this->M_menu->get_be_menu_tree();
		$data['arrayMenu'] = [];

		$this->themeadmin->tampilkan('tambahroles',$data);



	}



	function simpan___()

	{

		

		

		$datainputan=$this->input->post();



		$hasil  = $this->M_roles->simpanRoles($datainputan);



		if ($hasil=="gagal") 

		{

			$isipesan="<br><div class='alert alert-danger'>Roles Sudah Terdaftar, Gunakan Roles Lain!</div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect("roles/tambah");

		}

		else

		{

			$isipesan="<br><div class='alert alert-success'>Data Roles Berhasil ditambah</div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect("roles");

		}



	}



	function hapus($id)



	{



		$hasil = $this->M_roles->hapus_roles($id);



		if ($hasil=="gagal") 

		{

			$isipesan="<br><div class='alert alert-danger'>Data Gagal dihapus Karena Sedang digunakan!</div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect("roles");

		}

		else

		{

			$isipesan="<br><div class='alert alert-success'>Data Berhasil dihapus</div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect("roles");

		}



	}



	function ubah__($idRaw)



	{

		$id = decrypt_url($idRaw);


		//authorize('roles');

		$inputan=$this->input->post();



		if ($inputan) 



		{

			$this->M_roles->ubah_roles($inputan,$id);

			$isipesan="<br><div class='alert alert-success'>Data Berhasil Diubah </div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect('roles');



		}



		$data['judul']="Ubah Roles";



		$data['admin']=$this->M_roles->ambil_roles($id);



		$this->themeadmin->tampilkan("tambahroles",$data);



	}






	
	function ubah($idRaw)



	{

		$id = decrypt_url($idRaw);


		//authorize('roles');

		$inputan=$this->input->post();


		$data['judul']="Ubah Roles";


		
		
		$data['admin']=$this->M_roles->ambil_roles($id);
		$data['url'] = 'roles/edit/'.$idRaw;
		
		$data['roles'] =  $this->M_menu->get_be_menu_tree();
		$data['arrayMenu']  =  $this->M_menu->getArrayDesaMenuByRoles($id);


		$this->themeadmin->tampilkan("tambahroles",$data);



	}


	function simpan()
	{
		$post = $this->input->post();

		$result = true;

		if (empty($post['roles'])) {
			$result = false;
			$err_msg[] = 'Roles setidaknya memiliki satu akses module';
		}

		if (empty($post['name'])) {
			$result = false;
			$err_msg[] = 'Roles tidak boleh kosong';
		}


		if($result){
	
			$param = array(
				'name'=>$post['name']
			);
			
			$result = $this->M_roles->simpan_name_roles($param);
			

			foreach ($post['roles'] as $item) {
				
				$param = array(
					'id_menu'=>$item,
					'id_roles'=>$result['insert_id'],
				);
			

				$this->M_roles->insert_menu_roles($param);
			
			}




			
		}

		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}



	public function edit($idRaw)
	{
		$post = $this->input->post();
		

		$id = decrypt_url($idRaw);


		$result = true;

		if (empty($post['name'])) {
			$result = false;
			$err_msg[] = 'Nama Roles tidak boleh kosong';
		}

		if (empty($post['roles'])) {
			$result = false;
			$err_msg[] = 'Roles setidaknya memiliki satu akses module';
		}
		
		
		
		if($result){


			$param = array(
				'name'=>$post['name']
			);

			$result = $this->M_roles->ubah_name_roles($param,$id);
			
			$result = $this->M_roles->delete_menu_roles($id);
			
			foreach ($post['roles'] as $item) {
				
				$param = array(
					'id_menu'=>$item,
					'id_roles'=>$id,
				);
			

				$result = $this->M_roles->insert_menu_roles($param);
			
			}
			

		}



		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}
	}


	function setting()

	{	

		authorize('setRoles');

		$data['id'] =  $this->input->get('id');



		$data['judul'] = 'Management Rules';

		$data['act'] = 'management';

		$data['roles'] =$this->M_user->getRoles();

		$this->themeadmin->tampilkan("settingroles",$data);

	}



	function simpanAkses()

	{

		$inputan =$this->input->post();



		$hasil = $this->M_roles->updateAkses($inputan);



		if ($hasil=='sukses') {

			$isipesan="<br><div class='alert alert-success'>Data Berhasil Disimpan </div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect('roles/setting');

		} 

		



	}



}



?>