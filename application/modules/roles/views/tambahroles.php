<!-- data pelanggan ada di var $pelanggan -->



<div class="row">



	<div class="col-md-12">



		<div class="box">



			<div class="box-header">



				<h3 class="box-title"><?php echo $judul ?></h3>



				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">



					<div class="box-body">



						<div class="form-group">



							<label>Roles</label>

							<input type="text" name="name" class="form-control" value="<?php echo @$admin['name']; ?>">

							
						</div>

				<div class="row">

				
					<?php
					

					
					foreach ($roles as $fe) { 
						
						$checked = '';
						if (in_array($fe['id_menu'], $arrayMenu)) {
							$checked = 'checked';
						  }
						
					echo '<div class="col-md-4">';
						echo '<div class="panel panel-default">';
							echo '<div class="panel-heading"><b>'.strtoupper($fe['menu']).'</b></div>';
						
							echo '<ul class="list-group">';
								echo '<li class="list-group-item"><input '.$checked.' type="checkbox"  name="roles[]" value="'.$fe['id_menu'].'"> '.$fe['menu'].'</li>';
								
								if( !empty($fe['submenu'])){
									
									foreach ($fe['submenu']  as $keyz => $fe_submenu) {

										$checked = '';
										if (in_array($fe_submenu['id_submenu'], $arrayMenu)) {
											$checked = 'checked';
										  }

										echo '<li class="list-group-item"><input '.$checked.' type="checkbox" name="roles[]" value="'.$fe_submenu['id_submenu'].'"> '.$fe_submenu['submenu'].'</li>';

									}
								}


							echo '</ul>';
					echo '</div>';
						
						echo '</div>';

						 } ?>

				</div>
						

					</div>

					


					<div class="box-footer">



						<button type="submit" class="btn btn-primary">Ubah</button>

						<a class="btn btn-default" href="<?php echo base_url("roles")?>">Batal</a>



					</div>



				</form>



			</div>

		</div>		

	</div>

</div>

<script>
	$('.form-kirim').ajaxForm({ 
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){



			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			console.log(result);

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('roles') ?>';

			}
		}
</script>