<!-- data pelanggan ada di var $pelanggan -->



<div class="row">



	<div class="col-md-12">



		<div class="box">



			<div class="box-header">



				<h3 class="box-title"><?php echo $judul ?></h3>



			</div>



			<div class="box-body">

				<br>

				<div class="table-filter table-filter__wrapper">

						<div class="row">

							<div class="col-xs-8">

								<div class="table-filter__left">

									<a href="<?php echo site_url('roles/tambah') ?>" class="btn btn-filter btn-danger "><i class="fa fa-plus"></i> Tambah Roles Baru</a>



								</div>

							</div>

							

						</div>

					</div>



				<table  style="margin-top: 20px" class="table table-bordered table-hover">



					<thead>



						<tr>



							<th>Roles</th>

							<th>Aksi</th>



						</tr>



					</thead>



					<tbody>



					<?php foreach ($roles as $key => $value): ?>



					<!-- karena data banyak dan array , ditampilkan pakai foreach -->



						<tr>



							<td><?php echo $value['name']; ?></td>



						<td>



							<form action="<?php echo base_url("roles/hapus/$value[id]"); ?>" method="post" accept-charset="utf-8">

								<a href="<?php echo base_url("roles/ubah/".encrypt_url($value['id'])) ?>" class="btn btn-warning btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>

								<button type="submit" class="button_delete btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>

							</form>



						</td>



						</tr>



						<?php endforeach ?>







					</tbody>



				</table>



			</div>

		

		</div>



	</div>



</div>



<script>

	$('.button_delete').on('click',function(e){

		e.preventDefault();

		var form = $(this).parents('form');

		swal({

			title: "Apakah Anda Yakin",

			text: "Data yang telah dihapus tidak dapat dikembalikan",

			type: "error",

			showCancelButton: true,

			cancelButtonClass: 'btn-default btn-md waves-effect',

			confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',

			confirmButtonText: 'Delete!'

		}, 

		function(willDelete) {

			if (willDelete) {

				form.submit();



			}     

		});

	});

</script>