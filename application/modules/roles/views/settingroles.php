<style type="text/css" media="screen">
	label{
		text-decoration: underline;
	}
	th.selectbox
	{
		width: 50px;
	}
</style>
<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title"><?php echo $judul ?></h3>

			</div>

			<div class="box-body">
				<div class="col-md-5">
					<label>Pilih Roles</label>

					<form action="<?php echo base_url("roles/setting") ?>" method="GET" >
						<select class="form-control" name="id">
							<option value="">Pilih Roles</option>
							<?php foreach ($roles as $key => $value): ?>
								<option value="<?php echo $value['id'] ?>" <?php if($value['id']==$id){echo"selected";} ?> ><?php echo $value['name']; ?></option>
							<?php endforeach ?>						
						</select>
						<div style="margin-top: 20px;">
							<button type="submit" class="btn btn-info">Pilih</button>
						</div>
					</form>

				</div>

				<div class="col-md-7">

					<?php if ($id==''): ?>

						<?php else: ?>

							<form action="<?php echo base_url('roles/simpanAkses') ?>" method="POST">
								<input type="hidden" value="<?php echo $id?>" name="id_role">
								<table class="table table-bordered roleTable">
									<tbody>
										<tr>
											<th class="no-sort selectbox bg-gray-light"></th>
											<th class="no-sort bg-gray-light">Setup</th>
										</tr>
										<tr>
											<td>
												<input class="dt-check" <?php echo checkedHelp($id,'informasiPerusahaan') ?> name="menu[]" type="checkbox" value="informasiPerusahaan">
											</td>
											<td>Informasi Perusahaan</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'kodeAkun') ?> name="menu[]" type="checkbox" value="kodeAkun"></td>
											<td>Kode Akun</td>
										</tr>
										<tr>
											<td>
												<input class="dt-check" <?php echo checkedHelp($id,'penandatanganan') ?> name="menu[]" type="checkbox" value="penandatanganan">
											</td>
											<td>Penandatanganan</td>
										</tr>
										<tr>
											<td>
												<input class="dt-check" <?php echo checkedHelp($id,'konfigurasi') ?> name="menu[]" type="checkbox" value="konfigurasi">
											</td>
											<td>Konfigurasi</td>
										</tr>
									</tbody>
								</table>
								<table class="table table-bordered roleTable">
									<tbody>
										<tr>
											<th class="no-sort selectbox bg-gray-light"></th>
											<th class="no-sort bg-gray-light">Transaksi</th>
										</tr>
										<tr>
											<td>
												<input class="dt-check"  <?php echo checkedHelp($id,'saldoAwal') ?> name="menu[]" type="checkbox" value="saldoAwal">
											</td>
											<td>Saldo Awal</td>
										</tr>
										<tr>
											<td><input class="dt-check"  <?php echo checkedHelp($id,'jurnal') ?> name="menu[]" type="checkbox" value="jurnal"></td>
											<td>Jurnal</td>
										</tr>

									</tbody>
								</table>
								<table class="table table-bordered roleTable">
									<tbody>
										<tr>
											<th class="no-sort selectbox bg-gray-light"></th>
											<th class="no-sort bg-gray-light">Laporan</th>
										</tr>
										<tr>
											<td>
												<input class="dt-check" <?php echo checkedHelp($id,'jurnalumum') ?> name="menu[]" type="checkbox" value="jurnalumum">
											</td>
											<td>Jurnal Umum</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'bukubesar') ?> name="menu[]" type="checkbox" value="bukubesar"></td>
											<td>Buku Besar</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'labarugi') ?> name="menu[]" type="checkbox" value="labarugi"></td>
											<td>Laba Rugi</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'ekuitas') ?> name="menu[]" type="checkbox" value="ekuitas"></td>
											<td>Perubahan Ekuitas</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'neraca') ?> name="menu[]" type="checkbox" value="neraca"></td>
											<td>Neraca</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'aruskas') ?> name="menu[]" type="checkbox" value="aruskas"></td>
											<td>Arus Kas</td>
										</tr>
									</tbody>
								</table>
								<table class="table table-bordered roleTable">
									<tbody>
										<tr>
											<th class="no-sort selectbox bg-gray-light"></th>
											<th class="no-sort bg-gray-light">Manajemen Pengguna</th>
										</tr>
										<tr>
											<td>
												<input class="dt-check" <?php echo checkedHelp($id,'pengguna') ?> name="menu[]" type="checkbox" value="pengguna">
											</td>
											<td>Pengguna</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'roles') ?> name="menu[]" type="checkbox" value="roles"></td>
											<td>Roles</td>
										</tr>
										<tr>
											<td><input class="dt-check" <?php echo checkedHelp($id,'setRoles') ?> name="menu[]" type="checkbox" value="setRoles"></td>
											<td>Set Roles</td>
										</tr>
									</tbody>
								</table>
								<button type="submit" class="btn btn-primary">Simpan</button>
							</form>

						<?php endif ?>

					</div>


				</div>

			</div>

		</div>

	</div>

	<script>
		$('.button_delete').on('click',function(e){
			e.preventDefault();
			var form = $(this).parents('form');
			swal({
				title: "Apakah Anda Yakin",
				text: "Data yang telah dihapus tidak dapat dikembalikan",
				type: "error",
				showCancelButton: true,
				cancelButtonClass: 'btn-default btn-md waves-effect',
				confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
				confirmButtonText: 'Delete!'
			}, 
			function(willDelete) {
				if (willDelete) {
					form.submit();

				}     
			});
		});
	</script>