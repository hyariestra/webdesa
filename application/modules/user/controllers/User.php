<?php 



/**

* 

*/

class user extends CI_controller

{



	function __construct()



	{



		parent::__construct();



		if (!$this->session->userdata("pengguna"))



		{



			redirect("pengguna/login");



		}



		$this->load->model('M_user');

		$this->load->library('form_validation');





	}

	

	function index()



	{

		authorize('pengguna');

		$data['judul']="Daftar Pengguna";



		$data['act'] = 'user';



		$data['admin']=$this->M_user->tampil_admin();



		$this->themeadmin->tampilkan('tampilpengguna',$data);



	}



	function tambah()



	{

		authorize('pengguna');

		$data['judul']="Tambah Pengguna";



		$data['act'] = 'userTambah';



		$data['roles'] =$this->M_user->getRoles();



		$this->themeadmin->tampilkan('tambahpengguna',$data);



	}



	function simpan()

	{

		$this->load->model("M_user");



		$this->form_validation->set_rules('email','Username','required');



		if ($this->form_validation->run() != false) {

			$datainputan=$this->input->post();



			$hasil  = $this->M_user->simpanPengguna($datainputan);



			if ($hasil=="gagal") 

			{

				$isipesan="<br><div class='alert alert-danger'>Username Sudah Terdaftar, Gunakan Username Lain!</div>";

				$this->session->set_flashdata("pesan",$isipesan);

				redirect("user/tambah");

			}

			else

			{

				$isipesan="<br><div class='alert alert-success'>Data Pengguna Berhasil ditambah</div>";

				$this->session->set_flashdata("pesan",$isipesan);

				redirect("user");

			}

		} else {

			$isipesan="<br><div class='alert alert-danger'>Username Tidak Boleh Kosong, Harap Isi Username!</div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect("user/tambah");

		}

		









	}



	function hapus($id)



	{



		$this->M_user->hapus_pengguna($id);



		redirect('user');



	}



	public function ubah($id)
	{
		$post = $this->input->post();
		$result= true;

	
		$id = decrypt_url($id);

		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '.' . $file_extension;
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_user/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar"]["size"] > 2097152 OR $_FILES["gambar"]["size"]==0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;	
			}

		}

		if ($result) {
		
			if( isset($_FILES['gambar']) ) {

				$param = array(
		
					'foto_admin_uniq' => isset($namaFile) ? $namaFile : '',
					'foto_admin' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
		
				);
			
		    	move_uploaded_file($namaSementara, $dirUpload.$namaFile);

		    	$result = $this->M_user->ubah_gambar_profile($param,$id);
			

			}	

			if($post['password']==""){


				$param = array(
					'email'=>$post['email'],
					'nama'=>$post['nama'],
					'telp'=>$post['telp'],
					'alamat'=>$post['alamat'],
					'jenis_kelamin'=>isset($post['jenis_kelamin']) ? $post['jenis_kelamin'] : null,
					'id_prov'=> isset($post['id_prov']) ? $post['id_prov'] : null,
					'id_kab'=> isset($post['id_kab']) ? $post['id_kab'] : null,
					'id_kec'=> isset($post['id_kec']) ? $post['id_kec'] :null,
					'id_desa'=> isset($post['id_desa']) ? $post['id_desa'] :null,
					'status'=> $post['status'],
					'role'=> $post['role'],

				);


			}else{

				$param = array(
					'email'=>$post['email'],
					'password'=> md5($post['password']),
					'nama'=>$post['nama'],
					'telp'=>$post['telp'],
					'alamat'=>$post['alamat'],
					'jenis_kelamin'=>isset($post['jenis_kelamin']) ? $post['jenis_kelamin'] : null,
					'id_prov'=> isset($post['id_prov']) ? $post['id_prov'] : null,
					'id_kab'=> isset($post['id_kab']) ? $post['id_kab'] : null,
					'id_kec'=> isset($post['id_kec']) ? $post['id_kec'] :null,
					'id_desa'=> isset($post['id_desa']) ? $post['id_desa'] :null,
					'status'=> $post['status'],
					'role'=> $post['role'],

				);


			}

			$result = $this->M_user->ubah($param,$id);
		
		
		
		}else{

		
		
		
		
		
		}



		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}




	}



	function ubah__($id)



	{

		$id = decrypt_url($id);

		authorize('pengguna');

		$this->load->model("M_user");



	

		$inputan=$this->input->post();



		if ($inputan) 



		{



			$this->M_user->ubah_admin($inputan,$id);

		

			$isipesan="<br><div class='alert alert-success'>Data Berhasil Diubah </div>";

			$this->session->set_flashdata("pesan",$isipesan);

			redirect('user');



		}



		$data['judul']="Ubah Pengguna";



		$data['admin']=$this->M_user->ambil_admin($id);



		$data['roles'] =$this->M_user->getRoles();

	
		$this->themeadmin->tampilkan("ubahpengguna",$data);



	}



	function profile($idRaw)

	{

		$id = decrypt_url($idRaw);

		$idAdmin = $this->session->userdata['pengguna']['id_admin'];


		$data['provinsi'] = $this->M_globalfunc->get_provinsi();


		if ($idAdmin==$id) {



			$inputan=$this->input->post();



			if ($inputan) 



			{

				$this->M_user->ubah_admin($inputan,$id);

				$isipesan="<br><div class='alert alert-success'>Data Berhasil Diubah </div>";

				$this->session->set_flashdata("pesan",$isipesan);

				redirect('user/profile/'.$id);

			}

			$data['judul']="Ubah Pengguna";
			$data['admin']=$this->M_user->ambil_admin($id);


			$data['url'] = 'user/ubah/'.$idRaw;
			$data['urlProfile'] = 'user/profile/'.$idRaw;
			$data['roles'] =$this->M_user->getRoles();

			$this->themeadmin->tampilkan("ubahpengguna",$data);

		}else{

			redirect("dashboard/notFound");

		}

	}




}



?>