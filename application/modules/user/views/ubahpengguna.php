<!-- data pelanggan ada di var $pelanggan -->



<div class="row">



	<div class="col-md-12">



		<div class="box">



			<div class="box-header">



				<h3 class="box-title"><?php echo $judul ?></h3>



				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">



					<div class="box-body">



						<div class="form-group">



							<label>Username</label>



							<input readonly type="text" name="email" class="form-control" value="<?php echo $admin['email']; ?>"></input>



							<label>Password</label>



							<input autocomplete="new-password" placeholder="kosongkan jika tidak ingin diubah.." type="password" name="password" class="form-control" value=""></input>



							<label>Nama</label>



							<input type="text" name="nama" class="form-control" value="<?php echo $admin['nama']; ?>"></input>


							<label>Telephone</label>



							<input type="text" name="telp" class="form-control" value="<?php echo $admin['telp']; ?>"></input>


						
							<div class="prov">

							
							<label>Provinsi</label>

							<select onchange="load_kabupaten(this)" id="provinsi" class="select2 form-control" name="id_prov">

							<option value="">--Pilih Provinsi--</option>

								<?php foreach ($provinsi as $key => $value): ?>



									<option value="<?php echo $value['id_prov']; ?>" <?php if($value['id_prov']==$admin['id_prov']){echo"selected";} ?>><?php echo $value['nama_prov']; ?></option>



								<?php endforeach ?>



							</select>

							</div>


					
							<div class="kabkot">
							<label>Kabupaten / Kota</label>
							
							<select onchange="load_kecamatan(this)" id="kabkot" class="select2 form-control" name="id_kab">
								</select>
							</div>


							<div class="kecamatan">
							<label>Kecamatan</label>
							<select onchange="load_desa(this)" id="kecamatan" class="select2 form-control" name="id_kec">
								</select>
							</div>

							<div class="desa">
							<label>Desa</label>
							<select  id="desa" class="select2 form-control" name="id_desa">
								</select>
							</div>

							<div class="alamat">
							<label>Alamat</label>
							<textarea class="form-control" name="alamat" id="" cols="20" rows="3"><?php echo $admin['alamat'] ?></textarea>
							</div>

							<div class="jenis_kelamin">
							<label>Jenis Kelamin</label>
							<select id="jenis_kelamin" class="select2 form-control" name="jenis_kelamin">
								<option value="">--Pilih Jenis Kelamin--</option>
								<option <?php if($admin['jenis_kelamin']=='L'){echo"selected";} ?> value="L">Laki-Laki</option>
								<option <?php if($admin['jenis_kelamin']=='P'){echo"selected";} ?> value="P">Perempuan</option>
						
							</select>
							</div>

							<div class="status">
							<label>Status</label>

							<select class="form-control" name="status">

								<option value="1" <?php if($admin['status'] == '1'){ echo 'selected'; } ?> >Aktif</option>

								<option value="0" <?php if($admin['status'] == '0'){ echo 'selected'; } ?> >Tidak Aktif</option>

							</select>
							</div>


							<div class="role">
							<label>Role</label>

							<select class="form-control" name="role">

								<?php foreach ($roles as $key => $value): ?>

									<option value="<?php echo $value['id']; ?>" <?php if($value['id']==$admin['role']){echo"selected";} ?>><?php echo $value['name']; ?></option>

								<?php endforeach ?>

							</select>
							</div>

							<label>Foto Profil</label>

							<div class="form-group">
				
									<div class="input-group">
										<input  accept="image/*" class="form-control" readonly="" type="file" name="gambar">
									</div>
									<br>
									<img width="300px" src="<?php echo base_url('asset/foto_user/'.@$admin['foto_admin_uniq']) ?>" alt="">
									<p><i>*file yang diizinkan bertipe .png, .jpeg, .jpg dan maksimal 2Mb </i></p>
										
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Ubah</button>

						<a class="btn btn-default" href="<?php echo base_url("user")?>">Batal</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
$( document ).ready(function() {


	load_kabupaten();
});

	function load_kabupaten()
	{
		var prov_id = $('#provinsi').val();
		
		var id_kab = '<?php echo $admin['id_kab']; ?>';
		
		$('select[name=id_kab]').html("");
		$('select[name=id_kab]').append('<option value="">--Pilih Kabupaten / Kota--</option>');
	
		$.ajax({
				dataType: "json",
				url: "<?php echo base_url('globalfunc/get_kabkot') ?>",
				data: {
					prov_id: prov_id
				},
				success: function(result){
					
					$.each(result.kabkot, function(key,val) {

						var select = val.kabkot_id == id_kab  ? 'selected' : '';

       			    	$('#kabkot').append("<option "+select+" value="+val.kabkot_id+">"+val.kabkot_nama+"</option>");
     			    });
					 $('#kabkot').trigger('change'); 
				}
			});
	}

	function load_kecamatan(){
		
		var kabkot_id = $('#kabkot').val();

		var id_kec = '<?php echo $admin['id_kec']; ?>';

		$('#kecamatan').html("");
		$('#kecamatan').append('<option value="">--Pilih Kecamatan--</option>');
	
		$.ajax({
				dataType: "json",
				url: "<?php echo base_url('globalfunc/get_kecamatan') ?>",
				data: {
					kabkot_id: kabkot_id
				},
				success: function(result){
					
					$.each(result.kecamatan, function(key,val) {

						var select = val.kecamatan_id == id_kec  ? 'selected' : '';

       			    	$('#kecamatan').append("<option  "+select+" value="+val.kecamatan_id+">"+val.kecamatan_nama+"</option>");
     			    });
					 $('#kecamatan').trigger('change'); 
				}
			});
	}

	function load_desa(){
		
		var id = $('#kecamatan').val();

		var id_desa = '<?php echo $admin['id_desa']; ?>';
	
		$('#desa').html("");
		$('#desa').append('<option value="">--Pilih Desa--</option>');
	
		$.ajax({
				dataType: "json",
				url: "<?php echo base_url('globalfunc/get_desa') ?>",
				data: {
					id: id
				},
				success: function(result){
					
					$.each(result.desa, function(key,val) {

						var select = val.desa_id == id_desa  ? 'selected' : '';

       			    	$('#desa').append("<option  "+select+" value="+val.desa_id+">"+val.desa_nama+"</option>");
     			    });
					 $('#desa').trigger('change'); 
				}
			});
	


	}


	$('.form-kirim').ajaxForm({ 
		dataType:  'json', 
		beforeSubmit: function(formData, jqForm, options){
			
		},
		success:   processJson,
		error: processJsonError
	});
	
	
	function processJsonError(result) {
		result = result.responseJSON;
		processJson(result, true);
	}
	
	function processJson(result) { 
		
		console.log(result);
		
		var urlProfile = '<?php echo base_url($urlProfile); ?>';

		new Noty({
			text: result.message,
			type: result.status_code,
			timeout: 3000,
			theme: 'semanticui'
		}).show();
		
		if(result.status == 201){
			window.location = urlProfile;
			
		}
	}
</script>