<!-- data pelanggan ada di var $pelanggan -->

<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title"><?php echo $judul ?></h3>
				<form action="<?php echo base_url("user/simpan") ?>" method="POST" >
					<div class="box-body">

						<div class="form-group">

							<label>Username</label>

							<input required="" type="text" name="email" class="form-control"></input>

							<label>Password</label>

							<input required="" autocomplete="new-password" placeholder="" type="password" name="password" class="form-control" ></input>

							<label>Nama</label>

							<input required="" type="text" name="nama" class="form-control"></input>

							<label>Role</label>

							<select class="form-control" name="role">

								<?php foreach ($roles as $key => $value): ?>

									<option value="<?php echo $value['id']; ?>">
										<?php echo $value['name']; ?>
									</option>

								<?php endforeach ?>

							</select>

							<label>Status</label>

							<select class="form-control" name="status">
								<option value="1">Aktif</option>
								<option value="0">Tidak Aktif</option>
							</select>	

						</div>

						

					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a class="btn btn-default" href="<?php echo base_url("user")?>">Batal</a>

					</div>

				</form>

				

			</div>

			

		</div>

		

	</div>

	

</div>