<style type="text/css">
	.container1 input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}
	.delete{
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				
				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>


						<div class="form-group">

							<label>Judul Portofolio</label>

							<input value="<?php echo @$porto['judul_porto'] ?>" type="text" name="judul_porto" class="judul_berita form-control">

						</div>

						<div class="form-group">

							<label>Client</label>

							<input value="<?php echo @$porto['client'] ?>" type="text" name="client_porto" class="judul_berita form-control">

						</div>

						<div class="form-group">

							<label>Project Url</label>

							<input value="<?php echo @$porto['project_url'] ?>" type="text" name="url_porto" class="judul_berita form-control">

						</div>


						<div class="form-group">

							<label>Kategori Portofolio</label>

						<select class="form-control" name="ref_porto" id="">
							
							<?php
							
						
							foreach ($ref_porto as $key => $value) 
							
							
							{ ?>
							
								<option <?php echo (@$porto['id_ref_porto'] == $value['id_ref_porto'] ) ? "selected": "" ?> value="<?php echo $value['id_ref_porto'] ?>"><?php echo $value['nama_porto'] ?></option>

							<?php } ?>

						</select>

						</div>

						<div class="form-group">

							<label>Project Start</label>

							<input value="<?php echo @$porto['project_start'] ?>" type="date" name="project_start" class="form-control">

						</div>

						<div class="form-group">

							<label>Project End</label>

							<input value="<?php echo @$porto['project_end'] ?>" type="date" name="project_end" class="form-control">

						</div>

		
						<div class="form-group">

							<label>Keterangan</label>

							<textarea name="isi_porto" class="form-control"  id="file-manager"><?php echo @$porto['isi_porto'] ?></textarea>

						</div>

					
								<?php
								
								for ($i=1; $i <4 ; $i++) { 
									
								
								?>

						<div class="form-group">

						<label>Gambar <?php echo $i ?> </label>

						</div>

						<div class="form-group">
							<table class="table table-bordered table-hover" style="width: 100%">

								<tbody>

									<div class="input-group">

										<input  accept="image/*" class="form-control" readonly="" type="file" name="gambar_<?php echo $i;?>">
									</div>

									<img style="width: 300px" src="<?php echo base_url('asset/foto_porto/'.@$porto['gambar_'.$i]) ?>" alt="">
									<p><i>*file yang diizinkan bertipe .png, .jpeg, .jpg dan maksimal 2Mb </i></p>
									
								</tbody>

							</table>
						</div>

						<?php } ?>

						<div class="form-group">

							<label>Status</label>
							<select class="form-control" name="status" id="">
								<option  <?php echo (@$porto['status'] == 'publish') ? "selected": "" ?> value="publish">Publish</option>
								<option  <?php echo (@$porto['status'] == 'draft') ? "selected": "" ?> value="draft">Draft</option>
							</select>

						</div>

					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('portofoliobe') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>		

	</div>

</div>




	<script>

		$('.form-kirim').ajaxForm({ 
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){

			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			console.log(result);

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('portofoliobe') ?>';

			}
		}


	</script>

