<?php 

class Portofoliobe extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))

		{

			redirect("pengguna/login");

		}

	}
	

	function index()

	{



		$this->themeadmin->tampilkan('tampilPorto',null);


	}

	function get_data_porto()
	{
		$list = $this->M_portofoliobe->get_datatables_porto();	
	//	debug_db();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->status_porto == 'publish') {
				$headline = "<span class='label label-danger'>Publish</span>";
			}else{
				$headline = "<span class='label label-default'>Draft</span>";
			}

			$idGaleri = encrypt_url($field->id_porto);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->judul_porto;
			$row[] = $field->client;
			$row[] = $field->project_url;
			$row[] = $headline;
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('portofoliobe/ubah/'.encrypt_url($field->id_porto))." >
			<span class='glyphicon glyphicon-edit'></span></a>
			<a onclick=delete_func('$idGaleri')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_portofoliobe->count_all(),
			"recordsFiltered" => $this->M_portofoliobe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['ref_porto'] =  $this->M_portofoliobe->get_ref_porto();
		$data['url'] = 'portofoliobe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		
		$this->themeadmin->tampilkan('tambahPorto',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['url'] = 'portofoliobe/edit/'.$id;
		$data['porto'] =$this->M_portofoliobe->get_detail_porto($idEn);
		$data['ref_porto'] =  $this->M_portofoliobe->get_ref_porto();
		
	
		
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahPorto',$data);

	}


	public function simpan()
	{



		$post = $this->input->post();
		$result= true;

	

		if ($post['judul_porto'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}

		
		
		
		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

	
		for ($i=1; $i < 4; $i++) { 

		
		if (isset($_FILES["gambar_".$i])) {

			$file_extension = pathinfo($_FILES["gambar_".$i]['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile[$i] = !empty($_FILES["gambar_".$i]['name']) ?  round(microtime(true)) . '_' . $_FILES["gambar_".$i]['name']  : 'aa';
			$namaSementara = $_FILES["gambar_".$i]['tmp_name'];

			$dirUpload = "asset/foto_porto/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar_".$i]["size"] > 250000 OR $_FILES["gambar_".$i]["size"]==0) {
				$err_msg[] = 'Gambar ke ' .$i. 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file ke ' .$i. 'tidak sesuai';
				$result = false;	
			}

		}

		if ( isset($namaFile[$i]) AND $result = TRUE) {
			move_uploaded_file($namaSementara, $dirUpload.$namaFile[$i]);
			
		}
	}



		if ($result) {

			$param = array(
				'judul_porto'=>$post['judul_porto'],
				'judul_porto_seo'=>slugify($post['judul_porto']),
				'id_ref_porto'=>$post['ref_porto'],
				'client'=>$post['client_porto'],
				'project_url'=>$post['url_porto'],
				'isi_porto'=>$post['isi_porto'],	
				'gambar_1' => isset($namaFile[1]) ? $namaFile[1] : '',
				'gambar_2' => isset($namaFile[2]) ? $namaFile[2] : '',
				'gambar_3' => isset($namaFile[3]) ? $namaFile[3] : '',
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'project_start'=>$post['project_start'],	
				'project_end'=>$post['project_end'],
				'tanggal'=> date("Y-m-d H:i:s"),	
				'status'=>$post['status']	

			);

		//	move_uploaded_file($namaSementara, $dirUpload.$namaFile);

			$result = $this->M_portofoliobe->simpan_porto($param);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		


		$id = decrypt_url($id);


		if ($post['judul_porto'] == "") {

			$err_msg[] = 'Judul kosong';
			$result = false;

		}


		$namaSementara ="";
		$namaFile ="";
		$dirUpload ="";

		for ($i=1; $i < 4; $i++) { 

		
			if (isset($_FILES["gambar_".$i])) {
	

			$file_extension = pathinfo($_FILES["gambar_".$i]['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile[$i] = round(microtime(true)) . '_' . $_FILES["gambar_".$i]['name'];
			$namaSementara = $_FILES["gambar_".$i]['tmp_name'];

			$dirUpload = "asset/foto_porto/";
			$valid_ext = array('png','jpeg','jpg','gif');


			if ($_FILES["gambar_".$i]["size"] > 250000 OR $_FILES["gambar_".$i]["size"]==0) {
				$err_msg[] = 'Gambar ke ' .$i. 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if(!in_array($file_extension,$valid_ext)){
				$err_msg[] = 'Ekstensi file ke ' .$i. 'tidak sesuai';
				$result = false;	
			}

			if ( isset($namaFile[$i]) AND $result = TRUE) {
				move_uploaded_file($namaSementara, $dirUpload.$namaFile[$i]);
				
			}

		}
	}


		if ($result) {

			for ($i=1; $i < 4; $i++) { 
			
			if (!isset($_FILES["gambar_".$i])) {
				
				
					$param = array(
						'judul_porto'=>$post['judul_porto'],
						'judul_porto_seo'=>slugify($post['judul_porto']),
						'id_ref_porto'=>$post['ref_porto'],
						'client'=>$post['client_porto'],
						'project_url'=>$post['url_porto'],
						'isi_porto'=>$post['isi_porto'],	
						'id_user'=>getSession(),
						'id_desa'=>getSettingDesa()['id_desa'],
						'project_start'=>$post['project_start'],	
						'project_end'=>$post['project_end'],
						'tanggal'=> date("Y-m-d H:i:s"),	
						'status'=>$post['status']	
					);
					
				

			}else{

				$param = array(
					'judul_porto'=>$post['judul_porto'],
					'judul_porto_seo'=>slugify($post['judul_porto']),
					'id_ref_porto'=>$post['ref_porto'],
					'client'=>$post['client_porto'],
					'project_url'=>$post['url_porto'],
					'isi_porto'=>$post['isi_porto'],	
					'gambar_'.$i => isset($namaFile[$i]) ? $namaFile[$i] : '',
					'id_user'=>getSession(),
					'id_desa'=>getSettingDesa()['id_desa'],
					'project_start'=>$post['project_start'],	
					'project_end'=>$post['project_end'],
					'tanggal'=> date("Y-m-d H:i:s"),	
					'status'=>$post['status']	

				);

			}
			$result = $this->M_portofoliobe->ubah_porto($param,$id);
		}
		
	
		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}
	function delete($id){

		$idEn = decrypt_url($id);

		$data =$this->M_portofoliobe->get_detail_porto($idEn);


		$dirUpload = "asset/foto_porto/";

		for ($i=1; $i < 4; $i++) { 
			$files[$i] = $dirUpload.$data['gambar_'.$i];

		
			if (!empty($data['gambar_'.$i])) {
			
				if (file_exists($files[$i])) {
					unlink($files[$i]);
				} 
	
			}

		}


		$result = $this->M_portofoliobe->delete_porto($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>