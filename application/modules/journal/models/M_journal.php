<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class M_journal extends CI_model
{



  function loadDataAkun()



  {



    echo $this->treeDataKodeAkun();



  }



  public function getData($rowno,$rowperpage,$data=[])

  {





   $this->db->select('*');

   $this->db->from('trx_ju');







   if ($data['cari']) {

    $query = $this->db->like("nomor", $data['cari']);

    $query = $this->db->or_like('uraian', $data['cari']);

    $data['has'] = 'show'; 

  }



  if ($data['idakun']) {

    $query = $this->db->like("id_akun", $data['idakun']);

    $data['has'] = 'show'; 

  }



  if ($data['start']) {

    $query = $this->db->where('tanggal >=', $data['convStart']);

    $query = $this->db->where('tanggal <=', $data['convEnd']);

    $data['has'] = 'show'; 

  } 







  $query = $this->db->order_by("tanggal", "DESC");

  $query =  $this->db->join('trx_judet', 'trx_judet.id_ju = trx_ju.id_ju');

  $query =  $this->db->where('id_jenis_trans', '1');

  $query =  $this->db->group_by('trx_ju.id_ju');

  $query =  $this->db->limit($rowperpage, $rowno); 

  $query = $this->db->get();

  $data['hasil'] =  $query->result_array();





  return $data;



}



public function getrecordCount($data=[]) {





  $this->db->select('*');

  $this->db->from('trx_ju');





  if ($data['cari']) {

    $query = $this->db->like("nomor", $data['cari']);

    $query = $this->db->or_like('uraian', $data['cari']);

    $data['has'] = 'show'; 

  }



  if ($data['idakun']) {

    $query = $this->db->like("id_akun", $data['idakun']);

    $data['has'] = 'show'; 

  }



  if ($data['start']) {

    $query = $this->db->where('tanggal >=', $data['convStart']);

    $query = $this->db->where('tanggal <=', $data['convEnd']);

    $data['has'] = 'show'; 

  } 







  $query = $this->db->order_by("tanggal", "DESC");

  $query =  $this->db->join('trx_judet', 'trx_judet.id_ju = trx_ju.id_ju');

  $query =  $this->db->where('id_jenis_trans', '1');

  $query =  $this->db->group_by('trx_ju.id_ju');

  

  $query = $this->db->get()->num_rows();



  return $query;

}





private  function treeDataKodeAkun($tipeAkun = '')

{



  $strHeader = '';

  $strJsonData = '[';



  $strJsonData .= $this->recursiveDataKodeAkun(0, $strJsonData);

  $strJsonData = substr($strJsonData, 1, strlen($strJsonData) - 2);

  $strJsonData .= ']';





  return $strJsonData;

}





private function recursiveDataKodeAkun($parent=0, $hasil)

{



  $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 

   kode_akun AS KodeAkun, header as Header, 

   nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 

   saldo_normal as SaldoNormal

   FROM mst_akun 

   WHERE id_induk='".$parent."' 

   ORDER BY kodeUrut ASC ");



  foreach( $selectQuery->result_array() as $row )

  {



   $IDAkun         = $row['IDAkun'];

   $IDInduk        = $row['IDInduk'];

   $kodeInduk      = $row['KodeInduk'];

   $header         = $row['Header'];

   $kodePlainText  = $row['KodeAkun'];

   $namaPlainText  = $row['NamaAkun']; 

   $saldoNormal    = $row['SaldoNormal'];

   $level          = $row['Level'];

   $concatAkun     = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

   $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 

   $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

   $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;

   $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;



   $strSpace = 0;



   for($i = 1; $i <= $level ; $i++)

   {

    $strSpace += 10;

  }



  $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 

    FROM mst_akun 

    WHERE id_induk = '".$IDAkun."' ");



  $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 



  $CountAkun  = $arrSelectQuery['CountAkun'];



  $btnTambah   = $header == 0 ? '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-success\' onclick=\'PilihAkun(this);\'><span class=\'glyphicon glyphicon-plus-sign\' aria-hidden=\'true\'></button>&nbsp;' : '';  



  $strPadding = ($level > 1) ?  $strSpace."px" : "0px";



  $actionList = $btnTambah; 



  $hasil .= '{"idAkun"         : "'.$IDAkun .'", 

  "kodeWithFormat"  : "'.$kodeWithFormat.'",

  "namaWithFormat"  : "'.$namaWithFormat.'",

  "saldoNormal"     : "'.$saldoNormal.'",

  "concatAkun"      : "'.$concatAkun.'", 

  "strPadding"      : "'.$strPadding.'", 

  "tombol"          : "'.$actionList.'"},';



  $hasil = $this->recursiveDataKodeAkun($row['IDAkun'], $hasil);

          }  //foreach( $selectQuery->result_array() as $row )



          return $hasil;

        } 



        function loadDataAkunSaldoAwal()



        {



          echo $this->treeDataKodeAkunSaldoAwal();



        }



        private  function treeDataKodeAkunSaldoAwal($tipeAkun = '')

        {



          $strHeader = '';

          $strJsonData = '[';



          $strJsonData .= $this->recursiveDataKodeAkunSaldoAwal(0, $strJsonData);

          $strJsonData = substr($strJsonData, 1, strlen($strJsonData) - 2);

          $strJsonData .= ']';





          return $strJsonData;

        }





        private function recursiveDataKodeAkunSaldoAwal($parent=0, $hasil)

        {



          $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 

           kode_akun AS KodeAkun, header as Header, 

           nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 

           saldo_normal as SaldoNormal

           FROM mst_akun 

           WHERE id_induk='".$parent."' 

           ORDER BY kodeUrut ASC ");



          foreach( $selectQuery->result_array() as $row )

          {



           $IDAkun         = $row['IDAkun'];

           $IDInduk        = $row['IDInduk'];

           $kodeInduk      = $row['KodeInduk'];

           $header         = $row['Header'];

           $kodePlainText  = $row['KodeAkun'];

           $namaPlainText  = $row['NamaAkun']; 

           $saldoNormal    = $row['SaldoNormal'];

           $level          = $row['Level'];

           $concatAkun     = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

           $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 

           $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

           $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;

           $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;



           $strSpace = 0;



           for($i = 1; $i <= $level ; $i++)

           {

            $strSpace += 20;

          }



          $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 

            FROM mst_akun 

            WHERE id_induk = '".$IDAkun."' ");



          $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 



          $CountAkun  = $arrSelectQuery['CountAkun'];

          $strPadding = ($level > 1) ?  $strSpace."px" : "0px";

          $strPadding = ($level == 2) ?  "30px" : $strPadding;



          $arrSaldoAwal = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS TotalSaldoAwal FROM trx_ju a

           LEFT JOIN trx_judet b

           ON a.id_ju = b.id_ju

           LEFT JOIN mst_akun c

           ON c.id_akun = b.id_akun 

           WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$concatAkun."%' AND id_jenis_trans = (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA')");



          $saldoAwal = $arrSaldoAwal->num_rows() > 0 ? $arrSaldoAwal->first_row()->TotalSaldoAwal : 0;



          $inputSaldoAwal = $header == 0 ? '<input type=\'hidden\' name=\'IDAkun[]\' value=\''.$IDAkun.'\'><input type=\'text\' style=\'text-align:right;\' role=\'numeric\' name=\'nominal[]\' onclick=\'clearNominal(this)\' class=\'form-control\' value=\''.$saldoAwal.'\' tipe=\'inputNominal\'>' : '<span style=\'text-align:right;\'><b>'.formatCurrency($saldoAwal).'</b></span>';



          $hasil .= '{"idAkun"         : "'.$IDAkun .'", 

          "kodeWithFormat"  : "'.$kodeWithFormat.'",

          "namaWithFormat"  : "'.$namaWithFormat.'",

          "saldoNormal"     : "'.$saldoNormal.'",

          "concatAkun"      : "'.$concatAkun.'", 

          "saldoAwal"       : "'.$inputSaldoAwal.'",

          "strPadding"      : "'.$strPadding.'"},';



          $hasil = $this->recursiveDataKodeAkunSaldoAwal($row['IDAkun'], $hasil);

          }  //foreach( $selectQuery->result_array() as $row )



          return $hasil;

        } 





        function loadDataAkunCari($KataKunci=''){



          $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 

           kode_akun AS KodeAkun, header as Header, 

           nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 

           saldo_normal as SaldoNormal

           FROM mst_akun 

           WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '%".$KataKunci."%' OR nama_akun LIKE '%".$KataKunci."%'

           ORDER BY kodeUrut ASC ");



          foreach( $selectQuery->result_array() as $row )

          {



           $IDAkun         = $row['IDAkun'];

           $IDInduk        = $row['IDInduk'];

           $kodeInduk      = $row['KodeInduk'];

           $header         = $row['Header'];

           $kodePlainText  = $row['KodeAkun'];

           $namaPlainText  = $row['NamaAkun']; 

           $saldoNormal    = $row['SaldoNormal'];

           $level          = $row['Level'];

           $concatAkun     = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

           $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 

           $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

           $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;

           $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;



           $strSpace = 0;



           for($i = 1; $i <= $level ; $i++)

           {

            $strSpace += 10;

          }

          $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 

            FROM mst_akun 

            WHERE id_induk = '".$IDAkun."' ");



          $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 



          $CountAkun  = $arrSelectQuery['CountAkun'];



          $strPadding = ($level > 1) ?  $strSpace."px" : "0px";



          $btnTambah   = $header == 0 ? '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-success\' onclick=\'PilihAkun(this);\'><span class=\'glyphicon glyphicon-plus-sign\' aria-hidden=\'true\'></button>&nbsp;' : '';  



          $actionList = $btnTambah; 



          $hasil["idAkun"]         = $IDAkun;

          $hasil["kodeWithFormat"] = $kodeWithFormat;

          $hasil["namaWithFormat"] = $namaWithFormat;

          $hasil["saldoNormal"]    = $saldoNormal;

          $hasil["concatAkun"]     = $concatAkun;

          $hasil["tombol"]         = $actionList;

          $hasil["strPadding"]     = $strPadding;



          $json[] = $hasil;



        }



        echo json_encode($json);



      }



      function loadDataAkunCariSaldoAwal($KataKunci=''){



        $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 

         kode_akun AS KodeAkun, header as Header, 

         nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 

         saldo_normal as SaldoNormal

         FROM mst_akun 

         WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '%".$KataKunci."%' OR nama_akun LIKE '%".$KataKunci."%'

         ORDER BY kodeUrut ASC ");



        foreach( $selectQuery->result_array() as $row )

        {



         $IDAkun         = $row['IDAkun'];

         $IDInduk        = $row['IDInduk'];

         $kodeInduk      = $row['KodeInduk'];

         $header         = $row['Header'];

         $kodePlainText  = $row['KodeAkun'];

         $namaPlainText  = $row['NamaAkun']; 

         $saldoNormal    = $row['SaldoNormal'];

         $level          = $row['Level'];

         $concatAkun     = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

         $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 

         $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;

         $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;

         $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;



         $strSpace = '';



         for($i = 1; $i <= $level ; $i++)

         {

          $strSpace .= "&nbsp;";

        }



        $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 

          FROM mst_akun 

          WHERE id_induk = '".$IDAkun."' ");



        $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 



        $CountAkun  = $arrSelectQuery['CountAkun'];



        $imgRoot   = "<span class='glyphicon glyphicon-home'></span>".$strSpace;

        $imgFolder = $strSpace."<span class='glyphicon glyphicon-folder-open'></span>&nbsp;";

        $imgItem   = $strSpace."<span class='glyphicon glyphicon-folder-close'></span>&nbsp;";



        $imgChild  = ( $CountAkun > 0 ) ? $imgFolder : $imgItem;



        $kodeWithFormat = ($level == 1) ? $imgRoot.$kodeWithFormat : $kodeWithFormat;

        $kodeWithFormat = ($level > 1) ?  $imgChild.$kodeWithFormat : $kodeWithFormat;



        $arrSaldoAwal = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS TotalSaldoAwal FROM trx_ju a

         LEFT JOIN trx_judet b

         ON a.id_ju = b.id_ju

         LEFT JOIN mst_akun c

         ON c.id_akun = b.id_akun 

         WHERE b.id_akun = '".$IDAkun."' AND id_jenis_trans = (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA')");



        $saldoAwal = $arrSaldoAwal->num_rows() > 0 ? $arrSaldoAwal->first_row()->TotalSaldoAwal : 0;



        $inputSaldoAwal = $header == 0 ? '<input type=\'hidden\' name=\'IDAkun[]\' value=\''.$IDAkun.'\'><input type=\'text\' style=\'text-align:right;\' role=\'numeric\' name=\'nominal[]\' onclick=\'clearNominal(this)\' class=\'form-control\' value=\''.$saldoAwal.'\' tipe=\'inputNominal\'>' : '';



        $hasil["idAkun"]         = $IDAkun;

        $hasil["kodeWithFormat"] = $kodeWithFormat;

        $hasil["namaWithFormat"] = $namaWithFormat;

        $hasil["saldoNormal"]    = $saldoNormal;

        $hasil["concatAkun"]     = $concatAkun;

        $hasil["saldoAwal"]      = $inputSaldoAwal;



        $json[] = $hasil;



      }



      echo json_encode($json);



    }





    public function SimpanJU($Nomor = '', $Tanggal = '', $Uraian = '', $data = array()){



      $IDAkun = $data['IDAkunSimpan'];

      $Debit  = $data['debit'];

      $Kredit = $data['kredit'];

      $Memo   = $data['memo'];



      $jumlah = count($data);



      $selectQuery = $this->db->query("SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'JU'");



      $IDJenisTrans = $selectQuery->first_row()->id_jenis_trans;



      $this->db->trans_begin();



      $this->db->query("INSERT INTO trx_ju (id_jenis_trans, nomor, uraian, tanggal, date_entry)

        VALUES ('".$IDJenisTrans."', '".$Nomor."', '".$Uraian."', '".$Tanggal."', NOW())");



      $IDJU = $this->db->insert_id();



        //for ($i=0; $i < $jumlah; $i++) { 

      $i=0;

      foreach ($IDAkun as $key => $value){



        $this->db->query("INSERT INTO trx_judet (id_ju, id_akun, memo, debet, kredit, date_entry)

          VALUES ('".$IDJU."', '".$IDAkun[$i]."', '".$Memo[$i]."', '".strToCurrDB($Debit[$i])."', '".strToCurrDB($Kredit[$i])."', NOW())");

        $i++;

      }



      if ($this->db->trans_status() === FALSE)

      {  

        $this->db->trans_rollback();  



        $strMessage['pesan']  = 'Gagal dalam menyimpan data akun';



        $strMessage['hasil']  = 'gagal';



      }  

      else

      {



        $this->db->trans_commit();



        $strMessage['pesan']  = 'Akun Berhasil Ditambahkan';



        $strMessage['hasil']  = 'berhasil';



      }



      echo json_encode($strMessage);





    }



    function SimpanSaldoAwal($data=array()){



      $IDAkun  = $data['IDAkun'];

      $Nominal = $data['nominal'];



      $Nomor = getNextNo(getAutoNum('JU'));



      $this->db->trans_begin();



        //delete semua dulu

      $this->db->query("DELETE a,b FROM trx_ju a LEFT JOIN trx_judet b ON a.id_ju = b.id_ju WHERE id_jenis_trans IN 

        (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA')");



      $queryJenisTrans = $this->db->query("SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA'");



      $IDJenisTrans = $queryJenisTrans->first_row()->id_jenis_trans;



        //baru insert

      $this->db->query("INSERT INTO trx_ju (id_jenis_trans, nomor, uraian, tanggal, date_entry)

        VALUES ('".$IDJenisTrans."', '".$Nomor."', 'Saldo Awal', '2019-01-01', NOW())");



      $IDJU = $this->db->insert_id();



      $i=0;

      foreach ($IDAkun as $key => $value){



        $querySaldoNormal = $this->db->query("SELECT saldo_normal FROM mst_akun WHERE id_akun = '".$IDAkun[$i]."'");



        $SaldoNormal = $querySaldoNormal->first_row()->saldo_normal;





        $Debet  = ($SaldoNormal == 'D' && $Nominal[$i] >= 0) || ($SaldoNormal == 'K' && $Nominal[$i] < 0) ? $Nominal[$i] < 0 ? $Nominal[$i] * -1 : $Nominal[$i] : '0';



        $Kredit = ($SaldoNormal == 'D' && $Nominal[$i] < 0)  || ($SaldoNormal == 'K' && $Nominal[$i] >= 0) ? $Nominal[$i] < 0 ? $Nominal[$i] * -1 : $Nominal[$i] : '0';



        $this->db->query("INSERT INTO trx_judet (id_ju, id_akun, memo, debet, kredit, date_entry)

          VALUES ('".$IDJU."', '".$IDAkun[$i]."', '', '".strToCurrDB($Debet)."', '".strToCurrDB($Kredit)."', NOW())");

        $i++;

      }



      if ($this->db->trans_status() === FALSE)

      {  

        $this->db->trans_rollback();  



        $strMessage['pesan']  = 'Gagal dalam menyimpan data saldo awal';



        $strMessage['hasil']  = 'gagal';



      }  

      else

      {



        $this->db->trans_commit();



        $strMessage['pesan']  = 'Saldo Awal Berhasil Disimpan';



        $strMessage['hasil']  = 'berhasil';



      }



      echo json_encode($strMessage);



    }



    public function EditJU($Nomor = '', $Tanggal = '', $Uraian = '', $data = array(), $IDJU = 0){



      $IDAkun = $data['IDAkunSimpan'];

      $Debit  = $data['debit'];

      $Kredit = $data['kredit'];

      $Memo   = $data['memo'];



      $selectQuery = $this->db->query("SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'JU'");



      $IDJenisTrans = $selectQuery->first_row()->id_jenis_trans;



      $this->db->trans_begin();



      $this->db->query("UPDATE trx_ju SET nomor = '".$Nomor."', tanggal = '".$Tanggal."', uraian = '".$Uraian."', date_entry = NOW()

        WHERE id_ju = '".$IDJU."'");



      $this->db->query("DELETE FROM trx_judet WHERE id_ju = '".$IDJU."'");



        //for ($i=0; $i < $jumlah; $i++) { 

      $i=0;

      foreach ($IDAkun as $key => $value){



        $this->db->query("INSERT INTO trx_judet (id_ju, id_akun, memo, debet, kredit, date_entry)

          VALUES ('".$IDJU."', '".$IDAkun[$i]."', '".$Memo[$i]."', '".strToCurrDB($Debit[$i])."', '".strToCurrDB($Kredit[$i])."', NOW())");

        $i++;

      }



      if ($this->db->trans_status() === FALSE)

      {  

        $this->db->trans_rollback();  



        $strMessage['pesan']  = 'Gagal dalam menyimpan data akun';



        $strMessage['hasil']  = 'gagal';



      }  

      else

      {



        $this->db->trans_commit();



        $strMessage['pesan']  = 'Akun Berhasil Ditambahkan';



        $strMessage['hasil']  = 'berhasil';



      }



      echo json_encode($strMessage);





    }





    public function HapusJU($IDJU = 0){

      $this->db->trans_begin();

      $this->db->query("DELETE a,b FROM trx_ju a LEFT JOIN trx_judet b ON a.id_ju = b.id_ju WHERE a.id_ju = '".$IDJU."' ");

      if ($this->db->trans_status() === FALSE)

      {  

        $this->db->trans_rollback();  

  $hasil = 'gagal';

      }  

      else

      {

        $this->db->trans_commit();
    $hasil = 'berhasil';


      }
  return $hasil;
    }

  }
?>