<style type="text/css" media="screen">
	.control-label{
		text-align: left !important;
	}
</style>
<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $judul ?></h3>

			</div>
			<div class="box-body">
				<div class="form-horizontal text-left">

					<?php
					foreach ($jurnal as $key) {

						$IDJUBaru = $key['id_ju'];
						?>

						<div class="box-body">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">No Transaksi #</label>
								<div class="col-sm-10">
									<input type="hidden" name="IDJUSimpan" id="IDJUSimpan" value="<?php echo $key['id_ju']?>">
									<input class="form-control" id="NomorTransaksi" type="text" readonly="readonly" value="<?php echo $key['nomor']?>">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Tanggal</label>
								<div class="col-sm-10">
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input  style="background-color: white" readonly="" type="text" class="form-control pull-right" id="datepicker" value="<?php echo formatDateIndo($key['tanggal'])?>" data-date-format='dd-mm-yyyy' required>
									</div>
								</div>
							</div>


							<div class="form-group" style="margin-top: 20px;">
								<label for="" class="col-sm-2 control-label">Uraian</label>
								<div class="col-sm-10">
									<textarea class="form-control" id="Uraian" rows="3" placeholder=""><?php echo $key['uraian']?></textarea>
								</div>
							</div>
						<!-- <div class="form-group">
							<label for="" class="col-sm-2 control-label">Source</label>
							<div class="col-sm-10">
								<div class="input-group input-group--file">
									<div class="input-group-addon">
										<span class="fa fa-upload"></span>
										Upload
									</div>
									<label class="input-file-box form-control">
										<input id="upload-file" name="" class="file" aria-invalid="false" type="file">
										
									</label>
								</div>
							</div>
						</div> -->
						<div class="form-group" style="padding: 0 15px;">
							<h5 style="display: inline-block;border-bottom: 1px solid rgba(0,0,0,.2);padding-bottom: 5px;">Detail Transaksi</h5>
							<form id="FormTransaksi">
								<table id="tableEdit" class="table table-bordered table-hover" style="width: 100%">
									<thead>
										<tr>
											<th>Kode Akun</th>
											<th>Nama</th>
											<th>Debit</th>
											<th>Kredit</th>
											<th>Memo</th>
											<th class="text-center"><a href="#" class="repeat-add"><i class="fa fa-plus-circle"></i></a></th>
										</tr>
									</thead>
									<tbody>
										<?php 

										$EditDetail = $this->db->query("SELECT * FROM trx_judet LEFT JOIN mst_akun 
											ON mst_akun.id_akun = trx_judet.id_akun
											WHERE id_ju = '".$IDJUBaru."'")->result_array();

										$i=1;
										foreach ($EditDetail as $key => $row) {

											$strAutoFillDebet  = $i == 1 ? 'role="debetAwal" onblur="autoFillDebet(this)"' : '';

											$strAutoFillKredit = $i == 1 ? 'role="KreditAwal" onblur="autoFillKredit(this)"' : '';

											$strDebetAkhit 	   = $i == 2 ? 'role="debetAkhir"' : '';

											$strKreditAkhit    = $i == 2 ? 'role="kreditAkhir"' : '';

											?>
											<tr role="row<?php echo $i?>">
												<td>
													<div class="input-group">
														<input class="form-control" readonly="" type="text" name="KodeAkun[]" 
														value="<?php echo $row['kode_induk'].'.'.$row['kode_akun']?>">
														<input type="hidden" name="IDAkunSimpan[]" id="IDAkunSimpan[]" value="<?php echo $row['id_akun']?>">
														<span class="input-group-btn">

															<a class="btn btn-primary" onclick="loadDataAkun(this)" data-toggle="modal" href='#modal-id'><i class="fa fa-plus-square" aria-hidden="true"></i></a>
														</span>
													</div>
												</td>
												<td>
													<input  readonly="" type="text" class="form-control" name="NamaAkun[]" value="<?php echo $row['nama_akun']?>">
												</td>
												<td>
													<div class="input-group">
														<span class="input-group-addon">Rp</span>
														<input class="form-control currency-format debit" type="text" name="debit[]" <?php echo $strAutoFillDebet?> 
														<?php echo $strDebetAkhit?> value="<?php echo formatCurrencyNonDecimal(substr($row['debet'],0,-5))?>">
													</div>
												</td>
												<td>
													<div class="input-group">
														<span class="input-group-addon">Rp</span>
														<input class="form-control currency-format kredit" type="text" name="kredit[]" <?php echo $strAutoFillKredit?> 
														<?php echo $strKreditAkhit?> value="<?php echo formatCurrencyNonDecimal(substr($row['kredit'],0,-5))?>">
													</div>
												</td>

												<td>
													<input type="text" class="form-control" name="memo[]" value="<?php echo $row['memo']?>">
												</td>
												<td class="text-center">
													<?php if ($key==0 OR $key==1 ): ?>


														<?php else: ?>

															<a href='#' class='repeat-remove'><i class='fa fa-minus-circle'></i></a>
														<?php endif ?>
													</td>
												</tr>
												<?php 

												$i++;

											} 
										}
										?>
										<tr id="total_row">
											<td></td>
											<td class="text-right"><strong>Total</strong></td>
											<td class="text-right" id="total_debit"></td>
											<td class="text-right" id="total_kredit"></td>
											<td></td>
										</tr>
										<tr id="total_balance">
											<td></td>
											<td class="text-right"><strong>Out of Balance</strong></td>
											<td class="text-right" id="balance"></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>
					</div>
					<input readonly="" class="form-control" id="totalDetail" type="hidden" name="total">
					<input readonly="" class="form-control" id="totalDetail2" type="hidden" name="total">
					<div class="box-footer text-right">
						<a href="<?php echo site_url('journal') ?>" class="btn btn-default">Batal</a>
						<button type="submit" id="simpan" class="btn btn-primary">Simpan</button>
					</div>

				</div>

				<!-- /.col -->
			</div>
		</div>

	</div>
</div>	



<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Browse Kode Akun</h4>
			</div>
			<div class="modal-body">
				<div class="row">

					<div style="margin-bottom: 20px" class="col-sm-3 col-sm-offset-9">
						<div class="input-group">
							<input type="hidden" name="roleTampungan" id="roleTampungan" value="">
							<input placeholder="masukan pencarian..." id="InputKataKunci" name="InputKataKunci" type="text" class="form-control" aria-label="...">
							<div class="input-group-btn">
								<button type="button" class="btn btn-default dropdown-toggle" onclick="cariAkun()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="fa fa-search"></span>
								</button>
							</div><!-- /btn-group -->
						</div><!-- /input-group -->
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover" id="tabelAkun">
					<thead style="background-color: #ecf0f1">
						<tr>
							<th>Kode</th>
							<th>Nama</th>
							<th></th>
						</tr>
					</thead>
					
					<tbody name="tabelContent" id="tabelContent">
						
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
    //Initialize Select2 Elements

    //Date picker
    $('#datepicker').datepicker({
    	autoclose: true,
    	dateFormat: 'dd-mm-yyyy'
    });

    // currency
    $(document).on('keyup', '.currency-format', function() {
    	var amount = $(this).val().replace(/\D/g,'');
    	$(this).val(accounting.formatMoney(amount, "", 0, ","));
    });

    // get uploading file name
    $('.file').bind('change', function() {
    	var file = this.files[0];
    	var filename = file.name;

    	$(this).parent().find('.file-name').html(filename);
    });
});

  // COUNT TOTAL
  $(document).ready( function () { 

		//$("#tabelku").DataTable();

  	//getAutoNum();
  	countDebit();
  	countCredit();
  	countBalance();

  	$("input[name='InputKataKunci']").on('keypress', function(e){
  		if(e.which == 13)
  		{
			//alert("oke");
			cariAkun();
		}
	});

  	$(document).on('keyup', '.currency-format', function() {

  		countBalance();

  	});

  	$(document).on('keyup', '.debit', function() {
  		var totalcost= $(this).val().replace(/\D/g,'');
  		countDebit();
  	});
  	$(document).on('keyup', '.kredit', function() {
  		var totalcost= $(this).val().replace(/\D/g,'');
  		countCredit();
  	});       
  	//var lanjut = 2;
  	$(".repeat-add").click(function(e){
  		e.preventDefault();
  		var balance = countBalance()

  		var money  = accounting.formatMoney(balance, "", 0, ",");

  		var money = money.replace('-','');

  		console.log(money);
  		
  		var lanjut = $("#tableEdit tr").length;
  		var masukan = "<tr role='row"+lanjut+"'>";
  		masukan += "<td>";
  		masukan += "<div class='input-group'>";
  		masukan += "<input class='form-control' readonly='' type='text' name='KodeAkun[]'>";
  		masukan += "<input type='hidden' name='IDAkunSimpan[]' id='IDAkunSimpan[]' value=''>";
  		masukan += "<span class='input-group-btn'>";
  		masukan += "<a class='btn btn-primary' onclick='loadDataAkun(this)' data-toggle='modal' href='#modal-id'>";
  		masukan += "<i class='fa fa-plus-square' aria-hidden='true'></i></a>";
  		masukan += "</span>";
  		masukan += "</div>";
  		masukan += "</td>";
  		masukan += "<td>";
  		masukan += "<input type='text' readonly='' class='form-control' name='NamaAkun[]'>";
  		masukan += "</td>";
  		masukan += "<td>";
  		masukan += "<div class='input-group'>";
  		masukan += "<span class='input-group-addon'>Rp</span>";
  		if (balance<0) {

				masukan += "<input class='form-control currency-format debit' value='"+money+"' type='text' name='debit[]'></div>"
				masukan += "</td>";
				masukan += "<td>";
				masukan += "<div class='input-group'> <span class='input-group-addon'>Rp</span>";
				masukan+= "<input class='form-control currency-format kredit' value='0' type='text' name='kredit[]'></div>"
			}else{
				masukan += "<input class='form-control currency-format debit' value='0' type='text' name='debit[]'></div>"
				masukan += "</td>";
				masukan += "<td>";
				masukan += "<div class='input-group'> <span class='input-group-addon'>Rp</span>";
				masukan+= "<input class='form-control currency-format kredit' value='"+money+"' type='text' name='kredit[]'></div>"
			}
  		masukan+= "</td>";
  		masukan+="<td>";
  		masukan+="<input type='text' class='form-control' name='memo[]'>";
  		masukan+="</td>";


  		masukan+= "<td class='text-center'> <a href='#' class='repeat-remove'><i class='fa fa-minus-circle'></i></a></td>";
  		masukan+= "</tr>";
  		$('#total_row').before(masukan); 
  		countDebit();
  		countCredit();
  		countBalance();                      

  	});  
  	$(document).on('click', '.repeat-remove', function(e){
  		e.preventDefault();
  		$(this).closest('tr').remove();
  		countDebit();
  		countCredit();
  		countBalance();
  	});   
  }); 

  $(function () {


  	$('#simpan').on('click',function(e){
  		e.preventDefault();
  		var form = $(this).parents('form');

  		var total1 = $('#totalDetail').val();
  		var total2 = $('#totalDetail2').val();

  		var AnswerInput = document.getElementsByName('NamaAkun[]');
  		for (i=0; i<AnswerInput.length; i++)
  		{
  			if (AnswerInput[i].value == "")
  			{
  				swal({
  					title: "Data Gagal Disimpan",
  					text: "Periksa Kembali Inputan Anda",
  					type: "error",
  					confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
  					closeOnConfirm: false,
  					closeOnCancel: false
  				});
  				return false;
  			}
  		}


  		if (total1 !== total2  ) {
  			swal({
  				title: "Data Gagal Disimpan",
  				text: "Saldo Anda Belum Balance",
  				type: "error",
  				confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
  				closeOnConfirm: false,
  				closeOnCancel: false
  			});
  		}else{

  			var FormSerialize  	 = $("#FormTransaksi").serialize();
  			var NomorTransaksi 	 = $("#NomorTransaksi").val();
  			var TanggalTransaksi = $("#datepicker").val();
  			var Uraian 			 = $("#Uraian").val();
  			var IDJU 			 = $("#IDJUSimpan").val();
  			
  			var target = "<?php echo site_url("journal/EditData")?>";

  			var data = {

  				FormSerialize : FormSerialize,
  				Nomor 		  : NomorTransaksi,
  				Tanggal 	  : TanggalTransaksi,
  				Uraian	      : Uraian,
  				IDJU 		  : IDJU

  			}

  			$.post(target, data, function(e){

	            	//console.log(e);

	            	var json = $.parseJSON(e);

	            	if (json.hasil == 'berhasil') {

	            		swal("Success", "Transaksi Telah Berhasil Diubah", "success");

	            		window.location.href = '<?php echo base_url()?>journal';

	            	}else{

	            		swal("Gagal", "Transaksi Tidak Dapat Diubah", "error");

	            	}


	            });

  		}


  	});


  });  

  function countBalance(){

  	var total1 = 0;
  	$('.debit').each(function() {
  		total1 = total1 + parseInt($(this).val().replace(/\D/g,''));
  		total1Clean = total1;
  	});  

  	var total2 = 0;
  	$('.kredit').each(function() {
  		total2 = total2 + parseInt($(this).val().replace(/\D/g,''));
  		total2Clean = total2;
  	});  

  	var e = total1Clean-total2Clean;

  	$('#balance').html(accounting.formatMoney(e, "", 0, ","));

  	return e;

  }

  function countDebit(){
  	var total = 0;
  	$('.debit').each(function() {
  		total = total + parseInt($(this).val().replace(/\D/g,''));
  	});        
  	$('#total_debit').html(accounting.formatMoney(total, "", 0, ",")); 
  	$('#totalDetail').val(total);
  }
  function countCredit(){
  	var total = 0;
  	$('.kredit').each(function() {
  		total = total + parseInt($(this).val().replace(/\D/g,''));
  	});        
  	$('#total_kredit').html(accounting.formatMoney(total, "", 0, ",")); 
  	$('#totalDetail2').val(total);

  }

  function autoFillDebet(Obj){

  	var Nilai = $(Obj).val();

  	$("input[role='kreditAkhir']").val(Nilai);

  	countDebit();
  	countCredit();
  	countBalance();

  }

  function autoFillKredit(Obj){

  	var Nilai = $(Obj).val();

  	$("input[role='debetAkhir']").val(Nilai);

  	countDebit();
  	countCredit();
  	countBalance();

  }

  function cariAkun(){

  	var KataKunci = $("#InputKataKunci").val();

  	if (KataKunci == '') {

  		loadDataAkun('mbalik');

  	}else{

  		loadDataAkunCari(KataKunci);

  	}

  }

  function loadDataAkunCari(Obj){

  	$("#tabelContent").empty();

  	var identitas = $("#roleTampungan").val();

  		//console.log(identitas);

  		var target = "<?php echo site_url("journal/loadDataAkunCari")?>";

  		var data = {

  			data : Obj
  		}

  		$.post(target, data, function(e){

	            	//console.log(e);

	            	var json = $.parseJSON(e);

	            	for(i = 0; i < json.length; i++)
	            	{

	            		var idAkun 		   = json[i].idAkun,
	            		kodeWithFormat = json[i].kodeWithFormat,
	            		namaWithFormat = json[i].namaWithFormat,
	            		saldoNormal    = json[i].saldoNormal,
	            		concatAkun     = json[i].concatAkun,
	            		tombol         = json[i].tombol;

	            		var table       = document.getElementById('tabelContent');  

	            		var row         = table.insertRow();

	            		row.id = idAkun;

	            		colKode         = row.insertCell(0);
	            		colNama		    = row.insertCell(1);
	            		colAction       = row.insertCell(2);

	            		colKode.style.paddingLeft = json[i].strPadding;

	            		colKode.innerHTML           = '<input type="hidden" name="IDAkun" id="IDAkun" value="'+idAkun+'">'+kodeWithFormat;
	            		colNama.innerHTML           = namaWithFormat;
	            		colAction.innerHTML         = '<input type="hidden" name="identitas" id="identitas" value="'+identitas+'">'+tombol;

	            	}

	            });

  	}

  	function loadDataAkun(Obj){

  		$("#tabelContent").empty();

  		if (Obj == 'mbalik') {

  			var identitas = $("#roleTampungan").val();

  		}else{

  			var identitas = $(Obj).closest("tr").attr("role");

  			$("#roleTampungan").val(identitas);

  		}

  		//console.log(identitas);

  		var target = "<?php echo site_url("journal/loadDataAkun")?>";

  		var data = {}

  		$.post(target, data, function(e){

	            	//console.log(e);

	            	var json = $.parseJSON(e);

	            	for(i = 0; i < json.length; i++)
	            	{

	            		var idAkun 		   = json[i].idAkun,
	            		kodeWithFormat = json[i].kodeWithFormat,
	            		namaWithFormat = json[i].namaWithFormat,
	            		saldoNormal    = json[i].saldoNormal,
	            		concatAkun     = json[i].concatAkun,
	            		tombol         = json[i].tombol;

	            		var table       = document.getElementById('tabelContent');  

	            		var row         = table.insertRow();

	            		row.id = idAkun;

	            		colKode         = row.insertCell(0);
	            		colNama		    = row.insertCell(1);
	            		colAction       = row.insertCell(2);

	            		colKode.style.paddingLeft = json[i].strPadding;

	            		colKode.innerHTML           = '<input type="hidden" name="IDAkun" id="IDAkun" value="'+idAkun+'">'+kodeWithFormat;
	            		colNama.innerHTML           = namaWithFormat;
	            		colAction.innerHTML         = '<input type="hidden" name="identitas" id="identitas" value="'+identitas+'">'+tombol;

	            	}

	            });

  	}

  	function PilihAkun(Obj){

  		var Identitas = $(Obj).prev().val();
  		var KodeAkun  = $(Obj).closest("tr").find("td:eq(0)").text();
  		var NamaAkun  = $(Obj).closest("tr").find("td:eq(1)").text();
  		var IDAkun    = $(Obj).closest("tr").find('td').eq(0).find('input[type="hidden"]').val();

    	//$("tr[role='"+Identitas+"']").closest("tr").find("td:eq(1)").val(NamaAkun);
    	$("tr[role='"+Identitas+"']").find('td').eq(0).find('input[type="text"]').val(KodeAkun);
    	$("tr[role='"+Identitas+"']").find('td').eq(0).find('input[type="hidden"]').val(IDAkun);
    	$("tr[role='"+Identitas+"']").find('td').eq(1).find('input').val(NamaAkun);

    	$("#modal-id").modal("hide");

    }

    function clearInput(){

    	$("#datepicker").val('<?php echo date('d-m-Y')?>');
    	$("#Uraian").val('');
    	$("input[name='KodeAkun[]']").val('');
    	$("input[name='IDAkunSimpan[]']").val('');
    	$("input[name='NamaAkun[]']").val('');
    	$("input[name='debit[]']").val('');
    	$("input[name='kredit[]']").val('');
    	$("input[name='memo[]']").val('');

    	//getAutoNum();
    }


</script>
