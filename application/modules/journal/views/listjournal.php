<style type="text/css" media="screen">
	#jurnalTable td, #jurnalTable th {
  border: 1px solid #ddd;
  padding: 8px;
}
</style>
<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $judul ?></h3>
				<div class="box-tools pull-right">
					<span class="btn btn-sm search-btn"><i class="fa fa-search"></i></span>              
				</div>
			</div>

			<div class="box-tools pull-right">

			</div>

			<div class="box-body">
				<div class="custom-table-wrapper">
					<!-- filter box -->
					<form action="<?php echo base_url("journal") ?>" method="GET" >
						<div class="boxArea">
							<div class="filter-box">
								<div class="row">
									<div class="col-md-2">
										<div class="form-group">
											<label>CoA</label>
											<div class="input-group">
												<input value="<?php echo $namaAKun?$namaAKun:''; ?>" name="namaAKun" class="form-control" readonly="" type="text" id="kodeakun" name="kodeakun">
												<input type="hidden" value="<?php echo $idakun?$idakun:''; ?>" name="IDAkun" id="IDAkun" value="">
												<span class="input-group-btn">

													<a class="btn btn-primary" onclick="loadDataAkun(this)" data-toggle="modal" href='#modal-id'><i class="fa fa-plus-square" aria-hidden="true"></i></a>
												</span>
											</div>
										</div>
									</div>

									<div class="col-md-2">
										<div class="form-group date-icon">
											<label>Tanggal Mulai</label>
											<div class="input-group date">                          
												<input readonly="" style="background-color: white" value="<?php echo $start?$start:''; ?>" name="start_date" class="form-control pull-right datepicker" id="start_date" type="text" data-date-format="dd-M-yyyy">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
									</div> 
									<div class="col-md-2">
										<div class="form-group date-icon">
											<label>Tanggal Selesai</label>
											<div class="input-group date">                          
												<input readonly="" style="background-color: white" value="<?php echo $end?$end:''; ?>" name="end_date" class="form-control pull-right datepicker" id="end_date" type="text" data-date-format="dd-M-yyyy">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
									</div>                                                                       
								</div>

							</div>     
							<!-- search box -->
							<div class="row search-box">
								<div class="form-group">
									<div class="col-md-5">
										<input placeholder="masukan nomor atau uraian jurnal..."  value="<?php echo $cari?$cari:''; ?>" type="text" class="form-control" name="nomorCari">
									</div>
									<div class="col-md-7">
										<button type="submit" class="btn btn-info"><i class="fa fa-search"></i>  Cari</button>
									</div>
								</div>		
							</div>     
						</div>                                      
					</form>

					<br>

					<div class="table-filter table-filter__wrapper">
						<div class="row">
							<div class="col-xs-8">
								<div class="table-filter__left">
									<a href="<?php echo site_url('journal/tambah') ?>" class="btn btn-filter btn-danger "><i class="fa fa-plus"></i> Tambah Transaksi Baru</a>

								</div>
							</div>
							
						</div>
					</div>
					<div style="margin-top: 20px">
						<table id="jurnalTable" class="main-table table table-bordered table-striped">
							<thead>
								<tr>

									<th class="no-sort">Tanggal</th>
									<th class="no-sort">Nomor</th>
									<th class="no-sort">Uraian</th>
									<th class="no-sort">Debet</th>
									<th class="no-sort">Kredit</th>
									<th width="100" class="no-sort">Action</th>
								</tr>
							</thead>
							<tbody id="tempelData">
								<?php

								foreach ($jurnal as $keyZ =>$key) {

									?>
									<tr>
										
										<td><strong><?php echo tgl_indo($key['tanggal'])?></strong></td>
										<td><strong><?php echo $key['nomor']?></strong></td>
										<td><strong><?php echo $key['uraian']?></strong></td>
										<td></td>
										<td></td>
										<td>
											<form action="<?php echo base_url('journal/HapusData'); ?>" method="post" accept-charset="utf-8">
												<input type="hidden" id="IDJurnal" name="IDJurnal" value="<?php echo $key['id_ju']?>">
												<a onclick="EditData(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>

												<button type="submit" class="button_delete btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
											</form>

										</td>
									</tr>
									<?php

									$totalDebet = 0;
									$totalKredit = 0;

									foreach ($detail[$keyZ] as $key2 => $value) {

										$Debet  = $value['debet'];
										$Kredit = $value['kredit'];

										?>	
										<tr>
											<td colspan="1"  headers=""></td>
											<td colspan=""  headers=""><?php echo $value['kode_induk'].'.'.$value['kode_akun'] ?></td>
											<td headers=""><?php echo $value['nama_akun'] ?></td>
											<td style="text-align: right;" colspan="" rowspan="" headers=""><?php echo formatCurrency($value['debet']) ?></td>
											<td style="text-align: right;" colspan="" rowspan="" headers=""><?php echo formatCurrency($value['kredit']) ?></td>
											<td colspan="" rowspan="" headers=""></td>
										</tr>

									<?php 
										$totalDebet  += $Debet;
										$totalKredit += $Kredit;
									} 

									?>
								<tr>
									<td colspan="2"></td>
									<td><strong>Total</strong></td>
									<td style="text-align: right;"><strong><?php echo formatCurrency($totalDebet)?></strong></td>
									<td style="text-align: right;"><strong><?php echo formatCurrency($totalKredit)?></strong></td>
									<td></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>  
					</div>
					<!-- custom info and pagination table -->    
					<div class="row">
						<div class="col-md-1">
							<div class="length">
								<!-- <form action="<?php echo base_url("journal") ?>" method="GET" >
									<select onchange="this.form.submit()" name="select-page" class="form-control">
										<option value="1">10</option>
										<option value="2">25</option>
										<option value="5">50</option>
									</select>
								</form> -->
								<!-- <span class="jumlah-data">
									showing <i class="dynamic-value">1</i>
									to 25
								of 49 entries</span> -->
							</div>
						</div>

						<div class="col-md-11">
							<?php 
							echo $this->pagination->create_links();
							?> 
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>	
</div>

<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Browse Kode Akun</h4>
			</div>
			<div class="modal-body">
				<div class="row">

					<div style="margin-bottom: 20px" class="col-sm-3 col-sm-offset-9">
						<div class="input-group">
							<input type="hidden" name="roleTampungan" id="roleTampungan" value="">
							<input placeholder="masukan pencarian..." id="InputKataKunci" name="InputKataKunci" type="text" class="form-control" aria-label="...">
							<div class="input-group-btn">
								<button type="button" class="btn btn-default dropdown-toggle" onclick="cariAkun()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="fa fa-search"></span>
								</button>
							</div><!-- /btn-group -->
						</div><!-- /input-group -->
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover" id="tabelAkun">
					<thead style="background-color: #ecf0f1">
						<tr>
							<th>Kode</th>
							<th>Nama</th>
							<th></th>
						</tr>
					</thead>
					
					<tbody name="tabelContent" id="tabelContent">
						
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>

		$(document).ready(function() {
		$('#jumlah_baris').change(function() {
			const currentUrl = window.location.href
			const url = queryStringUrlReplacement(currentUrl,'view',$(this).val())
			window.location = url
		})
	})

	function queryStringUrlReplacement(url, param, value) {
		let re = new RegExp("[\\?&]" + param + "=([^&#]*)"), match = re.exec(url), delimiter, newString;

		if (match === null) {
			const hasQuestionMark = /\?/.test(url);
			delimiter = hasQuestionMark ? "&" : "?";
			newString = url + delimiter + param + "=" + value;
		} else {
			delimiter = match[0].charAt(0);
			newString = url.replace(re, delimiter + param + "=" + value);
		}

		return newString;
	}


	$(document).on('click', '.button', function (e) {
		e.preventDefault();
		var id = $(this).data('id');
		swal({
			title: "Are you sure!",
			type: "error",
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes!",
			showCancelButton: true,
		},
		function() {
			$.ajax({
				type: "POST",
				url: "journal/HapusData",
				data: {id:id},
				success: function (data) {

				}         
			});
		});
	});
	$(function () {
		var has = '<?php echo $has; ?>';

		if (has=='hide') {
		$('.boxArea').hide();
		}else{
		$('.boxArea').show();
		}
		
    //Date picker
    $('.datepicker').datepicker({
    	autoclose: true,
    	dateFormat: 'dd-mm-yyyy'
    });

    $('.search-btn').click(function() {
    	$('.boxArea').slideToggle('slow');
    });

   
});

	$( document ).ready(function() {

	

	});

	$('.button_delete').on('click',function(e){
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Apakah Anda Yakin",
			text: "Data yang telah dihapus tidak dapat dikembalikan",
			type: "error",
			showCancelButton: true,
			cancelButtonClass: 'btn-default btn-md waves-effect',
			confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
			confirmButtonText: 'Delete!'
		}, 
		function(willDelete) {
			if (willDelete) {
				form.submit();

			}     
		});
	});



	function EditData(Obj){

		var IDJU = $(Obj).closest("tr").find("input").val();

		window.location.href = '<?php echo base_url()?>journal/edit?IDJU='+IDJU;

	}

	function cariAkun(){

	var KataKunci = $("#InputKataKunci").val();

	if (KataKunci == '') {

		loadDataAkun('mbalik');

	}else{

		loadDataAkunCari(KataKunci);

	}

  }

	 function loadDataAkunCari(Obj){

  		$("#tabelContent").empty();

  		var identitas = $("#roleTampungan").val();

  		//console.log(identitas);

  		var target = "<?php echo site_url("journal/loadDataAkunCari")?>";

  		var data = {

  			data : Obj
  		}

  		$.post(target, data, function(e){

	            	//console.log(e);

	            	var json = $.parseJSON(e);

	            	for(i = 0; i < json.length; i++)
	            	{

	            		var idAkun 		   = json[i].idAkun,
		            		kodeWithFormat = json[i].kodeWithFormat,
		            		namaWithFormat = json[i].namaWithFormat,
		            		saldoNormal    = json[i].saldoNormal,
		            		concatAkun     = json[i].concatAkun,
		            		tombol         = json[i].tombol;

	            		var table       = document.getElementById('tabelContent');  

	            		var row         = table.insertRow();

	            		row.id = idAkun;

	            		colKode         = row.insertCell(0);
	            		colNama		    = row.insertCell(1);
	            		colAction       = row.insertCell(2);

	            		colKode.innerHTML           = '<input type="hidden" name="IDAkun" id="IDAkun" value="'+idAkun+'">'+kodeWithFormat;
	            		colNama.innerHTML           = namaWithFormat;
	            		colAction.innerHTML         = '<input type="hidden" name="identitas" id="identitas" value="'+identitas+'">'+tombol;

	            	}

	            });

    }

	function loadDataAkun(Obj){

		$("#tabelContent").empty();

		var identitas = $(Obj).closest("tr").attr("role");

  		//console.log(identitas);

  		var target = "<?php echo site_url("journal/loadDataAkun")?>";

  		var data = {}

  		$.post(target, data, function(e){

	            	//console.log(e);

	            	var json = $.parseJSON(e);

	            	for(i = 0; i < json.length; i++)
	            	{

	            		var idAkun 		   = json[i].idAkun,
	            		kodeWithFormat = json[i].kodeWithFormat,
	            		namaWithFormat = json[i].namaWithFormat,
	            		saldoNormal    = json[i].saldoNormal,
	            		concatAkun     = json[i].concatAkun,
	            		tombol         = json[i].tombol;

	            		var table       = document.getElementById('tabelContent');  

	            		var row         = table.insertRow();

	            		row.id = idAkun;

	            		colKode         = row.insertCell(0);
	            		colNama		    = row.insertCell(1);
	            		colAction       = row.insertCell(2);

	            		colKode.innerHTML           = '<input type="hidden" name="IDAkun" id="IDAkun" value="'+idAkun+'">'+kodeWithFormat;
	            		colNama.innerHTML           = namaWithFormat;
	            		colAction.innerHTML         = '<input type="hidden" name="identitas" id="identitas" value="'+identitas+'">'+tombol;

	            	}

	            });

  	}

  	   function PilihAkun(Obj){

    	var Identitas = $(Obj).prev().val();
    	var KodeAkun  = $(Obj).closest("tr").find("td:eq(0)").text();
    	var NamaAkun  = $(Obj).closest("tr").find("td:eq(1)").text();
    	var IDAkun    = $(Obj).closest("tr").find('td').eq(0).find('input[type="hidden"]').val();

    	//$("tr[role='"+Identitas+"']").closest("tr").find("td:eq(1)").val(NamaAkun);
    
		$("#kodeakun").val(KodeAkun+' '+NamaAkun);
		$("#IDAkun").val(IDAkun);

    	$("#modal-id").modal("hide");

    }

  </script>

