<!-- data pelanggan ada di var $pelanggan -->
<style type="text/css">
	.spanMargin{
		margin-right: 7px;
	
	}
	table, th, td{
		border: 1px solid #ddd !important;
	}
</style>
<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title"><?php echo $judul ?></h3>

			</div>

			<div class="box-body">
				<div class="row sr-only">

					<div style="margin-bottom: 20px" class="col-sm-3 col-sm-offset-9">
						<div class="input-group">
							<input placeholder="masukan pencarian..." id="InputKataKunci" name="InputKataKunci" type="text" class="form-control" aria-label="...">
							<div class="input-group-btn">
								<button type="button" onclick="cariAkun()" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="fa fa-search"></span>
								</button>
							</div><!-- /btn-group -->
						</div><!-- /input-group -->
					</div>
				</div>
				<form id="formSaldoAwal">
					<table class="table table-striped table-bordered table-hover" id="tabelAkun">
						<thead style="background-color: #ecf0f1">
							<tr>
								<th>Kode</th>
								<th>Nama</th>
								<th>Saldo Awal</th>
							</tr>
						</thead>
						
						<tbody name="tabelContent" id="tabelContent">
							
						</tbody>
					</table>

					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-success pull-right" onclick="SimpanSaldoAwal()" id="btnSimpan" type="button">Simpan</button>
						</div>
					</div>
				</form>

			</div>



		</div>
	</div>
</div>

<script src="<?php echo base_url("template/dist/js/func.js") ?>"></script>

<script type="text/javascript">

	var decimalDigit   =  2;

	$('document').ready(function(){

		$("input[name='InputKataKunci']").on('keypress', function(e){
			if(e.which == 13)
			{
				//alert("oke");
				cariAkun();
			}
		});

		//var KataKunci = '';

		loadDataAkun();

    });

    function SimpanSaldoAwal(){

    	var target = "<?php echo site_url("journal/SimpanSaldoAwal")?>";

    	var formSerialize   = $("#formSaldoAwal").serialize();
	            
	            var data = {

	            		data   : formSerialize
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					var hasil = json.hasil;

					if (hasil == 'berhasil') {

						alert(json.pesan);

						$("#tabelContent").empty();

						loadDataAkun();

					}else{

						alert(json.pesan);

					}
				});

    }

    function cariAkun(){

    	var KataKunci = $("#InputKataKunci").val();

    	if (KataKunci == '') {

    		$('#tabelContent').empty();

    		loadDataAkun();

    	}else{

    		loadDataAkunCari(KataKunci);

    	}

    }

    function loadDataAkunCari(Obj){

    	$('#tabelContent').empty();

    	var target = "<?php echo site_url("journal/loadDataAkunCariSaldoAwal")?>";
	            
	            var data = {

	            	data : Obj
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					for(i = 0; i < json.length; i++)
                    {

                        var idAkun 		   = json[i].idAkun,
                        	kodeWithFormat = json[i].kodeWithFormat,
                        	namaWithFormat = json[i].namaWithFormat,
                        	concatAkun     = json[i].concatAkun,
                        	saldoAwal      = json[i].saldoAwal;

                        var table       = document.getElementById('tabelContent');  

                        var row         = table.insertRow();

                        row.id = idAkun;
                    
                        colKode         = row.insertCell(0);
                        colNama		    = row.insertCell(1);
                        colSaldo        = row.insertCell(2);

                        colKode.innerHTML           = kodeWithFormat;
                        colNama.innerHTML           = namaWithFormat;
                        colSaldo.innerHTML          = saldoAwal;

                        initAutoNumeric();
                        
                    }

				});

    }

    function loadDataAkun(){

    	var target = "<?php echo site_url("journal/loadDataAkunSaldoAwal")?>";
	            
	            var data = {
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					for(i = 0; i < json.length; i++)
                    {

                        var idAkun 		   = json[i].idAkun,
                        	kodeWithFormat = json[i].kodeWithFormat,
                        	namaWithFormat = json[i].namaWithFormat,
                        	concatAkun     = json[i].concatAkun,
                        	saldoAwal      = json[i].saldoAwal;

                        var table       = document.getElementById('tabelContent');  

                        var row         = table.insertRow();

                        row.id = idAkun;
                    
                        colKode         = row.insertCell(0);
                        colNama		    = row.insertCell(1);
                        colSaldo        = row.insertCell(2);

                        colSaldo.style.textAlign  = 'right';
                        colKode.style.paddingLeft = json[i].strPadding;

                        colKode.innerHTML           = kodeWithFormat;
                        colNama.innerHTML           = namaWithFormat;
                        colSaldo.innerHTML          = saldoAwal;

                        initAutoNumeric();
                        
                    }

				});

    }

    function clearNominal(Obj){

    	var Nilai = $(Obj).val();

    	if (Nilai == '0.00') {

    		$(Obj).val('');

    	}

    }

    
    function clearInput(){

    }
</script>