<?php 

class M_menu extends CI_model

{


	function produk_tree()
	{
		$data =  $this->db->query("	SELECT 
			*, 
			b.id_kategori AS id_kat_child,
			b1.id_kategori AS id_kat_head,
			b.nama_kategori AS nama_kategori_child
			FROM kategori b 
			LEFT JOIN kategori b1 ON b1.`id_kategori` = b.`id_parent`
			")->result_array();


		$temp_data = [];


		foreach ($data as $key => $value) {

			if ($value['id_kat_head']!=null) {

				$temp_data[$value['id_kat_head']]['id_head_kategori'] = $value['id_kategori'];
				$temp_data[$value['id_kat_head']]['name_head_kategori'] = $value['nama_kategori'];
				$temp_data[$value['id_kat_head']]['active_head_kategori'] = $value['active'];
				$temp_data[$value['id_kat_head']]['sub_kategory'][$value['id_kat_child']]['id_sub_kategori'] = $value['id_kat_child'];
				$temp_data[$value['id_kat_head']]['sub_kategory'][$value['id_kat_child']]['name_sub_kategori'] = $value['nama_kategori_child'];
				$temp_data[$value['id_kat_head']]['sub_kategory'][$value['id_kat_child']]['active_sub_kategori'] = $value['active'];

			}


		}





		return $temp_data;


	}



	public function getArrayDesaMenu()
	{


		//$data = $this->db->query("SELECT * FROM `be_menus` WHERE id_menu IN (".funcSession()['id_menu'].") ")->result_array();
		$data = $this->db->query("SELECT * FROM `ref_menu_roles` WHERE id_roles = ".funcSession()['role']."")->result_array();

		foreach ($data as $key => $value) {
			$data[$key] = $value['id_menu'];
		}

		return $data;

	}


	public function getArrayDesaMenuByRoles($id)
	{


		$data = $this->db->query(" SELECT * FROM `ref_menu_roles` WHERE id_roles = ".$id." ")->result_array();

		foreach ($data as $key => $value) {
			$data[$key] = $value['id_menu'];
		}

		return $data;

	}





	public function get_be_menu_tree()
	{

		$arrayMenu = $this->getArrayDesaMenu();

		$data = $this->db->query("SELECT 
			a.id_menu AS id_menu,
			a.id_parent AS id_parent,
			a.menu AS menu,
			a.`link_menu` AS link_menu,
			a.`is_dropdown`,
			b.`id_menu` AS id_submenu,
			b.`id_parent` AS id_subparent,
			b.`menu` AS submenu,
			b.`link_menu` AS link_submenu,
			a.`menu_logo` AS logo_parent,
			b.`menu_logo` AS logo_subparent
			FROM `be_menus` a 
			LEFT JOIN be_menus b ON a.`id_menu` = b.`id_parent` 
			WHERE a.is_active = 1
			ORDER BY a.order ASC
			")->result_array();


		$temp = [];


		foreach ($data as $key => $value) {



			if ($value['id_parent']==NULL) {



				$temp[$value['id_menu']]['menu'] = $value['menu'];
				$temp[$value['id_menu']]['id_menu'] = $value['id_menu'];
				$temp[$value['id_menu']]['link_menu'] = $value['link_menu'];
				$temp[$value['id_menu']]['logo_parent'] = $value['logo_parent'];
				$temp[$value['id_menu']]['is_dropdown'] = $value['is_dropdown'];



				if ($value['id_subparent'] != "") {

					$temp[$value['id_menu']]['submenu'][$value['id_submenu']]['id_submenu'] = $value['id_submenu'];
					$temp[$value['id_menu']]['submenu'][$value['id_submenu']]['submenu'] = $value['submenu'];
					$temp[$value['id_menu']]['submenu'][$value['id_submenu']]['link_submenu'] = $value['link_submenu'];
					$temp[$value['id_menu']]['submenu'][$value['id_submenu']]['logo_subparent'] = $value['logo_subparent'];

				}

			}


		}



		return $temp;

	}




}


?>