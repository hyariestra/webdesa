
<!-- data pelanggan ada di var $pelanggan -->

<div class="row">

	<div class="col-md-12">

			<div class="box">

				<div class="box-header">

					<h3 class="box-title"><?php echo $judul ?></h3>

					<hr/>

				</div>

				<div class="box-body">
					
					<form enctype="multipart/form-data" action="<?php echo base_url('pengaturan/simpanInfo')?>" method="POST">
						<div class="row" id="tempelPesan">

							<!-- <form id="formInformasi" name="formInformasi" method="post"> -->
							<div class="col-md-6">

								<div class="form-group">
								    <label for="NamaPerusahaan">Nama Perusahaan</label>
								    <input type="text" class="form-control" id="NamaPerusahaan" name="NamaPerusahaan" aria-describedby="NamaPerusahaan" placeholder="Masukan Nama Perusahaan" value="<?php echo $informasi->first_row()->nama_perusahaan?>">
							  	</div>

							  	<div class="form-group" id="LogoPerusahaan">
								    <label for="LogoPerusahaan">Logo Perusahaan</label>
								    <input type="file" id="LogoPerusahaan" name="LogoPerusahaan" aria-describedby="LogoPerusahaan">
								    <input type="text" style="display: none;" role="uploadTumbnail" name="uploadTumbnail">
								    <small id="emailHelp" class="form-text text-muted">* Logo Perusahaan yang akan ditampilkan pada laporan keuangan</small>
								    <div><img style="height: 150px;width: 500px;" src="<?php echo base_url(); ?>template/images/<?php echo $informasi->first_row()->logo_perusahaan?>"></div>
								    <i>Note: Ukuran Maksimal Gambar 2 MB</i>
							  	</div>

							  	<hr/>

							    <div class="form-group">
								    <label for="AlamatPerusahaan">Alamat</label>
								    <textarea class="form-control" id="AlamatPerusahaan" name="AlamatPerusahaan" placeholder="Masukan Alamat Perusahaan"><?php echo $informasi->first_row()->alamat?></textarea>
							    </div>

							    <div class="form-group">
								    <label for="KodePos">Kode Pos</label>
								    <input type="text" class="form-control" id="KodePos" name="KodePos" aria-describedby="KodePos" placeholder="Masukan Kode Pos Perusahaan" value="<?php echo $informasi->first_row()->kode_pos?>">
							    </div>
			
							</div>

							<div class="col-md-6">

							    <div class="form-group">
								    <label for="WebPerusahaan">Website</label>
								    <input type="text" class="form-control" id="WebPerusahaan" name="WebPerusahaan" aria-describedby="WebPerusahaan" placeholder="Masukan Nama Website Perusahaan" value="<?php echo $informasi->first_row()->website?>">
							    </div>

							    <div class="form-group">
								    <label for="AlamatPerusahaan">Email</label>
								    <input type="email" class="form-control" id="EmailPerusahaan" name="EmailPerusahaan" aria-describedby="EmailPerusahaan" placeholder="Masukan Email Perusahaan" value="<?php echo $informasi->first_row()->email?>">
							    </div>

							    <hr/>

							    <div class="form-group">
								    <label for="NoTelp">Nomor Telpon</label>
								    <input type="text" class="form-control" id="NoTelp" name="NoTelp" aria-describedby="NoTelp" placeholder="Masukan Nomor Telpon Perusahaan" value="<?php echo $informasi->first_row()->nomor_telp?>">
							    </div>

							    <div class="form-group">
								    <label for="AlamatPerusahaan">Fax</label>
								    <input type="text" class="form-control" id="NoFax" name="NoFax" aria-describedby="NoFax" placeholder="Masukan Nomor Fax Perusahaan" value="<?php echo $informasi->first_row()->fax?>">
							    </div>

							</div>

						
							
						</div>

				</div>

				<div class="box-footer">

					<button class="btn btn-primary pull-right" type="submit" id="">Simpan</button>

				</div>
			</form>

			</div>

	</div>

	<script type="text/javascript">

		var files;
		
		$('document').ready(function()
    	{
	
			$("#btnSimpan").click(function(e){
        
	            e.preventDefault();

	            var target = "<?php echo site_url("pengaturan/tambahInformasi")?>";

	            var formSerialize   = $("#formInformasi").serialize();
	            
	            var data = {

	                data   : formSerialize,
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					var hasil = json.hasil;

					if (hasil == 'berhasil') {

						var pesan = '<div class="col-md-12"><div class="alert alert-success">'+json.pesan+'</div></div>';

						$('#tempelPesan').prepend(pesan);

					}else{

						var pesan = '<div class="col-md-12"><div class="alert alert-warning">'+json.pesan+'</div></div>';

						$('#tempelPesan').prepend(pesan);

					}

				});
            
        	});

        	$("input[type='file']").on("change", prepareUpload);

    	}); 

    	function prepareUpload(event)
	{
		files = event.target.files;
		
		$("input[type='text'][role='uploadTumbnail']").val(files[0].name);

		uploadFiles(event);
	}

	function uploadFiles(event)
	{
		
		//console.log(event);
		event.stopPropagation(); // Stop stuff happening
		event.preventDefault(); // Totally stop stuff happening

		// START A LOADING SPINNER HERE
		

		// Create a formdata object and add the files
		var data = new FormData();
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});

		$.ajax({
			url: '<?php echo site_url("pengaturan/uploadFile")?>',
			type: 'POST',
			data: data,
			cache: false,
			processData: false, // Don't process the files
			contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			success: function(data, textStatus, jqXHR)
			{
				console.log(data);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				// Handle errors here
				console.log('ERRORS: ' + errorThrown);
				// STOP LOADING SPINNER
			}
		});
	}

	</script>

</div>