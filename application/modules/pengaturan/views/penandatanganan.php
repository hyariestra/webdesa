
<!-- data pelanggan ada di var $pelanggan -->
<style type="text/css">
	.control-label
	{
		margin-top: 5px;
	}
	.form-group{
		padding-top: 30px;
	}
</style>

<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title"><?php echo $judul ?></h3>

				<hr/>

			</div>

			<div class="box-body">

				
				
				<div class="row">
					<div class="col-md-12">
						<form action="<?php echo base_url("pengaturan/penandatanganan/simpan/1") ?>" method="post" accept-charset="utf-8">
							
						<div class="col-md-4">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Jabatan:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p1?$p1->jabatan:''; ?>" type="text" class="form-control" name="jabatan">   
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Nama:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p1?$p1->nama:''; ?>" type="text" class="form-control" name="nama">   
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">NIP:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p1?$p1->nip:''; ?>" type="text" class="form-control" name="nip">   
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-info">Simpan</button>
								</div>
							</div>
						</div>
					</form>
					<form action="<?php echo base_url("pengaturan/penandatanganan/simpan/2") ?>" method="post" accept-charset="utf-8">
						<div class="col-md-4">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Jabatan:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p2?$p2->jabatan:''; ?>" type="text" class="form-control" name="jabatan">   
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Nama:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p2?$p2->nama:''; ?>" type="text" class="form-control" name="nama">   
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">NIP:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p2?$p2->nip:''; ?>" type="text" class="form-control" name="nip">   
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-info">Simpan</button>
								</div>
							</div>
						</div>
					</form>
						<form action="<?php echo base_url("pengaturan/penandatanganan/simpan/3") ?>" method="post" accept-charset="utf-8">
						<div class="col-md-4">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Jabatan:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p3?$p3->jabatan:''; ?>" type="text" class="form-control" name="jabatan">   
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Nama:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p3?$p3->nama:''; ?>" type="text" class="form-control" name="nama">   
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">NIP:</label>
								<div class="col-sm-offset-1 col-sm-9">
									<input value="<?php echo $p3?$p3->nip:''; ?>" type="text" class="form-control" name="nip">   
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-info">Simpan</button>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
				

			</div>

		</div>

		

	</div>

	

</div>