<?php 
class Pengaturan extends CI_Controller



{



	function __construct()



	{



		parent::__construct();





		$this->load->model('M_pengaturan');

	

		if (!$this->session->userdata("pengguna"))



		 {



			redirect("pengguna/login");



		}



	}



	function index()

	{

		authorize('informasiPerusahaan');



		$data['act'] = 'inf';

		$data['judul'] = 'Halaman Informasi Perusahaan';



		$this->load->model("M_pengaturan");



		$data['informasi']=$this->M_pengaturan->tampilInformasi();



		$this->themeadmin->tampilkan('informasiperusahaan',$data);

	}





	public function simpanInfo()

	{

		$datainputan=$this->input->post();



		$this->M_pengaturan->update_pengaturan($datainputan);





		$isipesan="<br><div class='alert alert-success'>Data Berhasil Diubah!</div>";

		$this->session->set_flashdata("pesan",$isipesan);

		redirect("pengaturan/");

	}





	function tambahInformasi()



	{



		parse_str($this->input->post('data'), $data);



		$NamaPerusahaan     = $data['NamaPerusahaan'];

		$LogoPerusahaan     = $data['uploadTumbnail'];

		$AlamatPerusahaan 	= $data['AlamatPerusahaan'];

		$KodePos 			= $data['KodePos'];

		$WebPerusahaan 	    = $data['WebPerusahaan'];

		$EmailPerusahaan    = $data['EmailPerusahaan'];

		$NoTelp 	 	 	= $data['NoTelp'];

		$NoFax  			= $data['NoFax'];



		$this->load->model('M_pengaturan');



		$json = $this->M_pengaturan->tambahInformasi($NamaPerusahaan, $LogoPerusahaan, $AlamatPerusahaan, $KodePos, $WebPerusahaan, $EmailPerusahaan, $NoTelp, $NoFax);



		echo $json;





	}



	public function uploadFile()

	{

		$target_dir = "template/images/";

		$target_file = $target_dir . basename($_FILES[0]["name"]);

		$uploadOk = 1;

		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));



		move_uploaded_file($_FILES[0]["tmp_name"], $target_file);



	}



	function hapus($id)



	{



		$this->load->model("M_pengaturan");



		$this->M_pengaturan->hapus_pengaturan($id);



		redirect('pengaturan/tampil');



	}



	function ubah($id)



	{



		$this->load->model('M_pengaturan');



		$inputan=$this->input->post();







		if ($inputan) 



		{



			$this->M_pengaturan->ubah_pengaturan($inputan,$id);



			redirect("pengaturan/tampil");



		}



		$data['judul']="ubah Pengaturan";



		$data['pengaturan']=$this->M_pengaturan->ambil_pengaturan($id);



		$this->themeadmin->tampilkan("ubahpengaturan",$data);







	}







}

?>