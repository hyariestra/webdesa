<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/variable-pie.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<style>
	#chart2 {
    height: 500px;
	
}

.highcharts-figure,
.highcharts-data-table table {
    min-width: 320px;
    max-width: 700px;
    margin: 1em auto;

}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}

.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}

.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
    padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}

.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
</style>





<div class="row">



	<div class="col-md-12">

		<div class="box">

			<div class="box-header with-border">

				<h3 class="box-title"></h3>

				<div class="pull-right row col-sm-2">



					<form id="formId" action="<?php echo base_url("dashboard") ?>" method="GET" >
						<div class="input-group date">  

							<input style="background-color: white" readonly="" value="<?php echo $yearBfr; ?>" type="text" class="form-control datepicker" name="yearBfr" >



							<div class="input-group-addon">

								<i class="fa fa-calendar"></i>

							</div>



						</div>

						<div class="input-group date">  



							<input style="background-color: white" readonly="" value="<?php echo $yearNow; ?>" type="text" class="form-control datepicker" name="yearNow" >



							<div class="input-group-addon">

								<i class="fa fa-calendar"></i>

							</div>

						</div>
					</form>

				</div>

			</div>



			

			<div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


			
			
		</div>


			<div class="box">

			<div class="box-header with-border">
			<div class="pull-right row col-sm-2">


						<form id="formId2" action="<?php echo base_url("dashboard") ?>" method="GET" >

						<div class="input-group date">  

							<input style="background-color: white" readonly="" value="<?php echo $bulanPie; ?>-<?php echo $tahunPie; ?>" type="text" class="form-control datepicker2" name="yearPie" >



							<div class="input-group-addon">

								<i class="fa fa-calendar"></i>

							</div>



						</div>


						</form>


						</div>

							<figure style=" margin-top:70px;" class="highcharts-figure">


							

							
									<div id="chart2"></div>
									
							</figure>
</div>

		</div>

	</div>

</div>

<script>

	$('document').ready(function()

	{

		
		$('.datepicker').datepicker({

			autoclose: true,

			format: "yyyy",

			viewMode: "years", 

			minViewMode: "years"

		}).on('changeDate',function(ev){

			$('#formId').submit();

		});		


		$('.datepicker2').datepicker({

			autoclose: true,

			format: "mm-yyyy",
			viewMode: "months", 
			minViewMode: "months"

			}).on('changeDate',function(ev){

			$('#formId2').submit();

			});



	}); 
	

//kunjuanga visitor atas
	Highcharts.chart('chart', {

		chart: {

			type: 'column'

		},

		title: {

			text: 'Perbandingan Visitor Website Pada Tahun  <?php echo  $yearBfr ?> dan Tahun <?php echo  $yearNow ?> '

		},

		xAxis: {

			categories: <?php echo json_encode($bulan) ?>

		},

		credits: {

			enabled: false

		},

		series: [{

			name: '<?php echo $yearBfr ?>',

			data: <?php echo json_encode($ArrVisitorBfr)?>



		},  {

			name: '<?php echo $yearNow ?>',

			data: <?php echo json_encode($ArrVisitorNow)?>

		}]

	});

	//bawah

	Highcharts.chart('chart2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Perbandingan Visitor Berdasarkan Negara Pada Bulan  <?php echo  $bulanPie ?>  Tahun <?php echo  $tahunPie ?> '
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: <?php echo json_encode($negara) ?>
    }]
});

</script>








