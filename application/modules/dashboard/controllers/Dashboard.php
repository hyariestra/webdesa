<?php 

class Dashboard extends CI_controller



{

	function __construct()


	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))

		{

			redirect("pengguna/login");

		}

	}

	

	function index()

	{

		$year = $this->input->get('yearNow');
		$yearPie = $this->input->get('yearPie');
		$yearBfr = $this->input->get('yearBfr');

		if ($year=='') {
			$data['yearNow'] =  date('Y');
		} else {
			$data['yearNow'] = $year;
		}

	
		if ($yearPie=='') {
			 $data['bulanPie'] = date('m');
			 $data['tahunPie'] = date('Y');
		}else{
			
			$nilai = explode('-',$yearPie);
		
			$data['bulanPie'] = $nilai[0];
			$data['tahunPie'] = $nilai[1];
		}

		
		


		if ($yearBfr=='') {
			$data['yearBfr'] = date('Y')-1;
		} else {
			$data['yearBfr'] = $yearBfr;
		}

		
		$data['bulan'] = ['Januari', 'Febuari', 'Maret','April','Mei','Juni', 'Juli' , 'Agustus', 'September', 'Oktober','November','Desember'];



		$negara = $this->db->query("SELECT
		a.country, 
		a.`country_code`,
		a.`country`, COUNT(a.`country`) AS visit 
		 FROM statistik a
		LEFT JOIN ref_desa b ON a.`id_desa`  = b.`id_desa`
		WHERE b.`desa_kode_domain` = '".getNameDomain()."'
		AND  MONTH(a.tanggal) = '".$data['bulanPie']."'
		AND  YEAR(a.tanggal)  = '".$data['tahunPie']."'
		GROUP BY YEAR(a.tanggal), MONTH(a.tanggal), a.`continent_code` ")->result_array();	

		$data['negara'] = [];

		foreach ($negara as $key => $item) {

			$visit = ($item['visit']) ? (int)$item['visit'] : 0;

			$data['negara'][$key]['name'] = $item['country'];
			$data['negara'][$key]['y'] =  $visit;
		}


		for ($i=1; $i <=12 ; $i++) { 


			$data['statistik'] = $this->db->query("SELECT *,COUNT(a.`ip`) AS visit FROM statistik a
				LEFT JOIN ref_desa b ON a.`id_desa`  = b.`id_desa`
				WHERE b.`desa_kode_domain` = '".getNameDomain()."'
				AND  MONTH(a.tanggal) = '".$i."'
				AND  YEAR(a.tanggal)  = '".$data['yearBfr']."'
				GROUP BY YEAR(a.tanggal), MONTH(a.tanggal) ");		


			$data['ArrVisitorBfr'][] = ($data['statistik']->num_rows() > 0) ? (int)$data['statistik']->first_row()->visit : 0;




			$data['statistik'] = $this->db->query("SELECT *,COUNT(a.`ip`) AS visit FROM statistik a
				LEFT JOIN ref_desa b ON a.`id_desa`  = b.`id_desa`
				WHERE b.`desa_kode_domain` = '".getNameDomain()."'
				AND  MONTH(a.tanggal) = '".$i."'
				AND  YEAR(a.tanggal)  = '".$data['yearNow']."'
				GROUP BY YEAR(a.tanggal), MONTH(a.tanggal) ");		


			$data['ArrVisitorNow'][] = ($data['statistik']->num_rows() > 0) ? (int)$data['statistik']->first_row()->visit : 0;

		}


		$data['judul']="selamat datang"; 
		$data['act'] = 'dash';
		$this->themeadmin->tampilkan("dashboard",$data);

	}



	public function notFound()

	{

		$data['judul']="unauthorize"; 

		$this->themeadmin->tampilkan("401",$data);

	}

}

?>