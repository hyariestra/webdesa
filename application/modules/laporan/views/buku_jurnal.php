<?php

  $tipe         =  $this->uri->segment(5); 

  $periode      =  $this->uri->segment(3); 

  $nilai         =  $this->uri->segment(4);

  $tahunPeriode         =  $this->uri->segment(6);

  if ($tipe == 'pdf')

  {

    ob_start();

  }

  $strTableWidth  = ($tipe <> 'pdf') ? 'width:100%;' : '';


  $startMonth = 1;

  $strPeriodeHeader = '';

  $strPeriode = '';

  $strPeriodePembanding = '';

  $tahun = $tahunPeriode;

            if ($periode == 'Bulan')
            {
                $startDate = $tahun.'-'.$nilai.'-'.'01';
                $endDate   = $tahun.'-'.$nilai.'-'.'31';
                $tanggal   = GetLastDateInMonth($nilai);
                $strPeriodeHeader     = $tanggal. " ".GetMonthName($nilai)." ".$tahun;
                $strPeriode           = GetMonthName($nilai)." ".$tahun;
                $bulanPembanding     = ($nilai-1 ) > 1 ?  $nilai-1 : 1;
                $strPeriodePembanding = GetMonthName($bulanPembanding)." ".$tahun;
                $bulanberikut         = $nilai+1;
                $strPeriodeBerikut = GetMonthName($bulanberikut)." ".$tahun;
                $startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
                $endDateCompare   = $tahun.'-'.$bulanPembanding.'-'.'31';
            }

            if ($periode == 'Triwulan')
            {
                $startMonth = ($nilai == 1) ? 1 : 0; 
                $endMonth   = ($nilai == 1) ? 3 : 0;
                $startMonth = ($nilai == 2) ? 4 : $startMonth;
                $endMonth   = ($nilai == 2) ? 6 : $endMonth;
                $startMonth = ($nilai == 3) ? 7 : $startMonth;
                $endMonth   = ($nilai == 3) ? 9 : $endMonth;
                $startMonth = ($nilai == 4) ? 10 : $startMonth;
                $endMonth   = ($nilai == 4) ? 12 : $endMonth;                 
                $startDate  = $tahun.'-'.$startMonth.'-'.'01';
                $endDate    = $tahun.'-'.$endMonth.'-'.'31';
                $tanggal    = GetLastDateInMonth($endMonth);
                $strPeriodeHeader     = 'Triwulan '.$nilai.' Tahun 2019';
                $strPeriode            = 'Triwulan '.$nilai;
                $bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 3;
                $nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
                $strPeriodePembanding = 'Triwulan '.$nilaiPembanding; 
                $bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 1;
                $startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
                $bulanAkhirPembanding = ($endMonth-3) > 1 ?  $endMonth-3 : 3;
                $endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
            }

            if ($periode == 'Semester')
            {
                $startMonth = ($nilai == 1) ? 1 : 1;
                $endMonth   = ($nilai == 1) ? 6 : 6;
                $startMonth = ($nilai == 2) ? 7 : $startMonth;
                $endMonth   = ($nilai == 2) ? 12 : $endMonth;      
                $startDate = $tahun.'-'.$startMonth.'-'.'01';
                $endDate   = $tahun.'-'.$endMonth.'-'.'31';
                $tanggal   = GetLastDateInMonth($endMonth);
                $strPeriodeHeader     = 'Semester '.$nilai.' Tahun 2019';
                $strPeriode            = 'Semester '.$nilai;
                $bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 1;
                $startDateCompare     = $tahun.'-'.$bulanPembanding.'-'.'01';
                $bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 6;
                $nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
                $strPeriodePembanding = 'Semester '.$nilaiPembanding; 
                $bulanAkhirPembanding = ($endMonth-6) > 1 ?  $endMonth-6 : 6;
                $endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
            }

            if ($periode == 'Tahun')
            {
                $startDate = $tahun.'-01-01';
                $endDate   = $tahun.'-12-31';
                $tanggal = GetLastDateInMonth(12, $tahun);
                $strPeriodeHeader       = 'Tahun '.$tahun;
                $strPeriode             = $strPeriodeHeader;//GetMonthName(12)." ".$nilai;
                $tahunSebelumnya        = $tahun-1;
                $startDateCompare       = $tahunSebelumnya.'-01-01';
                $nilaiPembanding        = $tahun - 1;
                $strPeriodePembanding   = 'Tahun '.$nilaiPembanding;
                $endDateCompare         = $tahunSebelumnya.'-12-31';

            } 

?>  

 <page>

 <style type="text/css">

   .tabelItems  {font-size:12px;font-family:arial, sans-serif;border-collapse:collapse; border-spacing:0;margin-top:10px;<?php echo $strTableWidth?>}

   .tabelItems td

    {

      padding:5px 5px;

      border: 1px solid black;

      border-width:1px;

      overflow:hidden;word-break:normal;

    }

    .tabelItems th{

      font-weight:bold;padding:5px 5px;

      border-style:solid;border-width:2px;overflow:hidden;

      word-break:normal;

      border-left: none;

      border-right: none;

      text-align: center;

      padding:10px;

      vertical-align: center;

   }

   .textLeft{

    text-align: left;

   }

   .textCenter{

    text-align: center;

   }

   .textRight{

    text-align: right;

   }

   .title{

      font-size:14px;

      padding : 3px;

      font-weight: bold;

      border-style: none;

   }

   .title2{

      font-size:14px;

      font-weight: bold;

      border-left:none;

      border-right:none;

      border-bottom: 1px solid black;

   }

   .bold{

      font-weight:bold;

   }

   .fontHeader1

   {

     font-family:Arial; 

     font-size:15px;

   }

   .fontHeader2

   {

     font-family:Arial; 

     font-size:14px;

   }

   .fontHeader3

   {

     font-family:Arial; 

     font-size:12px;

   }

   .header

   {

      border-style:none;

      text-align: center;

      font-weight: bold;

      margin-bottom:3px;

    }

    .redFont{

      color:red;

    }

     .footer1{font-family:arial;font-size:12px;text-align:center;width:31%;display:inline;margin-right:5px;}

     .marginTop{margin-top:5px}



</style> 

    <?php

      $data["infoHeaderKanan"]   = "Jurnal Umum";

      $query = $this->db->query("SELECT * FROM mst_informasi")->first_row();

      $data["nama"] = $query->nama_perusahaan;
      $data["logo"] = $query->logo_perusahaan;
      $data["periodeawal"] = $strPeriode;
      $data["tahunPeriode"] = $tahunPeriode;
      $data["periode"] = $periode;

      echo $this->load->view("cover", $data);

    ?>


    <table class="tabelItems">

              <col style="width: 5%">

              <col style="width: 10%">

              <col style="width: 10%">

              <col style="width: 20%">

              <col style="width: 10%">

              <col style="width: 15%">

              <col style="width: 15%">

              <col style="width: 15%">

              <tr style="border:1px solid black;border-left:none;border-right:none;">

    				    <th style="border:1px solid;">No</th>

                <th style="border:1px solid;">No Transaksi</th>

                <th style="border:1px solid;">Tanggal</th>

                <th style="border:1px solid;">Uraian</th>

                <th style="border:1px solid;">Kode Akun</th>

                <th style="border:1px solid;">Nama Akun</th>

                <th style="border:1px solid;">Debet</th>

                <th style="border:1px solid;">Kredit</th>

              </tr>

              <?php

                $queryTransaksi = $this->db->query("SELECT id_ju AS IDJU, nomor AS Nomor, tanggal AS Tanggal, uraian AS Uraian FROM trx_ju 
                                                    WHERE (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') 
                                                    AND id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'JU') ORDER BY tanggal DESC ");
                $seq=1;
                $grandTotalDebet = 0;
                $grandTotalKredit = 0;
                foreach ($queryTransaksi->result_array() as $key) {

                  $IDJU = $key["IDJU"];

              ?>

              <tr style="background: #ccc;font-weight: bold">
                
                <td style="text-align: center;"><?php echo $seq;?></td>
                <td style="text-align: center;"><?php echo $key["Nomor"];?></td>
                <td style="text-align: center;"><?php echo formatDateIndo($key["Tanggal"]);?></td>
                <td colspan="5"><?php echo $key["Uraian"];?></td>

              </tr>

              <?php
                    
                  $queryDet = $this->db->query("SELECT CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, nama_akun AS NamaAkun, debet AS Debet, kredit AS Kredit
                                                FROM trx_judet a LEFT JOIN mst_akun b ON b.id_akun = a.id_akun
                                                WHERE id_ju = '".$IDJU."'");

                  $subTotalDebet  = 0;
                  $subTotalKredit = 0;

                  foreach ($queryDet->result_array() as $keys) {

                    $subTotalDebet  += $keys["Debet"];
                    $subTotalKredit += $keys["Kredit"];

                ?>
                  <tr>
                    <td colspan="4"></td>
                    <td style="text-align: center;"><?php echo $keys["KodeAkun"];?></td>
                    <td><?php echo $keys["NamaAkun"];?></td>
                    <td style="text-align: right;"><?php echo formatCurrency($keys["Debet"]);?></td>
                    <td style="text-align: right;"><?php echo formatCurrency($keys["Kredit"]);?></td>
                  </tr>

              <?php } ?>

                <tr style="font-weight: bold">
                  <td colspan="6" style="text-align: right;">Sub Total:</td>
                  <td style="text-align: right;"><?php echo formatCurrency($subTotalDebet)?></td>
                  <td style="text-align: right;"><?php echo formatCurrency($subTotalKredit)?></td>
                </tr>

              <?php

                  $grandTotalDebet  += $subTotalDebet;
                  $grandTotalKredit += $subTotalKredit;

                  $seq++;
                }
              
              ?>

              <tr style="font-weight: bold">
                <td colspan="6" style="text-align: right;">Grand Total:</td>
                  <td style="text-align: right;"><?php echo formatCurrency($grandTotalDebet)?></td>
                  <td style="text-align: right;"><?php echo formatCurrency($grandTotalKredit)?></td>
              </tr>  

    </table>

 </page> 

 <br>

 <br>

<?php

  if ($tipe == 'pdf')

  {

    $content = ob_get_clean();

    // conversion HTML => PDF

    require_once 'template/plugins/html2pdf_v4.03/html2pdf.class.php'; // arahkan ke folder html2pdf

    try

    {

      $html2pdf = new HTML2PDF('p','Legal','en', false, 'ISO-8859-15',array(5, 5, 5, 5)); //setting ukuran kertas dan margin pada dokumen anda

      //$html2pdf->setModeDebug();

      $html2pdf->pdf->SetDisplayMode('fullpage');

      $html2pdf->setDefaultFont('Arial');

      $html2pdf->writeHTML($content, isset($_GET['vuehtml']));

      $html2pdf->Output('Buku_Jurnal.pdf');

    }

    catch(HTML2PDF_exception $e) { echo $e; } 

  }

  if ($tipe == 'excel')

  {

      header("Content-type: application/vnd-ms-excel");

      header("content-Disposition:attachment;filename=LaporanBukuJurnal.xls");

  }


?>







