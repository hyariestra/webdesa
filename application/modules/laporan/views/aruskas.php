<?php

  $tipe         =  $this->uri->segment(5);

  $periode       =  $this->uri->segment(3);

  $tahunPeriode         =  $this->uri->segment(6); 

  if ($tipe == 'pdf')

  {

    ob_start();

  }

  $strTableWidth  = ($tipe <> 'pdf') ? 'width:100%;' : '';

?>  

 <page>

 <style type="text/css">

   .tabelItems  {font-size:12px;font-family:arial, sans-serif;border-collapse:collapse; border-spacing:0;margin-top:10px;<?php echo $strTableWidth?>}

   .tabelItems td

    {

      padding:5px 5px;

      border: 1px solid black;

      border-width:1px;

      overflow:hidden;word-break:normal;

    }

    .tabelItems th{

      font-weight:bold;padding:5px 5px;

      border-style:solid;border-width:2px;overflow:hidden;

      word-break:normal;

      border-left: none;

      border-right: none;

      text-align: center;

      padding:10px;

      vertical-align: center;

   }

   .textLeft{

    text-align: left;

   }

   .textCenter{

    text-align: center;

   }

   .textRight{

    text-align: right;

   }

   .title{

      font-size:14px;

      padding : 3px;

      font-weight: bold;

      border-style: none;

   }

   .title2{

      font-size:14px;

      font-weight: bold;

      border-left:none;

      border-right:none;

      border-bottom: 1px solid black;

   }

   .bold{

      font-weight:bold;

   }

   .fontHeader1

   {

     font-family:Arial; 

     font-size:15px;

   }

   .fontHeader2

   {

     font-family:Arial; 

     font-size:14px;

   }

   .fontHeader3

   {

     font-family:Arial; 

     font-size:12px;

   }

   .header

   {

      border-style:none;

      text-align: center;

      font-weight: bold;

      margin-bottom:3px;

    }

    .redFont{

      color:red;

    }

     .footer1{font-family:arial;font-size:12px;text-align:center;width:31%;display:inline;margin-right:5px;}

     .marginTop{margin-top:5px}



</style> 

    <?php

      $data["infoHeaderKanan"]   = "Arus Kas";

      $query = $this->db->query("SELECT * FROM mst_informasi")->first_row();

      $data["nama"] = $query->nama_perusahaan;
      $data["logo"] = $query->logo_perusahaan;
      $data["periodeawal"] = $strPeriode;
      $data["tahunPeriode"] = $tahunPeriode;
      $data["periode"] = $periode;

      echo $this->load->view("cover", $data);

    ?>


    <table class="tabelItems">

              <tr style="border:1px solid black;border-left:none;border-right:none;">

                <th style="border:1px solid;width: 70%;">Kode Akun</th>

                <th style="border:1px solid;width: 30%;"><?php echo $strPeriodeLaporan?></th>

              </tr>

              <?php

                if ($isitabel == '')
                {
                  echo "<tr><td colspan='2' style='border: 1px solid black;text-align:center'><b>:: Tidak ada data yang ditampilkan ::</b></td></tr>";
                }
                    
                echo $isitabel; 

              ?>

    </table>

 </page> 

 <br>

 <br>

<?php

  if ($tipe == 'pdf')

  {

    $content = ob_get_clean();

    // conversion HTML => PDF

    require_once 'template/plugins/html2pdf_v4.03/html2pdf.class.php'; // arahkan ke folder html2pdf

    try

    {

      $html2pdf = new HTML2PDF('p','Legal','en', false, 'ISO-8859-15',array(5, 5, 5, 5)); //setting ukuran kertas dan margin pada dokumen anda

      //$html2pdf->setModeDebug();

      $html2pdf->pdf->SetDisplayMode('fullpage');

      $html2pdf->setDefaultFont('Arial');

      $html2pdf->writeHTML($content, isset($_GET['vuehtml']));

      $html2pdf->Output('buku_besar.pdf');

    }

    catch(HTML2PDF_exception $e) { echo $e; } 

  }

  if ($tipe == 'excel')

  {

      header("Content-type: application/vnd-ms-excel");

      header("content-Disposition:attachment;filename=LaporanBukuBesar.xls");

  }


?>







