
<!-- data pelanggan ada di var $pelanggan -->

<div class="row">

	<div class="col-md-12">

			<div class="box">

				<div class="box-header">

					<h3 class="box-title"><?php echo $judul ?></h3>

					<hr/>

				</div>

				<div class="box-body">
					
						<div class="row" id="tempelPesan">

							<div class="col-md-4">
							</div>

							<div class="col-md-4">

								<div class="form-group">

								    <label for="NamaPerusahaan">Periode</label>

								    <select class="form-control" onchange="pilihPeriode(this)" id="periode">

								    	<option value="Bulan">Bulanan</option>

			                            <option value="Triwulan">Triwulan</option>

			                            <option value="Semester">Semester</option>

			                            <option value="Tahun">Tahunan</option>

								    </select>

							  	</div>

							    <div class="form-group">

								    <label for="AlamatPerusahaan" id="periode2">Bulan</label>

							    	<select class="form-control" id="bulan" name="bulan">

			                          <?php

			                            $data = $this->db->query("select * from ref_bulan");

			                            foreach ($data->result_array() as $row)

			                            {

			                              echo "<option value='".$row['id_bulan']."'>".$row['nama_bulan']."</option>";

			                            }

			                          ?>

			                        </select>

			                        <select class="form-control sr-only" id="triwulan" name="triwulan">

			                          <?php



			                            for($i=1;$i<=4;$i++)

			                            {

			                              echo "<option value='".$i."'>Triwulan ".$i."</option>";

			                            }

			                          ?>

			                        </select>

			                        <select class="form-control sr-only" id="semester" name="semester">

			                          <?php



			                            for($i=1;$i<=2;$i++)

			                            {

			                              echo "<option value='".$i."'>Semester ".$i."</option>";

			                            }

			                          ?>

			                        </select>

			                        <select class="form-control sr-only" id="tahun" name="tahun">

			                        <?php

			                            $tahun = 2019;

			                            echo "<option value='".$tahun."'>".$tahun."</option>";

			                        ?>

			                        </select>
							    
							    </div>

							    <div class="form-group">

				                    <label for="namaFilter">Kode Akun</label>

				                         <div class="input-group">

				                           <span class="input-group-addon" id="kodeAkun"></span>

				                              <input type="hidden" id="IDAkun" name="IDAkun"/>

				                              <input type="text" id="NamaAkun" class="form-control" name="NamaAkun"/>                                    

				                              <span class="input-group-btn">

				                                  <a data-toggle="modal" href='#modal-id' onclick="loadDataAkun(this)" class="btn btn-primary"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>&nbsp;Browse</a>

				                              </span>

				                        </div> <!--  <div class="input-group"> -->

				                </div> <!-- <div class="form-group"> -->
							<b>Tahun</b>
							<input style="background-color: white" readonly="" value="<?php echo date('Y'); ?>" type="text" class="form-control datepicker" id="tahunPeriode" name="tahunPeriode" >
							</div>

							<div class="col-md-4">

							</div>

				</div>

			</div>

			<div class="box-footer" align="center">

				<button class="btn btn-primary" type="button" onclick="cetak(this, 'pratinjau')" id="btnPratinjau">Pratinjau</button>
				<a class="btn btn-danger" type="button" onclick="cetak(this, 'pdf')" id="btnPDF">Cetak PDF</a>
				<a class="btn btn-success" type="button" onclick="cetak(this, 'excel')" id="btnExcel">Cetak Excel</a>

			</div>

		</div>

	</div>

</div>

<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Browse Kode Akun</h4>
			</div>
			<div class="modal-body">
				<div class="row">

					<div style="margin-bottom: 20px" class="col-sm-3 col-sm-offset-9">
						<div class="input-group">
							<input type="hidden" name="roleTampungan" id="roleTampungan" value="">
							<input placeholder="masukan pencarian..." id="InputKataKunci" name="InputKataKunci" type="text" class="form-control" aria-label="...">
							<div class="input-group-btn">
								<button type="button" class="btn btn-default dropdown-toggle" onclick="cariAkun()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="fa fa-search"></span>
								</button>
							</div><!-- /btn-group -->
						</div><!-- /input-group -->
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover" id="tabelAkun">
					<thead style="background-color: #ecf0f1">
						<tr>
							<th>Kode</th>
							<th>Nama</th>
							<th></th>
						</tr>
					</thead>
					
					<tbody name="tabelContent" id="tabelContent">
						
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="row" id="content">

</div>

	<script type="text/javascript">
		
		$('document').ready(function()
    	{
	
			$('.datepicker').datepicker({
			autoclose: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years"
		});	

    	}); 

    	function cariAkun(){

			var KataKunci = $("#InputKataKunci").val();

			if (KataKunci == '') {

				loadDataAkun('mbalik');

			}else{

				loadDataAkunCari(KataKunci);

			}

		  }

		function loadDataAkunCari(Obj){

		  		$("#tabelContent").empty();

		  		var identitas = $("#roleTampungan").val();

		  		//console.log(identitas);

		  		var target = "<?php echo site_url("laporan/loadDataAkunCari")?>";

		  		var data = {

		  			data : Obj
		  		}

		  		$.post(target, data, function(e){

			            	//console.log(e);

			            	var json = $.parseJSON(e);

			            	for(i = 0; i < json.length; i++)
			            	{

			            		var idAkun 		   = json[i].idAkun,
				            		kodeWithFormat = json[i].kodeWithFormat,
				            		namaWithFormat = json[i].namaWithFormat,
				            		saldoNormal    = json[i].saldoNormal,
				            		concatAkun     = json[i].concatAkun,
				            		tombol         = json[i].tombol;

			            		var table       = document.getElementById('tabelContent');  

			            		var row         = table.insertRow();

			            		row.id = idAkun;

			            		colKode         = row.insertCell(0);
			            		colNama		    = row.insertCell(1);
			            		colAction       = row.insertCell(2);

			            		colKode.innerHTML           = '<input type="hidden" name="IDAkun" id="IDAkun" value="'+idAkun+'">'+kodeWithFormat;
			            		colNama.innerHTML           = namaWithFormat;
			            		colAction.innerHTML         = '<input type="hidden" name="identitas" id="identitas" value="'+identitas+'">'+tombol;

			            	}

			            });

		    }

    		function loadDataAkun(Obj){

			  	$("#tabelContent").empty();

			  	if (Obj == 'mbalik') {

			  		var identitas = $("#roleTampungan").val();
			  		
			  	}else{

			  		var identitas = $(Obj).closest("tr").attr("role");

			  		$("#roleTampungan").val(identitas);

			  	}

			  		//console.log(identitas);

			  		var target = "<?php echo site_url("laporan/loadDataAkun")?>";

			  		var data = {}

			  		$.post(target, data, function(e){

				            	//console.log(e);

				            	var json = $.parseJSON(e);

				            	for(i = 0; i < json.length; i++)
				            	{

				            		var idAkun 		   = json[i].idAkun,
				            		kodeWithFormat = json[i].kodeWithFormat,
				            		namaWithFormat = json[i].namaWithFormat,
				            		saldoNormal    = json[i].saldoNormal,
				            		concatAkun     = json[i].concatAkun,
				            		tombol         = json[i].tombol;

				            		var table       = document.getElementById('tabelContent');  

				            		var row         = table.insertRow();

				            		row.id = idAkun;

				            		colKode         = row.insertCell(0);
				            		colNama		    = row.insertCell(1);
				            		colAction       = row.insertCell(2);

				            		colKode.innerHTML           = '<input type="hidden" name="IDAkun" id="IDAkun" value="'+idAkun+'">'+kodeWithFormat;
				            		colNama.innerHTML           = namaWithFormat;
				            		colAction.innerHTML         = '<input type="hidden" name="identitas" id="identitas" value="'+identitas+'">'+tombol;

				            	}

				            });

		  	}

		function PilihAkun(Obj){

	  		var Identitas = $(Obj).prev().val();
	  		var KodeAkun  = $(Obj).closest("tr").find("td:eq(0)").text();
	  		var NamaAkun  = $(Obj).closest("tr").find("td:eq(1)").text();
	  		var IDAkun    = $(Obj).closest("tr").find('td').eq(0).find('input[type="hidden"]').val();

	    	$("#IDAkun").val(IDAkun);
	    	$("#NamaAkun").val(NamaAkun);
	    	$("#kodeAkun").text(KodeAkun);

	    	$("#modal-id").modal("hide");

	    }

    	function pilihPeriode(objReference){

		    var periode = objReference.value;

		    var maxCount = document.getElementsByTagName('select').length - 1;

		    for(i=0;i<maxCount;i++)

		    {

		      document.getElementsByTagName('select')[i+1].setAttribute('class', 'form-control sr-only');

		    }

		    if (periode == 'Bulan')     document.getElementById('bulan').setAttribute('class', 'form-control');

		    if (periode == 'Triwulan')  document.getElementById('triwulan').setAttribute('class', 'form-control');

		    if (periode == 'Semester')  document.getElementById('semester').setAttribute('class', 'form-control');

		    if (periode == 'Tahun')   document.getElementById('tahun').setAttribute('class', 'form-control sr-only');

		    if (periode == 'Tahun') document.getElementById('periode2').setAttribute('class', 'sr-only'); 

			else document.getElementById('periode2').setAttribute('class', '');document.getElementById('periode2').innerHTML = periode;

		}

		function cetak(objReference, tipe){

		    var periode = document.getElementById('periode').value;

		    var nilai   = (periode == 'Bulan') ?    document.getElementById('bulan').value : '';

		        nilai   = (periode == 'Triwulan') ? document.getElementById('triwulan').value : nilai;

		        nilai   = (periode == 'Semester') ? document.getElementById('semester').value : nilai;

		        nilai   = (periode == 'Tahun') ?    document.getElementById('tahunPeriode').value : nilai;

		    var tahun = document.getElementById('tahunPeriode').value;

		    var IDAkun 	= $("#IDAkun").val();

		    var UrlLink = '<?php echo $html ?>';

		    var UrlLink  = UrlLink + '/' + periode + '/' + nilai + '/' + tipe + '/' + IDAkun + '/' + tahun;

		   // alert(UrlLink);



		    if (tipe !== 'pratinjau')

		    {

		      objReference.setAttribute('target', '_blank');

		      objReference.setAttribute('href', UrlLink);

		    }

		    else

		    {

		       $('#previewBoard').remove();

		       var strHTMLContentOut = ajaxFillGridJSON(UrlLink);

		       strHTMLOut = "<div class='col-sm-12'>";

		       strHTMLOut += "<div class='box' id='previewBoard'>";

		       strHTMLOut += '<div class="box-body">';

		       strHTMLOut += strHTMLContentOut;

		       strHTMLOut += "</div></div></div>";


		       $('#content').append(strHTMLOut);



		    }



		  }

		  function ajaxFillGridJSON(requestUrl, requestData) {
    
			   var returnValue = '{textStatus : "failed"}';

			    $.ajax({
			        async: false,
			        url: requestUrl,
			        data: requestData,
			        type: 'POST',
			        success: function(data, textStatus) {
			            returnValue =  data;
			        },
			        error: function(data, textStatus) {
			        }
			    });
			  
			    return returnValue;
			  }

	</script>

</div>