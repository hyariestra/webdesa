
<!-- data pelanggan ada di var $pelanggan -->

<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title"><?php echo $judul ?></h3>

				<hr/>

			</div>

			<div class="box-body">

				<div class="row" id="tempelPesan">

					<div class="col-md-4">
					</div>

					<div class="col-md-4">

						<div class="form-group">

							<label for="NamaPerusahaan">Periode</label>

							<select class="form-control" onchange="pilihPeriode(this)" id="periode">

								<option value="Bulan">Bulanan</option>

								<option value="Triwulan">Triwulan</option>

								<option value="Semester">Semester</option>

								<option value="Tahun">Tahunan</option>

							</select>

						</div>

						<div class="form-group">

							<label for="AlamatPerusahaan" id="periode2">Bulan</label>

							<select class="form-control" id="bulan" name="bulan">

								<?php

								$data = $this->db->query("select * from ref_bulan");

								foreach ($data->result_array() as $row)

								{

									echo "<option value='".$row['id_bulan']."'>".$row['nama_bulan']."</option>";

								}

								?>

							</select>

							<select class="form-control sr-only" id="triwulan" name="triwulan">

								<?php



								for($i=1;$i<=4;$i++)

								{

									echo "<option value='".$i."'>Triwulan ".$i."</option>";

								}

								?>

							</select>

							<select class="form-control sr-only" id="semester" name="semester">

								<?php



								for($i=1;$i<=2;$i++)

								{

									echo "<option value='".$i."'>Semester ".$i."</option>";

								}

								?>

							</select>

							<select class="form-control sr-only" id="tahun" name="tahun">

			                        <?php

			                            $tahun = 2019;

			                            echo "<option value='".$tahun."'>".$tahun."</option>";

			                        ?>

			                        </select>

						</div>
							<b>Tahun</b>
							<input style="background-color: white" readonly="" value="<?php echo date('Y'); ?>" type="text" class="form-control datepicker" id="tahunPeriode" name="tahunPeriode" >

					</div>

					<div class="col-md-4">
					</div>

				</div>

			</div>

			<div class="box-footer" align="center">

				<button class="btn btn-primary" type="button" onclick="cetak(this, 'pratinjau')" id="btnPratinjau">Pratinjau</button>
				<a class="btn btn-danger" type="button" onclick="cetak(this, 'pdf')" id="btnPDF">Cetak PDF</a>
				<a class="btn btn-success" type="button" onclick="cetak(this, 'excel')" id="btnExcel">Cetak Excel</a>

			</div>

		</div>

	</div>

</div>

<div class="row" id="content">

</div>

<script type="text/javascript">

	$('document').ready(function()
	{

		$('.datepicker').datepicker({
			autoclose: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years"
		});		

	}); 

	function pilihPeriode(objReference){

		var periode = objReference.value;

		var maxCount = document.getElementsByTagName('select').length - 1;

		for(i=0;i<maxCount;i++)

		{

			document.getElementsByTagName('select')[i+1].setAttribute('class', 'form-control sr-only');

		}

		if (periode == 'Bulan')     document.getElementById('bulan').setAttribute('class', 'form-control');

		if (periode == 'Triwulan')  document.getElementById('triwulan').setAttribute('class', 'form-control');

		if (periode == 'Semester')  document.getElementById('semester').setAttribute('class', 'form-control');

		if (periode == 'Tahun')   document.getElementById('tahun').setAttribute('class', 'form-control sr-only');

		if (periode == 'Tahun') document.getElementById('periode2').setAttribute('class', 'sr-only'); 

		else document.getElementById('periode2').setAttribute('class', '');document.getElementById('periode2').innerHTML = periode;

	}

	function cetak(objReference, tipe){

		var periode = document.getElementById('periode').value;

		var nilai   = (periode == 'Bulan') ?    document.getElementById('bulan').value : '';

		nilai   = (periode == 'Triwulan') ? document.getElementById('triwulan').value : nilai;

		nilai   = (periode == 'Semester') ? document.getElementById('semester').value : nilai;

		nilai   = (periode == 'Tahun') ?    document.getElementById('tahunPeriode').value : nilai;

		var tahun = document.getElementById('tahunPeriode').value;

		var UrlLink = '<?php echo $html ?>';

		var UrlLink  = UrlLink + '/' + periode + '/' + nilai + '/' + tipe + '/' + tahun;

		   // alert(UrlLink);



		   if (tipe !== 'pratinjau')

		   {

		   	objReference.setAttribute('target', '_blank');

		   	objReference.setAttribute('href', UrlLink);

		   }

		   else

		   {

		   	$('#previewBoard').remove();

		   	var strHTMLContentOut = ajaxFillGridJSON(UrlLink);

		   	strHTMLOut = "<div class='col-sm-12'>";

		   	strHTMLOut += "<div class='box' id='previewBoard'>";

		   	strHTMLOut += '<div class="box-body">';

		   	strHTMLOut += strHTMLContentOut;

		   	strHTMLOut += "</div></div></div>";


		   	$('#content').append(strHTMLOut);



		   }



		}

		function ajaxFillGridJSON(requestUrl, requestData) {

			var returnValue = '{textStatus : "failed"}';

			$.ajax({
				async: false,
				url: requestUrl,
				data: requestData,
				type: 'POST',
				success: function(data, textStatus) {
					returnValue =  data;
				},
				error: function(data, textStatus) {
				}
			});

			return returnValue;
		}

	</script>

</div>