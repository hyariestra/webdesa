<?php 



/**

* 

*/

class M_laporan extends CI_model

{


  function getDataBukuJurnal($periode='',$nilai=0){

    if ($periode == 'Bulan') {

      $nilaiPembanding = $nilai - 1;
      
      $strTanggal = $nilai == 1 ? "MONTH(tanggal) = ".$nilai : "(MONTH(tanggal) > ".$nilaiPembanding." AND MONTH(tanggal) <= ".$nilai.")" ;

    }elseif ($periode == 'Triwulan') {

      $nilai          *= 3;
      
      $nilaiPembanding = $nilai - 3;

      $strTanggal = $nilai == 1 ? "MONTH(tanggal) <= ".$nilai : "(MONTH(tanggal) > ".$nilaiPembanding." AND MONTH(tanggal) <= ".$nilai.")" ;

    }elseif ($periode == 'Semester') {
      
      $nilai *= 6;

      $nilaiPembanding = $nilai - 6;

      $strTanggal = $nilai == 1 ? "MONTH(tanggal) <= ".$nilai : "(MONTH(tanggal) > ".$nilaiPembanding." AND MONTH(tanggal) <= ".$nilai.")" ;


    }elseif ($periode = 'Tahun') {
      
      $strTanggal = "YEAR(tanggal) <= ".$nilai;

    }

    $query    = $this->db->query("SELECT nomor AS Nomor, uraian AS Uraian, CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, tanggal AS Tanggal, nama_akun AS NamaAkun,
                                  debet AS Debet, kredit AS Kredit FROM trx_ju trxJU
                                  LEFT JOIN trx_judet trxJudet ON trxJU.id_ju  = trxJudet.id_Ju
                                  LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun WHERE
                                  ".$strTanggal." ORDER BY Tanggal, Nomor ASC");

    $seq = 1;

    foreach ($query->result_array() as $key) {

      $data['No']       = $seq;
      $data['Nomor']    = $key['Nomor'];
      $data['Uraian']   = $key['Uraian'];
      $data['KodeAkun'] = $key['KodeAkun'];
      $data['Tanggal']  = formatDateIndo($key['Tanggal']);
      $data['NamaAkun'] = $key['NamaAkun'];
      $data['Debet']    = formatCurrency($key['Debet']);
      $data['Kredit']   = formatCurrency($key['Kredit']);

      $json[] = $data;

      $seq++;

    }

    $hasil = $query->num_rows() > 0 ? $json : "gagal";

    echo json_encode($hasil);

  }

  function loadDataAkun()

  {

    echo $this->treeDataKodeAkun();

  }

  private  function treeDataKodeAkun($tipeAkun = '')
      {
        
          $strHeader = '';
          $strJsonData = '[';

          $strJsonData .= $this->recursiveDataKodeAkun(0, $strJsonData);
          $strJsonData = substr($strJsonData, 1, strlen($strJsonData) - 2);
          $strJsonData .= ']';


        return $strJsonData;
      }


      private function recursiveDataKodeAkun($parent=0, $hasil)
      {

          $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 
                                           kode_akun AS KodeAkun, header as Header, 
                                           nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 
                                           saldo_normal as SaldoNormal
                                           FROM mst_akun 
                                           WHERE id_induk='".$parent."' 
                                           ORDER BY kodeUrut ASC ");
        
          foreach( $selectQuery->result_array() as $row )
          {

           $IDAkun         = $row['IDAkun'];
           $IDInduk        = $row['IDInduk'];
           $kodeInduk      = $row['KodeInduk'];
           $header         = $row['Header'];
           $kodePlainText  = $row['KodeAkun'];
           $namaPlainText  = $row['NamaAkun']; 
           $saldoNormal    = $row['SaldoNormal'];
           $level          = $row['Level'];
           $concatAkun     = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 
           $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;
           $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;

           $strSpace = '';

           for($i = 1; $i <= $level ; $i++)
           {
            $strSpace .= "&nbsp;";
           }

           $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 
                                            FROM mst_akun 
                                            WHERE id_induk = '".$IDAkun."' ");

           $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 
           
           $CountAkun  = $arrSelectQuery['CountAkun'];

           $imgRoot   = "<span class='glyphicon glyphicon-home'></span>".$strSpace;
           $imgFolder = $strSpace."<span class='glyphicon glyphicon-folder-open'></span>&nbsp;";
           $imgItem   = $strSpace."<span class='glyphicon glyphicon-folder-close'></span>&nbsp;";
           
           $imgChild  = ( $CountAkun > 0 ) ? $imgFolder : $imgItem;

           $kodeWithFormat = ($level == 1) ? $imgRoot.$kodeWithFormat : $kodeWithFormat;
           $kodeWithFormat = ($level > 1) ?  $imgChild.$kodeWithFormat : $kodeWithFormat;

           $btnTambah   = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-success\' onclick=\'PilihAkun(this);\'><span class=\'glyphicon glyphicon-plus-sign\' aria-hidden=\'true\'></button>&nbsp;';  

           $actionList = $btnTambah; 

           $hasil .= '{"idAkun"         : "'.$IDAkun .'", 
                      "kodeWithFormat"  : "'.$kodeWithFormat.'",
                      "namaWithFormat"  : "'.$namaWithFormat.'",
                      "saldoNormal"     : "'.$saldoNormal.'",
                      "concatAkun"      : "'.$concatAkun.'", 
                      "tombol"          : "'.$actionList.'"},';

           $hasil = $this->recursiveDataKodeAkun($row['IDAkun'], $hasil);
          }  //foreach( $selectQuery->result_array() as $row )

          return $hasil;
      } 


      function loadDataAkunCari($KataKunci=''){

          $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 
                                           kode_akun AS KodeAkun, header as Header, 
                                           nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 
                                           saldo_normal as SaldoNormal
                                           FROM mst_akun 
                                           WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '%".$KataKunci."%' OR nama_akun LIKE '%".$KataKunci."%'
                                           ORDER BY kodeUrut ASC ");

          foreach( $selectQuery->result_array() as $row )
          {

           $IDAkun         = $row['IDAkun'];
           $IDInduk        = $row['IDInduk'];
           $kodeInduk      = $row['KodeInduk'];
           $header         = $row['Header'];
           $kodePlainText  = $row['KodeAkun'];
           $namaPlainText  = $row['NamaAkun']; 
           $saldoNormal    = $row['SaldoNormal'];
           $level          = $row['Level'];
           $concatAkun     = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 
           $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;
           $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;

           $strSpace = '';

           for($i = 1; $i <= $level ; $i++)
           {
            $strSpace .= "&nbsp;";
           }

           $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 
                                            FROM mst_akun 
                                            WHERE id_induk = '".$IDAkun."' ");

           $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 
           
           $CountAkun  = $arrSelectQuery['CountAkun'];

           $imgRoot   = "<span class='glyphicon glyphicon-home'></span>".$strSpace;
           $imgFolder = $strSpace."<span class='glyphicon glyphicon-folder-open'></span>&nbsp;";
           $imgItem   = $strSpace."<span class='glyphicon glyphicon-folder-close'></span>&nbsp;";
           
           $imgChild  = ( $CountAkun > 0 ) ? $imgFolder : $imgItem;

           $kodeWithFormat = ($level == 1) ? $imgRoot.$kodeWithFormat : $kodeWithFormat;
           $kodeWithFormat = ($level > 1) ?  $imgChild.$kodeWithFormat : $kodeWithFormat;

           $btnTambah   = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-success\' onclick=\'PilihAkun(this);\'><span class=\'glyphicon glyphicon-plus-sign\' aria-hidden=\'true\'></button>&nbsp;';  

           $actionList = $btnTambah; 

           $hasil["idAkun"]         = $IDAkun;
           $hasil["kodeWithFormat"] = $kodeWithFormat;
           $hasil["namaWithFormat"] = $namaWithFormat;
           $hasil["saldoNormal"]    = $saldoNormal;
           $hasil["concatAkun"]     = $concatAkun;
           $hasil["tombol"]         = $actionList;

           $json[] = $hasil;

          }

          echo json_encode($json);

      }


    public function getLaporanBukuBesar($IDAkun=0, $startDate='', $endDate='', $startDateCompare='', $endDateCompare=''){

      $queryKodeAkun   = $this->db->query("SELECT CONCAT(kode_induk,'.',kode_akun) AS KodeAkun FROM mst_akun WHERE id_akun = '".$IDAkun."'");

      $KodeAkun = $queryKodeAkun->first_row()->KodeAkun;

      $qryGetSaldoAwal = $this->db->query("SELECT COALESCE( SUM(CASE WHEN mstAkun.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total
                                          FROM trx_ju trxJu
                                          LEFT JOIN trx_judet trxJuDet
                                          ON trxJu.id_ju  = trxJuDet.id_ju
                                          LEFT JOIN mst_akun mstAkun
                                          ON mstAkun.id_akun = trxJuDet.id_akun
                                          WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' AND tanggal <= '".$endDateCompare." 23:59:59'");

      $saldoAwal = $qryGetSaldoAwal->first_row()->Total;

      $query          = $this->db->query("SELECT nomor, uraian, tanggal, debet, kredit, saldo_normal
                                          FROM trx_ju trxJu
                                          LEFT JOIN trx_judet trxJuDet
                                          ON trxJu.id_ju  = trxJuDet.id_ju
                                          LEFT JOIN mst_akun mstAkun
                                          ON mstAkun.id_akun = trxJuDet.id_akun
                                          WHERE id_jenis_trans = (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'JU') 
                                          AND CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' AND (tanggal >= '".$startDate."' AND tanggal <= '".$endDate."') ");

      $strHTMLOut  = '';

      $i=0;
      $totalDebet  = 0;
      $totalKredit = 0;
      $totalSaldoAkhir = 0;
      $seq=1;
      foreach ($query->result_array() as $key) {
        
        $Nomor          = $key["nomor"];
        $Uraian         = $key["uraian"];
        $Tanggal        = $key["tanggal"];
        $Debet          = $key["debet"];
        $Kredit         = $key["kredit"];
        $SaldoNormal    = $key["saldo_normal"];

        if ($SaldoNormal == 'D'){
            $saldoAkhir    = ($i == 0) ? ($saldoAwal + $Debet) - $Kredit : ($saldoAkhir + $Debet) - $Kredit;
        }else{
            $saldoAkhir    = ($i == 0) ? ($saldoAwal + $Kredit) - $Debet : ($saldoAkhir + $Kredit) - $Debet;
        }

        $totalDebet   += $Debet;
        $totalKredit  += $Kredit;

        $totalSaldoAkhir +=  $saldoAkhir;

        $strHTMLOut .= "<tr style='border:1px solid black;'><td style='border:1px solid black;'>".$seq."</td><td style='border:1px solid black;'>".$Nomor."</td><td style='border:1px solid black;'>".$Tanggal."</td><td style='border:1px solid black;'>".$Uraian."</td><td style='border:1px solid black;' class='textRight'>".formatCurrency($Debet)."</td><td style='border:1px solid black;' class='textRight'>".formatCurrency($Kredit)."</td><td style='border:1px solid black;' class='textRight'>".formatCurrency($saldoAkhir)."</td></tr>";
        
        $i++;$seq++;
      }

      $strHTMLOut.= "<tr style='border:1px solid black;'><td style='border:1px solid black;'>&nbsp;</td><td style='border:1px solid black;'>&nbsp;</td><td style='border:1px solid black;'>&nbsp;</td><td style='border:1px solid black;'>&nbsp;</td><td style='border:1px solid black;' class='textRight'><b><u>".formatCurrency($totalDebet)."</u></b></td><td style='border:1px solid black;' class='textRight'><b><u>".formatCurrency($totalKredit)."</u></b></td><td style='border:1px solid black;' class='textRight'>&nbsp;</td></tr>";

         return $strHTMLOut;

    }

    public function getSaldoAwalBukuBesar($IDAkun=0, $startDate = '', $endDate = ''){

      $queryKodeAkun   = $this->db->query("SELECT CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, nama_akun AS NamaAkun 
                                           FROM mst_akun WHERE id_akun = '".$IDAkun."'");

      $KodeAkun = $queryKodeAkun->first_row()->KodeAkun;
      $NamaAkun = $queryKodeAkun->first_row()->NamaAkun;

      $qryGetSaldoAwal = $this->db->query("SELECT COALESCE( SUM(CASE WHEN mstAkun.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total
                                          FROM trx_ju trxJu
                                          LEFT JOIN trx_judet trxJuDet
                                          ON trxJu.id_ju  = trxJuDet.id_ju
                                          LEFT JOIN mst_akun mstAkun
                                          ON mstAkun.id_akun = trxJuDet.id_akun
                                          WHERE id_jenis_trans = (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA') 
                                          AND CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' ");

      //$saldoAwal = $qryGetSaldoAwal->first_row()->Total;

      $querySebelumnya = $this->db->query("SELECT COALESCE( SUM(CASE WHEN mstAkun.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total
                                          FROM trx_ju trxJu
                                          LEFT JOIN trx_judet trxJuDet
                                          ON trxJu.id_ju  = trxJuDet.id_ju
                                          LEFT JOIN mst_akun mstAkun
                                          ON mstAkun.id_akun = trxJuDet.id_akun
                                          WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' AND tanggal <= '".$endDate." 23:59:59'");

      $saldoAwal = $querySebelumnya->first_row()->Total;

      $strHTMLOut = '
      <tr style="border:1px solid black;border-left:none;border-right:none;font-weight:bold;">
                <td style="border:1px solid;text-align:center;padding:10px;">Akun</td>
                <td style="border:1px solid;padding:10px;">'.$KodeAkun.'</td>
                <td style="border:1px solid;padding:10px;" colspan="3">'.$NamaAkun.'</td>
                <td style="border:1px solid;text-align:center;padding:10px;">Saldo Awal</td>
                <td style="border:1px solid;text-align:right;padding:10px;">'.formatCurrency($saldoAwal).'</td>
              </tr>';

         return $strHTMLOut;

    }


    public function getLaporanLabaRugi($startDate='', $endDate='', $startDateCompare='', $endDateCompare=''){

      /*$arrAkunPajak = array();

      $strAkunPajak = "";

      $arrAkunPajak     = explode(",", GetSettingValue('akun_pajak'));

      if (is_array($arrAkunPajak))

        {

            foreach ($arrAkunPajak as $key => $value) 

            { 

                $arrAkunPajak[] = $value;

                $strAkunPajak .= $value <> '' ? " CONCAT(kode_induk, '.', kode_akun) LIKE '".$value."%' or " : " CONCAT(kode_induk, '.', kode_akun) LIKE '-99999%' or";

                $akunPajak[] = $value;

            }

                $strAkunPajak = substr( $strAkunPajak, 0, strlen($strAkunPajak) - 3);

        }*/

      $queryPendapatan = $this->db->query("SELECT id_akun AS IDAkun, kode_akun AS KodeAkun, 
                                           nama_akun AS NamaAkun, header AS Header,
                                           saldo_normal AS SaldoNormal FROM mst_akun WHERE kode_akun IN ('4', '6') AND level = 1");

      $strHTMLOut = "<tr>
                          <td style='font-weight:bold;'>
                            PENDAPATAN
                          </td>
                          <td>
                          </td>
                        </tr>";
      $labakotor = 0;
      foreach ($queryPendapatan->result_array() as $key) {

        $KodeAkun    = $key['KodeAkun'];
        $strNamaAkun = $KodeAkun.' '.$key['NamaAkun'];

        $TotalPendapatan = $this->getTotalLabaRugi($KodeAkun, $startDate, $endDate);
        
        $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;padding-left:30px;'>
                            ".$strNamaAkun."
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($TotalPendapatan)."
                          </td>
                        </tr>";

        $queryPendapatanDet = $this->db->query("SELECT id_akun AS IDAkun, CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, 
                                                nama_akun AS NamaAkun, header AS Header,
                                                saldo_normal AS SaldoNormal FROM mst_akun WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%'");

        foreach ($queryPendapatanDet->result_array() as $keys) {
            
            $KodeAkunDet    = $keys["KodeAkun"];
            $strNamaAkunDet = $KodeAkunDet.' '.$keys['NamaAkun'];

            $TotalPendapatanDet = $this->getTotalLabaRugi($KodeAkunDet, $startDate, $endDate);

            $strHTMLOut .= "<tr>
                          <td style='padding-left:60px;'>
                            ".$strNamaAkunDet."
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($TotalPendapatanDet)."
                          </td>
                        </tr>";

        }

        $labakotor += $TotalPendapatan;

      }

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            LABA KOTOR
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($labakotor)."
                          </td>
                        </tr>";

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            BIAYA
                          </td>
                          <td>
                          </td>
                        </tr>";

      $queryBiaya = $this->db->query("SELECT id_akun AS IDAkun, kode_akun AS KodeAkun, 
                                           nama_akun AS NamaAkun, header AS Header,
                                           saldo_normal AS SaldoNormal FROM mst_akun WHERE kode_akun IN ('5', '7') AND level = 1");

      $jumlahbiaya = 0;
      foreach ($queryBiaya->result_array() as $row) {

        $KodeAkunBiaya    = $row['KodeAkun'];
        $strNamaAkunBiaya = $KodeAkunBiaya.' '.$row['NamaAkun'];

        $TotalBiaya = $this->getTotalLabaRugi($KodeAkunBiaya, $startDate, $endDate);
        
        $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;padding-left:30px;'>
                            ".$strNamaAkunBiaya."
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($TotalBiaya)."
                          </td>
                        </tr>";

        $queryBiayaDet = $this->db->query("SELECT id_akun AS IDAkun, CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, 
                                                nama_akun AS NamaAkun, header AS Header,
                                                saldo_normal AS SaldoNormal FROM mst_akun WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkunBiaya."%'");

        foreach ($queryBiayaDet->result_array() as $rows) {
            
            $KodeAkunBiayaDet    = $rows["KodeAkun"];
            $strNamaAkunBiayaDet = $KodeAkunBiayaDet.' '.$rows['NamaAkun'];

            $TotalBiayaDet = $this->getTotalLabaRugi($KodeAkunBiayaDet, $startDate, $endDate);

            $strHTMLOut .= "<tr>
                          <td style='padding-left:60px;'>
                            ".$strNamaAkunBiayaDet."
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($TotalBiayaDet)."
                          </td>
                        </tr>";

        }

        $jumlahbiaya += $TotalBiaya;

      }

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            JUMLAH BIAYA
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($jumlahbiaya)."
                          </td>
                        </tr>";

      $labasebelum = $labakotor - $jumlahbiaya;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            LABA SEBELUM PAJAK
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($labasebelum)."
                          </td>
                        </tr>";

      //$totalpajak = $this->getTotal($akunPajak, $startDate, $endDate);

      $presentasepajak = GetSettingValue("presentase_pajak") == '' ? 0 : GetSettingValue("presentase_pajak");

      $totalpajak = ($presentasepajak/100) * $labakotor;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            PAJAK
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($totalpajak)."
                          </td>
                        </tr>";

      $labasesudah = $labasebelum - $totalpajak;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            LABA BERSIH SESUDAH PAJAK
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($labasesudah)."
                          </td>
                        </tr>";

      return $strHTMLOut;


    }

    public function getLaporanPelanggan($IDPelanggan = 0, $startDate='', $endDate='', $startDateCompare='', $endDateCompare=''){

      $query = $this->db->query("SELECT * FROM trx_ju trxJu LEFT JOIN trx_judet trxJuDet ON trxJu.id_ju = trxJuDet.id_ju LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJuDet.id_akun 
                                 WHERE id_pelanggan = '".$IDPelanggan."' AND (tanggal >= '".$startDate."' AND tanggal <= '".$endDate."') ORDER BY tanggal ASC");

      $strHTMLOut = '';

      $i=0;
      $totalDebet  = 0;
      $totalKredit = 0;
      $totalSaldoAkhir = 0;
      foreach ($query->result_array() as $key) {
        
        $Nomor          = $key["nomor"];
        $Uraian         = $key["uraian"];
        $Tanggal        = $key["tanggal"];
        $Debet          = $key["debet"];
        $Kredit         = $key["kredit"];
        $KodeAkun       = $key["kode_induk"].'.'.$key["kode_akun"];
        $SaldoNormal    = $key["saldo_normal"];

        // if ($SaldoNormal == 'D'){
        //     $saldoAkhir    = ($i == 0) ? ($saldoAwal + $Debet) - $Kredit : ($saldoAkhir + $Debet) - $Kredit;
        // }else{
        //     $saldoAkhir    = ($i == 0) ? ($saldoAwal + $Kredit) - $Debet : ($saldoAkhir + $Kredit) - $Debet;
        // }

        $totalDebet   += $Debet;
        $totalKredit  += $Kredit;

        //$totalSaldoAkhir +=  $saldoAkhir;

        $strHTMLOut .= "<tr style='border:1px solid black;'><td style='border:1px solid black;'>".$Tanggal."</td><td style='border:1px solid black;'>".$Nomor."</td><td style='border:1px solid black;' class='textCenter'>".$KodeAkun."</td><td style='border:1px solid black;'>".$Uraian."</td><td style='border:1px solid black;' class='textRight'>".formatCurrency($Debet)."</td><td style='border:1px solid black;' class='textRight'>".formatCurrency($Kredit)."</td></tr>";
        
        $i++;
      }

      $strHTMLOut.= "<tr style='border:1px solid black;'><td class='textRight' style='border:1px solid black;' colspan='4'><b>Total</b></td><td style='border:1px solid black;' class='textRight'><b><u>".formatCurrency($totalDebet)."</u></b></td><td style='border:1px solid black;' class='textRight'><b><u>".formatCurrency($totalKredit)."</u></b></td></tr>";

      

      return $strHTMLOut;


    }

    public function getLaporanEkuitas($startDate='', $endDate='', $startDateCompare='', $endDateCompare='', $strPeriodeHeader='', $strPeriodePembanding=''){

      $saldoAwalPendapatan  = $this->getPendapatan($startDateCompare, $endDateCompare);
      $saldoAwalBiaya       = $this->getBiaya($startDateCompare, $endDateCompare);

      $saldoAwalEkuitas = $saldoAwalPendapatan - $saldoAwalBiaya;

      $strHTMLOut = "<tr>
                          <td>
                            Saldo ".$strPeriodePembanding."
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($saldoAwalEkuitas)."
                          </td>
                        </tr>";

      $strPenambahanModal = explode(",", GetSettingValue('akun_penambahan_modal'));

      $i=0;

      if (is_array($strPenambahanModal))

      {

          foreach ($strPenambahanModal as $key => $value) 

          {

              $akunPenambahanModal[] = $value;

          }

      }

      $totalPenambahanModal = $this->getTotal($akunPenambahanModal, $startDate, $endDate, $endDateCompare);

      $strHTMLOut .= "<tr>
                          <td>
                            Penambahan Modal
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($totalPenambahanModal)."
                          </td>
                        </tr>";

      $strAkunDeviden = explode(",", GetSettingValue('akun_deviden'));

      $i=0;

      if (is_array($strAkunDeviden))

      {

          foreach ($strAkunDeviden as $key => $value) 

          {

              $akunDeviden[] = $value;

          }

      }

      $totalDeviden = $this->getTotal($akunDeviden, $startDate, $endDate, $endDateCompare);

      $strHTMLOut .= "<tr>
                          <td>
                            Deviden
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($totalDeviden)."
                          </td>
                        </tr>";

      $saldoPendapatan  = $this->getPendapatan($startDate, $endDate);
      $saldoBiaya       = $this->getBiaya($startDate, $endDate);

      $saldoLabaRugi = $saldoPendapatan - $saldoBiaya;

      $strHTMLOut .= "<tr>
                          <td>
                            Laba Rugi
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($saldoLabaRugi)."
                          </td>
                        </tr>";

      $saldoAkhir = $saldoAwalEkuitas + $totalPenambahanModal - $totalDeviden + $saldoLabaRugi;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            Saldo Akhir ".$strPeriodeHeader."
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($saldoAkhir)."
                          </td>
                        </tr>";

      return $strHTMLOut;


    }


    public function getLaporanArusKas($startDate='', $endDate='', $startDateCompare='', $endDateCompare=''){

      /*$queryMasukOperasional = $this->db->query("SELECT CONCAT(kode_induk, '.', kode_akun) AS KodeAkun, nama_akun AS NamaAkun FROM mst_akun WHERE header = 0 
                                                 AND NOT CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%' AND id_akun IN (SELECT trxJudet.id_akun FROM
                                                 trx_judet trxJudet INNER JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun WHERE id_ju IN 
                                                 (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju 
                                                 LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun WHERE 
                                                 (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') 
                                                 AND trxJudet.id_akun IN 
                                                 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') AND debet <> 0 
                                                 AND NOT id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA'))))");*/

      $queryMasukOperasional = $this->db->query("SELECT CONCAT(kode_induk, '.', kode_akun) AS KodeAkun, nama_akun AS NamaAkun FROM trx_ju a 
      											 LEFT JOIN trx_judet b ON a.id_ju = b.id_ju 
												 LEFT JOIN mst_akun c ON c.id_akun = b.id_akun WHERE a.id_ju IN 
												 (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju 
												 LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun 
												 WHERE (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') 
												 AND trxJudet.id_akun IN 
												 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') 
												 AND NOT id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA')))
												 AND kredit <> 0 AND NOT CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%' GROUP BY KodeAkun");

      $strHTMLOut = "<tr>
                          <td style='font-weight:bold;'>
                            ARUS MASUK
                          </td>
                          <td>
                          </td>
                        </tr>";

      $jumlahArusMasuk = 0;
      foreach ($queryMasukOperasional->result_array() as $key) {

        $KodeAkun    = $key['KodeAkun'];
        $strNamaAkun = $KodeAkun.' '.$key['NamaAkun'];

        $TotalPendapatan = $this->getTotalArusKasMasuk($KodeAkun, $startDate, $endDate);
        
        $strHTMLOut .= "<tr>
                          <td style='padding-left:30px;'>
                            ".$strNamaAkun."
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($TotalPendapatan)."
                          </td>
                        </tr>";

        $jumlahArusMasuk += $TotalPendapatan;

      }

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            JUMLAH ARUS MASUK
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($jumlahArusMasuk)."
                          </td>
                        </tr>";

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            ARUS KELUAR
                          </td>
                          <td>
                          </td>
                        </tr>";

      /*$queryKeluarOP = $this->db->query("SELECT CONCAT(kode_induk, '.', kode_akun) AS KodeAkun, nama_akun AS NamaAkun FROM mst_akun WHERE header = 0 
                                         AND NOT CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%' AND id_akun IN 
                                         (SELECT trxJudet.id_akun FROM trx_judet trxJudet INNER JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun 
                                         WHERE id_ju IN (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju 
                                         LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun WHERE 
                                         (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') AND trxJudet.id_akun IN 
                                         (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') 
                                         AND kredit <> 0 AND NOT id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA'))))");*/

      $queryKeluarOP = $this->db->query("SELECT CONCAT(kode_induk, '.', kode_akun) AS KodeAkun, nama_akun AS NamaAkun FROM trx_ju a 
      									 LEFT JOIN trx_judet b ON a.id_ju = b.id_ju LEFT JOIN mst_akun c ON c.id_akun = b.id_akun
										 WHERE a.id_ju IN 
										 (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju 
      									 LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun 
      									 WHERE (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') 
      									 AND trxJudet.id_akun IN 
										 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') 
      									 AND NOT id_jenis_trans IN 
      									 (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA'))) AND debet <> 0
      									 AND NOT CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%' GROUP BY KodeAkun");

      $jumlahArusKeluar = 0;
      foreach ($queryKeluarOP->result_array() as $row) {

        $KodeAkunBiaya    = $row['KodeAkun'];
        $strNamaAkunBiaya = $KodeAkunBiaya.' '.$row['NamaAkun'];

        $TotalBiaya = $this->getTotalArusKasKeluar($KodeAkunBiaya, $startDate, $endDate);
        
        $strHTMLOut .= "<tr>
                          <td style='padding-left:30px;'>
                            ".$strNamaAkunBiaya."
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($TotalBiaya)."
                          </td>
                        </tr>";

        $jumlahArusKeluar += $TotalBiaya;

      }

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            JUMLAH ARUS KELUAR
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($jumlahArusKeluar)."
                          </td>
                        </tr>";

      $jumlahArusKas = $jumlahArusMasuk - $jumlahArusKeluar;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            KENAIKAN (PENURUNAN) BERSIH KAS
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($jumlahArusKas)."
                          </td>
                        </tr>";

      $querySA = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '1.1.1%'
                                 AND id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA')");

      $querySaldoAwal = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '1.1.1%'
                                 AND (tanggal >= '".$startDateCompare." 00:00:00' AND tanggal <= '".$endDateCompare." 23:59:59')");

      $saldoAwalKas = $querySA->first_row()->Total + $querySaldoAwal->first_row()->Total;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            KAS DAN SETARA KAS AWAL 
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($saldoAwalKas)."
                          </td>
                        </tr>";

      $arusKasBersih = $saldoAwalKas + $jumlahArusKas;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            KAS DAN SETARA KAS AKHIR
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($arusKasBersih)."
                          </td>
                        </tr>";

      return $strHTMLOut;


    }


    public function getLaporanNeraca($startDate='', $endDate='', $startDateCompare='', $endDateCompare='', $Periode = ''){

      $queryAktivaLancar = $this->db->query("SELECT CONCAT(kode_induk, '.', kode_akun) AS KodeAkun, nama_akun AS NamaAkun FROM
                                           mst_akun WHERE 
                                           CONCAT(kode_induk, '.', kode_akun) LIKE '1%' AND level = '2' ORDER BY KodeAkun");

      $strHTMLOut = "<tr>
                          <td style='font-weight:bold;'>
                            AKTIVA
                          </td>
                          <td>
                          </td>
                        </tr>";

      $totalAktiva=0;
      foreach ($queryAktivaLancar->result_array() as $key) {

        $KodeAkun    = $key['KodeAkun'];
        $strNamaAkun = $key['NamaAkun'];

        $TotalPerAkun = $this->getTotalArusKas($KodeAkun, $startDate, $endDate, $Periode);
        
        $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;padding-left:30px;'>
                            ".$strNamaAkun."
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($TotalPerAkun)."
                          </td>
                        </tr>";

        $queryAktivaLancarDet = $this->db->query("SELECT id_akun AS IDAkun, CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, 
                                                nama_akun AS NamaAkun, header AS Header, level AS LevelAkun,
                                                saldo_normal AS SaldoNormal FROM mst_akun WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' AND level > 2 ORDER BY KodeAkun");

        foreach ($queryAktivaLancarDet->result_array() as $keys) {
            
            $KodeAkunDet    = $keys["KodeAkun"];
            $level          = $keys["LevelAkun"];
            $Header         = $keys["Header"];

            $strSpace = 0;

             for($i = 3; $i <= $level ; $i++)
             {
              $strSpace += 30;
             }

             $strNamaAkunDet = $KodeAkunDet.' '.$keys['NamaAkun'];

            $TotalPerAkunDet = $this->getTotalArusKas($KodeAkunDet, $startDate, $endDate, $Periode);

            $strHTMLOut .= "<tr>
                          <td style='padding-left:".$strSpace."px'>
                            ".$strNamaAkunDet."
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($TotalPerAkunDet)."
                          </td>
                        </tr>";

        }

        $totalAktiva += $TotalPerAkun;

      }

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            TOTAL AKTIVA
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($totalAktiva)."
                          </td>
                        </tr>";

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            PASIVA
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                          </td>
                        </tr>";

      $queryPasiva = $this->db->query("SELECT CONCAT(kode_induk, '.', kode_akun) AS KodeAkun, nama_akun AS NamaAkun FROM
                                           mst_akun WHERE 
                                           CONCAT(kode_induk, '.', kode_akun) LIKE '2%' AND level = '2' ORDER BY KodeAkun");

      $totalPasiva=0;
      foreach ($queryPasiva->result_array() as $row) {

        $KodeAkun    = $row['KodeAkun'];
        $strNamaAkun = $row['NamaAkun'];

        $TotalPerAkunPasiva = $this->getTotalArusKas($KodeAkun, $startDate, $endDate, $Periode);
        
        $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;padding-left:30px;'>
                            ".$strNamaAkun."
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($TotalPerAkunPasiva)."
                          </td>
                        </tr>";

        $queryPasivaDet = $this->db->query("SELECT id_akun AS IDAkun, CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, 
                                                nama_akun AS NamaAkun, header AS Header, level AS LevelAkun,
                                                saldo_normal AS SaldoNormal FROM mst_akun WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' AND level > 2 ORDER BY KodeAkun");

        foreach ($queryPasivaDet->result_array() as $rows) {
            
            $KodeAkunDet    = $rows["KodeAkun"];
            $level          = $rows["LevelAkun"];
            $Header         = $rows["Header"];

            $strSpace = 0;

             for($i = 3; $i <= $level ; $i++)
             {
              $strSpace += 30;
             }

             $strNamaAkunDet = $KodeAkunDet.' '.$rows['NamaAkun'];

            $TotalPerAkunPasivaDet = $this->getTotalArusKas($KodeAkunDet, $startDate, $endDate, $Periode);

            $strHTMLOut .= "<tr>
                          <td style='padding-left:".$strSpace."px'>
                            ".$strNamaAkunDet."
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($TotalPerAkunPasivaDet)."
                          </td>
                        </tr>";

        }

        $totalPasiva += $TotalPerAkunPasiva;

      }

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            TOTAL PASIVA
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($totalPasiva)."
                          </td>
                        </tr>";

      /*$saldoAwalPendapatan  = $this->getPendapatan($startDateCompare, $endDateCompare);
      $saldoAwalBiaya       = $this->getBiaya($startDateCompare, $endDateCompare);

      $saldoAwalEkuitas = $saldoAwalPendapatan - $saldoAwalBiaya;

      $strPenambahanModal = explode(",", GetSettingValue('akun_penambahan_modal'));

      $i=0;

      if (is_array($strPenambahanModal))

      {

          foreach ($strPenambahanModal as $key => $value) 

          {

              $akunPenambahanModal[] = $value;

          }

      }

      $totalPenambahanModal = $this->getTotal($akunPenambahanModal, $startDate, $endDate);

      $strAkunDeviden = explode(",", GetSettingValue('akun_deviden'));

      $i=0;

      if (is_array($strAkunDeviden))

      {

          foreach ($strAkunDeviden as $key => $value) 

          {

              $akunDeviden[] = $value;

          }

      }

      $totalDeviden = $this->getTotal($akunDeviden, $startDate, $endDate);*/

      $saldoPendapatan  = $this->getPendapatan($startDate, $endDate, $Periode);
      $saldoBiaya       = $this->getBiaya($startDate, $endDate, $Periode);

      $saldoLabaRugi = $saldoPendapatan - $saldoBiaya;
      
      //$saldoAkhir = $saldoAwalEkuitas + $totalPenambahanModal - $totalDeviden + $saldoLabaRugi;

      /*$strAkunLabaDitahan = explode(",", GetSettingValue('akun_laba_ditahan'));

      $i=0;

      if (is_array($strAkunLabaDitahan))

      {

          foreach ($strAkunLabaDitahan as $key => $value) 

          {

              $akunLabaDitahan[] = $value;

          }

      }

      $totalLabaDitahan = $this->getTotal($akunLabaDitahan, $startDate, $endDate);

      $totalEkuitas = $saldoAkhir + $totalLabaDitahan;*/

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            EKUITAS
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                          </td>
                        </tr>";

      $queryEkuitas = $this->db->query("SELECT CONCAT(kode_induk, '.', kode_akun) AS KodeAkun, nama_akun AS NamaAkun FROM
                                           mst_akun WHERE 
                                           CONCAT(kode_induk, '.', kode_akun) LIKE '3%' AND level = '2' ORDER BY KodeAkun");

      $totalEkuitas=0;
      foreach ($queryEkuitas->result_array() as $asd) {

      	$KodeAkun    = $asd['KodeAkun'];

      	$queryEkuitasDet = $this->db->query("SELECT id_akun AS IDAkun, CONCAT(kode_induk,'.',kode_akun) AS KodeAkun, 
                                                nama_akun AS NamaAkun, header AS Header, level AS LevelAkun,
                                                saldo_normal AS SaldoNormal FROM mst_akun WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' AND level > 2 ORDER BY KodeAkun");

        $strNamaAkun = $queryEkuitasDet->num_rows() > 0 ? $asd['NamaAkun'] : $KodeAkun.' '.$asd['NamaAkun'];
        $strBold 	 = $queryEkuitasDet->num_rows() > 0 ? 'font-weight:bold;' : '';

        $TotalPerAkunEkuitas = $KodeAkun == '3.6' ? $saldoLabaRugi : $this->getTotalArusKas($KodeAkun, $startDate, $endDate, $Periode);
        
        $strHTMLOut .= "<tr>
                          <td style='padding-left:30px;".$strBold."'>
                            ".$strNamaAkun."
                          </td>
                          <td style='text-align:right;".$strBold."'>
                            ".formatCurrency($TotalPerAkunEkuitas)."
                          </td>
                        </tr>";

	        if ($queryEkuitasDet->num_rows() > 0) {
	        	
	        	foreach ($queryEkuitasDet->result_array() as $rowv) {
	            
			            $KodeAkunDet    = $rowv["KodeAkun"];
			            $level          = $rowv["LevelAkun"];
			            $Header         = $rowv["Header"];

			            $strSpace = 0;

			             for($i = 3; $i <= $level ; $i++)
			             {
			              $strSpace += 30;
			             }

			             $strNamaAkunDet = $KodeAkunDet.' '.$rowv['NamaAkun'];

			            $TotalPerAkunEkuitasDet = $this->getTotalArusKas($KodeAkunDet, $startDate, $endDate, $Periode);

			            $strHTMLOut .= "<tr>
			                          <td style='padding-left:".$strSpace."px'>
			                            ".$strNamaAkunDet."
			                          </td>
			                          <td style='text-align:right;'>
			                            ".formatCurrency($TotalPerAkunEkuitasDet)."
			                          </td>
			                        </tr>";

			        }

			        $totalEkuitas += $TotalPerAkunEkuitasDet;

	        }else{

	        	$totalEkuitas += $TotalPerAkunEkuitas;

	        }

    	}

      /*$strHTMLOut .= "<tr>
                          <td style='padding-left:30px;'>
                            Modal
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($saldoAkhir)."
                          </td>
                        </tr>";

      $strHTMLOut .= "<tr>
                          <td style='padding-left:30px;'>
                            Laba Ditahan
                          </td>
                          <td style='text-align:right;'>
                            ".formatCurrency($totalLabaDitahan)."
                          </td>
                        </tr>";*/

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            TOTAL EKUITAS
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($totalEkuitas)."
                          </td>
                        </tr>";

      $totalNeraca = $totalPasiva + $totalEkuitas;

      $strHTMLOut .= "<tr>
                          <td style='font-weight:bold;'>
                            TOTAL PASIVA DAN EKUITAS
                          </td>
                          <td style='font-weight:bold;text-align:right;'>
                            ".formatCurrency($totalNeraca)."
                          </td>
                        </tr>";



      return $strHTMLOut;


    }


    public function getTotalLabaRugi($KodeAkun='', $startDate='', $endDate=''){

      /*$arrAkunPajak = array();

      $strAkunPajak = "";

      $arrAkunPajak     = explode(",", GetSettingValue('akun_pajak'));

      if (is_array($arrAkunPajak))

        {

            foreach ($arrAkunPajak as $key => $value) 

            { 

                $arrAkunPajak[] = $value;

                $strAkunPajak .= $value <> '' ? " CONCAT(kode_induk, '.', kode_akun) LIKE '".$value."%' or " : " CONCAT(kode_induk, '.', kode_akun) LIKE '-99999%' or";

                $AkunPajak[] = $value;

            }

                $strAkunPajak = substr( $strAkunPajak, 0, strlen($strAkunPajak) - 3);

        }*/

      $query = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%' 
                                 AND (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59')");

      return $query->first_row()->Total;

    }


    public function getPendapatan($startDate='', $endDate='', $Periode = ''){

    	// if ($Periode == 'Tahun') {
    		
    	// 	$strTanggal = "tanggal <= '".$endDate." 23:59:59'";

    	// }else{

    	// 	$strTanggal = "(tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59')";

    	// }

    	$strTanggal = "tanggal <= '".$endDate." 23:59:59'";

      $query = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE (CONCAT(kode_induk,'.',kode_akun) LIKE '4%' OR CONCAT(kode_induk,'.',kode_akun) LIKE '6%')
                                 AND ".$strTanggal);

      return $query->first_row()->Total;

    }


    public function getBiaya($startDate='', $endDate='', $Periode = ''){

    	// if ($Periode == 'Tahun') {
    		
    	// 	$strTanggal = "tanggal <= '".$endDate." 23:59:59'";

    	// }else{

    	// 	$strTanggal = "(tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59')";

    	// }

    	$strTanggal = "tanggal <= '".$endDate." 23:59:59'";

      $query = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE (concat(kode_induk, '.', kode_akun) LIKE '5%' 
                                        or concat(kode_induk, '.', kode_akun) LIKE '7%')
                                 AND ".$strTanggal);

      return $query->first_row()->Total;

    }


    public function getTotal($KodeAkun=array(), $startDate='', $endDate='', $endDateCompare = ''){

      $total=0;

      foreach ($KodeAkun as $key => $value){

      $KodeAkun = $value <> '' ? $value : "-99999";

      $querySaldoAwal = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%'
                                 AND tanggal <= '".$endDateCompare." 23:59:59'");

      $total += $querySaldoAwal->first_row()->Total;

      $query = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%'
                                 AND (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59')");

      $total += $query->first_row()->Total;

      }

      return $total;

    }

    public function getTotalArusKas($KodeAkun='', $startDate='', $endDate='', $Periode = ''){

    	// if ($Periode == 'Tahun') {
    		
    	// 	$strTanggal = "tanggal <= '".$endDate." 23:59:59'";

    	// }else{

    	// 	$strTanggal = "(tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59')";

    	// }

    	$strTanggal = "tanggal <= '".$endDate." 23:59:59'";

      $query = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%'
                                 AND ".$strTanggal);

      return $query->first_row()->Total;

    }

    public function getTotalArusKasMasuk($KodeAkun='', $startDate='', $endDate=''){

     /* $query = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%'
                                 and debet <> 0
                                 AND (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59')
                                 AND NOT id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA')");*/

      /*$query = $this->db->query("SELECT COALESCE(SUM(COALESCE(kredit, 0) - COALESCE(debet, 0)),0) AS Total FROM trx_judet trxJudet INNER JOIN mst_akun mstAkun 
                                 ON mstAkun.id_akun = trxJudet.id_akun WHERE id_ju IN 
                                 (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju 
                                 LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun WHERE 
                                 (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') AND trxJudet.id_akun IN 
                                 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') AND debet <> 0 AND NOT id_jenis_trans IN 
                                 (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA'))) AND trxJudet.id_akun IN 
                                 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '".$KodeAkun."%')");*/

      $query = $this->db->query("SELECT SUM(kredit) AS Total FROM trx_ju a LEFT JOIN trx_judet b ON a.id_ju = b.id_ju LEFT JOIN mst_akun c ON c.id_akun = b.id_akun
								 WHERE a.id_ju IN (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju 
      							 LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun WHERE (
        						 tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') AND trxJudet.id_akun IN 
        						 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') AND NOT id_jenis_trans IN 
      							 (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA'))) AND kredit <> 0
      							 AND CONCAT(kode_induk, '.', kode_akun) LIKE '".$KodeAkun."%' ");

      return $query->first_row()->Total;

    }

    public function getTotalArusKasKeluar($KodeAkun='', $startDate='', $endDate=''){

     /* $query = $this->db->query("SELECT COALESCE( SUM(CASE WHEN c.saldo_normal = 'D' THEN COALESCE(debet, 0) - COALESCE(kredit, 0) ELSE 
                                 COALESCE(kredit, 0) - COALESCE(debet,0) END), 0) AS Total FROM trx_ju a
                                 LEFT JOIN trx_judet b
                                 ON a.id_ju = b.id_ju
                                 LEFT JOIN mst_akun c
                                 ON c.id_akun = b.id_akun 
                                 WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '".$KodeAkun."%'
                                 and kredit <> 0
                                 AND (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59')
                                 AND NOT id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode = 'SA')"); */

      /*$query = $this->db->query("SELECT COALESCE(SUM(COALESCE(debet, 0) - COALESCE(kredit, 0)),0) AS Total FROM trx_judet trxJudet INNER JOIN mst_akun mstAkun 
                                 ON mstAkun.id_akun = trxJudet.id_akun WHERE id_ju IN 
                                 (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju
                                 LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun WHERE 
                                 (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') AND trxJudet.id_akun IN 
                                 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') AND kredit <> 0 
                                 AND NOT id_jenis_trans IN (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA'))) AND trxJudet.id_akun IN 
                                 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '".$KodeAkun."%')");*/

      $query = $this->db->query("SELECT SUM(debet) AS Total FROM trx_ju a LEFT JOIN trx_judet b ON a.id_ju = b.id_ju LEFT JOIN mst_akun c ON c.id_akun = b.id_akun
								 WHERE a.id_ju IN (SELECT trxJU.id_ju FROM trx_ju trxJU LEFT JOIN trx_judet trxJudet ON trxJU.id_ju = trxJudet.id_ju 
								 LEFT JOIN mst_akun mstAkun ON mstAkun.id_akun = trxJudet.id_akun 
								 WHERE (tanggal >= '".$startDate." 00:00:00' AND tanggal <= '".$endDate." 23:59:59') AND trxJudet.id_akun IN 
      							 (SELECT id_akun FROM mst_akun WHERE CONCAT(kode_induk, '.', kode_akun) LIKE '1.1.1%') AND NOT id_jenis_trans IN 
      							 (SELECT id_jenis_trans FROM ref_jenis_trans WHERE kode IN ('SA'))) AND debet <> 0
      							 AND CONCAT(kode_induk, '.', kode_akun) LIKE '".$KodeAkun."%'");

      return $query->first_row()->Total;

    }


}



 ?>