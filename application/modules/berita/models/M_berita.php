<?php 

class M_berita extends CI_model

{

	var $table = 'berita'; //nama tabel dari database
    var $column_order = array(null, 'a.tanggal'); //field yang ada di table user
    var $column_search = array('judul','nama_kategori'); //field yang diizin untuk pencarian 
    var $order = array('a.tanggal' => 'desc'); // default order 


    function trimelement($data,$panjang)
    {

     $max_length = $panjang;

     if (strlen($data) > $max_length)
     {
      $offset = ($max_length - 3) - strlen($data);
      $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
    }

    return $data;
  }

  function data($number,$offset){
    return $query = $this->db->get('user',$number,$offset)->result();       
  }


  function update_dibaca_berita($id){

   $this->db->set('dibaca', 'dibaca+1', FALSE);
   $this->db->where('id_berita', $id);
   $this->db->update('berita');
 }

 function berita_recent($limit,$id){

   $this->db->select('*');
   $this->db->from('berita a');
   $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
   $this->db->where('a.status', 'publish');
   $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
   $this->db->where('a.id_berita >',$id );
   $this->db->order_by('a.id_berita', 'desc');
   $this->db->limit($limit);  
   $data = $this->db->get()->result_array();

   foreach ($data as $key => $value) {


    $data[$key]['id'] = $value['id_berita'];
    $data[$key]['judul'] = $this->trimelement($value['judul'],80);
    $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'],150);
    $data[$key]['kategori'] = $value['nama_kategori'];
    $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
    $data[$key]['tanggal'] = tgl_indo($value['tanggal']);
    $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
    $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
    $data[$key]['like'] = $value['like_berita'];
    $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

  }

  return $data;
}


function info_terkait($limit,$tag,$id){
  $pisah_kata  = explode(",",$tag);
  $jml_katakan = (integer)count($pisah_kata);
  $jml_kata = $jml_katakan-1; 
  $cari = "SELECT * FROM berita a
  LEFT JOIN `kategori` b ON b.`id_kategori` = a.`id_kategori` " ;
  $cari .= " WHERE  a.id_desa  =  '".getSettingDesa()['id_desa']."' AND a.id_berita != ".$id."   ";
  $cari .= " AND";
  $cari .= " ( ";
  for ($i=0; $i<=$jml_kata; $i++){

    $cari .= " tag LIKE '%$pisah_kata[$i]%' ";
    if ($i < $jml_kata ){
      $cari .= " OR ";
    }
  }
  $cari .= " ) ";
  $cari .= " ORDER BY id_berita DESC LIMIT $limit";
  $cari = $this->db->query($cari);


  $data = array();

  foreach ($cari->result_array() as $key => $value) {


    $data[$key]['id'] = $value['id_berita'];
    $data[$key]['judul'] = $this->trimelement($value['judul'],80);
    $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'],150);
    $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
    $data[$key]['kategori'] = $value['nama_kategori'];
    $data[$key]['tanggal'] = $value['tanggal'];
    $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
    $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
    $data[$key]['like'] = $value['like_berita'];
    $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

  }

  return $data;



}

function jumlah_data($cari='',$kategori='')

{


  $this->db->select('*');
  $this->db->from('berita a');
  $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
  $this->db->where('a.status', 'publish');
  $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );


  if (!empty($kategori)) {
    $this->db->where('d.slug_kategori', $kategori);   
  }

  if (!empty($cari['key'])) {
    $this->db->like('a.judul', $cari['key'], 'both');   
  }

  if (!empty($cari['tag'])) {

    $id_tag = $this->get_id_tag($cari['tag']);
    $this->db->like('a.tag', $id_tag, 'both');   
  }



  $data = $this->db->get()->num_rows();

  return $data;


}

function get_berita_popular($limit)
{

 $this->db->select('*');
 $this->db->from('berita a');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
 $this->db->order_by('a.like_berita', 'desc');
 $this->db->limit($limit);  
 $data = $this->db->get()->result_array();

 foreach ($data as $key => $value) {


  $data[$key]['id'] = $value['id_berita'];
  $data[$key]['judul'] = $this->trimelement($value['judul'],80);
  $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'],150);
  $data[$key]['kategori'] = $value['nama_kategori'];
  $data[$key]['tanggal'] = $value['tanggal'];
  $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
  $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
  $data[$key]['like'] = $value['like_berita'];
  $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

}

return $data;

}

function get_berita_popular_selected($limit, $kategori)
{

 $this->db->select('*');
 $this->db->from('berita a');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('d.slug_kategori', $kategori);
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
 $this->db->order_by('a.like_berita', 'desc');
 $this->db->limit($limit);  
 $data = $this->db->get()->result_array();

 foreach ($data as $key => $value) {


  $data[$key]['id'] = $value['id_berita'];
  $data[$key]['judul'] = $this->trimelement($value['judul'],80);
  $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'],150);
  $data[$key]['kategori'] = $value['nama_kategori'];
  $data[$key]['tanggal'] = $value['tanggal'];
  $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
  $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
  $data[$key]['like'] = $value['like_berita'];
  $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

}

return $data;

}


public function get_category_name($kategori)
{
  $this->db->select('*');
  $this->db->from('kategori a');
  $this->db->where('a.slug_kategori', $kategori );
  $data = $this->db->get()->row_array();

  return $data;
}

function get_tags()
{
  $this->db->select('*');
 $this->db->from('tags a');
 $this->db->where('a.status', 'Y');
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
 $this->db->or_where('a.id_desa IS NULL', null, false);
 $data = $this->db->get()->result_array();


 foreach ($data as $key => $value) {


  $data[$key]['id'] = $value['id_tag'];
  $data[$key]['nama_tag'] = $value['nama_tag'];
  $data[$key]['seo_tag'] = $value['seo_tag'];

}

return $data;


}


function get_berita_headline()
{

 $this->db->select('*');
 $this->db->from('berita a');
 $this->db->join('ref_desa_users b', 'a.id_user = b.id_admin', 'left');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('a.headline', 'Y');
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
 $this->db->order_by('a.id_berita', 'desc');
 $data = $this->db->get()->result_array();

 foreach ($data as $key => $value) {


  $data[$key]['id'] = $value['id_berita'];
  $data[$key]['judul'] = $this->trimelement($value['judul'],80);
  $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'],150);
  $data[$key]['kategori'] = $value['nama_kategori'];
  $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
  $data[$key]['tanggal'] = $value['tanggal'];
  $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.rawurlencode($value['gambar_uniq']) : 'img/imgnotfound.png';
  $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
  $data[$key]['like'] = $value['like_berita'];

}

return $data;



}


function get_id_tag($value='')
{
 $r = $this->db->query("SELECT id_tag FROM tags WHERE seo_tag = '".$value."'  ")->row_array();

 return $r['id_tag'];

}


function get_berita_homepage($cari, $perpage, $pagenum,$kategori='')
{


  if($pagenum>1)
  {
   $limit = $perpage;
   $start = $perpage * ($pagenum-1);
 }
 else
 {
   $limit = $perpage;
   $start = 0;
 }


 $this->db->select('*');
 $this->db->from('berita a');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->join('admin e', 'e.id_admin = a.id_user', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );


 if (!empty($kategori)) {
  $this->db->where('d.slug_kategori', $kategori);   
}

if (!empty($cari['key'])) {
  $this->db->like('a.judul', $cari['key'], 'both');   
}

if (!empty($cari['tag'])) {

  $id_tag = $this->get_id_tag($cari['tag']);
  $this->db->like('a.tag', $id_tag, 'both');   
}

if (!empty($cari['sort']=='popular')) {
  $this->db->order_by('a.dibaca', 'desc');
}

if (!empty($cari['sort']=='share')) {
  $this->db->order_by('a.dibagikan', 'desc');
}

if (!empty($cari['sort']=='like')) {
  $this->db->order_by('a.like_berita', 'desc');
}


$this->db->order_by('a.tanggal', 'desc');

$this->db->limit($limit,$start); 
$data = $this->db->get()->result_array();


foreach ($data as $key => $value) {


  $data[$key]['id'] = $value['id_berita'];
  $data[$key]['judul'] = $this->trimelement($value['judul'],80);
  $data[$key]['isi_berita'] = $this->trimelement(strip_tags($value['isi_berita']),150);
  $data[$key]['kategori'] = $value['nama_kategori'];
  $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
  $data[$key]['tanggal'] = $value['tanggal'];
  $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.rawurlencode($value['gambar_uniq']) : 'img/imgnotfound.png';
  $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
  $data[$key]['like'] = $value['like_berita'];
  $data[$key]['dibagikan'] = $value['dibagikan'];
  $data[$key]['dibaca'] = $value['dibaca'];
  $data[$key]['penulis'] = $value['nama'];
  $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

}

return $data;
}

function get_berita_all()
{

 $this->db->select('*');
 $this->db->from('berita a');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->join('admin e', 'e.id_admin = a.id_user', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
 $this->db->order_by("a.id_berita", "desc");

$data = $this->db->get()->result_array();


foreach ($data as $key => $value) {


  $data[$key]['id'] = $value['id_berita'];
  $data[$key]['judul'] = $this->trimelement($value['judul'],80);
  $data[$key]['isi_berita'] = $this->trimelement(strip_tags($value['isi_berita']),150);
  $data[$key]['kategori'] = $value['nama_kategori'];
  $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
  $data[$key]['tanggal'] = $value['tanggal'];
  $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.rawurlencode($value['gambar_uniq']) : 'img/imgnotfound.png';
  $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
  $data[$key]['like'] = $value['like_berita'];
  $data[$key]['dibagikan'] = $value['dibagikan'];
  $data[$key]['dibaca'] = $value['dibaca'];
  $data[$key]['penulis'] = $value['nama'];
  $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

}

return $data;
}


function get_count_kategori()
{

 $this->db->select('COUNT(a.id_kategori) AS kategori,
 COUNT(d.id_kategori) AS counts,
 d.nama_kategori,
 d.`slug_kategori`,
 judul');
 $this->db->from('berita a');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->join('admin e', 'e.id_admin = a.id_user', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
 $this->db->group_by("a.id_kategori");
 $this->db->order_by("counts", "desc");

$data = $this->db->get()->result_array();


foreach ($data as $key => $value) {


  $data[$key]['kategori'] = $value['nama_kategori'];
  $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
  $data[$key]['counts'] = $value['counts'];

}

return $data;
}

public function update_like_berita($id)
{

  return  $this->db->query("UPDATE berita SET like_berita = like_berita + 1 WHERE id_berita = '".$id."' ");


}

public function update_share_berita($id)
{

  return  $this->db->query("UPDATE berita SET dibagikan = dibagikan + 1 WHERE id_berita = '".$id."' ");


}

public function update_dislike_berita($id)
{

  return  $this->db->query("UPDATE berita SET like_berita = like_berita - 1 WHERE id_berita = '".$id."' ");


}

function getNametag($id)
{
 $tags = $this->db->query("SELECT nama_tag,seo_tag FROM tags WHERE id_tag = ".$id." ")->row_array();

 return $tags;

}


public function simpan_email($email)
{

  $now = date("Y-m-d H:i:s");

  return  $this->db->query("INSERT INTO `newsletter` (id_desa, email,tanggal_insert) VALUES ( ".getSettingDesa()['id_desa']." , '".$email."', '".$now."' )");


}

public function simpan_komen($params)
{
  $this->db->trans_begin();




  $result = $this->db->insert('comment',$params);
 
 
 
  if ($result == false){
   $this->db->trans_rollback();
 
 }else{
   $this->db->trans_commit();
 
 }
 
 return [
   'result' => $result
 ];
}

function get_comments($id)
{
  $data['berita'] = $this->db->query("SELECT *,
  (
  SELECT COUNT(`id_comment`)
   FROM `comment` 
  WHERE id_berita = '".$id."'
  ) AS jumlah
   FROM `comment` 
  WHERE id_berita = '".$id."'
  ")->result_array();
  

  $newArray = [];

  foreach ($data['berita'] as $key => $item) {
    $newArray[$key]['idc'] = $item['id_comment'];
    $newArray[$key]['nama'] = $item['nama'];
    $newArray[$key]['isi'] = $item['isi'];
    $newArray[$key]['tgl'] = tgl_indo($item['insert_time']);
    $newArray[$key]['first'] = substr($item['nama'],0,1);
    $newArray[$key]['jumlah'] = $item['jumlah'];
  }

  return $newArray;
}

public function get_detail_berita($id,$seo)
{
 $data['berita'] = $this->db->query("SELECT *,a.`tag` AS tags  FROM berita a
  LEFT JOIN `kategori` b ON b.`id_kategori` = a.`id_kategori`
  LEFT JOIN admin c ON c.`id_admin` = a.`id_user`
  WHERE a.id_berita = ".$id." AND judul_seo = '".$seo."' ")->row_array();
 $data['berita']['image'] =  !empty( $data['berita']['gambar_uniq']) ? '<img class="img-detail" src="'.base_url('asset/foto_berita/'.rawurlencode($data['berita']['gambar_uniq'])).'"><br><br><br>' : '';
 $data['berita']['url_kategori'] =  base_url('berita/kategori/'.$data['berita']['slug_kategori']);
 $data['berita']['gambar_custom'] =  !empty( $data['berita']['gambar_uniq']) ? base_url('asset/foto_berita/'.rawurlencode($data['berita']['gambar_uniq'])) : '';




 $data['tag']  = array();

 if (!empty($data['berita']['tag'])) {


  $tag = explode(',', $data['berita']['tag']);


  foreach ($tag as $key => $value) {


   $data['tag'][] =  $this->getNametag($value);

 }
}

return $data;

}


public function getDate($date)
{


  $createDate = new DateTime($date);

  $strip = $createDate->format('Y-m-d');
  return $strip; 


}

function get_all_post()
{
 $this->db->select('*');
 $this->db->from('berita a');
 $this->db->join('kategori d', 'd.id_kategori = a.id_kategori', 'left');
 $this->db->where('a.status', 'publish');
 $this->db->where('a.id_desa', getSettingDesa()['id_desa'] );
 $data = $this->db->get()->result_array();

 foreach ($data as $key => $value) {


  $data[$key]['id'] = $value['id_berita'];
  $data[$key]['judul'] = $this->trimelement($value['judul'],80);
  $data[$key]['isi_berita'] = $this->trimelement($value['isi_berita'],150);
  $data[$key]['kategori'] = $value['nama_kategori'];
  $data[$key]['url_kategori'] =  base_url('berita/kategori/'.$value['slug_kategori']);
  $data[$key]['tanggal'] = $this->getDate($value['tanggal']);
  $data[$key]['gambar'] = !empty($value['gambar_uniq']) ? 'foto_berita/'.$value['gambar_uniq'] : 'img/imgnotfound.png';
  $data[$key]['url'] = base_url('berita/detail/'.$value['id_berita'].'/'.$value['judul_seo']);
  $data[$key]['like'] = $value['like_berita'];
  $data[$key]['avatar'] = !empty($value['foto_admin_uniq']) ? 'foto_user/'.rawurlencode($value['foto_admin_uniq']) : 'img/user.png';

}

return $data;

}


}


?>