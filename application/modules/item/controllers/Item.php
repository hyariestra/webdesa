<?php 

class Item extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))



		{



			redirect("pengguna/login");



		}

	}
	

	function index()

	{


		$data['item'] = $this->M_item->get_item();

		$this->themeadmin->tampilkan('tampilitem',$data);


	}

	public function tambah()
	{


		$data['kategori'] =$this->M_menu->produk_tree();

		$this->themeadmin->tampilkan('tambahproduk',$data);
	}

	public function simpan()
	{
		$post = $this->input->post();
		$result= true;


		if ($post['nama_produk'] == "") {
			
			$err_msg[] = 'Produk Kosong';
			$result = false;

		}

		if ($post['id_kategori'] == "") {
			
			$err_msg[] = 'Kategori Kosong';
			$result = false;

		}
		if ($post['kode_produk'] == "") {
			
			$err_msg[] = 'Kode Kosong';
			$result = false;

		}
		if ($post['stok_produk'] == "") {
			
			$err_msg[] = 'Stok Kosong';
			$result = false;

		}

		if ($post['berat_produk'] == "") {
			
			$err_msg[] = 'Berat Kosong';
			$result = false;

		}

		if ($post['harga_produk'] == "") {
			
			$err_msg[] = 'Harga Produk Kosong';
			$result = false;

		}

		if (empty($_FILES['gambar'])) {

			$err_msg[] = 'Gambar Produk Kosong';
			$result = false;

		}

	
	
		$gambarArr = $this->M_item->reArrayFiles($_FILES['gambar']);

	
		$param = array('nama_produk'=>$post['nama_produk'],
			'id_kategori'=>$post['id_kategori'],
			'slug'=>$post['slug'],
			'kode_produk'=>$post['kode_produk'],
			'stok_produk'=>$post['stok_produk'],
			'berat_produk'=>$post['berat_produk'],
			'harga_produk'=>$post['harga_produk'],
			'diskon'=>$post['diskon'],
			'harga_potongan'=>$post['harga_diskon'],
			'keterangan_produk'=>$post['keterangan_produk'],
			'tanggal_produk'=>date("Y-m-d H:i:s"),
			
		);

		$hasil = $this->M_item->simpan_item($param,$gambarArr,$post['thumb']);
		$result = $hasil['result'];
		$err_msg[] = $hasil['pesan'];


	
		if ($result) {
			
			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}




}

?>