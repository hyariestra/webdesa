<?php 



/**

* 

*/

class M_akun extends CI_model

{

	

	function loadDataAkun()

	{

		echo $this->treeDataKodeAkun();

	}

	private  function treeDataKodeAkun($tipeAkun = '')
      {
        
          $strHeader = '';
          $strJsonData = '[';

          $strJsonData .= $this->recursiveDataKodeAkun(0, $strJsonData);
          $strJsonData = substr($strJsonData, 1, strlen($strJsonData) - 2);
          $strJsonData .= ']';


        return $strJsonData;
      }


      private function recursiveDataKodeAkun($parent=0, $hasil)
      {

          $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 
                                           kode_akun AS KodeAkun, header as Header, 
                                           nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 
                                           saldo_normal as SaldoNormal
                                           FROM mst_akun 
                                           WHERE id_induk='".$parent."' 
                                           ORDER BY kodeUrut ASC ");
        
          foreach( $selectQuery->result_array() as $row )
          {

           $IDAkun         = $row['IDAkun'];
           $IDInduk        = $row['IDInduk'];
           $kodeInduk      = $row['KodeInduk'];
           $header         = $row['Header'];
           $kodePlainText  = $row['KodeAkun'];
           $namaPlainText  = $row['NamaAkun']; 
           $saldoNormal    = $row['SaldoNormal'];
           $level          = $row['Level'];
           $concatAkun 	   = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 
           $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;
           $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;

           $strSpace = '';

           for($i = 1; $i <= $level ; $i++)
           {
            $strSpace .= "&nbsp;";
           }

           $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 
                                            FROM mst_akun 
                                            WHERE id_induk = '".$IDAkun."' ");

           $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 
           
           $CountAkun  = $arrSelectQuery['CountAkun'];

           $imgRoot   = "<span class='glyphicon glyphicon-home'></span>".$strSpace;
           $imgFolder = $strSpace."<span class='glyphicon glyphicon-folder-open'></span>&nbsp;";
           $imgItem   = $strSpace."<span class='glyphicon glyphicon-folder-close'></span>&nbsp;";
           
           $imgChild  = ( $CountAkun > 0 ) ? $imgFolder : $imgItem;

           $kodeWithFormat = ($level == 1) ? $imgRoot.$kodeWithFormat : $kodeWithFormat;
           $kodeWithFormat = ($level > 1) ?  $imgChild.$kodeWithFormat : $kodeWithFormat;

           $btnTambah   = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-success\' onclick=\'TambahAkun(this);\'><span class=\'glyphicon glyphicon-plus-sign\' aria-hidden=\'true\'></button>&nbsp;';  
           $btnUbah     = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-warning\' onclick=\'UbahAkun(this);\'><span class=\'glyphicon glyphicon-pencil\' aria-hidden=\'true\'></button>&nbsp;';
           $btnHapus    = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-danger\' onclick=\'HapusAkun(this);\'><span class=\'glyphicon glyphicon-trash\' aria-hidden=\'true\'></button>&nbsp;';  

           $actionList = $btnTambah.$btnUbah.$btnHapus; 

           $hasil .= '{"idAkun"         : "'.$IDAkun .'", 
                      "kodeWithFormat"  : "'.$kodeWithFormat.'",
                      "namaWithFormat"  : "'.$namaWithFormat.'",
                      "saldoNormal"     : "'.$saldoNormal.'",
                      "concatAkun"      : "'.$concatAkun.'", 
                      "tombol"          : "'.$actionList.'"},';

           $hasil = $this->recursiveDataKodeAkun($row['IDAkun'], $hasil);
          }  //foreach( $selectQuery->result_array() as $row )

          return $hasil;
      } 


      function loadDataAkunCari($KataKunci=''){

          $selectQuery = $this->db->query("SELECT (SELECT CAST(CONCAT(REPLACE(kode_induk,'.',''), kode_akun) AS UNSIGNED) ) AS kodeUrut, id_akun as IDAkun, 
                                           kode_akun AS KodeAkun, header as Header, 
                                           nama_akun AS NamaAkun, id_induk as IDInduk, kode_induk as KodeInduk, level as Level, 
                                           saldo_normal as SaldoNormal
                                           FROM mst_akun 
                                           WHERE CONCAT(kode_induk,'.',kode_akun) LIKE '%".$KataKunci."%' OR nama_akun LIKE '%".$KataKunci."%'
                                           ORDER BY kodeUrut ASC ");

          foreach( $selectQuery->result_array() as $row )
          {

           $IDAkun         = $row['IDAkun'];
           $IDInduk        = $row['IDInduk'];
           $kodeInduk      = $row['KodeInduk'];
           $header         = $row['Header'];
           $kodePlainText  = $row['KodeAkun'];
           $namaPlainText  = $row['NamaAkun']; 
           $saldoNormal    = $row['SaldoNormal'];
           $level          = $row['Level'];
           $concatAkun     = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeInduk      = ($kodeInduk == '0') ? $row['KodeAkun'] : $kodeInduk; 
           $kodePlainText  = ($level == 1) ? $kodePlainText : $kodeInduk.'.'.$kodePlainText;
           $kodeWithFormat = ($header == 1 ) ? "<b>".$kodePlainText."</b>" : $kodePlainText;
           $namaWithFormat = ($header == 1 ) ? "<b>".$namaPlainText."</b>" : $namaPlainText;

           $strSpace = '';

           for($i = 1; $i <= $level ; $i++)
           {
            $strSpace .= "&nbsp;";
           }

           $selectQuery = $this->db->query("SELECT count(id_akun) as CountAkun 
                                            FROM mst_akun 
                                            WHERE id_induk = '".$IDAkun."' ");

           $arrSelectQuery = ( $selectQuery->num_rows() > 0 ) ? $selectQuery->row_array() : array("CountAkun" => 0 ); 
           
           $CountAkun  = $arrSelectQuery['CountAkun'];

           $imgRoot   = "<span class='glyphicon glyphicon-home'></span>".$strSpace;
           $imgFolder = $strSpace."<span class='glyphicon glyphicon-folder-open'></span>&nbsp;";
           $imgItem   = $strSpace."<span class='glyphicon glyphicon-folder-close'></span>&nbsp;";
           
           $imgChild  = ( $CountAkun > 0 ) ? $imgFolder : $imgItem;

           $kodeWithFormat = ($level == 1) ? $imgRoot.$kodeWithFormat : $kodeWithFormat;
           $kodeWithFormat = ($level > 1) ?  $imgChild.$kodeWithFormat : $kodeWithFormat;

           $btnTambah   = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-success\' onclick=\'TambahAkun(this);\'><span class=\'glyphicon glyphicon-plus-sign\' aria-hidden=\'true\'></button>&nbsp;';  
           $btnUbah     = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-warning\' onclick=\'UbahAkun(this);\'><span class=\'glyphicon glyphicon-pencil\' aria-hidden=\'true\'></button>&nbsp;';
           $btnHapus    = '<button buttonType=\'actionItems\' class= \'btn btn-xs btn-danger\' onclick=\'HapusAkun(this);\'><span class=\'glyphicon glyphicon-trash\' aria-hidden=\'true\'></button>&nbsp;';  

           $actionList = $btnTambah.$btnUbah.$btnHapus; 

           $hasil["idAkun"]         = $IDAkun;
           $hasil["kodeWithFormat"] = $kodeWithFormat;
           $hasil["namaWithFormat"] = $namaWithFormat;
           $hasil["saldoNormal"]    = $saldoNormal;
           $hasil["concatAkun"]     = $concatAkun;
           $hasil["tombol"]         = $actionList;

           $json[] = $hasil;

          }

          echo json_encode($json);

      }


      function TambahAkun($ArusKas, $KodeAkun, $NamaAkun, $SaldoNormal, $DeskripsiAkun, $idAkun){

      	$selectQuery = $this->db->query("SELECT kode_induk AS KodeInduk, kode_akun AS KodeAkunDef, level AS Level, header AS Header
      									 FROM mst_akun WHERE id_akun = '".$idAkun."'");

      	$KodeInduk 	= $selectQuery->first_row()->KodeInduk;
      	$KodeAkunDef 	= $selectQuery->first_row()->KodeAkunDef;
      	$Level 		= $selectQuery->first_row()->Level;
      	$Header 	= $selectQuery->first_row()->Header;

      	$ConcatKodeAkun = ($Level == 1) ? $KodeAkunDef : $KodeInduk.'.'.$KodeAkunDef;

      	$levelBaru = $Level + 1;


      	$this->db->trans_begin();

      	if ($Header == 0) {
      		
      		$this->db->query("UPDATE mst_akun SET header = '1' WHERE id_akun = '".$idAkun."'");

      	}

      	$this->db->query("INSERT INTO mst_akun (id_aruskas_kel, kode_akun, nama_akun, id_induk, kode_induk, level, header, saldo_normal, deskripsi, date_entry)
						  VALUES ('".$ArusKas."', '".$KodeAkun."', '".$NamaAkun."', '".$idAkun."', '".$ConcatKodeAkun."', '".$levelBaru."', '0', '".$SaldoNormal."', '".$DeskripsiAkun."', NOW())");

      	$IDAkunBaru = $this->db->insert_id();

      	if ($this->db->trans_status() === FALSE)
          {  
            $this->db->trans_rollback();     

            $strMessage['IDAkun']  = '';

            $strMessage['pesan']  = 'Gagal dalam menyimpan data akun';

            $strMessage['hasil']  = 'gagal';

          }  
          else
          {
             
            $this->db->trans_commit();

            $strMessage['IDAkun']  = $IDAkunBaru;
                  
            $strMessage['pesan']  = 'Akun Berhasil Ditambahkan';

            $strMessage['hasil']  = 'berhasil';
          
          }

          echo json_encode($strMessage);

      }

      function HapusAkun($idAkun){

      	$selectQuery = $this->db->query("SELECT * FROM trx_judet WHERE id_akun = '".$idAkun."'");


      	$this->db->trans_begin();

      	if ($selectQuery->num_rows() > 0) {
      		
      		$strMessage['pesan']  = 'Kode Akun tidak dapat dihapus, ada beberapa transaksi dengan referensi kode akun tersebut';

            $strMessage['hasil']  = 'gagal';

      	}else{

      		$selectQuery2 = $this->db->query("SELECT * FROM mst_akun WHERE id_induk = '".$idAkun."'");

      		if ($selectQuery2->num_rows() > 0) {
      			
      			$strMessage['pesan']  = 'Kode Akun tidak dapat dihapus, ada beberapa kode akun dibawahnya';

            	$strMessage['hasil']  = 'gagal';

      		}else{

      			$query   = $this->db->query("SELECT id_induk AS IDInduk FROM mst_akun WHERE id_akun = '".$idAkun."'");

	      		$IDInduk = $query->first_row()->IDInduk;

	      		
	      		$this->db->query("DELETE FROM mst_akun WHERE id_akun = '".$idAkun."'");


	      		$queryHeader = $this->db->query("SELECT * FROM mst_akun WHERE id_induk = '".$IDInduk."'");

	      		if ($queryHeader->num_rows() == 0) {
	      			
	      			$this->db->query("UPDATE mst_akun SET header = '0' WHERE id_akun = '".$IDInduk."'");

	      		}

            $strMessage['pesan']  = 'Akun Berhasil DiHapus';

            $strMessage['hasil']  = 'berhasil';

      		}

      	}



      	if ($this->db->trans_status() === FALSE)
          {  
            $this->db->trans_rollback();     

            $strMessage['pesan']  = 'Gagal Dalam Penghapusan Data';

            $strMessage['hasil']  = 'gagal';

          }  
          else
          {
             
            $this->db->trans_commit();
          
          }

          echo json_encode($strMessage);


      }

      function AmbilDataAkun($idAkun){

      	$selectQuery = $this->db->query("SELECT * FROM mst_akun WHERE id_akun = '".$idAkun."'");

      	$data['IDAkun'] 	 = $idAkun;
      	$data['NamaAkun'] 	 = $selectQuery->first_row()->nama_akun;
      	$data['KodeAkun'] 	 = $selectQuery->first_row()->kode_akun;
      	$data['KodeInduk'] 	 = $selectQuery->first_row()->kode_induk;
      	$data['IDArusKas'] 	 = $selectQuery->first_row()->id_aruskas_kel;
      	$data['Deskripsi'] 	 = $selectQuery->first_row()->deskripsi;
      	$data['SaldoNormal'] = $selectQuery->first_row()->saldo_normal;

      	$selectQuery2 = $this->db->query("SELECT * FROM mst_akun WHERE id_induk = '".$idAkun."'");

      	$data['strDisable'] = $selectQuery2->num_rows() > 0 ? 'disable' : 'aktif';

      	echo json_encode($data);

      }

      function UbahAkun($ArusKas, $KodeAkun, $NamaAkun, $SaldoNormal, $DeskripsiAkun, $idAkun){

      	
      	$strKodeAkun = $KodeAkun == '' ? '' : " kode_akun = '".$KodeAkun."', ";

      	$this->db->trans_begin();

      	$this->db->query("UPDATE mst_akun SET id_aruskas_kel = '".$ArusKas."', ".$strKodeAkun." nama_akun = '".$NamaAkun."', saldo_normal = '".$SaldoNormal."',
      					  deskripsi = '".$DeskripsiAkun."' WHERE id_akun = '".$idAkun."'");


      	if ($this->db->trans_status() === FALSE)
          {  
            $this->db->trans_rollback();     

            $strMessage['IDAkun']  = '';

            $strMessage['pesan']  = 'Gagal dalam menyimpan data akun';

            $strMessage['hasil']  = 'gagal';

          }  
          else
          {
             
            $this->db->trans_commit();

            $strMessage['IDAkun']  = $idAkun;
                  
            $strMessage['pesan']  = 'Akun Berhasil Diubah';

            $strMessage['hasil']  = 'berhasil';
          
          }

          echo json_encode($strMessage);

      }


}



 ?>