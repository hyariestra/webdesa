<style type="text/css">
	.container1 input[type=text] {
		padding: 5px 0px;
		margin: 5px 5px 5px 0px;
	}

	.delete {
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">


				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>

						<div class="form-group">
							<label>Series</label>

							<input value="<?php echo @$series['series'] ?>" id="kategoris" data-toggle="modal" data-target="#myModal" readonly="" type="text" class="form-control">

							<input value="<?php echo @$series['id_series'] ?>" name="id_series" id="id_kategorinama" type="hidden">
							<input value="<?php echo @$series['id_series_folder'] ?>" id="id_series_folder_selected" type="hidden">

						</div>


						<div class="form-group">
							<label>Series Folder</label>

							<select class="form-control" name="id_series_folder" id="seriesfolder">
								<option value="">::Pilih Series Folder::</option>
							</select>

						</div>


						<div class="form-group">
							<label>Series Detail Judul</label>

							<input name="series_title_detail" value="<?php echo @$series['series_title_detail'] ?>" id="series_title_detail" type="text" class="form-control">



						</div>


						<div class="form-group">

							<label>Deskripsi</label>

							<textarea name="series_content" class="form-control" id="file-manager"><?php echo htmlspecialchars(@$series['series_content']) ?></textarea>

						</div>

						<div class="form-group">

							<label>Gambar Thumbnail</label>

						</div>

						<div class="form-group">
							<table class="table table-bordered table-hover" style="width: 100%">

								<tbody>

									<tr role="row1">

										<td>

											<div class="input-group">

												<input accept="image/*" class="form-control" readonly="" type="file" name="gambar">

												<span class="input-group-btn">

													<a class="repeat-remove btn btn-warning" href="#modal-id"><i class="fa fa-trash" aria-hidden="true"></i></a>

												</span>

											</div>
											<br>
											<img width="200px" src="<?php echo base_url('asset/foto_series_detail/' . @$series['gambar_uniq_detail']) ?>" alt="">
											<p><i>*file yang diizinkan bertipe .png, .jpeg, .jpg dan maksimal 2Mb </i></p>
										</td>

									</tr>
									<tr id="total_row">
									</tr>
								</tbody>

							</table>
						</div>
						<div class="file">

							<div class="form-group">

								<label>File Attachment</label>

							</div>
							<div class="form-group">
								<button type="button" class="repeat-add btn btn-success">Add File</button>
							</div>

							<div class="form-group">
								<table class="table table-bordered table-hover" style="width: 100%">

									<tbody>

										<?php if ($seriesfile) { ?>


											<tr role="row1">

												<?php foreach ($seriesfile as $key => $item) {

												?>
											<tr>

												<td>
													<div class="input-group">

														<input class="form-control" readonly="" type="file" name="files[]">
														<span class='input-group-btn'>
															<a class='repeat-remove btn btn-warning' href='#modal-id'><i class='fa fa-trash' aria-hidden='true'></i></a>
														</span>

													</div>
													<p><i><a  download="<?php echo $item['file_series']; ?>" href="<?php echo base_url('asset/file_series_detail/'.$item['file_series_uniq']); ?>"><?php echo $item['file_series']; ?></a></i></p>
												</td>
											</tr>

										<?php } ?>




										</tr>

									<?php } else { ?>
										<tr role="row1">
											<td>
												<div class="input-group">

													<input class="form-control" readonly="" type="file" name="files[]">
													<span class='input-group-btn'>
														<a class='repeat-remove btn btn-warning' href='#modal-id'><i class='fa fa-trash' aria-hidden='true'></i></a>
													</span>

												</div>

											</td>
										</tr>


									<?php } ?>

									<tr id="total_row_file">

									</tr>

									<tr>

										<p><i>*file yang diizinkan maksimal 2Mb </i></p>
									</tr>

									</tbody>

								</table>
							</div>

						</div>

						<div class="form-group">

							<label>Preview</label>

							<select class="form-control" name="is_preview" id="">
								<option <?php echo (@$series['is_preview'] == '0') ? "selected" : "" ?> value="0">Tidak</option>
								<option <?php echo (@$series['is_preview'] == '1') ? "selected" : "" ?> value="1">Iya</option>
							</select>

						</div>





						<div class="form-group">
							<label>Order</label>

							<input name="order" value="<?php echo @$series['order'] ?>" id="" type="number" class="form-control">

						</div>


					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('seriesdetail') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>

	</div>

</div>


<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Series Master</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-hover" id="">
					<thead>
						<tr>
							<th>Series Master</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($seriesmaster as $key => $v) {
						?>
							<tr>
								<td style="display: none;"><input type="hidden" value="<?php echo $v['id_series'] ?>"></td>
								<td><?php echo $v['series'] ?></td>
								<td>
									<button onclick="chosecategory(this)" type="button" data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-warning">
										<span class="glyphicon glyphicon-plus-sign"></span>
									</button>
								</td>
							</tr>


						<?php } ?>


					</tbody>

				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		load_seriesfolder();
	});


	function chosecategory(obj) {
		var idpelanggan = $(obj).parent().parent().find("td").eq(0).find("input:first").val();
		namapelanggan = $(obj).parent().parent().find("td").eq(1).text();

		$("#kategoris").val(namapelanggan);
		$("#id_kategorinama").val(idpelanggan);

		load_seriesfolder();
		$("#myModal").modal("remove");


	}


	$('.form-kirim').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(formData, jqForm, options) {



		},
		success: processJson,
		error: processJsonError
	});


	function processJsonError(result) {
		result = result.responseJSON;
		processJson(result, true);
	}

	function processJson(result) {


		new Noty({
			text: result.message,
			type: result.status_code,
			timeout: 3000,
			theme: 'semanticui'
		}).show();

		if (result.status == 201) {
			window.location = '<?php echo base_url('seriesdetail') ?>';

		}
	}

	function load_seriesfolder() {


		var id = $('#id_kategorinama').val();

		var idSeries = $('#id_series_folder_selected').val();

		$('#seriesfolder').html("");
		$('#seriesfolder').append('<option value="">--Pilih Series Folder--</option>');

		$.ajax({
			dataType: "json",
			url: "<?php echo base_url('seriesdetail/get_seriesfolder') ?>",
			data: {
				id: id
			},
			success: function(result) {

				var selected = '';

				$.each(result.kecamatan, function(key, val) {

					if (idSeries == val.id) {
						selected = 'selected';
					}

					$('#seriesfolder').append("<option " + selected + " value=" + val.id + ">" + val.nama + "</option>");
				});
				$('#seriesfolder').trigger('change');
			}
		});
	}

	var lanjut = 0;

	$(".repeat-add").click(function(e) {

		e.preventDefault();


		lanjut = lanjut + 1;

		var masukan = "<tr role='row" + lanjut + "'>";

		masukan += "<td>";


		masukan += "<div class='input-group'>";

		masukan += "<input class='form-control' readonly='' type='file' name='files[]'>";


		masukan += "<span class='input-group-btn'>";

		masukan += "<a class='repeat-remove btn btn-warning'  href='#modal-id'><i class='fa fa-trash' aria-hidden='true'></i></a>";

		masukan += "</span>";

		masukan += "</div>";

		masukan += "</td>";
		masukan += "</tr>";

		$('#total_row_file').before(masukan);


	});

	$(document).on('click', '.repeat-remove', function(e) {

		e.preventDefault();

		$(this).closest('tr').remove();



	});
</script>