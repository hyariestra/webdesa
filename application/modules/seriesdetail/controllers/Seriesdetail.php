<?php

class Seriesdetail extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna")) {



			redirect("pengguna/login");
		}
	}


	function index()

	{

		authorize('seriesdetail');

		$this->themeadmin->tampilkan('tampilSeries', null);
	}

	function get_data_series()
	{
		$list = $this->M_seriesdetail->get_datatables();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->is_preview == '1') {
				$preview = "<span class='label label-success'>Y</span>";
			} else {
				$preview = "<span class='label label-default'>N</span>";
			}


			$idBerita = encrypt_url($field->id_series_detail);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->series;
			$row[] = $field->series_folder_name;
			$row[] = $field->series_title_detail;
			$row[] = $preview;
			$row[] = $field->order;
			$html = "";

			if (authorizeView('ubahseriesdetail')) {
				$html .= "<a class='btn btn-success btn-xs' title='Edit Data' href=" . base_url('seriesdetail/ubah/' . encrypt_url($field->id_series_detail)) . " >
			<span class='glyphicon glyphicon-edit'></span></a>";
			}

			if (authorizeView('hapusseriesdetail')) {
				$html .= "<a onclick=delete_func('$idBerita')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";
			}
			$row[] = $html;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_seriesdetail->count_all(),
			"recordsFiltered" => $this->M_seriesdetail->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		authorize('tambahseriesdetail');

		$data['url'] = 'seriesdetail/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate();
		$data['seriesmaster'] = $this->M_seriesmaster->get_series_master_all();
		$data['seriesfile'] = array();
		$this->themeadmin->tampilkan('tambahSeries', $data);
	}


	public function ubah($id = '')
	{

		authorize('ubahseriesdetail');
		$idEn = decrypt_url($id);
		$data['url'] = 'seriesdetail/edit/' . $id;
		$data['series'] = $this->M_seriesdetail->get_detail_series($idEn);
		$data['seriesmaster'] = $this->M_seriesmaster->get_series_master_all();
		$data['seriesfile'] = $this->M_seriesdetail->get_series_file($idEn);


		// echo '<pre>'; 
		// print_r($data['seriesfile']);
		// echo '<pre>'; 
		// die();

		$data['breadcrumb'] = $this->breadcrumbcomponent->generate();
		$this->themeadmin->tampilkan('tambahSeries', $data);
	}


	public function simpan()
	{



		$post = $this->input->post();
		$result = true;


	
	
		if ($post['id_series_folder'] == "") {

			$err_msg[] = 'id series folder kosong';
			$result = false;
		}

		if ($post['series_title_detail'] == "") {

			$err_msg[] = 'series title kosong';
			$result = false;
		}

		if ($post['order'] == "") {

			$err_msg[] = 'order kosong';
			$result = false;
		}


		$namaSementara = "";
		$namaFile = "";
		$dirUpload = "";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '.' . $file_extension;
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_series_detail/";
			$valid_ext = array('png', 'jpeg', 'jpg', 'gif');


			if ($_FILES["gambar"]["size"] > 2097152 or $_FILES["gambar"]["size"] == 0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if (!in_array($file_extension, $valid_ext)) {
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;
			}
		}

	
		
		$files = isset($_FILES['files']) ? $_FILES['files'] : '';

	

		//$gambarArr = $this->M_produkbe->reArrayFiles($_FILES['files']);

		
		if ($result) {

			$param = array(
				'id_series_folder' => $post['id_series_folder'],
				'series_title_detail' => $post['series_title_detail'],
				'series_title_detail_slug' => slugify($post['series_title_detail']),
				'series_content' => $post['series_content'],
				'gambar_uniq' => isset($namaFile) ? $namaFile : '',
				'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'created_by' => getSession(),
				'is_preview' => $post['is_preview'],
				'order' => $post['order'],
				'insert_time' => date("Y-m-d H:i:s"),
			);

			move_uploaded_file($namaSementara, $dirUpload . $namaFile);

			$result = $this->M_seriesdetail->simpan_seriesdetail($param,$files);
		}




		if ($result['result']) {

			echo getResponse(201, 'Data Berhasil Tersimpan');
		} else {

			$err_msg[] =  implode("<hr>", $result['pesan']) ;

		

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result = true;


		$id = decrypt_url($id);


		if ($post['id_series_folder'] == "") {

			$err_msg[] = 'id series folder kosong';
			$result = false;
		}

		if ($post['series_title_detail'] == "") {

			$err_msg[] = 'series title kosong';
			$result = false;
		}

		if ($post['order'] == "") {

			$err_msg[] = 'order kosong';
			$result = false;
		}


		$namaSementara = "";
		$namaFile = "";
		$dirUpload = "";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '.' . $file_extension;
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_series_detail/";
			$valid_ext = array('png', 'jpeg', 'jpg', 'gif');


			if ($_FILES["gambar"]["size"] > 2097152 or $_FILES["gambar"]["size"] == 0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if (!in_array($file_extension, $valid_ext)) {
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;
			}
		}

		if ($result) {


			if (!isset($_FILES['gambar'])) {

				$param = array(
					'id_series_folder' => $post['id_series_folder'],
					'series_title_detail' => $post['series_title_detail'],
					'series_title_detail_slug' => slugify($post['series_title_detail']),
					'series_content' => $post['series_content'],
					'updated_by' => getSession(),
					'is_preview' => $post['is_preview'],
					'order' => $post['order'],
					'update_time' => date("Y-m-d H:i:s"),
				);
			} else {
				$param = array(
					'id_series_folder' => $post['id_series_folder'],
					'series_title_detail' => $post['series_title_detail'],
					'series_title_detail_slug' => slugify($post['series_title_detail']),
					'series_content' => $post['series_content'],
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'updated_by' => getSession(),
					'is_preview' => $post['is_preview'],
					'order' => $post['order'],
					'update_time' => date("Y-m-d H:i:s"),
				);
			}
			move_uploaded_file($namaSementara, $dirUpload . $namaFile);

			$result = $this->M_seriesdetail->ubah_seriesdetail($param, $id);
		}


		if ($result) {

			echo getResponse(201, 'Data Berhasil Diubah');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);
	}

	function delete($id)
	{

		authorize('hapusseriesdetail');
		$idEn = decrypt_url($id);

		$data = $this->M_seriesdetail->get_detail_series($idEn);

		$dirUpload = "asset/foto_series/";

		$files = $dirUpload . $data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {

			if (file_exists($files)) {
				unlink($files);
			}
		}


		$result = $this->M_seriesdetail->delete_series($idEn);


		if ($result) {

			echo getResponse(201, 'Data Berhasil Dihapus');
		} else {

			echo getResponse(400, 'Data gagal Dihapus');
		}
	}

	public function get_seriesfolder()
	{

		$id = $_GET['id'];

		$data['kecamatan'] = $this->M_seriesdetail->get_seriesfolder($id);

		echo json_encode($data);
		exit;
	}
}
