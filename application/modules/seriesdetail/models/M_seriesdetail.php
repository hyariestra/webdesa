<?php

class M_seriesdetail extends CI_model

{

    var $table = 'series'; //nama tabel dari database
    var $column_order = array(1 => 'c.series', 2 => 'b.series_folder_name', 3 => 'a.series_title_detail'); //field yang ada di table user
    var $column_search = array('a.series_title_detail', 'b.series_folder_name'); //field yang diizin untuk pencarian 
    var $order = array('a.id_series_detail' => 'desc'); // default order 


    function get_series_master_all()
    {
        return $this->db->query("SELECT * FROM series")->result_array();
    }

    function get_series_file($id)
    {
        $this->db->select('*');
        $this->db->from('series_file a');
        $data = $this->db->where('id_series_detail', $id)->get();

        return $data->result_array();
    }


    function trimelement($data, $panjang)
    {

        $max_length = $panjang;

        if (strlen($data) > $max_length) {
            $offset = ($max_length - 3) - strlen($data);
            $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
        }

        return $data;
    }



    function simpan_seriesdetail($param, $gambarArr)
    {

        $this->db->trans_begin();


        $result = $this->db->insert('series_detail', $param);
        $insert_id = $this->db->insert_id();

        if ($result) {

           $result =  $this->simpan_series_file($gambarArr, $insert_id);
            
        }

        if ($result == false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return [
            'result' => $result['result'],
            'pesan' => $result['pesan'],
        ];
    }

    function simpan_series_file($gambarArr, $insert_id){

        $pesan = array();
        $result  = true;

        if ($gambarArr) {


                $gambar = $this->M_produkbe->reArrayFiles($gambarArr);

                $i = 0;
                foreach ($gambar as $key => $value) {

                    $file_extension = pathinfo($value['name'], PATHINFO_EXTENSION);
                    $file_extension = strtolower($file_extension);


                    $namaFile = $value['name'];
                    $namaFileUniq = rand() . round(microtime(true)) . '.' . $file_extension;
                    $namaSementara = $value['tmp_name'];
                    $dirUpload = "asset/file_series_detail/";


                    if ($value['size'] > 2097152 or $value['size'] == 0) {
                        $pesan[] = 'Ukuran file melebihi 2mb';
                        $result = false;
                    }

                    if ($result) {

                        move_uploaded_file($namaSementara, $dirUpload . $namaFileUniq);
                        $param_gambar = array(
                            'id_series_detail' => $insert_id,
                            'file_series' => $namaFile,
                            'file_series_uniq' => $namaFileUniq
                        );
                        $this->db->insert('series_file', $param_gambar);
                        $i++;
                    }
                }
            
        }

        return [
            'result' => $result,
            'pesan' => $pesan,
        ];
    }

    function delete_series($id)
    {
        return $this->db->query("DELETE FROM series where id_series = " . $id . " ");
    }

    function ubah_seriesdetail($param, $id)
    {

        $this->db->trans_begin();




        $this->db->where('id_series_detail', $id);
        $result = $this->db->update('series_detail', $param);



        if ($result == false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return [
            'result' => $result
        ];
    }





    function reArrayFiles(&$file_post)
    {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }


    public function raw_db()
    {
        $this->db->select('*');
        $this->db->from('series_detail a');
        $data =  $this->db->join('series_folder b', 'b.id_series_folder = a.id_series_folder', 'left');
        $data =  $this->db->join('series c', 'c.id_series = b.id_series', 'left');
        $data =  $this->db->join('admin d', 'd.id_admin = a.created_by', 'left');

        return $data;
    }


    private function _get_datatables_query()
    {


        $this->raw_db();



        $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if ($i === 0) // looping awal
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();


        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {

        $this->raw_db();
        $r = $this->db->count_all_results();

        return $r;
    }

    public function getTagName($value)
    {
        $data =  $this->db->query("SELECT nama_tag FROM tags WHERE id_tag = " . $value . " ")->row_array();

        return $data['nama_tag'];
    }


    public function get_detail_series($id = '')
    {
        $data = $this->db->query("SELECT *, a.`gambar_uniq` AS gambar_uniq_detail  FROM `series_detail` `a` 
        LEFT JOIN `series_folder` `b` ON `b`.`id_series_folder` = `a`.`id_series_folder`
        LEFT JOIN `series` `c` ON `c`.`id_series` = `b`.`id_series` 
        WHERE  a.`id_series_detail` = " . $id . " ")->row_array();
        return $data;
    }

    function get_seriesfolder($id)
    {

        $this->db->select('id_series_folder AS id ,series_folder_name As nama');
        $this->db->from('series_folder a');
        $this->db->where('id_series', $id);
        $query = $this->db->get()->result_array();
        return $query;
    }
}
