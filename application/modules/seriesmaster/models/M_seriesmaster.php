<?php

class M_seriesmaster extends CI_model

{

    var $table = 'series'; //nama tabel dari database
    var $column_order = array(1 => 'a.series', 6 => 'a.insert_time'); //field yang ada di table user
    var $column_search = array('series'); //field yang diizin untuk pencarian 
    var $order = array('a.id_series' => 'desc'); // default order 


    function get_series_master_all()
    {
        return $this->db->query("SELECT * FROM series")->result_array();
    }


    function trimelement($data, $panjang)
    {

        $max_length = $panjang;

        if (strlen($data) > $max_length) {
            $offset = ($max_length - 3) - strlen($data);
            $data = substr($data, 0, strrpos($data, ' ', $offset)) . '...';
        }

        return $data;
    }



    function simpan_seriesmaster($param, $gambarArr, $desc)
    {

        $this->db->trans_begin();

        $result = $this->db->insert('series', $param);


        $insert_id = $this->db->insert_id();

        if ($result) {
            $i = 0;
            foreach ($gambarArr as $key => $value) {
                
				$file_extension = pathinfo($value['name'], PATHINFO_EXTENSION);
				$file_extension = strtolower($file_extension);

			
				$namaFile = $value['name'];
				$namaFileUniq = rand().round(microtime(true)).'.'.$file_extension;
				$namaSementara = $value['tmp_name'];
				$dirUpload = "asset/foto_screenshots/";

	

                if ($result) {

                    move_uploaded_file($namaSementara, $dirUpload.$namaFileUniq);
					$param_gambar = array(
						'id_series'=> $insert_id,
						'gambar_series'=> $namaFile,
						'gambar_series_uniq'=> $namaFileUniq,
						'desc_series'=>  isset($desc[$i]) ? $desc[$i] : null,

					);
					$this->db->insert('series_gambar',$param_gambar);
                    $i++;
                }


				
			}
          
        }

        if ($result == false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return [
            'result' => $result
        ];
    }

    function delete_series($id)
    {
        return $this->db->query("DELETE FROM series where id_series = " . $id . " ");
    }

    function ubah_seriesmaster($param, $id)
    {

        $this->db->trans_begin();




        $this->db->where('id_series', $id);
        $result = $this->db->update('series', $param);



        if ($result == false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return [
            'result' => $result
        ];
    }





    function reArrayFiles(&$file_post)
    {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }


    public function raw_db()
    {
        $this->db->select('*');
        $this->db->from('series a');
        $data =  $this->db->join('admin b', 'b.id_admin = a.created_by', 'left');

        return $data;
    }

    public function getDataLabel()
    {
        $this->db->select('*');
        $data = $this->db->from('ref_label a')->get()->result_array();

        return $data;
    }


    private function _get_datatables_query()
    {


        $this->raw_db();



        $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if ($i === 0) // looping awal
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();


        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {

        $this->raw_db();
        $r = $this->db->count_all_results();

        return $r;
    }

    public function getTagName($value)
    {
        $data =  $this->db->query("SELECT nama_tag FROM tags WHERE id_tag = " . $value . " ")->row_array();

        return $data['nama_tag'];
    }


    public function get_detail_series($id = '')
    {
        $data = $this->db->query("SELECT * FROM series a
        LEFT JOIN ref_label b on b.id_label = a.label_id
        WHERE a.id_series = " . $id . " ")->row_array();
        return $data;
    }

    public function getLabelByCode($kode){
        $this->db->select('*');
        $this->db->from('ref_label a');
        $data = $this->db->where('kode_label', $kode)->get()->row_array();
    
        return $data;
    
    } 


}
