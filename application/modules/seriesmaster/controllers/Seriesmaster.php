<?php

class Seriesmaster extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna")) {



			redirect("pengguna/login");
		}
	}


	function index()

	{

		authorize('seriesmaster');

		$this->themeadmin->tampilkan('tampilSeries', null);
	}

	function get_data_series()
	{
		$list = $this->M_seriesmaster->get_datatables();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			if ($field->is_headline == '1') {
				$headline = "<span class='label label-success'>Y</span>";
			} else {
				$headline = "<span class='label label-default'>N</span>";
			}

			if ($field->is_paid == '1') {
				$paid = "<span class='label label-success'>Y</span>";
			} else {
				$paid = "<span class='label label-default'>N</span>";
			}

			if ($field->is_publish == '1') {
				$publish = "<span class='label label-success'>Y</span>";
			} else {
				$publish = "<span class='label label-default'>N</span>";
			}

			$r = array();

			if (!empty($field->tags)) {


				$tag = explode(',', $field->tags);


				foreach ($tag as $key => $value) {

					$r[$key] = $this->M_seriesmaster->getTagName($value);
				}
			}


			$idBerita = encrypt_url($field->id_series);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->series;
			$row[] = $publish;
			$row[] = $paid;
			$row[] = $headline;
			$row[] = $field->nama;
			$row[] = tgl_indo_timestamp($field->insert_time);
			$row[] = array_unique($r);
			$html = "";

			if (authorizeView('ubahseriesmaster')) {
				$html .= "<a class='btn btn-success btn-xs' title='Edit Data' href=" . base_url('seriesmaster/ubah/' . encrypt_url($field->id_series)) . " >
			<span class='glyphicon glyphicon-edit'></span></a>";
			}

			if (authorizeView('hapusseriesmaster')) {
				$html .= "<a onclick=delete_func('$idBerita')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";
			}
			$row[] = $html;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_seriesmaster->count_all(),
			"recordsFiltered" => $this->M_seriesmaster->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		authorize('tambahseriesmaster');

		$data['url'] = 'seriesmaster/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate();
		$data['label'] = $this->M_seriesmaster->getDataLabel();
		$data['tag'] = $this->M_tag->getDataTag();

		$this->themeadmin->tampilkan('tambahSeries', $data);
	}


	public function ubah($id = '')
	{

		authorize('ubahseriesmaster');
		$idEn = decrypt_url($id);
		$data['url'] = 'seriesmaster/edit/' . $id;
		$data['series'] = $this->M_seriesmaster->get_detail_series($idEn);
		$data['tag'] = $this->M_tag->getDataTag();
		$data['label'] = $this->M_seriesmaster->getDataLabel();

		$data['breadcrumb'] = $this->breadcrumbcomponent->generate();
		$this->themeadmin->tampilkan('tambahSeries', $data);
	}


	public function simpan()
	{



		$post = $this->input->post();
		$result = true;
		
		if ($post['series'] == "") {
			
			$err_msg[] = 'Series kosong';
			$result = false;
		}


		if (isset($post['tag'])) {
			$tag = implode(',', $post['tag']);
		}

		$gambarArr = $this->M_produkbe->reArrayFiles($_FILES['gambar_series']);
		


		$namaSementara = "";
		$namaFile = "";
		$dirUpload = "";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '.' . $file_extension;
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_series/";
			$valid_ext = array('png', 'jpeg', 'jpg', 'gif');


			if ($_FILES["gambar"]["size"] > 2097152 or $_FILES["gambar"]["size"] == 0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if (!in_array($file_extension, $valid_ext)) {
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;
			}
		}



		foreach ($gambarArr as $key => $value) {
			
			$valid_ext_series = array('png', 'jpeg', 'jpg', 'gif');
			$file_extension_series = pathinfo($value['name'], PATHINFO_EXTENSION);
			$file_extension_series = strtolower($file_extension_series);

			if(!in_array($file_extension_series,$valid_ext_series)){
	
				$err_msg[] = 'Ekstensi file screenshot tidak sesuai';
				$result = false;	 
			
			}

			if ($value['size'] > 2097152 OR $value['size']==0) {
				$err_msg[] = 'Ukuran file screenshot melebihi 2mb';
				$result = false;
			}
		}


		if ($result) {


			$label = $this->M_seriesmaster->getLabelByCode($post['label_id']);

			$param = array(
				'series' => $post['series'],
				'series_seo' => slugify($post['series']),
				'id_instansi' => getSettingDesa()['id_desa'],
				'is_headline' => $post['is_headline'],
				'is_publish' => $post['is_publish'],
				'is_paid' => $post['is_paid'],
				'label_id' => $label['id_label'],
				'series_price' => $post['series_price'],
				'description' => $post['description'],
				'gambar_uniq' => isset($namaFile) ? $namaFile : '',
				'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
				'created_by' => getSession(),
				'insert_time' => date("Y-m-d H:i:s"),
				'tags' => isset($tag) ? $tag : '',
			);

			move_uploaded_file($namaSementara, $dirUpload . $namaFile);

			$result = $this->M_seriesmaster->simpan_seriesmaster($param,$gambarArr,$post['desc_series']);


		}


		if ($result) {

			echo getResponse(201, 'Data Berhasil Tersimpan');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result = true;


		$id = decrypt_url($id);


		if ($post['series'] == "") {

			$err_msg[] = 'series kosong';
			$result = false;
		}


		if (isset($post['tag'])) {
			$tag = implode(',', $post['tag']);
		}


		$namaSementara = "";
		$namaFile = "";
		$dirUpload = "";

		if (isset($_FILES['gambar'])) {

			$file_extension = pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
			$file_extension = strtolower($file_extension);

			$namaFile = round(microtime(true)) . '.' . $file_extension;
			$namaSementara = $_FILES['gambar']['tmp_name'];

			$dirUpload = "asset/foto_series/";
			$valid_ext = array('png', 'jpeg', 'jpg', 'gif');


			if ($_FILES["gambar"]["size"] > 2097152 or $_FILES["gambar"]["size"] == 0) {
				$err_msg[] = 'Ukuran file melebihi 2mb';
				$result = false;
			}

			if (!in_array($file_extension, $valid_ext)) {
				$err_msg[] = 'Ekstensi file tidak sesuai';
				$result = false;
			}
		}

		if ($result) {

			$label = $this->M_seriesmaster->getLabelByCode($post['label_id']);


			if (!isset($_FILES['gambar'])) {

				$param = array(
					'series' => $post['series'],
					'series_seo' => slugify($post['series']),
					'id_instansi' => getSettingDesa()['id_desa'],
					'is_headline' => $post['is_headline'],
					'is_publish' => $post['is_publish'],
					'is_paid' => $post['is_paid'],
					'series_price' => $post['series_price'],
					'description' => $post['description'],
					'label_id' => $label['id_label'],
					'updated_by' => getSession(),
					'update_time' => date("Y-m-d H:i:s"),
					'tags' => isset($tag) ? $tag : '',
				);
			} else {
				$param = array(
					'series' => $post['series'],
					'series_seo' => slugify($post['series']),
					'id_instansi' => getSettingDesa()['id_desa'],
					'is_headline' => $post['is_headline'],
					'is_publish' => $post['is_publish'],
					'is_paid' => $post['is_paid'],
					'series_price' => $post['series_price'],
					'description' => $post['description'],
					'label_id' => $label['id_label'],
					'gambar_uniq' => isset($namaFile) ? $namaFile : '',
					'gambar' => isset($_FILES["gambar"]["name"]) ? $_FILES["gambar"]["name"] : '',
					'updated_by' => getSession(),
					'update_time' => date("Y-m-d H:i:s"),
					'tags' => isset($tag) ? $tag : '',
				);
			}
			move_uploaded_file($namaSementara, $dirUpload . $namaFile);

			$result = $this->M_seriesmaster->ubah_seriesmaster($param, $id);
		}


		if ($result) {

			echo getResponse(201, 'Data Berhasil Diubah');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);
	}

	function delete($id)
	{

		authorize('hapusseriesmaster');
		$idEn = decrypt_url($id);

		$data = $this->M_seriesmaster->get_detail_series($idEn);

		$dirUpload = "asset/foto_series/";

		$files = $dirUpload . $data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {

			if (file_exists($files)) {
				unlink($files);
			}
		}


		$result = $this->M_seriesmaster->delete_series($idEn);


		if ($result) {

			echo getResponse(201, 'Data Berhasil Dihapus');
		} else {

			echo getResponse(400, 'Data gagal Dihapus');
		}
	}
}
