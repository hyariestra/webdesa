<style type="text/css">
	.container1 input[type=text] {
		padding: 5px 0px;
		margin: 5px 5px 5px 0px;
	}

	.delete {
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">


				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>


						<div class="form-group">

							<label>Series</label>

							<input value="<?php echo @$series['series'] ?>" type="text" name="series" class="form-control">

						</div>


						<div class="form-group">

							<label>Deskripsi</label>

							<textarea name="description" class="form-control" id="file-manager"><?php echo @$series['description'] ?></textarea>

						</div>



						<div class="form-group">

							<label>Tags</label>

							<span style='display:block;'>

								<?php

								$_arrTag = explode(',', @$series['tags']);

								foreach ($tag as $key => $value) {

									$_ck = (array_search($value['id_tag'], $_arrTag) === false) ? '' : 'checked';

								?>

									<input <?php echo $_ck ?> type=checkbox value='<?php echo $value['id_tag'] ?>' name=tag[]> <?php echo $value['nama_tag'] ?> &nbsp; &nbsp; &nbsp;

								<?php } ?>

							</span>

						</div>


						<div class="form-group">

							<label>Gambar Thumbnail</label>

						</div>



						<div class="form-group">
							<table class="table table-bordered table-hover" style="width: 100%">

								<tbody>

									<tr role="row1">

										<td>

											<div class="input-group">

												<input accept="image/*" class="form-control" readonly="" type="file" name="gambar">

												<span class="input-group-btn">

													<a class="repeat-remove btn btn-warning" href="#modal-id"><i class="fa fa-trash" aria-hidden="true"></i></a>

												</span>

											</div>
											<br>
											<img width="200px" src="<?php echo base_url('asset/foto_series/' . @$series['gambar_uniq']) ?>" alt="">
											<p><i>*file yang diizinkan bertipe .png, .jpeg, .jpg dan maksimal 2Mb </i></p>
										</td>

									</tr>

								</tbody>

							</table>
						</div>

						<div class="form-group">

							<label>Headline</label>

							<select class="form-control" name="is_headline" id="">
								<option <?php echo (@$series['is_headline'] == '0') ? "selected" : "" ?> value="0">Tidak</option>
								<option <?php echo (@$series['is_headline'] == '1') ? "selected" : "" ?> value="1">Iya</option>
							</select>

						</div>

						<div class="form-group">

							<label>Publish</label>

							<select class="form-control" name="is_publish" id="">
								<option <?php echo (@$series['is_publish'] == '0') ? "selected" : "" ?> value="0">Tidak</option>
								<option <?php echo (@$series['is_publish'] == '1') ? "selected" : "" ?> value="1">Iya</option>
							</select>

						</div>



						<div class="form-group">

							<label>Berbayar</label>

							<select class="form-control" name="is_paid" id="">
								<option <?php echo (@$series['is_paid'] == '0') ? "selected" : "" ?> value="0">Tidak</option>
								<option <?php echo (@$series['is_paid'] == '1') ? "selected" : "" ?> value="1">Iya</option>
							</select>

						</div>


						<div class="form-group">

							<label>Label</label>

							<select class="form-control" name="label_id" id="">

								<?php

								foreach ($label as $key => $value) {

									$selected = '';

									if ($series['label_id'] == $value['id_label']) {
										$selected = 'selected';
									}

								?>


									<option <?php echo $selected; ?> value="<?php echo $value['kode_label']; ?>"><?php echo $value['nama_label']; ?></option>

								<?php } ?>




							</select>

						</div>

						<div class="form-group">

							<label>Harga</label>

							<input value="<?php echo @$series['series_price'] ?>" type="number" min=0 name="series_price" class="form-control">

						</div>

					</div>

					<div class="form-group">
						<button type="button" class="repeat-add btn btn-success">Tambah Screenshots</button>
					</div>

					<div class="form-group">
						<table class="table table-bordered table-hover" style="width: 100%">

							<tbody>

								<tr role="row1">

									<td>

										<div class="input-group">

											<span class="input-group-addon">
												<i class="fa fa-picture-o" aria-hidden="true"></i>
											</span>
											<input accept="image/*" class="form-control" readonly="" type="file" name="gambar_series[]">
											<input class="form-control" name="desc_series[0]" placeholder="desc gambar" type="text">
										</div>

									</td>


								</tr>
								<tr id="total_row">

								</tr>

							</tbody>

						</table>
					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('seriesmaster') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>

	</div>

</div>

<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Kategori</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-hover" id="">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Kategori</th>
							<th>Pilih</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;

						foreach ($kategori as $key => $value) { ?>
							<tr>
								<th><?php echo $no ?></th>
								<th><?php echo $value['name_head_kategori']; ?></th>
								<th>

								</th>
							</tr>

							<?php foreach ($value['sub_kategory'] as $key => $v) {
							?>
								<tr>
									<td><input type="hidden" value="<?php echo $v['id_sub_kategori'] ?>"></td>
									<td><?php echo $v['name_sub_kategori'] ?></td>
									<td>
										<button onclick="chosecategory(this)" type="button" data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-warning">
											<span class="glyphicon glyphicon-plus-sign"></span>
										</button>
									</td>
								</tr>


							<?php } ?>



						<?php $no++;
						} ?>
					</tbody>

				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<script>
	function chosecategory(obj) {
		var idpelanggan = $(obj).parent().parent().find("td").eq(0).find("input:first").val();
		console.log(idpelanggan);
		namapelanggan = $(obj).parent().parent().find("td").eq(1).text();

		$("#kategoris").val(namapelanggan);
		$("#id_kategorinama").val(idpelanggan);

		$("#myModal").modal("remove");
	}



	$("#diskon").keyup(function() {

		var dInput = this.value;
		var harga_produk = $('#harga_produk').val();
		var harga_diskon = '';

		var diskon = (dInput * harga_produk) / 100;

		if (dInput != '') {
			var harga_diskon = harga_produk - diskon;

		}

		$('#harga_diskon').val(harga_diskon);

	});

	$(document).on('click', '.repeat-remove', function(e) {

		e.preventDefault();

		$(this).closest('tr').remove();



	});

	var lanjut = 0;

	$(".repeat-add").click(function(e) {

		e.preventDefault();


		lanjut = lanjut + 1;

		var masukan = "<tr role='row" + lanjut + "'>";

		masukan += "<td>";


		masukan += "<div class='input-group'>";

		masukan += "<span class='input-group-addon'>";
		masukan += "<i class='fa fa-picture-o' aria-hidden='true'></i>";
		masukan += "</span>";

		masukan += "<input  accept='image/*' class='form-control' readonly='' type='file' name='gambar_series[]'>";
		masukan += "<input class='form-control' name='desc_series[" + lanjut + "]' placeholder='desc gambar' type='text'>";


		masukan += "<span class='input-group-addon'>";
		masukan += "<a class='repeat-remove btn btn-warning'  href='#modal-id'><i class='fa fa-trash' aria-hidden='true'></i></a>";
		masukan += "</span>";

		masukan += "</div>";

		masukan += "</td>";
		masukan += "</tr>";

		$('#total_row').before(masukan);





	});




	$('.form-kirim').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(formData, jqForm, options) {



		},
		success: processJson,
		error: processJsonError
	});


	function processJsonError(result) {
		result = result.responseJSON;
		processJson(result, true);
	}

	function processJson(result) {

		console.log(result);

		new Noty({
			text: result.message,
			type: result.status_code,
			timeout: 3000,
			theme: 'semanticui'
		}).show();

		if (result.status == 201) {
			window.location = '<?php echo base_url('seriesmaster') ?>';

		}
	}

	// $( ".nama_produk_class" ).keyup(function() {

	// 	var nama = this.value;

	// 	var str = this.value;
	// 	str = str.replace(/\s+/g, '-').toLowerCase();


	// 	$('.slug_class').val(str);


	// });
</script>