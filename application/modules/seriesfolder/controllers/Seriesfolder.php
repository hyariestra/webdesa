<?php

class Seriesfolder extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna")) {



			redirect("pengguna/login");
		}
	}


	function index()

	{

		authorize('seriesfolder');

		$this->themeadmin->tampilkan('tampilSeries', null);
	}

	function get_data_series()
	{
		$list = $this->M_seriesfolder->get_datatables();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

	

			$idBerita = encrypt_url($field->id_series_folder);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->series_folder_name;
			$row[] = $field->series;
			$html = "";

			if (authorizeView('ubahseriesfolder')) {
				$html .= "<a class='btn btn-success btn-xs' title='Edit Data' href=" . base_url('seriesfolder/ubah/' . encrypt_url($field->id_series_folder)) . " >
			<span class='glyphicon glyphicon-edit'></span></a>";
			}

			if (authorizeView('hapusseriesfolder')) {
				$html .= "<a onclick=delete_func('$idBerita')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";
			}
			$row[] = $html;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_seriesfolder->count_all(),
			"recordsFiltered" => $this->M_seriesfolder->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		authorize('tambahseriesfolder');

		$data['url'] = 'seriesfolder/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate();
		$data['tag'] = $this->M_tag->getDataTag();
		$data['seriesmaster'] = $this->M_seriesmaster->get_series_master_all();

	

		$this->themeadmin->tampilkan('tambahSeries', $data);
	}


	public function ubah($id = '')
	{

		authorize('ubahseriesfolder');
		$idEn = decrypt_url($id);
		$data['url'] = 'seriesfolder/edit/' . $id;
		$data['series'] = $this->M_seriesfolder->get_detail_series($idEn);
		$data['seriesmaster'] = $this->M_seriesmaster->get_series_master_all();

		$data['breadcrumb'] = $this->breadcrumbcomponent->generate();
		$this->themeadmin->tampilkan('tambahSeries', $data);
	}


	public function simpan()
	{



		$post = $this->input->post();
		$result = true;

		if ($post['series'] == "") {

			$err_msg[] = 'nama series kosong';
			$result = false;
		}

		
		if ($post['id_series'] == "") {

			$err_msg[] = 'series master kosong';
			$result = false;
		}

		if ($post['order_folder'] == "") {

			$err_msg[] = 'oder folder kosong';
			$result = false;
		}


		if ($result) {

			$param = array(
				'series_folder_name' => $post['series'],
				'series_folder_name_slug' => slugify($post['series']),
				'id_series' => $post['id_series'],
				'order_folder' => $post['order_folder'],
				'created_by' => getSession(),
				'insert_time' => date("Y-m-d H:i:s"),
			);

	
			$result = $this->M_seriesfolder->simpan_seriesfolder($param);
		}


		if ($result) {

			echo getResponse(201, 'Data Berhasil Tersimpan');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result = true;


		$id = decrypt_url($id);


		if ($post['series'] == "") {

			$err_msg[] = 'nama series kosong';
			$result = false;
		}

		
		if ($post['id_series'] == "") {

			$err_msg[] = 'series master kosong';
			$result = false;
		}
		if ($post['order_folder'] == "") {

			$err_msg[] = 'oder folder kosong';
			$result = false;
		}




		if ($result) {


			$param = array(
				'series_folder_name' => $post['series'],
				'series_folder_name_slug' => slugify($post['series']),
				'id_series' => $post['id_series'],
				'order_folder' => $post['order_folder'],
				'updated_by' => getSession(),
				'update_time' => date("Y-m-d H:i:s"),
			);
			
		

			$result = $this->M_seriesfolder->ubah_seriesfolder($param, $id);
		}


		if ($result) {

			echo getResponse(201, 'Data Berhasil Diubah');
		} else {

			echo getResponse(400, implode("<hr>", $err_msg));
		}
	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);
	}

	function delete($id)
	{

		authorize('hapusberita');
		$idEn = decrypt_url($id);

		$data = $this->M_beritabe->get_detail_berita($idEn);

		$dirUpload = "asset/foto_berita/";

		$files = $dirUpload . $data['gambar_uniq'];

		if (!empty($data['gambar_uniq'])) {

			if (file_exists($files)) {
				unlink($files);
			}
		}


		$result = $this->M_beritabe->delete_berita($idEn);


		if ($result) {

			echo getResponse(201, 'Data Berhasil Dihapus');
		} else {

			echo getResponse(400, 'Data gagal Dihapus');
		}
	}
}
