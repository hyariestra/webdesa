<style type="text/css">
	.container1 input[type=text] {
		padding: 5px 0px;
		margin: 5px 5px 5px 0px;
	}

	.delete {
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">


				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>


						<div class="form-group">

							<label>Nama Series Folder</label>

							<input value="<?php echo @$series['series_folder_name'] ?>" type="text" name="series" class="form-control">

						</div>

						<div class="form-group">

							<label>Series</label>

							<input value="<?php echo @$series['series'] ?>" id="kategoris" data-toggle="modal" data-target="#myModal" readonly=""  type="text" class="form-control">

							<input value="<?php echo @$series['id_series'] ?>" name="id_series" id="id_kategorinama" type="hidden">
						</div>

						<div class="form-group">

							<label>Series Folder Order</label>

							<input value="<?php echo @$series['order_folder'] ?>" type="number" name="order_folder" class="form-control">

						</div>


					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('seriesfolder') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>

	</div>

</div>

<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Series Master</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-hover" id="">
					<thead>
						<tr>
							<th>Series Master</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
							<?php foreach ($seriesmaster as $key => $v) {
							?>
								<tr>
									<td style="display: none;"><input type="hidden" value="<?php echo $v['id_series'] ?>"></td>
									<td><?php echo $v['series'] ?></td>
									<td>
										<button onclick="chosecategory(this)" type="button" data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-warning">
											<span class="glyphicon glyphicon-plus-sign"></span>
										</button>
									</td>
								</tr>


							<?php } ?>


					</tbody>

				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<script>
	function chosecategory(obj) {
		var idpelanggan = $(obj).parent().parent().find("td").eq(0).find("input:first").val();
		console.log(idpelanggan);
		namapelanggan = $(obj).parent().parent().find("td").eq(1).text();

		$("#kategoris").val(namapelanggan);
		$("#id_kategorinama").val(idpelanggan);

		$("#myModal").modal("remove");
	}



	$("#diskon").keyup(function() {

		var dInput = this.value;
		var harga_produk = $('#harga_produk').val();
		var harga_diskon = '';

		var diskon = (dInput * harga_produk) / 100;

		if (dInput != '') {
			var harga_diskon = harga_produk - diskon;

		}

		$('#harga_diskon').val(harga_diskon);

	});

	$(document).on('click', '.repeat-remove', function(e) {

		e.preventDefault();

		$(this).closest('tr').remove();



	});
	var lanjut = 0;

	$(".repeat-add").click(function(e) {

		e.preventDefault();


		lanjut = lanjut + 1;

		var masukan = "<tr role='row" + lanjut + "'>";

		masukan += "<td>";


		masukan += "<div class='input-group'>";

		masukan += "<span class='input-group-addon'>";
		masukan += "<input value='1' name='thumb[" + lanjut + "]' class='thumbClass' title='jadikan Tumbnail' type='checkbox' aria-label='...'>";
		masukan += "</span>";

		masukan += "<input  accept='image/*' class='form-control' readonly='' type='file' name='gambar[]'>";


		masukan += "<span class='input-group-btn'>";

		masukan += "<a class='repeat-remove btn btn-warning'  href='#modal-id'><i class='fa fa-trash' aria-hidden='true'></i></a>";

		masukan += "</span>";

		masukan += "</div>";

		masukan += "</td>";
		masukan += "</tr>";

		$('#total_row').before(masukan);





	});




	$('.form-kirim').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(formData, jqForm, options) {



		},
		success: processJson,
		error: processJsonError
	});


	function processJsonError(result) {
		result = result.responseJSON;
		processJson(result, true);
	}

	function processJson(result) {

		console.log(result);

		new Noty({
			text: result.message,
			type: result.status_code,
			timeout: 3000,
			theme: 'semanticui'
		}).show();

		if (result.status == 201) {
			window.location = '<?php echo base_url('seriesfolder') ?>';

		}
	}

	// $( ".nama_produk_class" ).keyup(function() {

	// 	var nama = this.value;

	// 	var str = this.value;
	// 	str = str.replace(/\s+/g, '-').toLowerCase();


	// 	$('.slug_class').val(str);


	// });
</script>