<?php 

class Videobe extends CI_controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))



		{



			redirect("pengguna/login");



		}

	}
	

	function index()

	{



		$this->themeadmin->tampilkan('tampilVideo',null);


	}

	function get_data_video()
	{
		$list = $this->M_videobe->get_datatables();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {


			if ($field->status == 'Y') {
				$status = "<span class='label label-success'>active</span>";
			}else{
				$status = "<span class='label label-default'>non active</span>";
			}

			
			$idVideo = encrypt_url($field->id_video);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->judul_video;
			$row[] = "<iframe width='350' height='200' src='".$field->url_video."' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
			$row[] = $status;
			$row[] = "<a class='btn btn-success btn-xs' title='Edit Data' href=".base_url('videobe/ubah/'.encrypt_url($field->id_video))." >
			<span class='glyphicon glyphicon-edit'></span></a>
			<a onclick=delete_func('$idVideo')  class='btn btn-danger btn-xs' title='Delete Data' '><span class='glyphicon glyphicon-remove'></span></a>";



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_videobe->count_all(),
			"recordsFiltered" => $this->M_videobe->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}

	public function tambah()
	{
		$data['url'] = 'videobe/simpan';
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$this->themeadmin->tampilkan('tambahVideo',$data);
	}


	public function ubah($id='')
	{

		$idEn = decrypt_url($id);
		$data['url'] = 'videobe/edit/'.$id;
		$data['breadcrumb'] = $this->breadcrumbcomponent->generate(); 
		$data['video'] =$this->M_videobe->get_detail_video($idEn);
		$this->themeadmin->tampilkan('tambahVideo',$data);

	}


	public function simpan()
	{

		$post = $this->input->post();
		$result= true;

		if ($post['url_video'] == "") {

			$err_msg[] = 'url video kosong';
			$result = false;

		}

		if ($post['judul_video'] == "") {

			$err_msg[] = 'judul video kosong';
			$result = false;

		}


		if ($result) {

			$param = array(
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'judul_video'=>$post['judul_video'],
				'url_video'=>$post['url_video'],
				'tanggal'=> date("Y-m-d H:i:s"),
				'status' => $post['status'],

			);



			$result = $this->M_videobe->simpan_video($param);

		}

		if ($result) {

			echo getResponse(201,'Data Berhasil Tersimpan');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	public function edit($id)
	{



		$post = $this->input->post();
		$result= true;


		$id = decrypt_url($id);


		if ($post['url_video'] == "") {

			$err_msg[] = 'url video kosong';
			$result = false;

		}

		if ($post['judul_video'] == "") {

			$err_msg[] = 'judul video kosong';
			$result = false;

		}


		if ($result) {


			$param = array(
				'id_user'=>getSession(),
				'id_desa'=>getSettingDesa()['id_desa'],
				'judul_video'=>$post['judul_video'],
				'url_video'=>$post['url_video'],
				'tanggal_update'=> date("Y-m-d H:i:s"),
				'status' => $post['status'],

			);


			$result = $this->M_videobe->ubah_video($param,$id);

		}


		if ($result) {

			echo getResponse(201,'Data Berhasil Diubah');

		}else{

			echo getResponse(400, implode("<hr>", $err_msg) );
		}

	}


	function detail()

	{


		$data['content'] = $this->load->view("produk/detail", $data, true);

		echo $this->load->view("template", $data);


	}

	function delete($id){

		$idEn = decrypt_url($id);

		$result = $this->M_videobe->delete_video($idEn);


		if ($result) {

			echo getResponse(201,'Data Berhasil Dihapus');

		}else{

			echo getResponse(400, 'Data gagal Dihapus' );
		}


	}



}

?>