<style type="text/css">
	.container1 input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}
	.delete{
		background-color: #fd1200;
		border: none;
		color: white;
		padding: 5px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
	}
	.modal-body{
    height: 80vh;
    overflow-y: auto;
}
</style>

<div class="row">
	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				
				<form action="<?php echo base_url($url) ?>" class="form-kirim" method="post" enctype="multipart/form-data">

					<div class="box-body">


						<?php echo $breadcrumb; ?>



						<div class="form-group">

							<label>Judul video</label>

							<input value="<?php echo @$video['judul_video'] ?>" type="text" name="judul_video" class="form-control">

						</div>

						<div class="form-group">


							<label>Url video [ <a  data-toggle="modal" href='#modal-id'><i class="fa fa-question-circle" aria-hidden="true"></i></a> ]</label>

							<input value="<?php echo @$video['url_video'] ?>" type="text" name="url_video" class="form-control">

						</div>

						
						<div class="form-group">

							<label>Status</label>
							<select class="form-control" name="status" id="">
								<option  <?php echo (@$video['status'] == 'Y') ? "selected": "" ?> value="Y">Active</option>
								<option  <?php echo (@$video['status'] == 'N') ? "selected": "" ?> value="N">Non Active</option>
							</select>

						</div>	

					

						


					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?php echo base_url('videobe') ?>" class="btn btn-danger">Kembali</a>
					</div>
				</form>

			</div>

		</div>		

	</div>

</div>



<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Cara mengisi form url video</h4>
			</div>
			<div class="modal-body">
				<label for="">1. Klik share pada video youtube yg akan digunakan</label>
				<img src="<?php echo base_url('asset/img/tutor-1.jpg') ?>" alt="">
				<hr>
				<label for="">2. Klik embed</label>
				<img src="<?php echo base_url('asset/img/tutor-2.jpg') ?>" alt="">
				<hr>
				<label for="">3. Klik copy, lalu pastekan pada form url</label>
				<img src="<?php echo base_url('asset/img/tutor-3.jpg') ?>" alt="">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<script>



		$('.form-kirim').ajaxForm({ 
			dataType:  'json', 
			beforeSubmit: function(formData, jqForm, options){

			},
			success:   processJson,
			error: processJsonError
		});


		function processJsonError(result) {
			result = result.responseJSON;
			processJson(result, true);
		}

		function processJson(result) { 

			console.log(result);

			new Noty({
				text: result.message,
				type: result.status_code,
				timeout: 3000,
				theme: 'semanticui'
			}).show();

			if(result.status == 201){
				window.location = '<?php echo base_url('videobe') ?>';

			}
		}


	</script>



	

